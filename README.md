
# Apprentice: A Code Generator for CA DevTest Extensions

This is a library that simplifies the construction of DevTest plugins.
There are two modules to this library:

* apprentice-model

* apprentice-generator 


The **apprentice-model** contains POJOs and builders with which the plugin content is defined.

The **apprentice-generator** contains a _ProjectGenerator_ which generates the skeleton code for the plugin.
The output folder created by the ProjectGenerator includes an Ant build file that
compiles and jars the generated plugin code. That jar file is what gets deployed
to the DevTest _hotDeploy_ (or _lib_) folder.  

Once the skeleton code has been generated, the folder containing those assets can be imported into an IDE.


### Example

       import mmm.coffee.apprentice.generator.generator.ProjectGenerator;
       import mmm.coffee.apprentice.model.beans.*;
       import mmm.coffee.apprentice.model.stereotypes.*;
       
       /**
        * This example generates a custom DevTest step that has two fields, 'hostName' and 'port'.
        * The generated code is written to the "C:/workspace/StepPlugin" directory,
        * which is created if it does not exist.
        */
       public class StepExample {
       
           /**
            * This example generates a custom DevTest step that has two fields, 'hostName' and 'port'.
            * @param ignored these are ignored.
            */
           public static void main (String[] ignored)
           {
               // -----------------------------------------------------------------------------------------
               // Step 1: Define the Project assets
               // An easy workflow is to work backwards: create the parameters, create the components,
               // then create the project.
               // -----------------------------------------------------------------------------------------
               Parameter pHostName = ParameterBean.builder("hostName")
                                                  .withLabel( "Host Name:" )
                                                  .withDataType ( DataType.STRING )
                                                  .build();
                                        
               Parameter pPort     = ParameterBean.builder("port")
                                                  .withLabel ( "Port:" )
                                                  .withDataType ( DataType.STRING )
                                                  .build();
                                                         
               Step step            = StepBean.builder ( "mmm.coffee.example.step.MyCustomStep" )
                                                  .withParameter ( pHostName )
                                                  .withParameter ( pPort )
                                                  .build();    
                                                     
               // -----------------------------------------------------------------------------------------
               // Step 2: Define the Project-specific properties 
               // -----------------------------------------------------------------------------------------
			   
			   // This class provides i18n/l10n support 
               final String messageResource = "mmm.coffee.example.step.Messages";  
               
			   // The fully-qualified path to DevTest on this workstation. We'll default to latest version.
               final String devTestHome = "C:/Program Files/CA/DevTest";

			   // The path to which the generated code is written; this directory is created as needed
               final String workspaceFolder = "C:/Path/To/Workspace/ThisPlugin";
       
               Project myPlugin = ProjectBean.builder( devTestHome, workspacFolder, messageResource )
                                             .withPluginName("StepPlugin")  // The pluginName becomes the name 
                                                                            // of the jar that's created.
                                                                            // In this case, StepPlugin.jar 
                                             .withComponent( step )
                                             .build();               
               // -----------------------------------------------------------------------------------------
               // Step 3: Generate the project
               // -----------------------------------------------------------------------------------------
               ProjectGenerator generator = new ProjectGenerator();
               generator.generate( myPlugin );

               // -----------------------------------------------------------------------------------------
               // Step 4: Compile and Deploy the plugin
               // 4.1) Open a terminal window 
               // 4.2) Navigate to C:/Path/To/Workspace/ThisPlugin
               // 4.3) Run 'ant' or 
			   // 4.3.1) run 'gradle wrapper' to generate the wrapper
			   // 4.3.2) run 'gradlew build' to compile the code
               // 4.4) Import the C:/Path/To/Workspace/ThisPlugin into your IDE to modify the generated code.
               // 4.5) Rebuild the plugin with your code changes
			   // 4.6) Deploy the plugin jar
               // -----------------------------------------------------------------------------------------
           }
       }           
       
