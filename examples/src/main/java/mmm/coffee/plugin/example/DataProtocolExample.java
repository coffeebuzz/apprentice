/*
 * Copyright (c) 2016.  Jon Caulfield.
 *
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.plugin.example;


import mmm.coffee.apprentice.generator.generator.ProjectGenerator;
import mmm.coffee.apprentice.model.beans.*;
import mmm.coffee.apprentice.model.stereotypes.*;

/**
 * This example generates a custom DevTest data protocol handler.
 * The generated code is written to the {@code WORKSPACE_DESTINATION/DataProtocolPlugin} directory,
 * which is created if it does not exist.
 */
public class DataProtocolExample {

    /**
     * This example generates a custom DevTest data protocol handler.
     * @param ignored these are ignored.
     */
    public static void main (String[] ignored)
    {
        // -----------------------------------------------------------------------------------------
        // Step 1: Define the Project assets.
        // -----------------------------------------------------------------------------------------
        DataProtocol dph = DataProtocolBean.builder("mmm.coffee.example.dph.MyCustomDPH")
                                .withDisplayName("XYZ")
                                .withDescription("A sample data protocol")
                                .withRequestSide(true)
                                .build();

        // -----------------------------------------------------------------------------------------
        // Step 2: Define the Messages class that will enable localization of message strings.
        //
        // Generally, a custom DPH does not need localization support. Adding i18n support after
        // the fact takes more effort than generating it now, so we let the MessageResource
        // be a required entity of Projects.
        //
        // Note: If you are going to deploy several plugin jars, be mindful to keep the package name
        // of the MessageResource unique across plugins to avoid naming collisions. For each MessageResource,
        // a messages.properties file is created.  If all the message.properties use the same
        // package name, the Java classloader will only load the first one encountered.
        // -----------------------------------------------------------------------------------------
        final String messageResource = "mmm.coffee.example.dph.Messages";

        // -----------------------------------------------------------------------------------------
        // Step 3: Build the Project with its assets
        // -----------------------------------------------------------------------------------------
        final String devTestHome = Constants.DEVTEST_HOME;
        final String workspaceFolder = Constants.WORKSPACE_DESTINATION + "/DataProtocol";

        Project myPlugin = ProjectBean.builder( devTestHome, workspaceFolder, messageResource )
                                .withPluginName("DataProtocolPlugin")  // The pluginName becomes the name
                                                                       // of the jar that's created
                                .withComponent( dph )
                                .build();

        // -----------------------------------------------------------------------------------------
        // Step 4: Generate the project
        // -----------------------------------------------------------------------------------------
        ProjectGenerator generator = new ProjectGenerator();
        generator.generate( myPlugin );
    }

}
