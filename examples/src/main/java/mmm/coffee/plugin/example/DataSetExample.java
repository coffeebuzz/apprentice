/*
 * Copyright (c) 2016.  Jon Caulfield.
 *
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.plugin.example;


import mmm.coffee.apprentice.generator.generator.ProjectGenerator;
import mmm.coffee.apprentice.model.beans.*;
import mmm.coffee.apprentice.model.stereotypes.*;

/**
 * This example generates a custom DevTest data set.
 * The generated code is written to the {@code WORKSPACE_DESTINATION/DataSetPlugin} directory,
 * which is created if it does not exist.
 */
public class DataSetExample {

    /**
     * This example generates a custom DevTest DataSet.
     * @param ignored these are ignored.
     */
    public static void main (String[] ignored)
    {
        // -----------------------------------------------------------------------------------------
        // Step 1: Define the project assets.
        // Since the ApplicationTarget, WorkspaceTarget and MessageResource are required,
        // they appear as arguments to the builder method.  This is in keeping with the philosophy
        // that, when using the Builder pattern, place required field in the constructor,
        // while using 'fluent' methods to assign optional fields.
        // -----------------------------------------------------------------------------------------
        Parameter pSourceFile = ParameterBean.builder("sourceFile")
                                    .withDataType( DataType.FILE )
                                    .withLabel ( "Source File:" )
                                    .withTooltip("The data file")
                                    .build();

        Parameter pHasBOM = ParameterBean.builder("hasBOM")
                                    .withLabel ("Has Byte-order-mark?")
                                    .withTooltip("Does this file start with a byte-order-mark?")
                                    .withDataType( DataType.BOOLEAN )
                                    .build();

        DataSet dataSet = DataSetBean.builder("mmm.coffee.example.dataset.MyCustomDataSet")
                                    .withParameter ( pSourceFile )
                                    .withParameter ( pHasBOM )
                                    .build();

        // -----------------------------------------------------------------------------------------
        // Step 2: Define the Messages class that will enable localization of message strings
        // -----------------------------------------------------------------------------------------
        final String messageResource = "mmm.coffee.example.dataset.Messages";

        // -----------------------------------------------------------------------------------------
        // Step 3: Build the Project with its assets
        // -----------------------------------------------------------------------------------------
        final String devTestHome = Constants.DEVTEST_HOME;
        final String workspaceFolder = Constants.WORKSPACE_DESTINATION + "/DataSetPlugin";

        Project myPlugin = ProjectBean.builder( devTestHome, workspaceFolder, messageResource )
                                .withPluginName("DataSetPlugin")  // The pluginName becomes the name
                                                                  // of the jar that's created. In this case,
                                                                  // DataSetPlugin.jar
                                .withComponent( dataSet )
                                .build();

        // -----------------------------------------------------------------------------------------
        // Step 5: Generate the project
        // -----------------------------------------------------------------------------------------
        ProjectGenerator generator = new ProjectGenerator();
        generator.generate( myPlugin );
    }

}
