/*
 * Copyright (c) 2016.  Jon Caulfield.
 *
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.plugin.example;


import mmm.coffee.apprentice.generator.generator.ProjectGenerator;
import mmm.coffee.apprentice.model.beans.*;
import mmm.coffee.apprentice.model.stereotypes.*;

/**
 * This example generates a custom DevTest step that has four fields: directoryName,
 * fileName, encoding, and hasBOM.
 * The generated code is written to the {@code WORKSPACE_DESTINATION/MyCustomStep} directory,
 * which is created if it does not exist.
 */
public class StepExample {

    /**
     * This example generates a custom DevTest step that has four fields:
     * <ul>
     * <li>directoryName</li>
     * <li>fileName</li>
     * <li>encoding</li>
     * <li>hasBOM</li>
     * </ul>
     * @param ignored these are ignored.
     */
    public static void main (String[] ignored)
    {
        // -----------------------------------------------------------------------------------------
        // Step 1: Define the Project assets
        // For this example, we'll show how to build one of each kind of parameter:
        // directory, file, string, and boolean.
        // Directory and File parameters have a "Find" button that presents the FileFinder.
        // A Directory parameter only allows directories to be selected in the FileFinder,
        // while a File parameter allows only a file to be selected.
        // A String parameter presents a JTextBox in the UI, while a Boolean parameter
        // presents a JCheckBox.
        // -----------------------------------------------------------------------------------------

        Parameter pDirectory = ParameterBean.builder("directoryName")
                                    .withDataType(DataType.DIRECTORY)
                                    .withLabel("Directory:")
                                    .withTooltip("The directory to scan")
                                    .build();

        Parameter pFile = ParameterBean.builder("fileName")
                                    .withDataType(DataType.FILE)
                                    .withLabel("File name:")
                                    .withTooltip("The file to load")
                                    .build();

        Parameter pEncoding = ParameterBean.builder("encoding")
                                    .withDataType(DataType.STRING)
                                    .withLabel("Character encoding:")
                                    .withTooltip("The encoding of the file; default is UTF-8")
                                    .build();

        Parameter pHasBom = ParameterBean.builder("hasBOM")
                                    .withDataType(DataType.BOOLEAN)
                                    .withLabel("Has Byte-order-mark?")
                                    .withTooltip("Select this if the file begins with a byte-order-mark")
                                    .build();

        Step step = StepBean.builder("mmm.coffee.example.step.MyCustomStep")
                                    .withParameter( pDirectory )
                                    .withParameter( pFile )
                                    .withParameter( pEncoding )
                                    .withParameter( pHasBom )
                                    .build();

        // -----------------------------------------------------------------------------------------
        // Step 3: Define the Messages class that will enable localization of message strings
        // -----------------------------------------------------------------------------------------
        final String messageResource = "mmm.coffee.example.step.Messages";

        // -----------------------------------------------------------------------------------------
        // Step 3: Build the Project with its assets
        // -----------------------------------------------------------------------------------------
        final String devTestHome = Constants.DEVTEST_HOME;
        final String workspaceFolder = Constants.WORKSPACE_DESTINATION + "/StepPlugin";

        Project myPlugin = ProjectBean.builder( devTestHome, workspaceFolder, messageResource )
                                .withPluginName("StepPlugin") // The pluginName becomes the name of the jar
                                                              // that's created and deployed to DevTest.
                                                              // In this case, it will be StepPlugin.jar.
                                .withComponent( step )        // Add our custom step entity
                                .build();                     // Create our project entity

        // -----------------------------------------------------------------------------------------
        // Step 5: Generate the project
        // -----------------------------------------------------------------------------------------
        ProjectGenerator generator = new ProjectGenerator();
        generator.generate( myPlugin );
    }

}
