package mmm.coffee.plugin.example;

/**
 * This class defines the various constants needed for code generation
 */
public class Constants {

    // The fully qualified path to DevTest
    // TODO: CHANGE THIS TO MATCH YOUR ENVIRONMENT
    public static final String DEVTEST_HOME="/Applications/Lisa10.0";

    // The parent directory to which the generated plugins will be written.
    // This directory is created as needed.  Each generated plugin will
    // be found under this directory.
    // TODO: CHANGE THIS TO MATCH YOUR ENVIRONMENT
    public static final String WORKSPACE_DESTINATION = "/Users/joncaulfield/workspace/LEK_10_Examples/";
}
