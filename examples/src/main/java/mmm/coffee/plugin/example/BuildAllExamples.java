package mmm.coffee.plugin.example;

import org.apache.tools.ant.DefaultLogger;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;


/**
 * Runs all the examples, which will produce six folders for the six plugin examples.
 */
public class BuildAllExamples {

    /**
     * Builds all the example plugins
     * @param argv ignored
     */
    public static void main ( String[] argv )
    {
        AssertionExample.main ( argv );
        CompanionExample.main ( argv );
        DataProtocolExample.main ( argv );
        DataSetExample.main ( argv );
        FilterExample.main ( argv );
        StepExample.main ( argv );

        System.out.println ("The plugins have been generated in the folder " + Constants.WORKSPACE_DESTINATION );
        System.out.println ("Run Ant to compile the plugins, then deploy them to DevTest");
        System.out.println ("PS: Plugins that include custom companions have to be deployed to DevTest/lib, NOT to the hotDeploy folder.");

    }

    /**
     * Runs the Ant builds that are in each Plugin's workspace folder.
     * A developer will usually run Ant from the command line.
     * For the convenience of saving a developer from having to run all six builds,
     * we automated that task.
     */
    private static void runAntBuilds()
    {
        String[] plugins = {
                "AssertionPlugin",
                "CompanionPlugin",
                "DataProtocolPlugin",
                "DataSetPlugin",
                "FilterPlugin",
                "StepPlugin" };

        for ( String pluginName : plugins )
        {
            try {
                System.out.println ("Building plugin: " + pluginName );
                buildPlugin ( pluginName );
            }
            catch (IOException ioe ) {
                System.out.println ( ioe.getMessage() );
            }
        }
    }

    /**
     * Ant's API changed and this does not work with Ant 1.9.
     *
     * Executes the Ant build of the given plugin
     * @param pluginName the folder containing the plugin code. This must be a subdirectory
     *                   of {@code Constants.WORKSPACE_DESTINATION}.
     * @throws IOException for any errors
     */
    private static void buildPlugin ( String pluginName ) throws IOException
    {
        // -----------------------------------------------------------------
        // Step 1: Initialize an Ant Project
        // -----------------------------------------------------------------
        Project project = new Project();
        project.init();

        // -----------------------------------------------------------------
        // Step 2: Get the File handles to build.xml and build.properties
        // -----------------------------------------------------------------
        File pluginFolder = new File ( Constants.WORKSPACE_DESTINATION + "/" + pluginName );
        File buildFile = new File( pluginFolder, "build.xml" );
        File buildProperties = new File ( pluginFolder, "build.properties" );

        // -----------------------------------------------------------------
        // Step 3: Configure the project from the build.xml
        // -----------------------------------------------------------------
        ProjectHelper.configureProject( project, buildFile );

        // -----------------------------------------------------------------
        // Step 4: Copy the build.properties into the Ant Project
        // -----------------------------------------------------------------
        Properties properties = new Properties();
        properties.load ( new FileInputStream( buildProperties ));
        Enumeration en = properties.keys();
        while ( en.hasMoreElements() ) {
            String key = (String)en.nextElement();
            project.setProperty( key, properties.getProperty( key ));
        }

        // -----------------------------------------------------------------
        // Step 5: Add a BuildListener in case things go sideways
        // -----------------------------------------------------------------
        project.addBuildListener( new DefaultLogger() );

        // -----------------------------------------------------------------
        // Step 6: Run the 'master' task, which compiles and jars the plugin
        // -----------------------------------------------------------------
        project.executeTarget("master");
    }

}
