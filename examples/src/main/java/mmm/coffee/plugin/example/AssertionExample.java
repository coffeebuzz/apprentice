/*
 * Copyright (c) 2016.  Jon Caulfield.
 *
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.plugin.example;


import mmm.coffee.apprentice.generator.generator.ProjectGenerator;
import mmm.coffee.apprentice.model.beans.*;
import mmm.coffee.apprentice.model.stereotypes.*;

/**
 * This example generates a custom DevTest assertion that has two fields, 'fieldOne' and 'fieldTwo'.
 * The generated code is written to the {@code WORKSPACE_DESTINATION/AssertionPlugin} directory,
 * which is created if it does not exist.
 */
public class AssertionExample {

    /**
     * This example generates a custom DevTest assertion that has two fields, 'fieldOne' and 'fieldTwo'.
     * @param ignored these are ignored.
     */
    public static void main (String[] ignored)
    {
        // -----------------------------------------------------------------------------------------
        // Step 1: Define the project assets.  Projects can include more than one component.
        // An easy workflow is to work backwards: build the parameters, build the components,
        // then build the project.
        // -----------------------------------------------------------------------------------------
        Parameter pFieldOne = ParameterBean.builder("fieldOne")
                                .withDataType( DataType.STRING )
                                .withLabel ( "Field One:" )
                                .build();

        Parameter pFieldTwo = ParameterBean.builder("fieldTwo")
                                .withDataType ( DataType.STRING )
                                .withLabel ( "Field Two:" )
                                .build();

        Assertion assertion = AssertionBean.builder("mmm.coffee.example.assertion.MyCustomAssertion")
                                .withParameter ( pFieldOne )
                                .withParameter ( pFieldTwo )
                                .build();

        // -----------------------------------------------------------------------------------------
        // Step 2: Define the Messages class that will enable localization of message strings.
        //
        // Note: If you are going to deploy several plugin jars, be mindful to keep the package name
        // of the MessageResource unique across plugins to avoid naming collisions. For each MessageResource,
        // a messages.properties file is created.  If all the message.properties use the same
        // package name, the Java classloader will only load the first one encountered.
        // -----------------------------------------------------------------------------------------
        final String messageResource = "mmm.coffee.example.assertion.Messages";


        // -----------------------------------------------------------------------------------------
        // Step 3: Build the Project with its assets
        // -----------------------------------------------------------------------------------------
        final String devTestHome = Constants.DEVTEST_HOME;
        final String workspaceFolder = Constants.WORKSPACE_DESTINATION + "/AssertionPlugin";

        Project myPlugin = ProjectBean.builder( devTestHome, workspaceFolder, messageResource )
                                    .withPluginName("AssertionPlugin")  // The pluginName becomes
                                                                        // the name of the jar that's created
                                    .withComponent( assertion )
                                    .build();

        // -----------------------------------------------------------------------------------------
        // Step 4: Generate the project
        // -----------------------------------------------------------------------------------------
        ProjectGenerator generator = new ProjectGenerator();
        generator.generate( myPlugin );
    }

}
