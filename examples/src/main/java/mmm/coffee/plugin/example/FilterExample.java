/*
 * Copyright (c) 2016.  Jon Caulfield.
 *
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.plugin.example;


import mmm.coffee.apprentice.generator.generator.ProjectGenerator;
import mmm.coffee.apprentice.model.beans.*;
import mmm.coffee.apprentice.model.stereotypes.*;

/**
 * This example generates a custom DevTest filter that has two fields, 'fieldOne' and 'fieldTwo'.
 * The generated code is written to the {@code WORKSPACE_DESTINATION/FilterPlugin} directory,
 * which is created if it does not exist.
 */
public class FilterExample {

    /**
     * This example generates a custom DevTest step that has two fields, 'hostName' and 'port'.
     * @param ignored these are ignored.
     */
    public static void main (String[] ignored)
    {
        // -----------------------------------------------------------------------------------------
        // Step 1: Define the project assets.  Projects can include more than one component.
        // An easy workflow is to work backwards: build the parameters, build the components,
        // then build the project.
        // -----------------------------------------------------------------------------------------
        Parameter pProperty = ParameterBean.builder("property")
                                    .withLabel("Property to check:")
                                    .withDataType( DataType.STRING )
                                    .build();

        Parameter pExpression = ParameterBean.builder("expression")
                                    .withLabel ("Reg-Ex:")
                                    .withDataType ( DataType.STRING )
                                    .build();

        Filter filter = FilterBean.builder("mmm.coffee.example.filter.MyCustomFilter")
                                            // Since filters can leverage the default editors and controllers
                                            // provided by the DevTest framework, we only have to define
                                            // the model-layer class.
                                    .withGlobalScope( false )
                                            // This filter is not presented in the list of global filters
                                    .withLocalScope( true )
                                            // This filter is presented in the list of filters under each step
                                    .withParameter( pProperty )
                                    .withParameter( pExpression )
                                    .build();

        // -----------------------------------------------------------------------------------------
        // Step 2: Define the Messages class that will enable localization of message strings
        // -----------------------------------------------------------------------------------------
        final String messageResource = "mmm.coffee.example.filter.Messages";

        // -----------------------------------------------------------------------------------------
        // Step 3: Build the Project with its assets
        // -----------------------------------------------------------------------------------------
        final String devTestHome = Constants.DEVTEST_HOME;
        final String workspaceFolder = Constants.WORKSPACE_DESTINATION + "/FilterPlugin";

        Project myPlugin = ProjectBean.builder( devTestHome, workspaceFolder, messageResource )
                                .withPluginName("FilterPlugin")  // The pluginName becomes the name
                                                                 // of the jar that's created
                                .withComponent( filter )
                                .build();

        // -----------------------------------------------------------------------------------------
        // Step 4: Generate the project
        // -----------------------------------------------------------------------------------------
        ProjectGenerator generator = new ProjectGenerator();
        generator.generate( myPlugin );
    }

}
