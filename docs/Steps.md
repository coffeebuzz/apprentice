
# Steps

Steps allow tasks to be implemented. In a general sense, steps implement
a subroutine within the workflow of the test model. 

When creating a custom step, an editor and controller need to be defined. 
For all other custom components, the default controllers and editors built
into Workstation are sufficient.  For custom steps, there is no default editor
or controller, so custom editors and controllers must be created.

To create a custom step, the StepBuilder can be used:

    Step step = StepBean.builder("mmm.coffee.example.CustomStep")
						.withParameter(...)
						.withParameter(...)
						.withParameter(...)
						.build();

or the POJO can be used:

    Step step = new StepBean("mmm.coffee.example.CustomStep");
	step.getParameters().add(...);


## The Editor and Controller

Custom steps always have a custom editor and controller generated.
The default names of these classes is conjured by adding the
suffix "Editor" or "Controller" to the model-layer's classname.
As such, when you do this:

    Step step = StepBean.builder("mmm.coffee.example.CustomStep")

The editor class will be "mmm.coffee.example.CustomStepEditor" and
the controller will be "mmm.coffee.example.CustomStepController".

If you prefer to explicitly name these classes, you can do this:

    Step step = StepBean.builder( "mmm.coffee.example.CustomStep" )
                        .withEditor( "mmm.coffee.example.CustomEditor" )
                        .withController( "mmm.coffee.example.CustomController" )
                        .withParameter ( ... )
                        .build();
                        
                        

    
    
    




