
# Data Protocols

Data Protocols are filters that allow requests and responses to be interogated and
modified. Data protocols do not have parameters, nor editors or controllers; they
only have their model-layer class. 

To create a data protocol entity, do this:

    DataProtocol dph = DataProtocolBean.builder("mmm.coffee.example.dph.CustomDataProtocol")
									   .withDisplayName("MyDPH")
	                                   .withDescription("A description of my dph")
									   .withRequestSide(true|false)
									   .withResponseSide(true|false)
									   .build();

The display name is the name presented in the drop-down list, when, in the Request or Response step,
you select Filters > VSE > Data Protocols. 

The properties withRequestSide and withResponseSide affect how the lisaextension for the DPH is written,
but the skeleton code is the same regardless of their settings.  That is, the skeleton code always
has various flavors of updateRequest and updateResponse methods.  Since the syntax of a DPH declaration
within the **lisaextension** file requires us to declare whether the DPH handles requests, responses, or both,
the withRequestSide and withResponseSide must be set accordingly.  


