
# Parameters

Parameters embody the data points a component uses to execute its particular business logic.
Parameters have these attributes:
  * instance variable name
  * label text
  * data type
  * tool-tip text (optional)

The instance variable name is, as the name suggests, the name of the instance variable
given to this parameter in the generated code.  As such, its name is restricted to the 
syntax rules of Java concerning variable names.  There are checks in place within the
ParameterBean class to help detect invalid instance variable names. 

The label text is the text presented in the component's editor to denote the data
entry field for this parameter. These are values like, "Host:", "Port:", "File Name:"
and such.  The label is presented in the editor as given; no syntax such as colons
or question marks are added by the code generator; the label is presented as given.

The data type, of course, lets the code generator know how to present the data entry
fields for the parameter.  Four data types are supported: string, file, directory,
and boolean.  These for types are defined in the DataType class.  The data type
affects the kind of Swing component presented in the editor. Parameter of type DataType.STRING, 
DataType.FILE and DataType.DIRECTORY present a JTextField. 
A parameter of DataType.BOOLEAN presents a JCheckBox.  For parameters of
DataType.FILE and DataType.DIRECTORY, a "find" button is also presented (this button
is labeled with the elipses, "..." ).  The find button opens the FileFinder dialog, 
which allows the end-user to navigate the file system to find the file or directory. 
When the parameter is a DataType.FILE, the FileFinder allows a leaf file to be selected
as the choice, while, when the parameter is a DataType.DIRECTORY, the FileFinder 
allows a directory to be selected as the choice.  The default data type of a
parameter is DataType.STRING.

If you want a tool-tip presented with the data entry field of the parameter, that
text can be set in the parameter's toolTip attribute, either with:

    Parameter p = ParameterBean.builder("someField")
					  .withLabel ( "Some Field: ")
					  .withTooltip ( "This is the tool-tip of this field" )
					  .build();

or

    Parameter p = new ParameterBean("someField");
	p.setTooltip ( "This is the tool-tip of this field" );

The interface of a parameter is found in the class:

    mmm.coffee.apprentice.model.stereotypes.Parameter

The POJO for a parameter is found in the class:

    mmm.coffee.apprentice.model.beans.ParameterBean



