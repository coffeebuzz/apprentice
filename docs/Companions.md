
# Companions

Companions are invoked twice; once just before the test starts and once just after a test
finishes.  If you're familiar with JUnit, companions serve the same purpose as the set-up
and teardown methods in JUnit.

Companions are created by defining the model-layer classname and the parameters; for example:

    Companion companion = CompanionBean.builder("mmm.coffee.plugin.CustomCompanion")
                              .withParameter ( ... )
                              .withParameter ( ... )
                              .build();

Once the companion's skeleton code is generated, you will need to implement the 
companion's **testStarting** and **testEnding** methods. The testStarting method 
is called once, just before the first test cycle occurs; the testEnding method is called
once, after the test ends.  
