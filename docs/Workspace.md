

# The Workspace

The Workspace entity defines the base directory to which the code generator
will write its output.  This base directory is created as needed.  Once the
code is generated, this base directory can be imported into your favorite IDE.

After the code is generated, within this workspace directory you will find:

  build.properties   : defines Ant properties, especially the path to DevTest
  build.xml          : the Ant build file that will compile the generated code
                       and create the jar file that's deployed to DevTest
  src/main/java      : the directory containing the generated code
  src/main/resources : contains the messages.properties file, which contains the i18n
                       messages, and the lisaextensions file, which is the
					   deployment descriptor of the extension.

To create a Workspace entity, do this:

    Workspace workspace = WorkspaceBean.builder("C:/Projects/MyPlugin").build();

or

    Workspace workspace = new WorkspaceBean ("C:/Projects/MyPlugin");
    

The ProjectBean's API has been changed to allow you to imply the ApplicationTarget,
WorkspaceTarget, and MessageResource.  That is, instead of explicitly creating 
these three objects, you can do this:

    Project project = ProjectBean.builder( "C:/Program Files/CA/DevTest",
	                                       "C:/Path/To/Workspace",
										   "com.mycompany.widget.i18n.Messages").build();

This saves you from having to explicitly create the WorkspaceTarget;
its creation is handled by the ProjectBean.Builder.



