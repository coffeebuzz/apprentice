
# Filters

Filters allow the test to assign values to LISA properties.  To create a Filter entity,
the classname of the filter's model-layer class and parameters are specified, as such:

    Filter filter = FilterBean.builder("mmm.coffee.plugin.CustomFilter")
	                    .withParameter ( ... )
						.withParameter ( ... )
						.build();

In most cases, the default editor and controller built into the DevTest framework are
sufficient for filters.

Filters can be marked as global, local, or both.  In DevTest Workstation, a folder named
Global Filters is available at the top-layer of a Test model.  Filters marked as global
appear in this list.   Global filters are applied across all steps in the test model.

When filters are marked as local, those filters are added as needed to individual steps. 
The local filters are found in each steps Inspection Editor, in the Filters folder. 

To mark as Filter as global or local, do this:

    Filter filter = FilterBean.builder("mmm.coffee.plugin.CustomFilter")
	                    .withParameter ( ... )
						.withParameter ( ... )
						.withLocalScope ( true )
						.withGlobalScope ( false )
						.build();

By default, filters have local scope and not global scope, as this is the most common use case.
Currently, DevTest Workstation allows a filter to be both global and local, so that's supported
by the code generator as well.


