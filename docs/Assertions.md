
# Assertions

To construct an Assertion entity, the classname of the model-layer class is
given, along with the parameters:  

    Assertion assertion = AssertionBean.builder("mmm.coffee.plugin.CustomAssertion")
	                          .withParameter ( ... )
							  .withParameter ( ... )
							  .build();

Assertions can be global, local or both.  In DevTest Workstation, a folder is presented
containing global assertions.  If you declare the assertion as global, it will appear
in the Global Assertions list. Global assertions are automatically applied to all steps. 
A common example of a global assertion is creating an assertion that fails the test
if a Server-500 error is returned by an HTTP request.  The idea being, instead of having 
to add that assertion to every single step of a test, the global assertion essentially 
does that for you -- you get the same effect without having the same assertion copied to
each step. 

Assertions that are declared as 'local' can be added to individual steps. In the Workstation UI,
in each step's Inspection Editor is a tab for that steps Assertions.  Assertions declared
as 'local' appear in that list.  By default, Assertion entities created by Apprentice are local
and not global. 

To set the local or global flags, do this:

    Assertion assertion = AssertionBean.builder("mmm.coffee.plugin.CustomAssertion")
	                          .withParameter ( ... )
							  .withParameter ( ... )
							  .withLocalScope ( true )
							  .withGlobalScope ( false )
							  .build();

 Workstation currently supports assertions that are both global and local, if you want to do that. 


