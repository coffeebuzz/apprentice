
# Data Sets

The DataSet entity enables custom data sets to be generated. The builder or 
constructor take the classname of the data set's model-layer class.

    DataSet dataSet = DataSetBean.builder("mmm.coffee.plugin.CustomDataSet")
	                      .withParameter ( ... )
						  .withParameter ( ... )
						  .build();

or

    DataSet dataSet = new DataSetBean ("mmm.coffee.plugin.CustomDataSet");
	dataSet.getParameters().add ( ... );
	dataSet.getParameters().add ( ... );

Once the code is generated, you will need to implement the data set's
**getNextRecord** method, which is responsible for returning the
next record of the data set. Data sets also have a **destroy**
method to handle any resource clean-up when the data set is finished.

In general, if you search for TODO tags in the generated code, you'll
find the methods you need to address. 


