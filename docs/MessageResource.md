
# MessageResource

To enable i18n and localization support, each Project entity contains one
class to handle the look up of localized strings.  This class is defined
with the MessageResource entity.  The sole argument is the fully qualified
classname, as such:

    MessageResource messages = MessageResourceBean.builder("my.devtest.plugin.i18n.Messages").build();

or

    MessageResource messages = new MessageResourceBean ("my.devtest.plugin.i18n.Messages");

From the above example, the code generator does two things:

  1. Creates a **Messages** class with a **get(String key)** method to fetch the localized String
  2. Creates a **messages.properties** file in the project's src/main/resources/my/devtest/plugin/i18n directory. 
     The long path was preferred over src/main/resources/messages.properties to avoid
	 possible collisions with other messages.properties that might be in other jars in DevTest. 

All components within a project share this same message resource.
 
The ProjectBean's API has been changed to allow you to imply the ApplicationTarget,
WorkspaceTarget, and MessageResource.  That is, instead of explicitly creating 
these three objects, you can do this:

    Project project = ProjectBean.builder( "C:/Program Files/CA/DevTest",
	                                       "C:/Path/To/Workspace",
										   "my.devtest.plugin.i18n.Messages")
								 .build();
										   
This saves you from having to explicitly create a MessageResource entity; 
the ProjectBean will handle its creation.										   
										    
										    

