
# The ApplicationTarget


The ApplicationTarget entity defines the location and version of DevTest
to which the extension will be deployed.  The location field is applied in
the generated **build.properties** file to enable Ant to locate the DevTest jars.

The version field is used to generate source code specific to that 
DevTest version.  While there have been no API changes between 
DevTest versions 8, 9 and 10, there were API and internal changes 
between DevTest 5, 6, 7, and 8, so building in support for 
version-specific templates seemed prudent.  The version field is
optional, with the latest supported DevTest version being the default.
(The latests supported version is DevTest 10, which is still under
development.)
 
### Examples

    ApplicationTarget devTest = ApplicationTargetBean.builder("C:/Program Files/CA/DevTest")
	                                                 .build();

    ApplicationTarget devTest = ApplicationTargetBean.builder("C:/Program Files/CA/DevTest")
	                                                 .withVersion("10")
	                                                 .build();

	ApplicationTarget devTest = new ApplicationTargetBean ( "C:/Program Files/CA/DevTest" );

	ApplicationTarget devTest = new ApplicationTargetBean ( "C:/Program Files/CA/DevTest", "10" );


The ProjectBean's API has been changed to allow you to imply the ApplicationTarget,
WorkspaceTarget, and MessageResource.  That is, instead of explicitly creating 
these three objects, you can do this:

    Project project = ProjectBean.builder( "C:/Program Files/CA/DevTest",
	                                       "C:/Path/To/Workspace",
										   "com.mycompany.widget.i18n.Messages").build();


