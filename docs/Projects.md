

The Project entity in Apprentice represents the same notion a project
does in an IDE or, loosely, in Workstation itself.  The Project entity
contains the meta-data that tells the code generator where to write 
the emitted code, and for which version of DevTest the generated plugin
will support. 

A Project entity has three required arguments: 
   - The home directory of DevTest
   - The directory to which the generated code is written
   - the classname of the Messages class for this extension

For example, to create a Project entity, you can do this:

    final String devTestHome = "C:/Program Files/CA/DevTest";
	final String workspace = "C:/Path/To/Workspace/MyPlugin";
	final String messages = "mmm.coffee.widget.i18n.Messages";

	Project project = ProjectBean.builder ( devTestHome, workspace, messages ).build();


The old style of using explicit objects is still supported.  If you want to explicitly
build an ApplicationTarget, WorkspaceTarget, and MessageResource, you can still do this:

    ApplicationTarget devTestHome = ApplicationTargetBean.builder("C:/Program Files/CA/DevTest")
	                                                     .build();

    Workspace workspace = WorkspaceBean.builder("C:/Path/To/Workspace/MyPlugin").build();

	MessageResource messages = MessageResourceBean.builder("mmm.coffee.widget.i18n.Messages").build();


    Project project = ProjectBean.builder(devTestHome, workspace, messages).build();

