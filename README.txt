
This is a library that simplifies the construction of DevTest plugins.
There are two pieces to this library:

apprentice-model:      contains builder objects with which plugin elements are defined
apprentice-generator:  contains a ProjectGenerator that emits the Java code 

The output folder created by the ProjectGenerator contains an Ant build file that
compiles and jars the generated plugin code. The jar file produced is deployed
to the DevTest hotDeploy (or lib) folder and, with that, the plugin installed and
usable.

The code generated is only skeleton code. The business logic still needs to be
implemented using the generated code as a framework. 
