/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.generator.generator;

import mmm.coffee.apprentice.generator.utils.FileHelper;

import org.stringtemplate.v4.ST;

import java.io.*;
import java.nio.charset.Charset;

import java.util.Objects;
import java.util.logging.Logger;


/**
 * With the migration to the StringTemplate library, this class
 * handles writing the template to a file.
 * Once upon a time, this is a proxy for the Velocity template engine.
 *
 */
public class TemplateEngine {

    private static Logger LOG = Logger.getLogger(TemplateEngine.class.getName());

    /**
     * Generates source files from the template and context.  We're telling Velocity to merge the template
     * with the context and write the the results to {@code outputFile}.
     *
     * @param template the template to render
     * @param outputFile the file to which the rendered template is written.
     *
     * @throws java.io.FileNotFoundException if the output file does not already exist
     * @throws java.io.IOException if the output file cannot be created
     */
    public void generateCode (ST template, File outputFile) throws IOException
    {
        Objects.requireNonNull( template, "The argument 'template' cannot be null");
        Objects.requireNonNull ( outputFile, "The argument 'outputFile' cannot be null");

        // Step 1: Create the output file if it doesn't exist
        FileHelper.createFile(outputFile);

        // Step 2: Render the template and write it to the file
        try {
            PrintWriter pw = new PrintWriter( outputFile, Charset.defaultCharset().toString() );
            pw.println( template.render() );
            pw.flush();
            pw.close();
        }
        catch (FileNotFoundException err)
        {
            LOG.warning(err.getLocalizedMessage() == null ? err.toString() : err.getLocalizedMessage());
            throw err;
        }
    }
}
