/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.generator.decorator;

import mmm.coffee.apprentice.model.stereotypes.*;
import mmm.coffee.apprentice.model.helpers.StringHelper;

import java.io.File;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

/**
 * This class decorates a {@code ProjectBean} by providing information
 * needed for code generation.
 */
public class ProjectDecorator {


    private Project project;


    /**
     * Constructs this decorator for this {@code project}.
     *
     * @param project the Project being decorated; cannot be null.
     */
    public ProjectDecorator ( Project project ) {
        //
        // PRECONDITIONS
        //
        Objects.requireNonNull( project, "When constructing a ProjectDecorator, the Project cannot be null." );

        if ( project.getWorkspaceTarget() == null )
            throw new IllegalArgumentException("When constructing a ProjectDecorator, the Project must define a non-null Workspace.");

        if ( StringHelper.isEmpty ( project.getWorkspaceTarget().getBaseDirectory() ))
            throw new IllegalArgumentException("When constructing a ProjectDecorator, the Project must define a Workspace, and that Workspace must define a base directory.");

        // REAL WORK
        this.project = project;
    }



    /**
     * Returns the [pluginName]-messages.properties File.
     * This method does not create the File nor is the File guaranteed to exist yet.
     *
     * The {@code project} must be well-formed.
     * In particular, if the project.getOutput() is null, this method returns null.
     * Essentially, a null output destination is treated as a /dev/null destination.
     *
     * @return the File handle to the {@code messages.properties} file
     *
     */
    public File getMessagePropertiesFile()
    {


        // Build the fully qualified path to the messages.properties file.
        // An example is: '/path/to/workspace/MyPlugin/src/main/resources/com/mycompany/plugin/i18n/resources/plugin-message.properties'
        String fqMessageFileName = project.getWorkspaceTarget().getBaseDirectory()
                                 + "/"
                                 + project.getWorkspaceTarget().getResourceDirectory()
                                 + "/"
                                 + project.getDefaultMessageBundleDirectory()
                                 + "/messages.properties";


        return new File ( fqMessageFileName );
    }

    /**
     * Returns the fully qualified name of the message bundle; for example: "com.widget.i18n.messages"
     *
     * @return the fully-qualified bundle name
     */
    public String getBundleName()
    {
        String bundleName = project.getDefaultMessageBundleDirectory()
                          + ".messages";

        bundleName = bundleName.replace("/", "." );

        return bundleName;
    }

    /**
     * Returns the file instance for the build.xml file written to the generated project
     *
     * @return the File handle for the build.xml file
     */
    public File getAntBuildFile()
    {
        if ( project.getWorkspaceTarget() != null && project.getWorkspaceTarget().getBaseDirectory() != null ) {
            String fileName = project.getWorkspaceTarget().getBaseDirectory() + "/build.xml";
            return new File(fileName);
        }
        return null;
    }

    /**
     * Returns the File instance for the build.properties file written to the generated project
     *
     * @return the File handle for the build.properties file
     */
    public File getAntBuildPropertiesFile ()
    {
        if ( project.getWorkspaceTarget() != null && project.getWorkspaceTarget().getBaseDirectory() != null ) {
            String fileName = project.getWorkspaceTarget().getBaseDirectory() + "/build.properties";
            return new File(fileName);
        }
        return null;
    }

    public File getGradleBuildFile()
    {
        if ( project.getWorkspaceTarget() != null && project.getWorkspaceTarget().getBaseDirectory() != null ) {
            String fileName = project.getWorkspaceTarget().getBaseDirectory() + "/build.gradle";
            return new File(fileName);
        }
        return null;
    }

    public File getGradleSettingsFile()
    {
        if ( project.getWorkspaceTarget() != null && project.getWorkspaceTarget().getBaseDirectory() != null ) {
            String fileName = project.getWorkspaceTarget().getBaseDirectory() + "/settings.gradle";
            return new File(fileName);
        }
        return null;
    }

    public File getLoggingPropertiesFile()
    {
        if ( project.getWorkspaceTarget() != null && project.getWorkspaceTarget().getBaseDirectory() != null ) {
            String fileName = project.getWorkspaceTarget().getBaseDirectory() + "/logging.properties";
            return new File(fileName);
        }
        return null;
    }



    public File getLisaExtensionsFile ()
    {
        // Step 1. Scrub the plugin name of any spaces.
        String pluginName = project.getPluginName();
        if ( StringHelper.isEmpty ( pluginName )) {
            pluginName = "plugin";
        }
        else {
            // remove any leading/trailing whitespace, and replace any other spaces with underscores.
            pluginName = pluginName.trim().replace(' ', '_');
        }

        String path = project.getWorkspaceTarget().getBaseDirectory()
                    + "/"
                    + project.getWorkspaceTarget().getResourceDirectory()
                    + "/"
                    + pluginName
                    + ".lisaextensions";

        return new File ( path );
    }

    /**
     * Returns the Assertions contained in this project, or an empty set
     * @return the set of Assertions found in this project
     */
    public Set<Assertion> getAssertions()
    {
        Set<Assertion> results = new LinkedHashSet<>();
        for ( Component c : project.getComponents() ) {
            if ( c instanceof Assertion )
                results.add ( (Assertion)c );
        }
        return results;
    }

    /**
     * Returns {@code true} if this project contains any Assertion components.
     *
     * Note: The method name is based on what StringTemplate will support.
     * In a template, we write something like: if(obj.foo). StringTemplate
     * finds the 'foo' method using reflection; it tries for 'getFoo' and 'isFoo'.
     * If neither is found, the boolean expression defaults and returns 'false'.
     *
     * @return {@code true} if an Assertion is one of the components.
     */
    public boolean getHasAssertions()
    {
        for ( Component c : project.getComponents() ) {
            if (c instanceof Assertion) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns a set of the Companions found in this project, or an empty set
     * @return the set of Companions found in this project
     */
    public Set<Companion> getCompanions()
    {
        Set<Companion> results = new LinkedHashSet<>();
        for ( Component c : project.getComponents() ) {
            if ( c instanceof Companion )
                results.add ((Companion)c);
        }
        return results;
    }

    /**
     * Returns {@code true} if this project contains any Companion component
     * @return {@code true} if any Companions are part of this project
     */
    public boolean getHasCompanions()
    {
        for ( Component c : project.getComponents() ) {
            if ( c instanceof  Companion )
                return true;
        }
        return false;
    }

    /**
     * Returns a set of the Data Sets found in this project, or an empty set
     * @return the data sets found in this project
     */
    public Set<DataSet> getDataSets()
    {
        Set<DataSet> results = new LinkedHashSet<>();
        for ( Component c : project.getComponents() ) {
            if ( c instanceof DataSet )
                results.add ( (DataSet)c );
        }
        return results;
    }

    /**
     * Returns {@code true} if a DataSet is one of the project's components
     * @return {@code true} if a DataSet is one of the project's components
     */
    public boolean getHasDataSets()
    {
        for ( Component c : project.getComponents() ) {
            if ( c instanceof  DataSet ) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns a set of the data protocols found in this project, or an empty set
     * @return a set of the data protocols found in this project
     */
    public Set<DataProtocol> getDataProtocols()
    {
        Set<DataProtocol> results = new LinkedHashSet<>();
        for ( Component c : project.getComponents() ) {
            if (c instanceof DataProtocol)
                results.add((DataProtocol) c);
        }
        return results;
    }

    /**
     * Returns {@code true} if this project contains any DataProtocol components
     * @return {@code true} if there are any DataProtocols in this project
     */
    public boolean getHasDataProtocols()
    {
        for ( Component c : project.getComponents() ) {
            if ( c instanceof DataProtocol )
                return true;
        }
        return false;
    }

    /**
     * Returns a set of the Filters found in this project, or an empty set
     * @return a set of the filters found in this project
     */
    public Set<Filter> getFilters()
    {
        Set<Filter> results = new LinkedHashSet<>();
        for ( Component c: project.getComponents() ) {
            if ( c instanceof Filter )
                results.add ( (Filter)c );
        }
        return results;
    }

    /**
     * Returns {@code true} if this project contains any Filters
     * @return {@code true} if there are any Filters in this project
     */
    public boolean getHasFilters()
    {
        for ( Component c: project.getComponents() ) {
            if ( c instanceof Filter ) {
                return true;
            }
        }
        return false;
    }


    /**
     * Returns a set of the Steps found in this project, or an empty set
     * @return the set of Steps found in this project
     */
    public Set<Step> getSteps()
    {
        Set<Step> results = new LinkedHashSet<>();

        for ( Component c : project.getComponents() )
        {
            if ( c instanceof Step )
                results.add ( (Step)c );
        }
        return results;
    }

    /**
     * Returns {@code true} if this project contains any Step components
     * @return {@code true} if there are any Steps in this project
     */
    public boolean getHasSteps() {
        for ( Component c : project.getComponents() ) {
            if ( c instanceof Step )
                return true;
        }
        return false;
    }





}
