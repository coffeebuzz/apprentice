/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * The classes in this package provide template-specific services.
 * The {@code TemplateCatalog} class provides look-up methods
 * for all supported templates.  The {@code TemplateCatalog} hides the
 * template repository structure from the end-user.
 *
 * The {@code TemplateVersion} class is an enumeration of the supported
 * LISA&copy;/DevTest&copy; versions.
 */
package mmm.coffee.apprentice.generator.template;