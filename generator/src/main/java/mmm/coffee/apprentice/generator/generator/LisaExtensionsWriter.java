/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.generator.generator;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.List;

import mmm.coffee.apprentice.generator.utils.FileHelper;
import mmm.coffee.apprentice.model.stereotypes.*;
import mmm.coffee.apprentice.model.helpers.StringHelper;

/**
 * This class handles the generation of the lisaextensions file
 */
public class LisaExtensionsWriter {


    private static final String DEFAULT_DISPLAY_NAME = "Custom";
    private static final String DEFAULT_DESCRIPTION = "a custom data protocol";

    private PrintWriter writer;

    /**
     * Constructor
     *
     * @param extensionsFile the handle to the ".lisaextensions" file being written.  This file
     * @throws IOException if the extensions file does not exist or cannot be created
     */
    public LisaExtensionsWriter ( File extensionsFile ) throws IOException
    {
        if ( extensionsFile == null )
            throw new NullPointerException("A LisaExtensionsWriter cannot be created with a null File");

        FileHelper.createFile(extensionsFile);

        writer = new PrintWriter ( extensionsFile, Charset.defaultCharset().toString() );

        writer.println ("#");
        writer.println ("# DevTest Extensions file");
        writer.println ("#");

        writer.flush();
    }


    /**
     * Writes the declaration of assertions
     *
     * @param assertions the assertions in the deployment; if null or empty, this method silently returns
     */
    public void writeAssertions ( List<Assertion> assertions )
    {
        if ( assertions == null || assertions.size() == 0 )
            return;


        // Step 1: Print the filters declarations
        writer.println ();
        writer.println ("#");
        writer.println ("# Custom assertions");
        writer.println ("#");

        writeDeclarations("asserts", assertions);

        // Step 2: Print the node/controller/editor relationships
        writeMVCRelationships( assertions );

        writer.flush();

    }


    /**
     * Writes the declaration of assertions
     *
     * @param companions the companions in the deployment; if null or empty, this method silently returns
     */
    public void writeCompanions ( List<Companion> companions )
    {
        if ( companions == null || companions.size() == 0 )
            return;


        // Step 1: Print the filters declarations
        writer.println ();
        writer.println ("#");
        writer.println ("# Custom companions");
        writer.println ("#");

        writeDeclarations("companions", companions);

        // Step 2: Print the node/controller/editor relationships
        writeMVCRelationships( companions );

        writer.flush();

    }


    /**
     * Writes the declaration of custom steps
     *
     * @param steps the steps in the deployment; if null or empty, this method silently returns
     */
    public void writeSteps ( List<Step> steps )
    {
        if ( steps == null || steps.size() == 0 )
            return;

        // Step 1: Print the Nodes declarations
        writer.println ();
        writer.println ("#");
        writer.println ("# Custom Steps");
        writer.println ("#");

        writeDeclarations("nodes", steps);

        // Step 2: Print the node/controller/editor relationships
        writeMVCRelationships( steps );

        writer.flush();

    }

    /**
     * Writes the declaration of filters
     *
     * @param filters the filters in the deployment; if null or empty, this method silently returns
     */
    public void writeFilters ( List<Filter> filters )
    {
        if ( filters == null || filters.size() == 0 )
            return;


        // Step 1: Print the filters declarations
        writer.println ();
        writer.println ("#");
        writer.println ("# Custom filters");
        writer.println ("#");

        writeDeclarations("filters", filters);

        // Step 2: Print the node/controller/editor relationships
        writeMVCRelationships(filters);

        writer.flush();
    }

    public void writeDataSets ( List<DataSet> dataSets )
    {
        if ( dataSets == null || dataSets.size() == 0 )
            return;

        // Step 1: Print the filters declarations
        writer.println ();
        writer.println ("#");
        writer.println ("# Custom data sets");
        writer.println ("#");

        writeDeclarations("datasets", dataSets);

        // Step 2: Print the node/controller/editor relationships
        writeMVCRelationships(dataSets);

        writer.flush();
    }

                // vseProtocols={{dph.className}}
            // {{dph.className}}=[supports-clause],dph.displayName,dph.shortHelp
            // where supports-clause is:
            // data:req if DPH only handles request data
            // data:resp if DPH only handles response data
            // data:req:resp if the DPH handles request & response data
    public void writeDataProtocolHandlers ( List<DataProtocol> dphList )
    {
        if ( dphList == null || dphList.size() == 0 )
            return;

        // Step 1: Print the filters declarations
        writer.println ();
        writer.println ("#");
        writer.println ("# Custom data protocol handlers");
        writer.println ("#");

        writeDeclarations("vseProtocols", dphList );

        // Step 2: Print the descriptor
        StringBuilder sb = new StringBuilder();
        for ( DataProtocol bean : dphList ) {
            sb.setLength(0);
            sb.append (bean.getClassName())
                      .append("=");

            sb.append ("data");


            if ( bean.isRequestSide() )
            {
                sb.append(":req");
            }
            if ( bean.isResponseSide() )
            {
                sb.append(":resp");
            }
            sb.append (",");

            // If no displayName or description was given, print a default
            if ( StringHelper.isEmpty( bean.getDisplayName() ))
                sb.append ( DEFAULT_DISPLAY_NAME ).append(",");
            else
                sb.append(bean.getDisplayName()).append(",");

            if ( StringHelper.isEmpty( bean.getDescription()))
                sb.append ( DEFAULT_DESCRIPTION );
            else
                sb.append(bean.getDescription());

            writer.println ( sb.toString() );
        }

        writer.flush();
    }


    /**
     * Prints the declarations of the form:
     *
     * name=modelClassName,\
     * modelClassName,
     * modelClassName
     *
     * @param name the component type being listed
     * @param list the list of those components
     */
    private void writeDeclarations ( String name, List<?> list )
    {
        writer.print ( name + "=" );

        boolean needsComma = false;
        for ( Object obj : list ) {
            Component bean = (Component)obj;
            if ( needsComma ) {
                writer.println(",\\");
            }
            writer.print ( bean.getClassName() );
            needsComma = true;
        }
        writer.println();
        writer.println();
    }

    /**
     * Print lines of the form:  modelClassName=controllerClassName,editorClassName
     *
     * @param list the ModelLayerBeans to print
     */
    private void writeMVCRelationships ( List<?> list )
    {
        StringBuilder sb = new StringBuilder();
        for ( Object obj : list ) {
            MVCEntity bean = (MVCEntity) obj;

            if ( bean.getController() == null )
                throw new IllegalArgumentException( String.format ( "The entity '%s' is missing a required Controller entity", bean.getSimpleName()));
            if ( bean.getEditor() == null )
                throw new IllegalArgumentException( String.format("The entity '%s' is missing a required Editor entity", bean.getSimpleName()));

            sb.setLength(0);
            sb.append (bean.getClassName())
                      .append("=")
                      .append(bean.getController().getClassName())
                      .append(",")
                      .append(bean.getEditor().getClassName());
            writer.println ( sb.toString() );
        }
    }

    public void close() {
        if ( writer != null )
            writer.close();
    }


}
