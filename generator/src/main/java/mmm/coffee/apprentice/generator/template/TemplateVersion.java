/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.generator.template;

import mmm.coffee.apprentice.model.helpers.StringHelper;

/**
 * TemplateVersions correlate to DevTest versions.  Since API changes could
 * occur across DevTest versions, we keep a list of templates specific to
 * each version of DevTest.
 */
public enum TemplateVersion {

    //---------+----------------
    // Id      | TemplateFolder
    //---------+----------------
    VERSION_7    (  "7.0" ),
    VERSION_8    (  "8.0" ),
    VERSION_9    (  "9.0" ),
    VERSION_10   ( "10.0" ),
    LATEST       ( "10.0" );


    private String version;


    TemplateVersion( String version )
    {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public String toString() {
        return version;
    }

    /**
     * Translates {@code version} into the best-guess of the TemplateVersion.
     * We're translating product {@code version} of DevTest/Lisa into which version
     * of templates to apply to generate the appropriate code for product {@code version}.
     * If {@code version} is null or does not match a known template version,
     * we'll return {@code TemplateVersion.LATEST}.
     *
     * @param version some product version of DevTest (a.k.a LISA).
     * @return our best guess of the template versions to apply
     */
    public static TemplateVersion fromString ( String version ) {

        if (StringHelper.isEmpty( version ) )
            return LATEST;
        if ( version.startsWith ("7") )
            return VERSION_7;
        if ( version.startsWith ("8"))
            return VERSION_8;
        if ( version.startsWith ("9"))
            return VERSION_9;
        if ( version.startsWith("10"))
            return VERSION_10;

        return LATEST;
    }
}
