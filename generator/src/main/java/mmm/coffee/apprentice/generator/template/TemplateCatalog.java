/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.generator.template;

import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupDir;

/**
 * An instance of a TemplateCatalog provides getter's
 * to the templates of specific DevTest version.
 * For example
 * <code>
 *     TemplateCatalog catalog = new TemplateCatalog ( "9" );
 * </code>
 * returns the templates appropriate for DevTest 9 code generation,
 * while
 * <code>
 *     TemplateCatalog catalog = new TemplateCatalog ( "8" );
 * </code>
 * returns the templates appropriate for DevTest 8 code generation.
 * And, finally,
 * <code>
 *     TemplateCatalog catalog = new TemplateCatalog ( TemplateVersion.LATEST );
 * </code>
 * returns the templates for whatever the latest supported version of DevTest is.
 * By 'supported', we mean 'supported by this code generator'.
 */
public class TemplateCatalog {

    private TemplateProvider stepTemplates;
    private TemplateProvider assertionTemplates;
    private TemplateProvider companionTemplates;
    private TemplateProvider filterTemplates;
    private TemplateProvider dataSetTemplates;
    private TemplateProvider dataProtocolTemplates;
    private TemplateVersion version;

    private STGroup templateGroup;


    /**
     * Creates a {@code TemplateCatalog} containing the templates of the given DevTest {@code version}.
     * If {@code version} is {@code null}, the latest version (found in {@code TemplateVersion.LATEST}, is used.
     *
     * @param version the DevTest version, may be {@code null}
     */
    public TemplateCatalog ( TemplateVersion version )
    {
        this ( version == null ? TemplateVersion.LATEST.getVersion() : version.getVersion() );
    }

    /**
     * Creates a {code TemplateCatalog} containing the templates of the given DevTest {@code version}.
     * If {@code version} is {@code null} or an empty string, the latest version (found in {@code TemplateVersion.LATEST},
     * is used.
     *
     * @param version the DevTest version; may be {@code null}
     */
    public TemplateCatalog ( String version )
    {
        if ( version == null )
            version = TemplateVersion.LATEST.getVersion();

        this.version = TemplateVersion.fromString( version );
        String path = String.format("templates/%s", version );
        templateGroup = new STGroupDir( path );
        loadProviders();
    }

    /**
     * Returns the DevTest version that this catalog's templates represent.
     * Since API's and code can change across DevTest versions, we have a catalog of templates
     * for each major version of DevTest. As you would expect, if the catalog version is "9.0",
     * then the templates are targeted for DevTest 9.0.
     *
     * @return
     */
    public TemplateVersion getVersion() {
        return version;
    }

    private void loadProviders()
    {
        stepTemplates = new StepTemplateProvider ( this );
        assertionTemplates = new AssertionTemplateProvider( this );
        companionTemplates = new CompanionTemplateProvider( this );
        filterTemplates = new FilterTemplateProvider( this );
        dataSetTemplates = new DataSetTemplateProvider( this );
        dataProtocolTemplates = new DataProtocolTemplateProvider( this );
    }

    public final TemplateProvider getStepTemplates()
    {
        return stepTemplates;
    }

    public final TemplateProvider getAssertionTemplates()
    {
        return assertionTemplates;
    }

    public final TemplateProvider getCompanionTemplates()
    {
        return companionTemplates;
    }

    public TemplateProvider getFilterTemplates()
    {
        return filterTemplates;
    }

    public final TemplateProvider getDataSetTemplates()
    {
        return dataSetTemplates;
    }

    public final TemplateProvider getDataProtocolTemplates()
    {
        return dataProtocolTemplates;
    }


    public ST getMessagesLookupTemplate() {

        return getTemplate ( TemplateType.MESSAGES.getTemplateName() );
    }

    public ST getAntBuildTemplate () {
        return getTemplate ( TemplateType.ANT_BUILD.getTemplateName() );
    }

    public ST getAntBuildPropertiesTemplate () {
        return getTemplate ( TemplateType.BUILD_PROPERTIES.getTemplateName() );
    }

    public ST getGradleBuildTemplate() {
        return getTemplate ( TemplateType.GRADLE_BUILD.getTemplateName() );
    }

    public ST getGradleSettingsTemplate() {
        return getTemplate ( TemplateType.GRADLE_SETTINGS.getTemplateName() );
    }

    public ST getLoggingPropertiesTemplate() {
        return getTemplate ( TemplateType.LOGGING_PROPERTIES.getTemplateName() );
    }

    public ST getLisaExtensionsTemplate() {
        return getTemplate ( TemplateType.EXTENSIONS.getTemplateName() );
    }

    /**
     * Returns the given template, or null.
     *
     * @param templateName the template to fetch
     *
     * @return the Velocity template appropriate to the file being generated and
     * the version of DevTest in which the file will be used.
     */

    public final ST getTemplate ( String templateName ) {
        return templateGroup.getInstanceOf( templateName );
    }


}
