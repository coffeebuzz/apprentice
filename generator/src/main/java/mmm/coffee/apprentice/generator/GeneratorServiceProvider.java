/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.generator;

import mmm.coffee.apprentice.generator.generator.ProjectGenerator;
import mmm.coffee.apprentice.model.stereotypes.Project;

/**
 * A default implementation of a GeneratorService
 */
public class GeneratorServiceProvider implements GeneratorService {

    /**
     * Generates the artifacts of {@code project
     * }
     * @param project the meta-data of the project to generate
     * @throws Exception if the code generation is unsuccessful
     */
    public void generate ( Project project ) throws Exception
    {
        if ( project == null )
            return;

        ProjectGenerator generator = new ProjectGenerator ();
        project.accept ( generator );
    }
}
