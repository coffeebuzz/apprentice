/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * The package contains the code generators.  The main entry point is the {@code ProjectGenerator},
 * since you will usually start with a {@code ProjectBean} and generate all the artifacts of
 * that project.  To initiate the generation of the project, follow this example
 * <code>
 *     ProjectBean project = ProjectReader.read( projectFile );
 *     ProjectGenerator generator = new ProjectGenerator ();
 *     projectBean.accept ( generator );
 * </code>
 */
package mmm.coffee.apprentice.generator.generator;