/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.generator.generator;

import mmm.coffee.apprentice.model.visitor.Visitor;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * This is the base class for classes that implement the Visitor pattern.
 * The {@code visit} method implemented here uses reflection
 * to invoke the specific {@code visit} methods implemented in the subclasses.
 * Polymorphism will handle resolving to the correct {@code visit} method.
 *
 * For example, suppose a subclass implements these visit methods:
 *
 * public void visit ( Companion bean ) { ... }
 * public void visit ( Filter bean ) { ... }
 *
 * Using reflection, we ask the subclass, "find the visit() method that takes [x] as its argument".
 * When [x] is a CompanionBean, reflection finds {@code visit (Companion bean)};
 * when [x] is a FilterBean, reflection finds {@code visit (Filter bean)}.
 *
 */
public abstract class AbstractVisitor implements Visitor {

    /**
     * Uses reflection to invoke the visit() method of the underlying object
     *
     * @param obj the target processed by the visit() method
     */
    public void visit ( Object obj )
    {
        if ( obj == null )
            return;

        try
        {
            Method visitMethod = this.getClass().getMethod("visit", obj.getClass() );
            visitMethod.invoke(this, obj );
        }
        catch (NoSuchMethodException noSuchMethodException)
        {
            // Reflection is peculiar about interfaces. In some cases, reflection does not
            // match a method, say, "visit(Project p)", where Project is an interface and 'p' is a ProjectBean,
            // which implements the Project interface.
            //
            // To handle cases where the "visit(T t)" method takes an interface T as its argument,
            // and the call to getMethod("visit", T) fails to find the visit() method,
            // we'll try the interfaces implemented by the "obj" class and see if we get a match.
            // Once we do find a visit method to invoke, we invoke it then break out of the loop.
            Class<?>[] ifaces = obj.getClass().getInterfaces();
            for ( Class<?> iface : ifaces ) {
                try {
                    // Method visitMethod = this.getClass().getMethod("visit", new Class<?>[]{iface});
                    Method visitMethod = this.getClass().getMethod("visit", iface );
                    visitMethod.invoke ( this, obj );
                    break;
                }
                catch (NoSuchMethodException ex ) {
                    // If there's no "visit(T t)" method for the interface we tried,
                    // move on to the next interface of the class.
                }
                catch (InvocationTargetException | IllegalAccessException err ) {
                    this.defaultVisit( obj );
                }
            }
        }
        catch ( InvocationTargetException | IllegalAccessException err ) {
            this.defaultVisit( obj );
        }
    }

    /**
     * Subclasses implement this class as a fall-back when no mapping to a visit method
     * is found.  During the search for a visit method, if we're asked, "find the visit()
     * method that takes [foo] as its argument", and we don't find a "visit (Foo obj)" method,
     * then "defaultVisit" is called instead.
     *
     * @param obj the object to be processes
     */
    public abstract void defaultVisit (Object obj);



}
