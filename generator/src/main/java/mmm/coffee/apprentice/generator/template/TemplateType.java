/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.generator.template;

/**
 * This class returns the paths of the various templates. This class
 * takes ownership of the template file names so as to centralize
 * those file names.
 */
public enum TemplateType {

    ANT_BUILD ( "AntBuild" ),
    BUILD_PROPERTIES ( "BuildProperties" ),

    ASSERTION_MODEL ( "AssertionModel" ),
    ASSERTION_EDITOR ( "EmptyDefaultEditor" ),
    ASSERTION_CONTROLLER ( "EmptyDefaultController" ),
    ASSERTION_TEST ( "AssertionTest" ),

    COMPANION_MODEL ( "CompanionModel" ),
    COMPANION_EDITOR ( "EmptyDefaultEditor" ),
    COMPANION_CONTROLLER ( "EmptyDefaultController" ),
    COMPANION_TEST ( "CompanionTest" ),

    DATA_SET_MODEL ( "DataSetModel" ),
    DATA_SET_EDITOR ( "EmptyDefaultEditor" ),
    DATA_SET_CONTROLLER ( "EmptyDefaultController" ),
    DATA_SET_TEST ( "DataSetTest"),

    DATA_PROTOCOL_MODEL ( "DphModel" ),
    DATA_PROTOCOL_EDITOR ( "EmptyDefaultEditor" ),
    DATA_PROTOCOL_CONTROLLER ( "EmptyDefaultController" ),
    DATA_PROTOCOL_TEST ( "DphTest" ),

    EXTENSIONS ( "LisaExtensions" ),


    FILTER_MODEL ( "FilterModel" ),
    FILTER_EDITOR ( "EmptyDefaultEditor" ),
    FILTER_CONTROLLER ( "EmptyDefaultController" ),
    FILTER_TEST ( "FilterTest" ),

    GRADLE_BUILD ( "GradleBuild" ),
    GRADLE_SETTINGS ( "GradleSettings" ),

    LOGGING_PROPERTIES ( "LoggingProperties" ),

    MESSAGES ( "MessageLookup" ),

    STEP_MODEL ( "StepModel" ),
    STEP_EDITOR ( "StepEditor" ),
    STEP_CONTROLLER ( "StepController" ),
    STEP_TEST ( "StepTest" );



    private String templateName; // the name of our template


    /**
     * Constructor
     *
     * @param templateName the template paired to this constant
     */
    TemplateType(String templateName)
    {
        this.templateName = templateName;
    }

    /**
     * Returns the name of the template file, such as 'step-mode.vm'.
     * @return the name of the template file
     */
    public String getTemplateName() {
        return templateName;
    }


}
