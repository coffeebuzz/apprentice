/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.generator.template;

/**
 * Implementation of a TemplateProvider for the DataSet templates
 */
public class DataSetTemplateProvider extends AbstractTemplateProvider {

    public DataSetTemplateProvider(TemplateCatalog catalog )
    {
        super ( catalog,
                TemplateType.DATA_SET_MODEL,
                TemplateType.DATA_SET_EDITOR,
                TemplateType.DATA_SET_CONTROLLER,
                TemplateType.DATA_SET_TEST );
    }
}
