/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.generator;

/**
 * This exception indicates a runtime error was encountered
 * during code generation.
 */
public class CodeGeneratorException extends RuntimeException {

    static final long serialVersionUID = 3532565653867917386L;

    /**
     * Constructs the exception
     *
     * @param msg the error message to present to the user
     * @param t the error wrapped by this exception..
     */
    public CodeGeneratorException(String msg, Throwable t) {
        super ( msg, t );
    }


}
