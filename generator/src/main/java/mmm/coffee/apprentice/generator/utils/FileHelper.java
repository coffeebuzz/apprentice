/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.generator.utils;


import java.io.File;
import java.io.IOException;

import java.util.Objects;
import java.util.logging.Logger;
import java.util.logging.LogManager;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;

/**
 * File manipulation utilities.  The class is named as is to avoid a name
 * collision with the Apache FileUtils class.
 */
public class FileHelper {

    private static final Logger LOG = LogManager.getLogManager().getLogger ( FileHelper.class.getName() );

    /**
     * Creates the empty {@code file} as well as its parent directory.
     * If {@code file} is null, the method silently returns.
     *
     * @param file the file to create, such as "/some/path/src/main/java/Output.java"
     * @throws IOException when things go wrong
     */
    public static void createFile ( File file ) throws IOException
    {
        // ---------------------------------------------------------------------
        // Precondition: if the file is null, simply return
        // ---------------------------------------------------------------------
        if ( file == null )
            return;

        // ---------------------------------------------------------------------
        // Step 1: if 'file' does not exist, then...
        // a) create its parent folder, if the parent folder does not exist
        // b) create 'file'
        // ---------------------------------------------------------------------
        if ( !file.exists() )
        {
            File parent = file.getParentFile();
            if  ( !parent.exists() ) {
                FileUtils.forceMkdir( parent );
            }
            boolean status = file.createNewFile();
            if (!status && LOG != null) {
                LOG.warning("An inconsistent value was encountered. file.exists() claims a file does not exist, but file.createNewFile() claims the file already exists");
                LOG.warning("The filename in question is: " + file.getAbsolutePath());
            }
        }
    }

    /**
     * Finds the first occurrence of a File named {@code fileName} within {@code directory} or its subdirectories.
     *
     * @param directory the starting point of the search
     * @param fileName the filename to find
     * @return the File's handle, if its found; or {@code null} if not found
     */
    public static File findFileWithinDirectory ( File directory, String fileName )
    {
        Objects.requireNonNull( directory, "The argument 'directory' cannot be null");
        Objects.requireNonNull( fileName, "The argument 'fileName' cannot be null");
        if ( !directory.isDirectory() )
            throw new IllegalArgumentException( "The directory argument is not a directory on the file system");

        return walk( directory, fileName );
    }

    /**
     * Recursively scans {@code directory} searching for the file named {@code fileName}
     * @param directory the directory being scanned
     * @param fileName the filename to find
     * @return the File handle, if the file is found, or {@code null}.
     */
    private static File walk (File directory, String fileName )
    {
        // ---------------------------------------------------------------------
        // Precondition: Ensure 'directory' is actually a directory. If its not,
        // we don't have anything to walk, so we'll return null.
        // ---------------------------------------------------------------------
        if ( !directory.isDirectory())
            return null;

        // ---------------------------------------------------------------------
        // Step 1: Get the list of files in 'directory'
        // ---------------------------------------------------------------------
        File[] fileList = directory.listFiles();

        if ( fileList == null )
            return null;

        // ---------------------------------------------------------------------
        // Step 2: Iterate the list, looking for a match to 'fileName'
        // ---------------------------------------------------------------------
        for ( File f : fileList )
        {
            // If this is the file being sought, return its handle
            if ( f.isFile() && f.getName().equals ( fileName ))
                return f;

            // If this is a subdirectory, walk it
            if ( f.isDirectory() ) {
                // If the answer was found in the walk, return it,
                // otherwise continue to the next item in fileList
                File tmp = walk(f, fileName);
                if (tmp != null)
                    return tmp;
            }
        }

        // If the file wasn't found in the directory, return null
        return null;
    }


    /**
     * Verifies that a directory is empty or does not exist. Semantically, this is similar
     * to checking for an empty string, where a null string or zero-length string are considered
     * empty.  In the same way, a null {@code directory} or empty directory both return {@code true}.
     * If {@code directory} is not a directory, or contains any Files, {@code false} is returned.
     *
     * @param someDirectory the directory to check; nullable
     * @return {@code true} if {@code someDirectory} is null or contains no files
     *         returns {@code false} if {@code someDirectory} is not a directory or if it contains files
     */
    public static boolean isEmptyDirectory(File someDirectory)
    {
        // A null directory is considered empty
        if ( someDirectory == null )
            return true;

        // A non-existent directory is considered empty
        if ( !someDirectory.exists())
            return true;

        // If this is not even a directory, return false
        if ( !someDirectory.isDirectory() )
            return false;

        // Get a list of the directory's content
        String[] s = someDirectory.list();

        // If the directory contains no files, return true
        if ( s == null || s.length == 0 )
            return true;

        // some files were found, so return false
        return false;
    }

    /**
     * Builds a random file name, of length {@code fileNameLength}.
     * Its the responsibility of the caller to use a length that's appropriate for the underlying file system.
     *
     * @param optionalPrefix the prefix of the filename. A null value is silently ignored.
     * @return the randomly generated name
     */
    public static String getUniqueFileName( String optionalPrefix )
    {
        long millis = System.currentTimeMillis();

        String suffix = RandomStringUtils.randomAlphanumeric( 8 ) + "_" + millis;

        if ( optionalPrefix != null )
            return optionalPrefix + "_" + suffix;
        else
            return suffix;
    }
}
