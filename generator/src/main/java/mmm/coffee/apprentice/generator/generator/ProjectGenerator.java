/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.generator.generator;

import mmm.coffee.apprentice.generator.CodeGeneratorException;
import mmm.coffee.apprentice.generator.decorator.StepDecorator;
import mmm.coffee.apprentice.generator.template.TemplateCatalog;
import mmm.coffee.apprentice.generator.template.TemplateProvider;
import mmm.coffee.apprentice.generator.utils.FileHelper;
import mmm.coffee.apprentice.model.stereotypes.*;
import mmm.coffee.apprentice.generator.template.TemplateVersion;
import mmm.coffee.apprentice.generator.decorator.ProjectDecorator;
import org.apache.commons.io.FileUtils;
import org.stringtemplate.v4.ST;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.*;

import java.util.logging.Logger;
import java.util.logging.LogManager;


/**
 * Generates a project for the plugin.  This includes creating the
 * workspace directory to which the generated code is written,
 * generating the Java code to implement the plugin, an ant build file
 * to compile the generated code, and the lisaextensions file used by
 * DevTest when the plugin is deployed.
 */
public class ProjectGenerator extends AbstractGenerator {

    private static Logger LOG = LogManager.getLogManager().getLogger(ProjectGenerator.class.getName());

    //
    // These are the parameter names used by the StringTemplate templates
    //
    public static final String STEP_KEY = "step";
    public static final String FILTER_KEY = "filter";
    public static final String ASSERTION_KEY = "assertion";
    public static final String COMPANION_KEY = "companion";
    public static final String DATASET_KEY = "dataset";
    public static final String DPH_KEY = "dph";
    public static final String MESSAGE_KEY = "messages";
    public static final String PROJECT_KEY = "project";
    public static final String PACKAGE_KEY = "packageName";
    public static final String BUNDLE_NAME_KEY = "bundleName";
    public static final String DEVTEST_HOME_KEY = "devTestHome";


    private Project projectBean;
    private ProjectDecorator projectDecorator;


    // We allow a project to specify multiple target versions.
    // We iterate through the list of target versions and,
    // for each target version, generate the code artifacts.
    // Since templates are version-specific, we need to know which
    // template version to fetch.  This variable tracks which
    // target version is currently being processed.
    private TemplateVersion currentTemplateVersion;

    // These lists contain references to each component generated.
    // The lists are used to generate the lisaextensions file.
    private List<Step> steps = new LinkedList<>();
    private List<Filter> filters = new LinkedList<>();
    private List<Assertion> assertions = new LinkedList<>();
    private List<Companion> companions = new LinkedList<>();
    private List<DataSet> dataSets = new LinkedList<>();
    private List<DataProtocol> dphList = new LinkedList<>();

    // Our handle to the Java generator
    private JavaCodeGenerator generator;

    // The TemplateCatalog provides the Velocity templates
    private TemplateCatalog templateCatalog;

    /**
     * Handle objects for which we did not implement a visit method.  Most likely, this is an
     * oversight, so we print a debug message so we know which class we missed.
     *
     * @param obj the object to be processed
     */
    public void defaultVisit(Object obj) {
        if (obj == null)
            return;

        // defaultVisit is our fail-safe to catch objects of class T that don't have a "visit (T t)" method
        // If we're trying to visit (T t), then most likely we need to implement "visit (T t)".
        if ( LOG != null )
            LOG.fine ("(defaultVisit) - No visit method was found for: " + obj.getClass().getName());
        else
            System.out.println("(defaultVisit) - No visit method was found for: " + obj.getClass().getName());
    }


    /**
     * A convenience method for the sake of presenting a more obvious entry point for
     * code generation.
     *
     * @param project the Project to generate
     */
    public void generate ( Project project )
    {
        visit ( project );
    }

    /**
     * Generates the artifacts for the given project.
     *
     * @param projectBean the meta-data of the project being generated
     */
    public void visit(Project projectBean) {
        if (projectBean == null)
            return;

        this.projectBean = projectBean;
        projectDecorator = new ProjectDecorator( projectBean );

        if ( LOG != null ) LOG.fine ("(visit) - visiting projectBean: " + projectBean.getPluginName());

        generator = new JavaCodeGenerator(projectBean);


        ApplicationTarget productTarget = projectBean.getApplicationTarget();
        currentTemplateVersion = translateProductVersionsToTemplateVersions(productTarget);

        templateCatalog = new TemplateCatalog( currentTemplateVersion );

        List<Component> componentList = projectBean.getComponents();

        // -----------------------------------------------------------------------------
        // Step 1: Generate the Messages.java class and the messages.properties file
        // If no Java files are being generate, then we don't need a Messages.java file,
        // so we -do- skip generating Messages.java for empty projects
        //
        // The Messages file is generated first because the other Java files
        // will import this class. If a MessageResource was not explicitly set,
        // we want to set a default so the Java files generated in Step 2 will
        // know what to import.
        // -----------------------------------------------------------------------------
        if ( componentList.size() > 0 ) {
            visit ( projectBean.getMessageResource() );
        }

        // -----------------------------------------------------------------------------
        // Step 2: Generate the Java source files
        //
        // Iterate through each component and invoke the visitor pattern.
        // The bean's accept() method will invoke our visit() method.
        // With polymorphism, the visit() method appropriate to the kind of
        // component being generated is invoked. We chose this approach to
        // avoid having a series of instanceof checks.  With any approach,
        // we have to determine which templates to load and resolve.
        // -----------------------------------------------------------------------------
        for (Component component : componentList) {
            component.accept(this);
            }



        // -----------------------------------------------------------------------------
        // Step 3: Generate the flavors of build files
        // -----------------------------------------------------------------------------
        generateAntBuildFile();
        generateGradleArtifacts();

        // -----------------------------------------------------------------------------
        // Step 4: Generate miscellaneous file
        // -----------------------------------------------------------------------------
        generateLoggingPropertiesFile();
        generateLisaExtensions();
    }

    /**
     * Generates the code for a custom step.
     *
     * @param bean the meta-data Step being generated
     */
    @SuppressWarnings("unused")
    public void visit(Step bean) throws CodeGeneratorException {
        if (bean == null)
            return;

        steps.add(bean);

        StepDecorator decorator = new StepDecorator(bean);

        generateCode(decorator, STEP_KEY, templateCatalog.getStepTemplates());
    }


    /**
     * Generates the code for a custom filter
     *
     * @param bean the meta-data of the Filter being generated
     */
    @SuppressWarnings("unused")
    public void visit(Filter bean) {
        if (bean == null)
            return;

        filters.add(bean);

        generateCode(bean, FILTER_KEY, templateCatalog.getFilterTemplates());
    }

    /**
     * Generates the code for a custom companion
     *
     * @param bean the meta-data of the Companion being generated
     */
    @SuppressWarnings("unused")
    public void visit(Companion bean) {
        if (bean == null)
            return;

        companions.add(bean);

        generateCode(bean, COMPANION_KEY, templateCatalog.getCompanionTemplates());
    }


    /**
     * Generates the code for a custom assertion
     *
     * @param bean the meta-data of the Assertion being generated.
     */
    @SuppressWarnings("unused")
    public void visit(Assertion bean) {
        if (bean == null)
            return;

        assertions.add(bean);

        generateCode(bean, ASSERTION_KEY, templateCatalog.getAssertionTemplates());
    }

    /**
     * Generates the code for a custom data set
     *
     * @param bean the meta-data of the DataSet to generate
     */
    @SuppressWarnings("unused")
    public void visit(DataSet bean) {
        if (bean == null)
            return;

        dataSets.add(bean);

        generateCode(bean, DATASET_KEY, templateCatalog.getDataSetTemplates());
    }


    /**
     * Generates the code for a custom data protocol handler
     *
     * @param bean the meta-data of the DataProtocol to generate
     */
    @SuppressWarnings("unused")
    public void visit(DataProtocol bean) {
        if (bean == null)
            return;

        dphList.add(bean);

        generateCode(bean, DPH_KEY, templateCatalog.getDataProtocolTemplates());
    }

    /**
     * Generates the Messages.java class and the messages.properties file
     *
     * @param bean the meta-data of the MessageResource to generate
     */
    @SuppressWarnings("unused")
    public void visit(MessageResource bean) {
        if (bean == null)
            return;

        try {
            // Step 1. Generate the Messages.java file
            ST template =  getTemplateCatalog().getMessagesLookupTemplate();
            template.add ( MESSAGE_KEY, bean );
            template.add ( BUNDLE_NAME_KEY, projectDecorator.getBundleName() );

            generator.generateJava(bean, MESSAGE_KEY, template);

            // Step 2: Generate the messages.properties files
            generateMessagesPropertiesFile();
        } catch (IOException e) {
            // LOG.debug ( e.getMessage() );
        }
    }


    /**
     * Translates product versions into template versions.  The product version indicates the
     * version of DevTest&copy; or LISA&copy;.  The template version indicates which version of
     * templates will generate the code for each product version.
     *
     * @param productVersions defines to which version of DevTest the plugin code will be deployed
     * @return the TemplateVersion that matches the productVersion
     */
    private TemplateVersion translateProductVersionsToTemplateVersions(ApplicationTarget productVersions) {

        if (productVersions == null ) {
            return TemplateVersion.LATEST;
        } else {
            return TemplateVersion.fromString ( productVersions.getVersion() );
        }
    }

    /**
     * Generates the model, editor, and controller Java classes for the {@code bean}.
     * Editor and Controller classes are optional, since the default editors and
     * controllers built into DevTest/Lisa can be used instead of custom editors or controllers.
     * Model-layer classes are always be generated, since they implement the business logic
     * of the custom component.
     *
     * @param bean               the step/filter/assertion/companion/dataset/dph bean
     * @param templateProvider  provides the model, editor, and controller templates
     */
    private void generateCode(MVCEntity bean, String cacheKey, TemplateProvider templateProvider)
            throws CodeGeneratorException {

        try {
            // Step 1: Generate the Model class
            ST modelTemplate = templateProvider.getModelTemplate();
            modelTemplate.add( MESSAGE_KEY, projectBean.getMessageResource());
            modelTemplate.add( cacheKey, bean );
            generator.generateJava(bean, cacheKey, modelTemplate);

            // Step 2: If a Controller is defined and needs to be generated, do so
            if (bean.getController() != null && bean.getController().isGenerateCode()) {
                ST controllerTemplate = templateProvider.getControllerTemplate();
                controllerTemplate.add ( cacheKey, bean );
                controllerTemplate.add ( MESSAGE_KEY, projectBean.getMessageResource() );
                generator.generateJava(bean.getController(), cacheKey, controllerTemplate );
            }

            // Step 3: If an Editor is defined and needs to be generated, do so
            if (bean.getEditor() != null && bean.getEditor().isGenerateCode()) {
                ST editorTemplate = templateProvider.getEditorTemplate();
                editorTemplate.add ( cacheKey, bean );
                editorTemplate.add ( MESSAGE_KEY, projectBean.getMessageResource() );
                generator.generateJava(bean.getEditor(), cacheKey, editorTemplate );
            }

            // Step 4: Generate the JUnit test skeleton
            ST junitTemplate = templateProvider.getJUnitTemplate();
            if ( junitTemplate != null ) {
                junitTemplate.add(cacheKey, bean);
                generator.generateJUnit(bean, cacheKey, junitTemplate);
            }
        } catch ( IOException err ) {
            // We'll land here if the template is not found
            throw new CodeGeneratorException( err.getMessage(), err );
        }
    }

    /**
     * Generates the code for the single class, {@code bean}.
     *
     * @param bean the class metadata to generate as code
     * @param cacheKey the name of the class entity expected by the template
     * @param templateProvider provides look-up of the model-layer template
     */
    private void generateCode ( Component bean, String cacheKey, TemplateProvider templateProvider)
    {
        try {
            // Step 1: Generate the class
            ST template = templateProvider.getModelTemplate();
            template.add ( cacheKey, bean );
            generator.generateJava(bean, cacheKey, template );

            // Step 2: Generate the JUnit test class
            ST junitTemplate = templateProvider.getJUnitTemplate();
            if ( junitTemplate != null ) {
                junitTemplate.add ( cacheKey, bean );
                generator.generateJUnit( bean, cacheKey, junitTemplate );
            }

        } catch ( IOException err ) {
            // We'll land here if the template is not found
            throw new CodeGeneratorException( err.getMessage(), err );
        }
    }

    /**
     * Writes the messages.properties file into its proper directory within the generated project.
     *
     * @throws IOException if the messages.properties file cannot be created
     */
    private void generateMessagesPropertiesFile() throws IOException {
        MessageStringCollector decorator = new MessageStringCollector(projectBean);
        Properties properties = decorator.getProperties();

        File propertiesFile = projectDecorator.getMessagePropertiesFile();
        FileHelper.createFile(propertiesFile);
        PrintWriter pw = new PrintWriter(propertiesFile, Charset.defaultCharset().toString());

        properties.store(pw, "This file contains localized messages");

        pw.close();
    }

    /**
     * Generates the ant build.xml file and build.properties file
     *
     * @throws CodeGeneratorException if the build.xml file cannot be created
     */
    private void generateAntBuildFile() {
        try {

            TemplateEngine engine = new TemplateEngine();

            // Step 2: generate the build.xml file
            ST antBuildTemplate = getTemplateCatalog().getAntBuildTemplate();
            antBuildTemplate.add ( PROJECT_KEY, projectBean );

            File fBuild = projectDecorator.getAntBuildFile();
            engine.generateCode(antBuildTemplate, fBuild);

            // Step 3: generate the build.properties file
            // In the build.properties, we define the path to DevTestHome
            ST bpTemplate = getTemplateCatalog().getAntBuildPropertiesTemplate();
            bpTemplate.add ( DEVTEST_HOME_KEY, projectBean.getApplicationTarget().getHomeDirectory());
            File fProps = projectDecorator.getAntBuildPropertiesFile();
            engine.generateCode(bpTemplate, fProps);
        } catch (IOException ioe) {
            throw new CodeGeneratorException("An error occurred generating the Ant artifacts", ioe);
        }
    }

    /**
     * Generates the gradle.build and gradle.settings files
     *
     * @throws CodeGeneratorException if either file cannot be created
     */
    private void generateGradleArtifacts() {
        try {
            // Step 1: Generate the build.gradle file
            TemplateEngine engine = new TemplateEngine();
            ST gradleBuildTemplate = getTemplateCatalog().getGradleBuildTemplate();
            gradleBuildTemplate.add ( PROJECT_KEY, projectBean );
            gradleBuildTemplate.add ( DEVTEST_HOME_KEY, projectBean.getApplicationTarget().getHomeDirectory());

            File fBuild = projectDecorator.getGradleBuildFile();
            engine.generateCode ( gradleBuildTemplate, fBuild );

            // Step 2: Generate the settings.gradle file
            ST gradleSettingsTemplate = getTemplateCatalog().getGradleSettingsTemplate();
            gradleSettingsTemplate.add ( PROJECT_KEY, projectBean );

            File fSettings = projectDecorator.getGradleSettingsFile();
            engine.generateCode( gradleSettingsTemplate, fSettings );

        }
        catch (IOException ioe) {
            throw new CodeGeneratorException("An error occurred generating the Gradle artifacts", ioe );
        }
    }

    /**
     * Generates the logging.properties file. The presence of the logging.properties
     * file enables the generated code to execute within the debugger of an IDE.
     * The first thing DevTest components do is initialize their loggers, so we need
     * a logging.properties in the classpath.
     */
    private void generateLoggingPropertiesFile() {
        try {
            // Step 1: Generate the logging.properties file
            TemplateEngine engine = new TemplateEngine();
            ST loggingPropertiesTemplate = getTemplateCatalog().getLoggingPropertiesTemplate();

            String packageName;
            if ( ! projectBean.getComponents().isEmpty() ) {
                Component c = projectBean.getComponents().get(0);
                packageName = c.getPackageName();
            } else {
                packageName = "some.package.name";
            }

            loggingPropertiesTemplate.add ( PACKAGE_KEY, packageName );

            File fBuild = projectDecorator.getLoggingPropertiesFile();
            engine.generateCode ( loggingPropertiesTemplate, fBuild );
        }
        catch (IOException ioe) {
            throw new CodeGeneratorException("An error occurred generating the Gradle artifacts", ioe );
        }
    }



    private void generateLisaExtensions() {
        try {
            // Step 1: Create the file to which the extensions will be written
            File fExtensions = projectDecorator.getLisaExtensionsFile();
            FileUtils.forceMkdir( fExtensions.getParentFile() );
            PrintWriter pw = new PrintWriter ( fExtensions, Charset.defaultCharset().toString()  );

            // Step 2: Set up the template
            ST template = getTemplateCatalog().getLisaExtensionsTemplate();
            template.add( PROJECT_KEY, projectDecorator );

            // Step 3: Generate the template
            pw.write( template.render() );

            // Step 4: Close the file
            pw.close();

        } catch (IOException ioe) {
            throw new CodeGeneratorException("An error occurred generating the lisaextensions file.", ioe);
        }
    }

    /**
     * Returns the TemplateCatalog for the appropriate DevTest version.
     *
     * @return the TemplateCatalog
     */
    private TemplateCatalog getTemplateCatalog()
    {
        if (templateCatalog == null) {
            if (projectBean.getApplicationTarget() != null)
                templateCatalog = new TemplateCatalog(projectBean.getApplicationTarget().getVersion());
            else
                templateCatalog = new TemplateCatalog(TemplateVersion.LATEST);
        }
        return templateCatalog;
    }

}
