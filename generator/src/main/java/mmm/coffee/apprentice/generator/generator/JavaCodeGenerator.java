/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.generator.generator;

import mmm.coffee.apprentice.model.beans.ProjectBean;
import mmm.coffee.apprentice.model.stereotypes.Component;
import mmm.coffee.apprentice.model.stereotypes.Project;
import mmm.coffee.apprentice.model.stereotypes.WorkspaceTarget;
import mmm.coffee.apprentice.model.helpers.StringHelper;
import org.stringtemplate.v4.ST;

import java.io.*;


import java.util.Objects;
import java.util.logging.Logger;

/**
 * The {@code JavaCodeGenerator} generates Java class files, specifically
 * the model class, editor class and controller class of a plugin.
 */
public class JavaCodeGenerator extends AbstractGenerator {

    private Project project;
    private String template;
    private TemplateEngine engine;

    private static Logger logger = Logger.getLogger(JavaCodeGenerator.class.getName());


    /**
     * Constructor
     *
     * @param project the project being generated
     */
    public JavaCodeGenerator(Project project) {

        Objects.requireNonNull( project, "The argument 'project' cannot be null" );

        this.project = project;
        engine = new TemplateEngine();
    }

    public void setProject ( ProjectBean project ) {
        this.project = project;
    }
    public Project getProject() {
        return project;
    }

    public void setTemplate ( String template ) {

        this.template = template;
    }

    public String getTemplate() {

        return template;
    }


    /**
     * {@inheritDoc}
     *
     * @param obj {@inheritDoc}
     */
    public void defaultVisit ( Object obj )
    {
        this.visit ( obj );
    }

    /**
     * Our visit method that enables the Visitor pattern for us.
     *
     * @param obj the target processed by the visit() method
     */
    @Override
    public void visit ( Object obj )
    {
        // empty
    }

    /**
     * A convenience method for the sake of presenting a more obvious entry point for
     * code generation.
     *
     * @param project the Project to generate
     */
    public void generate ( Project project )
    {
        visit ( project );
    }

    /**
     * Generates a Java source file
     *
     * @param bean contains the meta-data about the Java file to generate
     * @param cacheKey the cache key passed to Velocity.  The Velocity template references this value.
     * @param template the StringTemplate template being rendered
     *
     * @return the File handle of the generated source file
     * @throws IOException if the Java source file cannot be created
     */
    public File generateJava ( Component bean, String cacheKey, ST template ) throws IOException
    {
        Objects.requireNonNull ( bean, "A null Component object is not allowed") ;
        Objects.requireNonNull ( template, "A null template is not allowed" );
        if ( StringHelper.isEmpty( cacheKey ))
            throw new IllegalArgumentException("An empty cacheKey is not allowed");

        logger.entering(JavaCodeGenerator.class.getName(), "generateJava");
        logger.finest("bean: "+bean);
        logger.finest("cacheKey: "+cacheKey);
        logger.finest("template: "+template.getName());
        logger.fine ("Generating Java file for " + bean.getClassName() + " with template: " + template.getName());

// TODO: is this block needed? Do we have a code path where the message resource is missing?
        // ------------------------------------------------------------------------------------------
        // If a MessageResource exists for the project and the MessageResource has not
        // already be cached in the context, then add it.
        // ------------------------------------------------------------------------------------------
//        if ( project.getMessageResource() != null )
//            context.put ( ProjectGenerator.MESSAGE_KEY, project.getMessageResource() );

        File outputFile = getSourceFile (project.getWorkspaceTarget(), bean.getClassName());

        engine.generateCode( template, outputFile );
        logger.exiting (JavaCodeGenerator.class.getName(), "generateJava");
        return outputFile;
    }

    /**
     * Generates a JUnit source file
     *
     * @param bean contains the meta-data about the Java file to generate
     * @param cacheKey the cache key passed to Velocity.  The Velocity template references this value.
     * @param template the StringTemplate template being rendered
     *
     * @return the File handle of the generated source file
     * @throws IOException if the JUnit source file cannot be created
     */
    public File generateJUnit ( Component bean, String cacheKey, ST template ) throws IOException
    {
        Objects.requireNonNull ( bean, "A null Component object is not allowed") ;
        Objects.requireNonNull ( template, "A null template is not allowed" );
        if ( StringHelper.isEmpty( cacheKey ))
            throw new IllegalArgumentException("An empty cacheKey is not allowed");

        File outputFile = getJUnitFile ( project.getWorkspaceTarget(), bean.getTestClassName() );
        engine.generateCode( template, outputFile );
        return outputFile;
    }


    /**
     * Returns the Java file for the given {@code className}. The file may not exist, since
     * our role is to determine the fully-qualified path to the file the code generator will write.
     *
     *
     * @param className the fully qualified classname
     * @return the Java File, which may not exist.
     */
    private File getSourceFile ( WorkspaceTarget workspaceTarget, final String className )
    {
        StringBuilder sb = new StringBuilder();

        // Step 1: If an workspaceTarget directory was specified, make that the parent of the source file
        if ( workspaceTarget != null ) {
            if (!StringHelper.isEmpty(workspaceTarget.getBaseDirectory())) {
                // TODO: ENHANCEMENT:
                // Implement a FileHelper.normalizeWithEndSeparator method
                // to normalize the path and add '/' to the end of the path as needed.
                sb.append(workspaceTarget.getBaseDirectory()).append("/");
            }
            if (!StringHelper.isEmpty(workspaceTarget.getSourceDirectory())) {
                sb.append(workspaceTarget.getSourceDirectory()).append("/");
            }
        }
        // Step 2: Replace all '.' in the packageName with '/', and add '.java' to the end of the name
        // So, com.widget.MyWidget becomes com/widget/MyWidget.java
        String fqPath = StringHelper.safeString(sb.toString()) + className.replace(".", "/") + ".java";

        // Step 3: Create the File object. Remember that creating a File object does not
        // guarantee the parent directory of the file exists; creating the directories is handled later.
        return new File( fqPath );
    }


    /**
     * Returns the JUnit source file for the given {@code className}. The file may not exist, since
     * our role is to determine the fully-qualified path to the file the code generator will write.
     *
     * @param className the fully qualified classname
     * @return the Java File, which may not exist.
     */
    private File getJUnitFile ( WorkspaceTarget workspaceTarget, final String className )
    {
        StringBuilder sb = new StringBuilder();

        // Step 1: If an workspaceTarget directory was specified, make that the parent of the source file
        if ( workspaceTarget != null ) {
            if (!StringHelper.isEmpty(workspaceTarget.getBaseDirectory())) {
                // TODO: ENHANCEMENT:
                // Implement a FileHelper.normalizeWithEndSeparator method
                // to normalize the path and add '/' to the end of the path as needed.
                sb.append(workspaceTarget.getBaseDirectory()).append("/");
            }
            if (!StringHelper.isEmpty(workspaceTarget.getSourceDirectory())) {
                sb.append(workspaceTarget.getTestSourceDirectory()).append("/");
            }
        }
        // Step 2: Replace all '.' in the packageName with '/', and add '.java' to the end of the name
        // So, com.widget.MyWidget becomes com/widget/MyWidget.java
        String fqPath = StringHelper.safeString(sb.toString()) + className.replace(".", "/") + ".java";

        // Step 3: Create the File object. Remember that creating a File object does not
        // guarantee the parent directory of the file exists; creating the directories is handled later.
        return new File( fqPath );
    }


}
