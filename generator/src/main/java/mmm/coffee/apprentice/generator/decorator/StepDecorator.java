/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.generator.decorator;

import mmm.coffee.apprentice.model.stereotypes.Component;
import mmm.coffee.apprentice.model.stereotypes.MVCEntity;
import mmm.coffee.apprentice.model.stereotypes.Parameter;
import mmm.coffee.apprentice.model.stereotypes.Step;
import mmm.coffee.apprentice.model.visitor.Visitor;

import java.util.List;

/**
 * This class decorates a StepBean, adding methods needed for
 * the code generator. Specifically, the methods {@code getExecuteArguments}
 * and {@code getExecuteSignature} provide pieces of the {@code execute}
 * method produced by the code generator.  Otherwise, most of the
 * methods found here are simply delegated to the {@code bean}.
 *
 * The {@code getExecuteSignature} returns a well-formed {@code execute} method signature,
 * such as
 * <code>
 *     public static void execute ( ...args... );
 * </code>
 * while the {@code getExecuteArguments} method returns a well-formed Java statement
 * of the Step invoking the {@code execute} method, something like
 * <code>
 *     Step.execute ( ...args... );
 * </code>
 *
 * In both cases, the args list is determined by the parameter's in the step.
 * For example, if the parameters are named 'hostName' and 'port', the {@code getExecuteSignature}
 * will return something like
 * <code>
 *     public static void execute ( String hostName, String port ) { ... }
 * </code>
 * while {@code getExecuteArguments} will return something like
 * <code>
 *     Step.execute ( hostName, port );
 * </code>
 */
public class StepDecorator implements MVCEntity {

    private Step bean;

    /**
     * Constructor
     *
     * @param bean the StepBean being decorated; should not be null.
     */
    public StepDecorator ( Step bean )
    {
        this.bean = bean;
    }


    /**
     * Returns the UUID of the underlying {@code StepBean}.
     * Null is returned if the {@code StepBean} is null.
     * @return the UUID of the decorated {@code StepBean}
     */
    final public String getUUID() {
        if ( bean != null )
            return bean.getUUID();
        return null;
    }

    /**
     * Assigns a UUID to the underlying {@code StepBean}.
     * @param uuid the UUID to assign. A null value can be assigned, which essentially clears the UUID.
     */
    final public void setUUID ( String uuid ) {
        if ( bean != null )
            bean.setUUID ( uuid );
    }

    /**
     * Returns the EditorBean for this Step
     * @return the EditorBean of the step
     */
    public Component getEditor() {
        return bean.getEditor();
    }

    /**
     * Assigns the EditorBean to this Step
     * @param editor the editor of this Step
     */
    public void setEditor ( Component editor ) {
        bean.setEditor ( editor );
    }


    /**
     * Returns the ControllerBean of this Step
     * @return the controller of this Step
     */
    public Component getController() {
        return bean.getController();
    }

    /**
     * Assigns the ControllerBean
     * @param controller the controller of this Step
     */
    public void setController ( Component controller ) {
        bean.setController( controller );
    }

    /**
     * Returns the data fields for this assertion.
     * These fields appear in the UI and allow the user to set parameters for the assertion.
     *
     * @return the assertion's data fields. May return {@code null}.
     */
    public List<Parameter> getParameters() {
        return bean.getParameters();
    }

    /**
     * Assigns the fields of this assertion.
     *
     * @param fields the fields for this assertion.  {@code Null} is allowed.
     */
    public void setParameters(List<Parameter> fields) {
        bean.setParameters(fields);
    }


    /**
     * Accepts the {@code visitor}, to enable the Visitor pattern
     *
     * @param visitor the visitor; must not be null
     */
    public void accept ( Visitor visitor ) {
        visitor.visit(this);
    }

    /**
     * Returns the package name of a class
     * @return the step's package name, such as {@code "mmm.coffee.plugin"}
     */
    public String getPackageName() {
        return bean.getPackageName();
    }

    /**
     * Returns the fully qualified class name, such as {@code "com.foo.example.MyWidget"}
     * @return the fully qualified class name
     */
    @Override public String getClassName() {
        return bean.getClassName();
    }

    @Override public void setClassName(String className) {
        bean.setClassName( className );
    }

    @Override public String getTestClassName() {
        return bean.getTestClassName();
    }

    @Override public void setTestClassName(String className) {
        bean.setTestClassName( className );
    }


    /**
     * Returns the classname without the package name.
     * For example, if the classname is 'org.sample.MyApp',
     * this method returns 'MyApp'
     *
     * @return the class name without the package name
     */
    @Override public String getSimpleName() {
        return bean.getSimpleName();
    }

    /**
     * Returns the classname without the package name.
     * For example, if the classname is 'org.sample.MyApp',
     * this method returns 'MyApp'
     *
     * @return the class name without the package name
     */
    @Override public String getSimpleTestName() {
        return bean.getSimpleTestName();
    }


    /**
     * Returns {@code true} if the code generator should generate this class.
     * Returns {@code false} when a default class provided by DevTest is used.
     *
     * @return {@code true} if the code generator should generate this class; {@code false} otherwise.
     */
    @Override public boolean isGenerateCode() {
        return bean.isGenerateCode();
    }

    @Override public void setGenerateCode( boolean flag ) {
        bean.setGenerateCode( flag );
    }
    @Override public boolean getGenerateCode() {
        return bean.getGenerateCode();
    }

    /**
     * Returns the arg list that will appear in the code that calls this object's execute method.
     * The code generator will generate an {@code execute} method that looks like
     * <code>
     * Step.execute ( ${getExecuteArguments} ) { ... };
     * </code>
     * The returned string will look like this: "arg0, arg1, arg2, arg3"
     *
     * This method is called by the Velocity templates.
     *
     * @return the argument list
     */
    public String getExecuteArguments() {
        StringBuilder sb = new StringBuilder();

        int i = 0;
        int count = bean.getParameters().size();
        for ( Parameter field : bean.getParameters() )
        {
            sb.append ( field.getAlternateVariableName() );
            i++;
            if ( i < count ) sb.append ( ", ");
        }
        return sb.toString();

    }

    /**
     * Returns the arguments that will appear in the execute( ... ) method.  The String returned
     * is injected where the "..." appears.
     *
     * @return the arguments that will appear in the execute method
     */
    public String getExecuteSignature() {
        StringBuilder sb = new StringBuilder();

        int i = 0;
        int count = bean.getParameters().size();
        for ( Parameter field : bean.getParameters() )
        {
            sb.append ( field.getDataType().getJavaType() ).append (" ").append(field.getVariableName());
            i++;

            // Decide if we need a comma to separate us from the next field
            if ( i < count ) sb.append ( ", ");
        }
        return sb.toString();
    }


    @Override public boolean canEqual ( Object obj ) {
        return bean.canEqual(obj);
    }


    public StepDecorator deepCopy() {
        return new StepDecorator( bean );
    }
}
