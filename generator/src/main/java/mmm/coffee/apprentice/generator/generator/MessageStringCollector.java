/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.generator.generator;

import mmm.coffee.apprentice.model.stereotypes.*;
import mmm.coffee.apprentice.model.helpers.StringHelper;

import java.util.List;
import java.util.Properties;

/**
 * This class collects a list of the message strings that need to be externalized.
 * The general usage is:
 * <code>
 *     MessageStringCollector collector = new MessageStringCollector ( projectBean );
 *     collector.getProperties();
 * </code>
 */
public class MessageStringCollector extends AbstractVisitor {


    private Properties properties = new Properties();

    /**
     * Constructor
     * @param bean the project being searched for message strings.
     */
    public MessageStringCollector(Project bean) {
        if ( bean != null )
            bean.accept ( this );
    }

    /**
     * Returns the message strings to be externalized. The returned list may be empty, but is never null.
     * @return the message strings to externalize
     */
    public Properties getProperties() {
        return properties;
    }

    /**
     * Populates an internal lookup table with the key/value pairs
     * that need to be written to the messages.properties file.
     * These key/value pairs are for localization support.
     *
     * Example entries are
     * <code>
     *     processing.messages=Processing...
     *     execute.button=Execute
     *     someWidget.someField.label=Field Value:
     *     someWidget.someField.tooltip=help text goes here
     * </code>
     *
     * When this method returns, calling {@code getProperties()} returns the key/value pairs.
     *
     * @param project the project being scanned for message string candidates
     */
    @SuppressWarnings("unused")
    public void visit (Project project)
    {
        // Step 1: Add almost-always-present values
        properties.put ( "executeButton.label", "Execute" );
        properties.put ( "testButton.label", "Test");
        properties.put ( "selectButton.label", "Select" );
        properties.put ( "onRuntimeError.label", "On Error:");
        properties.put ( "message.processing", "Processing...");
        properties.put ( "message.executionError", "Error:" );

        // Step 2: For each field of each component, add each label and tooltip's key/value pairs

        if ( project != null )
        {
            List<Component> components = project.getComponents();
            if ( components != null ) {
                for ( Component item : components ) {
                    item.accept(this);
                }
            }
        }
    }

    /**
     * Externalize the step's messages to a resource bundle
     *
     * @param bean the step whose message strings are externalized by this method
     */
    @SuppressWarnings("unused")
    public void visit ( Step bean )
    {
        if ( bean.getController() != null ) {
            properties.put(bean.getController().getSimpleName() + ".helpString", "Help text goes here");
            properties.put(bean.getController().getSimpleName() + ".editorName", bean.getSimpleName() );
        }
        properties.put ( bean.getSimpleName() + ".typeName",  bean.getSimpleName() );
        properties.put ( bean.getSimpleName() + ".defaultStepName", bean.getSimpleName());

        addParameters(bean.getParameters());
    }


    /**
     * Externalize the assertion's messages to a resource bundle
     *
     * @param bean the assertion whose message strings are externalized by this method
     */
    @SuppressWarnings("unused")
    public void visit ( Assertion bean )
    {
        String typeKey = bean.getSimpleName() + ".typeName";
        String typeValue = bean.getSimpleName();

        properties.put(typeKey, typeValue);

        String whenTrueKey = bean.getSimpleName() + ".whenVerdictIsTrue";
        String whenFalseKey = bean.getSimpleName() + ".whenVerdictIsFalse";

        String whenTrueValue = " returned a verdict of: true";
        String whenFalseValue = " returned a verdict of: false";

        properties.put ( whenTrueKey, whenTrueValue );
        properties.put ( whenFalseKey, whenFalseValue );

        addParameters(bean.getParameters());
    }

    /**
     * Externalize the filter's messages to a resource bundle
     *
     * @param bean the filter whose message strings are externalized by this method
     */
    @SuppressWarnings("unused")
    public void visit ( Filter bean )
    {
        String typeKey = bean.getSimpleName() + ".typeName";
        String typeValue = bean.getSimpleName();

        properties.put(typeKey, typeValue);

        addParameters(bean.getParameters());
    }

    /**
     * Externalize the companion's messages to a resource bundle
     *
     * @param bean the filter whose message strings are externalized by this method
     */
    @SuppressWarnings("unused")
    public void visit ( Companion bean )
    {
        String typeKey = bean.getSimpleName() + ".typeName";
        String typeValue = bean.getSimpleName();

        properties.put(typeKey, typeValue);

        addParameters(bean.getParameters());
    }

    /**
     * Externalize the companion's messages to a resource bundle
     *
     * @param bean the filter whose message strings are externalized by this method
     */
    @SuppressWarnings("unused")
    public void visit ( DataSet bean )
    {
        String typeKey = bean.getSimpleName() + ".typeName";
        String typeValue = bean.getSimpleName();

        properties.put(typeKey, typeValue);

        addParameters(bean.getParameters());
    }


    /**
     * Adds the field's strings to the properties list
     *
     * @param fields the fields to process
     */
    private void addParameters(List<Parameter> fields)
    {
        for ( Parameter field : fields ) {
            if ( field.getTooltip() != null ) {
                Message tt = field.getTooltip();
                properties.put ( tt.getKey(), tt.getValue() );
            }
            if ( field.getLabel() != null ) {
                Message mb = field.getLabel();
                properties.put ( mb.getKey(), mb.getValue() );
            }
        }
    }

    /**
     * Add the localized messages of a field
     *
     * @param fieldBean the field whose message strings are externalized
     */
    @SuppressWarnings("unused")
    public void visit ( Parameter fieldBean ) {
        if ( fieldBean.getLabel() != null ) {
            String key = fieldBean.getLabel().getKey();
            String value = fieldBean.getLabel().getValue();
            key = StringHelper.safeString(key);
            value = StringHelper.safeString(value);
            properties.put(key,value);
        }
        if ( fieldBean.getTooltip() != null ) {
            String key = fieldBean.getTooltip().getKey();
            String value = fieldBean.getTooltip().getValue();
            key = StringHelper.safeString(key);
            value = StringHelper.safeString(value);
            properties.put(key,value);
        }
    }


    /**
     * This is a No-Op method for objects that don't have message strings that need to be externalized.
     *
     * @param obj the object
     */
    public void defaultVisit ( Object obj )
    {
        // empty
    }


}
