/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.generator.template;


import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupDir;

/**
 * Concrete implementations of TemplateProviders will extend this class.
 * This class provides general implementations of the {@code getModelTemplate},
 * {@code getEditorTemplate} and {@code getControllerTemplate} methods.
 */
public abstract class AbstractTemplateProvider implements TemplateProvider {

    private TemplateType model;
    private TemplateType view;
    private TemplateType controller;
    private TemplateType junit;

    private TemplateCatalog catalog;


    /**
     * Constructor
     * @param catalog the catalog containing our templates. Catalogs are specific to the DevTest version,
     *                which means there's a catalog for DevTest 8.0 templates, another catalog for DevTest 9.0 templates,
     *                and another for DevTest 10.0, and so on.
     * @param model the dictionary entry for the model-layer template
     * @param view the dictionary entry for the editor-layer template
     * @param controller the dictionary entry for the controller-layer template
     */
    public AbstractTemplateProvider (TemplateCatalog catalog, TemplateType model, TemplateType view, TemplateType controller, TemplateType junitTemplate)
    {
        this.model = model;
        this.view = view;
        this.controller = controller;
        this.catalog = catalog;
        this.junit = junitTemplate;
    }


    // TODO: Since the version is the same for a given project, we can create one Provider.getProvider(version),
    // and then provider.getTemplate ( TemplateType );
    protected final ST getTemplate (TemplateType template ) {
        return catalog.getTemplate ( template.getTemplateName() );
    }

    @Override
    public ST getModelTemplate() {
        return getTemplate ( model );
    }

    @Override
    public ST getControllerTemplate() {
        return getTemplate ( controller );
    }

    @Override
    public ST getEditorTemplate() {
        return getTemplate ( view );
    }

    @Override
    public ST getJUnitTemplate() { return getTemplate ( junit ); }

}
