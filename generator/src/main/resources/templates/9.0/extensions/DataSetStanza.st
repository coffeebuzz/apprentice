DataSetStanza(dataSets) ::=
<<
#
# Custom data sets
#
datasets=<dataSets : { ds | <ds.className>}; separator=",\\\n">

<dataSets : { ds |
<ds.className>=<ds.controller.className>,<ds.editor.className>
}>
>>