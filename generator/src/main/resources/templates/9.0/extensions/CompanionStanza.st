CompanionStanza(companions) ::=
<<
#
# Custom companions
#
companions=<companions : { c | <c.className>}; separator=",\\\n">

<companions : { c |
<c.className>=<c.controller.className>,<c.editor.className>
}>
>>