FilterStanza(filters) ::=
<<
#
# Custom filters
#
filters=<filters : { f | <f.className>}; separator=",\\\n">

<filters : { f |
<f.className>=<f.controller.className>,<f.editor.className>
}>
>>