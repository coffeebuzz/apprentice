StepStanza(steps) ::=
<<
#
# Custom steps
#
nodes=<steps : { s | <s.className>}; separator=",\\\n">

<steps : { s |
<s.className>=<s.controller.className>,<s.editor.className>
}>
>>