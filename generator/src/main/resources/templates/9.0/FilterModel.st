FilterModel(filter,messages) ::=
<<
<common/Copyright()>

package <filter.packageName>;

import com.itko.lisa.annotations.*;
import com.itko.lisa.annotations.Assertion;
import com.itko.lisa.test.FilterBaseImpl;
import com.itko.lisa.test.TestExec;
import com.itko.lisa.test.TestRunException;
import com.itko.lisa.test.TestDefException;
import com.itko.util.ParameterList;
import com.itko.util.Parameter;
import com.itko.util.StrUtil;
import com.itko.util.XMLUtils;
import com.itko.lisa.annotations.Filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.w3c.dom.Element;

import <messages.className>;



/**
 * TODO: Document the behavior of this filter
 *
 */
public class <filter.simpleName> extends FilterBaseImpl {

    private static final Logger LOG = LoggerFactory.getLogger( <filter.simpleName>.class.getName() );

    /*
     * Instance variables
     */
    <common/InstanceVariables(filter.parameters)>

    /*
     * Get/Set methods
     */
    <common/GetSetMethods(filter.parameters)>


    /**
     * Returns the display name of this filter.
     */
    public String getTypeName() throws Exception {
        return <messages.simpleName>.get ( "<filter.simpleName>.typeName" );
    }

    /**
     * TODO: document my behavior
     *
     * @param context the runtime context
     * @return false if this filter needs to process a DevTest property, like LAST_RESPONSE; otherwise returns true
     * @throws TestRunException for runtime exceptions
     */
    @Override
    public boolean subPreFilter(TestExec context) throws TestRunException {
<filter.parameters : { p |
        <p.dataType.javaType> <p.alternateVariableName> = context.parseInState ( <p.variableName> );
}>
        // TODO: Implement whatever your want this filter to do before step execution starts

        // If this method returns false, the filter's editor will show
        // the "Filter in:" text field, in which a property name
        // can be placed.  If this method returns true, that field is not shown.
        //
        // If this filter needs to process a DevTest property, like LAST_RESPONSE,
        // then return false,
        // and a combo-box will be presented that allows you to pick the property to filter.
        return false;
    }

    /**
     * TODO: document my behavior
     *
     * @param context the runtime context
     * @return false if this filter needs to process a DevTest property, like LAST_RESPONSE; otherwise returns true
     * @throws TestRunException for runtime exceptions
     */
    @Override
    public boolean subPostFilter(TestExec context) throws TestRunException {
<filter.parameters : { p |
        <p.dataType.javaType> <p.alternateVariableName> = context.parseInState ( <p.variableName> );
}>

        // TODO: Implement whatever you want this filter to do after step execution finishes

        // If this method returns false, the filter's editor will include
        // a field for filtering in a DevTest property.  If this method returns true,
        // the "Filter in:" text field is not shown.  If this filter needs to
        // process a DevTest property, like LAST_RESPONSE, then return false,
        // and a combo-box will be presented that allows you to pick the property to filter.
        return false;
    }

    public void initialize(Element element) throws TestDefException {
<filter.parameters : { p |
    <if(p.boolean)>
        <p.variableName> = StrUtil.safeBoolean ( XMLUtils.getAttributeOrChildText ( element, "<p.xmlTag>" ));
    <else>
        <p.variableName> = XMLUtils.getAttributeOrChildText (element, "<p.xmlTag>" );
    <endif>
}>
    }

    public ParameterList getParameters() {
        ParameterList pl = new com.itko.util.ParameterList();

<filter.parameters : { p |
    <if(p.text)>
        <p.dataType.javaType> <p.alternateVariableName> = StrUtil.safeString ( <p.variableName> );
        pl.addParameter(new Parameter( Messages.get("<p.label.key>"), "<p.xmlTag>", <p.alternateVariableName>, <p.dataType.parameterType>.class ));
    <elseif(p.boolean)>
        pl.addParameter(new Parameter( Messages.get("<p.label.key>"), "<p.xmlTag>", Boolean.toString( <p.variableName> ), <p.dataType.parameterType>.class ));
    <else>
        pl.addParameter(new Parameter( Messages.get("<p.label.key>"), "<p.xmlTag>", <p.variableName>, <p.dataType.parameterType>.class ));
    <endif>
}>
        return pl;
    }

    /**
     * Returns true if this filter is enabled to add to individual steps
     */
    public boolean isLocalScope() {
        return <filter.localScope>;
    }

    /**
     * Returns true if this filter is enabled to be added as a global filter.
     * Global filters are applied to every step in the model.
     */
    public boolean isGlobalScope() {
        return <filter.globalScope>;
    }
}
>>
