LoggingProperties(packageName) ::=
<<
# See http://logging.apache.org/log4j/1.2/manual.html

log4j.rootCategory=INFO,stdout

log4j.logger.<packageName>=DEBUG

# If you prefer routing log messages to stdout
log4j.appender.stdout=org.apache.log4j.ConsoleAppender
log4j.appender.stdout.Target=System.out
log4j.appender.stdout.layout=org.apache.log4j.PatternLayout
log4j.appender.stdout.layout.ConversionPattern=%d{yyyy-MM-dd HH:mm:ss} %-5p %c{1}:%L - %m%n


# If you prefer routing log messages to a file
log4j.appender.file.File=debug.log
log4j.appender.file.MaxFileSize=10MB
log4j.appender.file.MaxBackupIndex=3
log4j.appender.file.layout=org.apache.log4j.PatternLayout
log4j.appender.file.layout.ConversionPattern=%d{yyyy-MM-dd HH:mm:ss} %-5p %c{1}:%L - %m%n
>>
