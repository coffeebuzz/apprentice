StepEditor(step,messages) ::=
<<
<common/Copyright()>

package <step.editor.packageName>;


import com.itko.lisa.editor.CustomEditor;
import com.itko.lisa.editor.NodeListDropdown;
import com.itko.lisa.editor.TestCaseInfo;
import com.itko.lisa.editor.TestNodeInfo;
import com.itko.lisa.gui.LisaGuiPanel;
import com.itko.lisa.gui.LisaPrefs;
import com.itko.lisa.gui.ViewResultMasterPanel;
import com.itko.lisa.repo.ProjectManager;
import com.itko.lisa.test.TestExec;
import com.itko.lisa.test.TestCase;
import com.itko.lisa.test.TestRunException;
import com.itko.util.StrUtil;
import com.itko.util.swing.Borders;
import com.itko.util.swing.GridPanel;
import com.itko.util.swing.PrefillCombo;
import com.itko.util.swing.panels.ProcessingCallback;
import com.itko.util.swing.panels.ProcessingDialogCancelledException;

import <messages.className>;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;


/**
 * This is the UI editor of the <step.simpleName>
 */
public class <step.editor.simpleName> extends CustomEditor {

<step.parameters : { p |
    private static final String <p.labelName> = <messages.simpleName>.get ( "<p.label.key>" );
}>
    /*
     * Instance variables
     */
<step.parameters : { p |
    private <p.swingType> <p.swingVariableName>;
}>
    private JPanel editor;
    private NodeListDropdown onError;
    private ViewResultMasterPanel results;

    /**
     * Build the editor components
     */
    private void setup() {
        setupEditor();
        setupResponseEditor();

        setLayout(new BorderLayout());
        add(editor, BorderLayout.NORTH);
        add(results, BorderLayout.CENTER);
    }

    /**
     * Build the panel used for data entry by the end user
     */
    private void setupEditor() {
<step.parameters : { p |
<if ( p.presentsPrefillCombo) >
        <p.swingVariableName> = new PrefillCombo (getClass().getName() + ".<p.variableName>", null, null, LisaPrefs.getLisaPrefsObj(), true, this );
<if(p.tooltip)>
        <p.swingVariableName>.setTooltipText ( <messages.simpleName>.get( "<p.tooltip.key>" ));
<endif>
<else>
        <p.swingVariableName> = new <p.swingType>();
<if( p.tooltip )>
        <p.swingVariableName>.setToolTipText ( <messages.simpleName>.get( "<p.tooltip.key>" ));
<endif>
<endif>
}>
        onError = new NodeListDropdown( <messages.simpleName>.get( "onRuntimeError.label" ));
        onError.setAllowEmpty(false);
        onError.setAllowProperty(true);

        JButton trymeB = new JButton( <messages.simpleName>.get( "executeButton.label" ));
        trymeB.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                execute();
            }
        });

        JPanel execholder = new JPanel(new FlowLayout(FlowLayout.LEFT));
        execholder.add(trymeB);

<step.parameters : { p |
<if(p.presentsFindButton)>
        JButton <p.findButtonName> = new JButton( " ... ");
        <p.findButtonName>.setBorder(BorderFactory.createEtchedBorder());
        <p.findButtonName>.addActionListener( new ActionListener() {
            public void actionPerformed( ActionEvent e )
            {
                <if(p.dataType.directory)>
                doDirectoryChooser( <p.swingVariableName> );
                <else>
                doFileChooser ( <p.swingVariableName> );
                <endif>
            \}
        \});
<endif>
}>
        GridPanel gp = new GridPanel();

<step.parameters : { p |
        <if(p.presentsPrefillCombo)>
        gp.add ( new JLabel ( <p.labelName> ), <p.swingVariableName>.getComponent(), <p.findButtonName> );
        <else>
        gp.add ( new JLabel ( <p.labelName> ), <p.swingVariableName> );
        <endif>
}>
        gp.add(onError);
        gp.add(new JLabel(" "), execholder);

        editor = new JPanel(new BorderLayout());
        editor.setBorder(Borders.createGroupBox(null));
        editor.add(gp.getPanel(), BorderLayout.CENTER);
    }

    /**
     * Build the panel that renders the response
     */
    private void setupResponseEditor() {
        results = new ViewResultMasterPanel(false, false);
    }


    /**
     * Opens a FileChooser that enables selection of a file
     *
     * @param cb the parent component
     */
    private void doFileChooser(PrefillCombo cb) {
        try {
            JFileChooser chooser;
            String cur = cb.getText();

            if (cur != null && cur.length() > 0)
                chooser = new JFileChooser(cur);
            else
                chooser = new JFileChooser();

            chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            chooser.setApproveButtonText( <messages.simpleName>.get("selectButton.label") );

            int option;

            option = chooser.showOpenDialog(this);

            if (option == JFileChooser.APPROVE_OPTION) {
                File shownFile = chooser.getSelectedFile();
                cb.setText(ProjectManager.convertPathToPropertyString(shownFile.getCanonicalPath(),
                        ((TestNodeInfo) getController()).getTestCaseInfo().getProjectRoot()));
            }
        } catch (IOException e) {
            // do nothing by design
        }
    }


    /**
     * Opens a FileChooser that enables selection of a directory
     *
     * @param cb the parent component
     */
    private void doDirectoryChooser(PrefillCombo cb) {
        try {
            JFileChooser chooser;
            String cur = cb.getText();

            if (cur != null && cur.length() > 0)
                chooser = new JFileChooser(cur);
            else
                chooser = new JFileChooser();

            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            chooser.setApproveButtonText( <messages.simpleName>.get( "selectButton.label" ));

            int option;

            option = chooser.showOpenDialog(this);

            if (option == JFileChooser.APPROVE_OPTION) {
                File shownFile = chooser.getSelectedFile();
                cb.setText(ProjectManager.convertPathToPropertyString (
                        shownFile.getCanonicalPath(),
                        ((TestNodeInfo) getController()).getTestCaseInfo().getProjectRoot()));
                }
        }
        catch (IOException e) {
            // do nothing by design
        }
    }


    /**
     * Saves the editor values into the Step object
     */
    public void save() {
        TestNodeInfo ni = (TestNodeInfo) getController();
        ni.getTestCaseInfo().getTestExec().saveNodeResponse(ni.getName(), ni.getRet());

        <step.simpleName> step = ( <step.simpleName> ) ni.getAttribute( <step.controller.className>.CACHE_KEY );
<step.parameters : { p |
        <if(p.text)>
        step.<p.setterMethodName> ( <p.swingVariableName>.getText() );
        <elseif(p.boolean)>
        step.<p.setterMethodName> ( <p.swingVariableName>.isSelected() );
        <endif>
}>
        step.setErrorNode(onError.getCurrentNodeName());
    }

    /**
     * Displays the step's dialog
     */
    public void display() {

        if (editor == null) {
            setup();
        }

        // Step 1: Fetch our Step object from cache
        TestNodeInfo ni = (TestNodeInfo) getController();
        <step.simpleName> step = ( <step.simpleName> ) ni.getAttribute( <step.controller.className>.CACHE_KEY);

        // Step 2: Copy values from the Step object into our editor parameters
<step.parameters : { p |
        <if(p.text)>
        <p.swingVariableName>.setText ( step.<p.getterMethodName>() );
        <elseif(p.boolean)>
        <p.swingVariableName>.setSelected ( step.<p.getterMethodName>() );
        <endif>
}>

        // Step 3: Update the onError step
        onError.setList(ni.getTestCaseInfo().getNodes());
        if (!StrUtil.isEmpty(step.getErrorNode()))
            onError.setCurrentNodeName(step.getErrorNode());
        else
            onError.setCurrentNodeName( TestCase.FAIL_NODE );

        // Step 4: Link the results panel to our TestNodeInfo
        results.setNodeInfo(ni.getTestCaseInfo(), ni);
    }

    /**
     * Returns an error message for any required parameters that are missing
     *
     * @return an error message to display to the user about data entry errors, or \{@code null}
     *         if there are no errors.
     */
    public String isEditorValid() {
        // For now, we'll allow any field to be blank.
        // Blank parameters may result in runtime errors, but reduce nagging by the UI
        return null;
    }


    /**
     * Perform a design-time execution of this step
     */
    private void execute() {
        final TestNodeInfo ni = (TestNodeInfo) getController();
        TestCaseInfo tci = ni.getTestCaseInfo();
        TestExec ts = tci.getTestExec();

        String msg = isEditorValid();
        if (msg != null) {
            LisaGuiPanel.infoMsg(this, getName(), msg);
            return;
        }

        // declaring these as 'final' keeps the callback happy
<step.parameters : { p |
        <if(p.text)>
        final String <p.alternateVariableName> = ts.parseInState ( <p.swingVariableName>.getText() );
        <elseif(p.boolean)>
        final boolean <p.alternateVariableName> = <p.swingVariableName>.isSelected();
        <endif>
}>
        final <step.simpleName> step = ( <step.simpleName> ) ni.getAttribute( <step.controller.className>.CACHE_KEY );

        try {
            LisaGuiPanel.doProcessingDialog(getName(), this,
                    <messages.simpleName>.get( "message.processing" ),
                    new ProcessingCallback() {
                        public void doCallback(Object o) throws Throwable {
                            Object obj = <step.simpleName>.execute( <step.executeArguments> );
                            ni.setRet( obj );
                        }
                    }, true);
        } catch (ProcessingDialogCancelledException can) {
            // You gave up, but the step will continue until it completes in the background.
        } catch (Throwable tw) {
            String errorMessage = <messages.simpleName>.get ( "message.executionError" );
            TestRunException tre = new TestRunException(errorMessage, tw);
            LisaGuiPanel.errorMsg(this, getName(), errorMessage, tre);
        }

        results.setNodeInfo(tci, ni);
    }

}
>>
