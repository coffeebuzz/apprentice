StepController(step,messages) ::=
<<
<common/Copyright()>

package <step.controller.packageName>;


import java.io.PrintWriter;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itko.lisa.editor.TestNodeInfo;
import com.itko.lisa.test.StepConnection;
import com.itko.lisa.test.StepConnectionType;
import com.itko.lisa.test.StepNameChangeListener;


import <messages.className>;


/**
 * This is the controller between the <step.simpleName> and the <step.editor.simpleName>.
 *
 */
public class <step.controller.simpleName> extends TestNodeInfo implements StepNameChangeListener
{
    private static final Logger LOG = LoggerFactory.getLogger( <step.controller.simpleName>.class.getName() );

    // This is a globally unique key that identifies our Node in the cache.
    static final String CACHE_KEY = "<step.controller.className>.key";


    @Override
    public String getHelpString() {
        return <messages.simpleName>.get( "<step.controller.simpleName>.helpString" );
    }


    /**
     * This method supports the \{@code StepNameChangeListener} interface.
     */
    @Override
    public void stepNameChanged(String oldName, String newName) {
        <step.simpleName> step = ( <step.simpleName> ) getAttribute( CACHE_KEY );
        String onException = step == null ? null : step.getErrorNode();

        if (oldName.equals(onException))
            step.setErrorNode(newName);
    }


    /**
     * This method should be overridden by subclasses to provide any step connections beyond
     * the standard "next" step connection.  By default, this method does nothing.
     * This is in support of the \{@code StepNameChangeListener} interface.
     *
     * @param stepConnections the collection to add new connections to.
     */
    @Override public void gatherOtherConnections(Collection\<StepConnection> stepConnections)
    {
        <step.simpleName> step = ( <step.simpleName> ) getAttribute( CACHE_KEY );
        String onException = step == null ? null : step.getErrorNode();
        StepConnection.onError(getName(), onException, this, stepConnections);
    }

    /**
     * This method is used to inform the step controller that a secondary step connection needs
     * to be updated.  The framework will handle notifying any related editor as appropriate.
     * This is in support of the \{@code StepNameChangeListener} interface.
     *
     * @param connetion the connection whose target step is being changed.
     * @param newTargetStep the name of the new target step for the connection.
     */
    @Override public void secondaryStepReferenceChanged(StepConnection connection, String newTargetStep)
    {
        <step.simpleName> step = ( <step.simpleName> ) getAttribute( CACHE_KEY );

        if (step != null && connection.getType() == StepConnectionType.ON_ERROR &&
            connection.getSourceStep().equals(getName()))
            step.setErrorNode(newTargetStep);
    }

    /**
    * Lifecycle method
    */
    @Override
    public void initNewOne() {
        <step.simpleName> step = new <step.simpleName>();
        putAttribute ( CACHE_KEY, step );
    }

    @Override
    public String loadLargeIcon() {
        return "doc.gif";
    }

    @Override
    public String loadSmallIcon() {
        return "doc.gif";
    }


    @Override
    public void writeSubXML(PrintWriter pw) {
        <step.simpleName> step = (<step.simpleName>) getAttribute ( CACHE_KEY );
        step.writeSubXML ( pw );
    }

    @Override
    public String getEditorName() {
        return <messages.simpleName>.get("<step.controller.simpleName>.editorName");
    }

    @Override
    public void migrate(Object node) {
        <step.simpleName> step = ( <step.simpleName> ) node;
        putAttribute ( CACHE_KEY, step );
    }
}
>>
