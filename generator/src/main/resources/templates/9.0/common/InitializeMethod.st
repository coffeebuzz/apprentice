InitializeMethod(parameters) ::=
<<
/**
 * This method initialized the object using the content of {@code element}
 *
 * @param element the XML stanza for this assertion from the ".tst" file
 */
public void initialize(Element element) throws TestDefException {
<parameters : { p |
  <if(p.boolean)>
  <p.variableName> = StrUtil.safeBooleanValue ( XMLUtils.getAttributeOrChildText ( element, "<p.xmlTag>" ), false );
  <elseif(p.text)>
  <p.variableName> = StrUtil.safeString ( XMLUtils.getAttributeOrChildText ( element, "<p.xmlTag>" ));
  <else>
  <p.variableName> = XMLUtils.getAttributeOrChildText ( element, "<p.xmlTag>" );
  <endif>
}>
}
>>