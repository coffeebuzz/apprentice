MessageLookup(messages,bundleName) ::=
<<
<common/Copyright()>

package <messages.packageName>;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Locale;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class provides localized messages
 */
public class Messages {

    private static Logger log = LoggerFactory.getLogger ( Messages.class.getName() );

    private static ResourceBundle resources;

    //
    // Initialize the resource bundle
    //
    static {

        try {
            Locale currentLocale = Locale.getDefault();
            resources = ResourceBundle.getBundle("<bundleName>", currentLocale );
        }
        catch (Exception e) {
            log.error ( e.getMessage() != null ? e.getMessage() : e.toString(), e );
        }
    }

    /**
     * Fetch a message. If {@code key} is not found, return \{@code key} as the value.
     * That is, \<code>get("undefinedKey")\</code> returns \<code>"undefinedKey"\</code>.
     *
     * @param key the message key
     *
     * @return the value of {@code key}. If no value is found, \{@code key} is returned.
     *         If \{@code key} is null, an empty string is returned.
     */
    public static String get (String key) {
        if ( resources == null )
            return key;

        if ( key == null )
            return "";

        try {
            return resources.getString(key);
        }
        catch ( java.util.MissingResourceException e ) {
            String errMsg = String.format ( "No value for the key '%s' was found in the bundle '%s'", key, "<bundleName>" );
            log.error ( errMsg, e );
            return key;
        }
    }
}
>>
