EmptyDefaultEditor(bean) ::=
<<
<common/Copyright()>

package <bean.packageName>;

/**
 * For advanced users only. Typically, you want to use the default editor.
 */
public class <bean.simpleName> {}
>>