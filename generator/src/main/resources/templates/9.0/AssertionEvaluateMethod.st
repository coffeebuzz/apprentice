AssertionEvaluateMethod(parameters) ::=
<<
/**
 * Determine if the conditions of the assertion are met.
 *
 * @return true if the conditions are met, and false otherwise.
 */
protected boolean evaluate ( TestExec context, Object lastResponse )
{
<parameters : { p |
<if(p.text)>
    <p.dataType.javaType> <p.alternateVariableName> = context.parseInState( <p.variableName> );
<endif>
}>

    // TODO: Implement the algorithm to determine if the conditions of the assertion are met
    // and return the appropriate verdict.  For now, we default to 'true'
    boolean verdict = true;

    return verdict;
}
>>