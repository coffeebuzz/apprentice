FilterTest(filter) ::=
<<
<common/Copyright()>

package <filter.packageName>;

import com.itko.lisa.test.TestExec;
import com.itko.lisa.test.TestRunException;
import com.itko.util.ParameterList;
import com.itko.util.Parameter;

<common/JUnitImports()>

/**
 * Unit tests of the <filter.simpleName> class
 */
public class <filter.simpleTestName> {

    /**
     * When: Before a step is executed ...
     * Expected behavior: This filter sets ...
     */
    // @Test
    public void whenPreFilterCondition_expectOutcome() throws TestRunException
    {
        TestExec testExec = TestExec.makeSimpleTestExec();
        // testExec.setStateValue("someProperty", "someValue" );

        <filter.simpleName> filter = new <filter.simpleName>();

        // subPreFilter is called before the step is executed
        boolean flag = filter.subPreFilter ( testExec );

        // TODO: Implement assertions
        // String someValue = testExec.getStateValue("somePropertySetByFilter");
        // assertThat ("When condition, expect...", someValue, notNullValue());
    }


    /**
     * When: After a step is executed and ...some condition...
     * Expected behavior: This filter sets ...
     */
    // @Test
    public void whenPostFilterCondition_expectOutcome() throws TestRunException
    {
        TestExec testExec = TestExec.makeSimpleTestExec();
        // testExec.setStateValue("someProperty", "someValue" );

        <filter.simpleName> filter = new <filter.simpleName>();

        // subPostFilter is called after the step is executed
        boolean flag = filter.subPostFilter ( testExec );

        // TODO: Implement assertions
        // String someValue = testExec.getStateValue("somePropertySetByFilter");
        // assertThat ("When condition, expect...", someValue, notNullValue());
    }



}

>>