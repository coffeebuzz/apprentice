DataSetTest(dataset) ::=
<<
<common/Copyright()>

package <dataset.packageName>;

import com.itko.lisa.test.TestRunException;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;
import java.io.Serializable;

<common/JUnitImports()>

/**
 * Unit tests of the <dataset.simpleName> class.
 */
public class <dataset.simpleTestName>  {

    // @Test
    public void whenCondition_expectOutcome() throws RemoteException, TestRunException
    {
        Map\<Serializable,Serializable> testState = new HashMap();

        <dataset.simpleName> dataSet = new <dataset.simpleName>();

        Map\<String,?> resultSet = dataSet.getRecord ( testState );

        // TODO: Fill in your assertions
        // assertThat ( "The resultSet should not be null", resultSet, notNullValue());
        // assertThat ( "Some condition", resultSet, ... );
    }

}
>>