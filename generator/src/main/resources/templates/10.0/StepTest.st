StepTest(step) ::=
<<
<common/Copyright()>

package <step.packageName>;

import com.itko.lisa.test.TestExec;
import com.itko.lisa.test.TestRunException;
import com.itko.util.ParameterList;
import com.itko.util.Parameter;
import com.itko.util.StrUtil;
import com.itko.util.XMLUtils;

<common/JUnitImports()>

/**
 * Unit tests of the <step.simpleName> class
 */

public class <step.simpleTestName> {

    // @Test
    public void whenCondition_expectOutcome()  throws TestRunException
    {
        TestExec testExec = TestExec.makeSimpleTestExec();

<step.parameters : { p |
        <if(p.boolean)>
        <p.dataType.javaType> <p.variableName> = true;
        <elseif(p.text)>
        <p.dataType.javaType> <p.variableName> = "";
        <else>
        <p.dataType.javaType> <p.variableName> = null;
        <endif>
}>
        Object response = <step.simpleName>.execute ( <step.parameters : { p | <p.variableName> }; separator=", "> );

        // TODO: Write assertions to ensure the contract was fulfilled
        assertThat ("When [some condition], the expected behavior is [...]", response, notNullValue());
    }

}
>>