AssertionTest(assertion) ::=
<<
<common/Copyright()>

package <assertion.packageName>;

import com.itko.lisa.test.Assertion;
import com.itko.lisa.test.TestDefException;
import com.itko.lisa.test.TestExec;
import com.itko.util.ParameterList;
import com.itko.util.Parameter;
import com.itko.util.StrUtil;
import com.itko.util.XMLUtils;

<common/JUnitImports()>

/**
 * Unit tests of the <assertion.simpleName> class.
 */
public class <assertion.simpleTestName>  {

    /**
     * On some condition, we expect the assertion to return a verdict of: true
     */
    // @Test
    public void whenCondition_expectVerdictOfTrue()
    {
        TestExec testExec = TestExec.makeSimpleTestExec();

        Object lastResponse = new Object();

        Assertion assertion = new <assertion.simpleName>();

        boolean verdict = assertion.execute ( testExec, lastResponse );

        // TODO: Write assertions to verify the contract was fulfilled
        assertThat ("When some condition, we expect a verdict of: true", verdict, is(true));
    }

    /**
     * On some condition, we expect the assertion to return a verdict of: false
     */
    // @Test
    public void whenCondition_expectVerdictOfFalse()
    {
        TestExec testExec = TestExec.makeSimpleTestExec();

        Object lastResponse = new Object();

        Assertion assertion = new <assertion.simpleName>();

        boolean verdict = assertion.execute ( testExec, lastResponse );

        // TODO: Write assertions to verify the contract was fulfilled
        assertThat ("When some condition, we expect a verdict of: false", verdict, is(false));
    }

}

>>