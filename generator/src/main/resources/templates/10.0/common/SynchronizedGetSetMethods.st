SynchronizedGetSetMethods(parameters) ::=
<<
<parameters : { p | <\n>synchronized public <p.dataType.javaType> <p.getterMethodName>() { return this.<p.variableName>; \} }>
<parameters : { p | <\n>synchronized public void <p.setterMethodName>( <p.dataType.javaType> <p.variableName>) { this.<p.variableName> = <p.variableName>; \} }>
>>