GetCustomParametersMethod(parameters) ::=
<<
/**
 * Returns the custom parameters
 * @return the custom parameters
 */
public com.itko.util.ParameterList getCustomParameters() {
    ParameterList pl = new com.itko.util.ParameterList();
<parameters : { p |
<if(p.text)>
    <p.dataType.javaType> <p.alternateVariableName> = <p.variableName> == null ? "" : <p.variableName>;
    pl.addParameter ( new Parameter ( Messages.get ("<p.label.key>"), "<p.xmlTag>", <p.alternateVariableName>, <p.dataType.parameterType>.class ));
<elseif(p.boolean)>
    pl.addParameter ( new Parameter ( Messages.get ("<p.label.key>"), "<p.xmlTag>", Boolean.toString(<p.variableName>), <p.dataType.parameterType>.class ));
<else>
    pl.addParameter ( new Parameter ( Messages.get ("<p.label.key>"), "<p.xmlTag>", <p.variableName>, <p.dataType.parameterType>.class ));
<endif>
}>
    return pl;
}
>>