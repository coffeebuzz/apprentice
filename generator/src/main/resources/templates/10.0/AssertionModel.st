AssertionModel(assertion,messages) ::=
<<
<common/Copyright()>

package <assertion.packageName>;

import com.itko.lisa.test.Assertion;
import com.itko.lisa.test.TestDefException;
import com.itko.lisa.test.TestExec;
import com.itko.util.ParameterList;
import com.itko.util.Parameter;
import com.itko.util.StrUtil;
import com.itko.util.XMLUtils;

import org.w3c.dom.Element;
import java.io.File;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import <messages.className>;

/**
 * This assertion verifies ...
 * When [condition] is met, it returns a verdict of {@code true}.
 * Otherwise, it returns a verdict of {@code false}.
 */
public class <assertion.simpleName> extends Assertion
{
    private static final Logger LOG = LoggerFactory.getLogger( <assertion.simpleName>.class.getName() );

	/*
	 * Instance variables
	 */
	<common/InstanceVariables(assertion.parameters)>

    /*
     * Get/Set methods
     */
	<common/GetSetMethods(assertion.parameters)>

	<common/GetCustomParametersMethod(assertion.parameters)>

	<AssertionEvaluateMethod(assertion.parameters)>

	<common/InitializeMethod(assertion.parameters)>

    @Override
    public String getTypeName() throws Exception {
        return <messages.simpleName>.get ( "<assertion.simpleName>.typeName" );
    }


    public String getDescriptive()
    {
        StringBuilder sb = new StringBuilder( 128 );

        sb.append ("Assertion [").append(getName()).append("]");
        if (isAssertTrue()) {
            sb.append ( <messages.simpleName>.get ( "<assertion.simpleName>.whenVerdictIsTrue" ));
        } else {
            sb.append ( <messages.simpleName>.get ( "<assertion.simpleName>.whenVerdictIsFalse" ));
        }

        return sb.toString();
    }

    /**
     * Returns true if this assertion can be added to individual steps
     */
    public boolean isLocalScope() {
        return <assertion.localScope>;
    }

    public boolean isGlobalScope() {
        return <assertion.globalScope>;
    }
}
>>