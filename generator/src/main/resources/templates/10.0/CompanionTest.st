CompanionTest(companion) ::=
<<
<common/Copyright()>

package <companion.packageName>;

import com.itko.lisa.test.TestRunException;
import com.itko.lisa.test.TestExec;
import com.itko.util.ParameterList;
import com.itko.util.Parameter;
import com.itko.util.StrUtil;
import com.itko.util.XMLUtils;

<common/JUnitImports()>

/**
 * Unit tests of the <companion.simpleName> class.
 */
public class <companion.simpleTestName> {


    // @Test
    public void whenTestStarting_expectOutcome() throws TestRunException
    {
        ParameterList parameterList = new ParameterList();
        TestExec testExec = TestExec.makeSimpleTestExec();

        <companion.simpleName> companion = new <companion.simpleName>();

        companion.testStarting ( parameterList, testExec );

        // assertThat ("Something happened", ... );
    }

    // @Test
    public void whenTestEnding_expectOutcome() throws TestRunException
    {
        ParameterList parameterList = new ParameterList();
        TestExec testExec = TestExec.makeSimpleTestExec();

        <companion.simpleName> companion = new <companion.simpleName>();

        companion.testEnded ( parameterList, testExec );

        // assertThat ("Something happened", ... );
    }
}
>>