MessageProperties(prototype) ::=
<<

#foreach ( $attribute in $prototype.attributes )
${attribute.label.constantName}=${attribute.metaName}
#end

${prototype.canonicalAssertionName}.typeName=My Type
>>
