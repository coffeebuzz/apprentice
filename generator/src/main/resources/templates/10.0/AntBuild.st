AntBuild(project) ::=
<<
\<?xml version="1.0"?>
\<!--

    Builds the jar for this devtest plugin.

    There are 3 tasks:
      - init  : creates the scratch directory; this is were the compiler
                writes the .class files. This is also the directory that
                gets zipped up into the plugin jar.

      - compile: compiles the code

      - assemble: creates the jar file

-->
\<project name="<project.pluginName>" default="master" basedir="." >

    \<property file="build.properties" />
    \<property environment="env" />
    \<property name="src"  value="${basedir}/src" />

    \<echo message="DEVTEST_HOME=${DEVTEST_HOME}" />

    \<available file="${DEVTEST_HOME}/lisa.properties" property="rightDEVTEST_HOME"/>
    \<fail message="You must define DEVTEST_HOME as an environment variable." unless="rightDEVTEST_HOME"/>

    \<!-- Make all jars in the lib directory part of the compile classpath -->
    \<path id="project.class.path">
        \<fileset dir="${DEVTEST_HOME}/lib/core">
            \<include name="**.jar"/>
        \</fileset>
        \<fileset dir="${DEVTEST_HOME}/lib/shared">
            \<include name="**.jar" />
        \</fileset>
    \</path>

	\<target name="clean">
	    \<delete dir="${basedir}/scratch" quiet="yes" />
	\</target>

    \<target name="init">
       \<mkdir dir="${basedir}/scratch" />
    \</target>

	\<target name="compile">
        \<javac
            srcdir="${src}"
            destdir="${basedir}/scratch"
            compiler="javac1.8"
            source="1.8"
            target="1.8"
            verbose="no"
            debug="true"
            fork="true"
            includeantruntime="false"
            memoryinitialsize="64m"
            memorymaximumsize="128m">
          \<classpath refid="project.class.path" />
        \</javac>
	\</target>

	\<target name="assemble">
        \<!-- copy the resources to the scratch directory -->
        \<copy toDir="${basedir}/scratch" >
            \<fileset dir="${basedir}/<project.workspaceTarget.resourceDirectory>" includes="**" />
        \</copy>

        \<!-- Jar up the classes of our project -->
        \<jar basedir="${basedir}/scratch" destfile="${basedir}/${ant.project.name}.jar" />
	\</target>

    \<target name="master" depends="init,compile,assemble" />

\</project>
>>
