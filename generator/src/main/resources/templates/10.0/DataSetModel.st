DataSetModel(dataset,messages) ::=
<<
<common/Copyright()>

package <dataset.packageName>;


import com.itko.lisa.test.DataSetImpl;
import com.itko.lisa.test.TestDefException;
import com.itko.lisa.test.TestExec;
import com.itko.lisa.test.TestRunException;
import com.itko.util.Parameter;
import com.itko.util.ParameterList;
import com.itko.util.StrUtil;
import com.itko.util.XMLUtils;

import org.w3c.dom.Element;

import java.rmi.RemoteException;
import java.io.Serializable;
import java.util.Collections;
import java.util.Map;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import <messages.className>;

/**
 * TODO: Document the behavior of this dataset.
 *
 * Since data sets need to be thread-safe, all public methods are synchronized.
 *
 */
public class <dataset.simpleName> extends DataSetImpl implements Serializable
{
    private static final Logger LOG = LoggerFactory.getLogger( <dataset.simpleName>.class.toString() );

    /*
     * Instance variables
     */
    <common/InstanceVariables(dataset.parameters)>

    /**
     * Default constructor
     * @throws RemoteException
     */
    public <dataset.simpleName>() throws RemoteException
    {
        super();
    }

    <common/SynchronizedGetSetMethods(dataset.parameters)>


    /**
     * Returns the next record from the data source.  A check is made on whether to return a random row
     * or the next sequential row.
     *
     * @param testState the runtime state of the framework
     *
     * @throws TestRunException for runtime exceptions
     * @throws RemoteException for network exceptions, such as lost connections between the coordinator and simulator
     */
    @Override synchronized public Map\<String,?> getRecord ( Map\<Serializable,Serializable> testState ) throws RemoteException, TestRunException
    {
        if ( isRandom() )
            return getRandomRecord ( testState );  // note: getRandomRecord is implemented by the superclass
        else
            return getNextRecord ( testState );
    }


    /**
     * This method returns the next sequential record from the data source.
     *
     * @param testState
     *
     * @return the next row, where the key is the column name and value is the column value. Returns null
     *         if a cancel operation is initiated.
     *
     * @throws TestRunException for runtime exceptions
     * @throws RemoteException for network exceptions, such as lost connections between the coordinator and simulator
     */
    @Override protected Map\<String,?> getNextRecord ( Map\<Serializable,Serializable> testState )
        throws RemoteException, TestRunException
    {
        // NOTE: This is only a template for how you MIGHT want to implement this method.
        // The only required behavior is that a Map containing the row data be returned.

        if ( testState == null )
            testState = Collections.emptyMap();

        try {
            checkValid ( testState );
        }
        catch ( TestDefException e ) {
            throw new TestRunException ( e.getMessage() != null ? e.getMessage() : e.toString(), e );
        }

        // TODO: Populate the record.  Read a result set, read a row from a file, or whatever
        // Put each field into its own cell in the map.  The key is the columnName; the value is the columnValue.
        Map\<String,String> row = new HashMap();
        row.put("FIRSTNAME", "Lisa");
        row.put("LASTNAME", "Simpson");
        return row;
    }

    /**
     * Returns the display name of this data set
     */
    @Override synchronized public String getTypeName() {
        return <messages.simpleName>.get ( "<dataset.simpleName>.typeName" );
    }

    /**
     * Initialize the dataset with the parameter list and runtime context
     *
     * @param pl the parameters, as provided by the framework
     * @param context the runtime context, provided by the framework
     *
     * @throws TestDefException is the data set is misconfigured
     * @throws RemoteException for the usual network-related problems, such as lost connectivity
     */
    synchronized public void initialize ( ParameterList pl, TestExec context ) throws RemoteException, TestDefException
    {
<dataset.parameters : { p |
<if(p.text)>
        <p.variableName> = context.parseInState ( pl.getParameterValue ( "<p.xmlTag>" ) );
<elseif(p.boolean)>
        <p.variableName> = StrUtil.safeBooleanValue ( context.parseInState ( pl.getParameterValue ( "<p.xmlTag>" ) ), false );
<endif>
}>
    }

    /**
     * Initialize the dataset from its XML stanza
     *
     * @param element the XML stanza of this data set
     * @param ignored never used
     *
     */
    synchronized public void initialize ( Element element, Map\<String,?> ignored) throws RemoteException, TestDefException
    {
        try {
<dataset.parameters : { p |
        <if(p.boolean)>
            // TODO: set the default of this field as needed. Here, the default is 'false'
            <p.variableName> = StrUtil.safeBooleanValue ( XMLUtils.getAttributeOrChildText ( element, "<p.xmlTag>" ), false );
        <elseif(p.text)>
            <p.variableName> = StrUtil.safeString ( XMLUtils.getAttributeOrChildText (element, "<p.xmlTag>" ));
        <else>
            <p.variableName> = XMLUtils.getAttributeOrChildText (element, "<p.xmlTag>" );
        <endif>
}>
        }
        catch (Exception e) {
            // super.name = the name given to the data set by the test author
            throw new TestDefException ( super.name, "Error reading data set", e );
        }
    }


    /**
     * This method returns the parent parameters plus our own parameters
     */
    synchronized public ParameterList getParameters() throws RemoteException
    {
        ParameterList pl = super.getParameters();

        /*
         * Add any parameters that are not already in the super's parameterList
         */
        Parameter param;
<dataset.parameters : { p |
        param = pl.getParameter ( "<p.xmlTag>" );
        if ( param == null ) {
        <if(p.text)>
            <p.dataType.javaType> <p.alternateVariableName> = StrUtil.safeString ( <p.variableName> );
            pl.addParameter(new Parameter( Messages.get("<p.label.key>"), "<p.xmlTag>", <p.alternateVariableName>, <p.dataType.parameterType>.class ));
        <elseif(p.boolean)>
            pl.addParameter(new Parameter( Messages.get("<p.label.key>"), "<p.xmlTag>", Boolean.toString(<p.variableName>), <p.dataType.parameterType>.class ));
        <else>
            pl.addParameter(new Parameter( Messages.get("<p.label.key>"), "<p.xmlTag>", <p.variableName>, <p.dataType.parameterType>.class ));
        <endif>
        \}
}>
        return pl;
    }

    /**
     * Performs runtime validation of a dataset
     */
    @Override synchronized public void validateData ( Map\<Serializable,Serializable> testState ) throws RemoteException, TestRunException
    {
        try {
            checkValid ( testState );
        }
        catch ( TestDefException e ) {
            throw new TestRunException ( e.getMessage(), e );
        }
    }

    /**
     * Performs design-time validation of a dataset
     *
     * @throws TestDefException if a required field is undefined
     */
    synchronized private void checkValid ( Map\<Serializable,Serializable> testState ) throws TestDefException
    {
        // Hint: super.getName() can throw a remote exception, so referencing the instance variable avoids
        // having to catch that exception.  The name is the logical name given the data set by the test author.
        String dsName = super.name;

<dataset.parameters : { p |
        <if(p.text)>
        if ( StrUtil.isEmpty ( <p.variableName> )) {
            String err = String.format("In the data set '%s', the required field '%s' is empty", dsName, "<p.label.value>" );
            throw new TestDefException ( super.name, err, null );
        \}
        <endif>
}>
    }

    /**
     * Releases any resources held by the data set.  Be sure to release all resources
     * to avoid memory leaks, connection leaks and similar maladies.
     */
    synchronized public void destroy()
    {
        // TODO:  dereference any resources so they can be garbage-collected
        try {
            super.destroy();
        }
        catch ( java.rmi.RemoteException e ) {
            // TODO: handle this as needed. Its probably safe to ignore it.
        }
    }

}
>>
