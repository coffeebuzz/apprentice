DphTest(dph) ::=
<<
<common/Copyright()>

package <dph.packageName>;

import com.itko.lisa.test.TestExec;
import com.itko.lisa.test.TestRunException;
import com.itko.lisa.vse.stateful.model.Request;
import com.itko.lisa.vse.stateful.model.Response;
import com.itko.lisa.vse.stateful.model.TransientResponse;
import com.itko.lisa.vse.stateful.protocol.DataProtocol;
import com.itko.util.Parameter;
import com.itko.util.ParameterList;
import com.itko.util.StrUtil;

<common/JUnitImports()>

/**
 * Unit tests of the <dph.simpleName> class.
 */
public class <dph.simpleTestName>  {

    /**
     * When: some given condition
     * Expected behavior: the request should have/contain/...
     */
    // @Test
    public void whenRequest_expectOutcome() {

        TestExec testExec = TestExec.makeSimpleTestExec();
        Request request = new Request();

        <dph.simpleName> dph = new <dph.simpleName>();

        dph.updateRequest ( testExec, request );

        // TODO: Implement assertions of how you expected the request to change
        // assertThat ("When condition, request should contain...", request, ... );
    }


    /**
     * When: some given condition
     * Expected behavior: the response should have/contain/...
     */
    // @Test
    public void whenResponse_expectOutcome()
    {
        TestExec testExec = TestExec.makeSimpleTestExec();
        Response response = new Response();

        <dph.simpleName> dph = new <dph.simpleName>();

        dph.updateResponse ( testExec, response );

        // TODO: Implement assertions of how you expected the response to change
        // assertThat ("When condition, response should contain...", response, ... );
    }

    /**
     * When: some given condition
     * Expected behavior: the response should have/contain/...
     */
    // @Test
    public void whenVirtualResponse_expectOutcome()
    {
        TestExec testExec = TestExec.makeSimpleTestExec();

        Response response = new Response();
        TransientResponse virtualResponse = new TransientResponse(response);

        <dph.simpleName> dph = new <dph.simpleName>();

        // TODO: Implement assertions of how you expected the transient response to change
        // assertThat ("When condition, transient response should contain...", virtualResponse, ... );
    }
}
>>