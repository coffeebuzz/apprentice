ProtocolDescriptor(p) ::=
<<
<! these have to be on one line if you want StringToken to write them on one line !>
<if(p.requestSide && p.responseSide)>data:req,resp<elseif(p.isRequestSide)>data:req<else>data:resp<endif>
>>
