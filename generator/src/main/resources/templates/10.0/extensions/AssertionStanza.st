AssertionStanza(assertions) ::=
<<
#
# Custom assertions
#
asserts=<assertions : { a | <a.className>}; separator=",\\\n">

<assertions : { a |
<a.className>=<a.controller.className>,<a.editor.className>
}>
>>