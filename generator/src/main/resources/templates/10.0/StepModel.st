StepModel(step,messages) ::=
<<
<common/Copyright()>

package <step.packageName>;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import org.w3c.dom.Element;

import com.itko.lisa.test.LisaException;
import com.itko.lisa.test.TestCase;
import com.itko.lisa.test.TestDefException;
import com.itko.lisa.test.TestExec;
import com.itko.lisa.test.TestNode;
import com.itko.lisa.test.TestRunException;
import com.itko.util.StrUtil;
import com.itko.util.XMLUtils;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import <messages.className>;


/**
 * This is the Model object for the generated class.
 */
public class <step.simpleName> extends TestNode
{
    private static final Logger LOG = LoggerFactory.getLogger ( <step.className>.class.getName() );

    // This is an XML tag that identifies which step to go to if a runtime error occurs
    public static final String ERROR_STEP_TAG = "onError";

    // This is the name of the on-runtime-error step, initialized to the default Abort step.
    private String errorStep = TestCase.ABORT_NODE;

    // This is a flag that tells us when prepare() has been done
    private AtomicBoolean prepared = new AtomicBoolean ( false );

    // Instance variables
    <common/InstanceVariables(step.parameters)>

    /**
     * Default constructor
     */
    public <step.simpleName>()
    {
    }

    /*
     * Get/Set methods
     */
    <common/GetSetMethods(step.parameters)>

    /**
     * This method performs any set up needed prior to step execution, such as acquiring connections.
     */
    public void prepare( TestCase test ) throws LisaException
    {
        // This method gets called frequently, so use good judgement when deciding
        // what to place here and what to place in {@code prepareOnce())}.

        if ( !prepared.get()) {
            prepareOnce();
            prepared.set(true);
        }
    }

    /**
     * Any set-up that needs to happen before execute() happens here.
     */
    private void prepareOnce()
    {
        // TODO: any set-up that needs to happen once, and only once, before execute() is called, happens here.
        // Lisa calls prepare() several times, so having initialization happen in prepare() can mean
        // data structures are recreated again and again.
    }


    /**
     * Returns the name of the step to execute when a runtime exception occurs.
     */
    public String getErrorNode() {
        if (StrUtil.isEmpty ( errorStep ))
            return TestCase.ABORT_NODE;
        return errorStep;
    }

    /**
     * Set the name of the step to execute when a runtime exception occurs.
     */
    public void setErrorNode ( String stepName ) {
        errorStep = stepName;
    }


    /**
     * Execute the business logic of this step.
     *
     * @param context the runtime context
     */
    protected void execute (TestExec context) throws TestRunException
    {
<step.parameters : { p |
        <if(p.text)>
        <p.dataType.javaType> <p.alternateVariableName> = context.parseInState ( <p.variableName> );
        <else>
        <p.dataType.javaType> <p.alternateVariableName> = <p.variableName>;
        <endif>
}>
        execute ( <step.executeArguments> );
    }

    /**
     * Executes the business logic of this Step without the need of a DevTest runtime context.
     * Basically, this allows a JUnit test to invoke this method, so the behavior of the plugin
     * can be tested before loading the plugin in DevTest.
     *
     * Its safe to change the return type of this method to some other object to meet your needs.
     */
    public static Object execute ( <step.executeSignature> ) throws TestRunException
    {
        return "TODO: Implement the execute method to return a response";
    }


    /**
     * Populates our instance using the content found in {@code element}.
     * The DevTest life cycle invokes this.  The {@code element} is our XML stanza from the testcase (".tst") file.
     */
    public void initialize ( TestCase test, Element element )
    {
        errorStep  = XMLUtils.findChildGetItsText ( element, ERROR_STEP_TAG );
<step.parameters : { p |
    <if(p.boolean)>
        <p.variableName> = StrUtil.safeBooleanValue ( XMLUtils.getAttributeOrChildText ( element, "<p.xmlTag>" ), false );
    <elseif(p.text)>
        <p.variableName> = StrUtil.safeString ( XMLUtils.getAttributeOrChildText (element, "<p.xmlTag>" ));
    <else>
        <p.variableName> = XMLUtils.getAttributeOrChildText (element, "<p.xmlTag>" );
    <endif>
}>
    }

    @Override
    public void destroy ( TestExec context )
    {
        // TODO: Add code to release any resources acquired in prepare/prepareOnce

        super.destroy ( context );
    }

    public void writeSubXML ( PrintWriter pw )
    {
        XMLUtils.streamTagAndChild( pw, ERROR_STEP_TAG, errorStep );
<step.parameters : { p |
        <if(p.text)>
        XMLUtils.streamTagAndChild ( pw, "<p.xmlTag>", <p.variableName> );
        <elseif(p.boolean)>
        XMLUtils.streamTagAndChild ( pw, "<p.xmlTag>", Boolean.toString ( <p.variableName> ));
        <endif>
}>
    }

    @Override
    public String getTypeName() throws Exception {
        return <messages.simpleName>.get ( "<step.simpleName>.typeName" );
    }

    /**
     * Returns the default name given to a Step by the editor.
     */
    @Override
    public String generateName() {
        return <messages.simpleName>.get( "<step.simpleName>.defaultStepName" );
    }
}
>>
