LisaExtensions(project) ::=
<<
#
# This is the lisaextensions file.
# The name of this file must be unique across all other lisaextensions being loaded.
# For example, if you have two jars containing, say, mystuff.lisaextensions, only the
# extensions from one of those jars will be honored and loaded (its undefined as to which one wins).
#
<if(project.hasAssertions)>
<extensions/AssertionStanza(project.assertions)>
<endif>

<if(project.hasCompanions)>
<extensions/CompanionStanza(project.companions)>
<endif>

<if(project.hasDataSets)>
<extensions/DataSetStanza(project.dataSets)>
<endif>

<if(project.hasFilters)>
<extensions/FilterStanza(project.filters)>
<endif>

<if(project.hasSteps)>
<extensions/StepStanza(project.steps)>
<endif>

<if(project.hasDataProtocols)>
<extensions/DataProtocolStanza(project.dataProtocols)>
<endif>
>>
