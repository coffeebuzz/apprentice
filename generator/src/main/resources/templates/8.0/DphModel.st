DphModel(dph) ::=
<<
<common/Copyright()>

package <dph.packageName>;

import com.itko.lisa.test.TestExec;
import com.itko.lisa.test.TestRunException;
import com.itko.lisa.vse.stateful.model.Request;
import com.itko.lisa.vse.stateful.model.Response;
import com.itko.lisa.vse.stateful.model.TransientResponse;
import com.itko.lisa.vse.stateful.protocol.DataProtocol;
import com.itko.util.Parameter;
import com.itko.util.ParameterList;
import com.itko.util.StrUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * TODO: Document the behavior of this protocol handler.
 *
 */
public class <dph.simpleName> extends DataProtocol {

    private static final Logger LOG = LoggerFactory.getLogger ( <dph.simpleName>.class.getName() );

    /**
     * Modifies the incoming request
     *
     * @param context contains the runtime context
     * @param playback the incoming request
     */
    public void updateRequest ( TestExec context, Request playback )
    {
        // TODO: implement whatever needs to happen during playback
    }

    /**
     * Modifies the incoming response
     *
     * @param context contains the runtime context
     * @param playback the incoming response
     */
    public void updateResponse ( TestExec context, Response playback )
    {
        // TODO: implement whatever needs to happen during playback
    }

    /**
     * Modifies the incoming response
     *
     * @param context contains the runtime context
     * @param playback the incoming response
     */
    public void updateResponse ( TestExec context, TransientResponse playback )
    {
        // TODO: implement whatever needs to happen during playback
    }
}
>>
