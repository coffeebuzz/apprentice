GradleBuild(project,devTestHome) ::=
<<
apply plugin: 'java'

def devTestLib = '<devTestHome>/lib/'

repositories {
  mavenLocal()
  mavenCentral()
  }

dependencies {
  compile fileTree (dir: devTestLib + 'core',     include: '*.jar' )
  compile fileTree (dir: devTestLib + 'shared',   include: '*.jar' )
  compile fileTree (dir: devTestLib + 'endorsed', include: '*.jar' )

  testCompile 'junit:junit:4.+'
  }

jar {
  baseName project.name
  destinationDir project.rootDir
  manifest {
    attributes 'Implementation-Title' : '<project.pluginName>: A DevTest Extension',
               'Implementation-Version' : '1.0'
  }
}
>>