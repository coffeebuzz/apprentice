DataProtocolStanza(protocols) ::=
<<
#
# Custom data protocols
#
vseProtocols=<protocols : { p | <p.className>}; separator=",\\\n">

<protocols : { p |
<p.className>=<ProtocolDescriptor(p)>,<p.displayName>,<p.description>
}>
>>
