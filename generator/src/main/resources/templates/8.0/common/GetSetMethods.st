GetSetMethods(parameters) ::=
<<
<parameters : { p | <\n>public <p.dataType.javaType> <p.getterMethodName>() { return this.<p.variableName>; \} }>
<parameters : { p | <\n>public void <p.setterMethodName>( <p.dataType.javaType> <p.variableName>) { this.<p.variableName> = <p.variableName>; \} }>
>>