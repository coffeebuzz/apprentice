CompanionModel(companion,messages) ::=
<<
<common/Copyright()>

package <companion.packageName>;


import com.itko.lisa.test.SimpleCompanion;
import com.itko.lisa.test.TestExec;
import com.itko.lisa.test.TestRunException;
import com.itko.util.Parameter;
import com.itko.util.ParameterList;
import com.itko.util.StrUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import <messages.className>;


/**
 * TODO: Document the behavior of this companion
 */
public class <companion.simpleName> extends SimpleCompanion
{

    private static final Logger LOG = LoggerFactory.getLogger ( <companion.simpleName>.class.getName() );

    <common/InstanceVariables(companion.parameters)>

    /**
     * Returns the display name of this companion
     */
    public String getTypeName()
    {
        return <messages.simpleName>.get ( "<companion.simpleName>.typeName" );
    }

    /**
     * Returns the custom parameters of this companion.
     */
    public ParameterList getParameters()
    {
        ParameterList pl = super.getParameters();

        Parameter param;

        /*
         * Add any parameters that are not already in the list
         */
<companion.parameters : { p |
        param = pl.getParameter ( "<p.xmlTag>" );
        if ( param == null ) {
        <if(p.text)>
            <p.dataType.javaType> <p.alternateVariableName> = StrUtil.safeString ( <p.variableName> );
            pl.addParameter(new Parameter( Messages.get("<p.label.key>"), "<p.xmlTag>", <p.alternateVariableName>, <p.dataType.parameterType>.class ));
        <elseif(p.boolean)>
            pl.addParameter(new Parameter( Messages.get("<p.label.key>"), "<p.xmlTag>", Boolean.toString( <p.variableName> ), <p.dataType.parameterType>.class ));
        <else>
            pl.addParameter(new Parameter( Messages.get("<p.label.key>"), "<p.xmlTag>", <p.variableName>, <p.dataType.parameterType>.class ));
        <endif>
        \}
}>

        return pl;
    }

    /**
     * This is the lifecycle method called by the framework when a test is starting.
     *
     * @param parameters our parameters, provided by the framework
     * @param context the runtime context of the framework
     */
    protected void testStarting ( ParameterList parameters, TestExec context ) throws TestRunException
    {
<companion.parameters : { p |
        <p.dataType.javaType> <p.alternateVariableName> = parameters.getParameterValue ( "<p.xmlTag>" );
}>

        // TODO: Implement whatever the companion needs to do in this phase
    }

    /**
     * This is the lifecycle method called by the framework when a test has ended.
     *
     * @param parameters our parameters, provided by the framework
     * @param context the runtime context
     */
    public void testEnded ( ParameterList parameters, TestExec context )
    {
<companion.parameters : { p |
        <p.dataType.javaType> <p.alternateVariableName> = parameters.getParameterValue ( "<p.xmlTag>" );
}>

        // TODO: Implement whatever the companion needs to do in this phase
    }
}
>>
