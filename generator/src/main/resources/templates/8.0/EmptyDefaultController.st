EmptyDefaultController(bean) ::=
<<
<common/Copyright()>

package <bean.packageName>;

/**
 * For advanced users only. Typically, you want to use
 * the default assertion controller.
 */
public class <bean.simpleName> {}
>>