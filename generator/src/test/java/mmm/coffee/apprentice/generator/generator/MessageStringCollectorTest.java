/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.generator.generator;


import mmm.coffee.apprentice.model.beans.*;
import mmm.coffee.apprentice.model.stereotypes.*;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;


/**
 * Unit tests of the MessageStringCollector class.
 */
public class MessageStringCollectorTest {

    // The keys on the right represent hard-coded keys that are
    // always added to the messages.properties file.
    private static final String[][] DEFAULT_MSG_KEYS = {
            { "onRuntimeError.label",   "Missing key: onRuntimeError.label" },
            { "executeButton.label",    "Missing key: executeButton.label"},
            { "testButton.label",       "Missing key: testButton.label" },
            { "selectButton.label",     "Missing key: select.button" },
            { "message.processing",     "Missing key: message.processing" },
            { "message.executionError", "Missing key: message.executionError" }
    };


    private Project project;

    @Before
    public void setUp()
    {
        ApplicationTarget applicationTarget = ApplicationTargetBean.builder()
                                                    .withHomeDirectory("/Applications/DevTest")
                                                    .withVersion("9")
                                                    .build();
        WorkspaceTarget workspaceTarget = WorkspaceTargetBean.builder("/tmp/workspace").build();

        MessageResource messageResource = MessageResourceBean.builder("mmm.coffee.i18n.Messages").build();

        project = ProjectBean.builder( applicationTarget, workspaceTarget, messageResource )
                                     .withComponent ( buildExampleAssertion() )
                                     .build();
    }


    /**
     * Checks that the default message keys exist in the Collector
     * @param collector the collector to analyze
     * @return always returns true; missing keys are reported
     */
    private boolean defaultMessagesAreDefined ( MessageStringCollector collector )
    {
        for ( String[] msgKey : DEFAULT_MSG_KEYS ) {
            assertThat ( msgKey[1], collector.getProperties().get(msgKey[0]), notNullValue() );
        }
        return true;
    }



    @Test public void whenProjectIsNull_MessageListIsEmpty()
    {
        MessageStringCollector collector = new MessageStringCollector(null);

        assertThat ( "Expected a non-null list", collector.getProperties(), notNullValue() );
        assertThat ( "Expected an empty list",   collector.getProperties().size() == 0 );
    }

    @Test public void whenProjectIsEmpty_DefaultMessagesAreDefined()
    {
        MessageStringCollector collector = new MessageStringCollector( project );

        assertThat ( "Default messages are missing", defaultMessagesAreDefined( collector ) );
    }


    /**
     * Components contain Parameters, and Parameters can define message-based elements.
     * Those message-based elements should be discovered and added to the message list.
     */
    @Test public void confirmMessageElementsAreDiscoveredAndAddedToMessageList()
    {
        project.getComponents().add ( buildExampleStep() );
        project.getComponents().add ( buildExampleAssertion() );

        MessageStringCollector collector = new MessageStringCollector( project );

        assertThat ( "Default messages are missing", defaultMessagesAreDefined( collector ));

        assertThat ( "Missing key: fieldOne.label", collector.getProperties().get("fieldOne.label"), notNullValue() );
        assertThat ( "Missing key: fieldTwo.label", collector.getProperties().get("fieldTwo.label"), notNullValue() );

        assertThat ( "Missing value for: fieldOne.label", collector.getProperties().get("fieldOne.label").equals( "Label of FieldOne") );
        assertThat ( "Missing value for: fieldTwo.label", collector.getProperties().get("fieldTwo.label").equals( "Label of FieldTwo") );
    }


    /**
     * Parameters can have both label and tooltip messages.
     * This test confirms both kinds are discovered and added to the message list.
     */
    @Test public void confirmLabelsAndTooltipsAreDiscoveredAndAddedToMessageList()
    {
        project.getComponents().add ( buildExampleStep() );
        project.getComponents().add ( buildExampleAssertion() );

        MessageStringCollector collector = new MessageStringCollector( project );

        assertThat ( "Default messages are missing", defaultMessagesAreDefined( collector ) );

        assertThat ( "Missing key: isBinary.label",   collector.getProperties().get("isBinary.label"),   notNullValue() );
        assertThat ( "Missing key: isBinary.tooltip", collector.getProperties().get("isBinary.tooltip"), notNullValue() );
        assertThat ( "Missing key: isText.label",     collector.getProperties().get("isText.label"),     notNullValue() );
        assertThat ( "Missing key: isText.tooltip",   collector.getProperties().get("isText.tooltip"),   notNullValue() );

        assertThat ( "Missing key: isBinary.label",   collector.getProperties().get("isBinary.label").equals("Binary") );
        assertThat ( "Missing key: isBinary.tooltip", collector.getProperties().get("isBinary.tooltip").equals("This is binary data") );
        assertThat ( "Missing key: isText.label",     collector.getProperties().get("isText.label").equals("Text") );
        assertThat ( "Missing key: isText.tooltip",   collector.getProperties().get("isText.tooltip").equals( "This is text data") );

    }


    /**
     * Returns a sample Step component
     * @return a sample Assertion component
     */
    private Step buildExampleStep()
    {
        return StepBean.builder("mmm.coffee.step.ExampleStep")
                        .withParameter(
                                ParameterBean.builder("fieldOne")
                                        .withDataType(DataType.STRING)
                                        .withLabel( MessageBean.builder()
                                                        .withKey("fieldOne.label")
                                                        .withValue("Label of FieldOne")
                                                        .build())
                                        .build()
                        )
                .withParameter(
                        ParameterBean.builder("fieldTwo")
                                .withDataType(DataType.STRING)
                                .withLabel( MessageBean.builder()
                                                .withKey("fieldTwo.label")
                                                .withValue("Label of FieldTwo")
                                                .build())
                                .build()
                )
                .build();
    }

    /**
     * Returns a sample Assertion component
     * @return a sample Assertion component
     */
    private Assertion buildExampleAssertion()
    {
        return AssertionBean.builder("mmm.coffee.assertion.AssertionOne")
                    .withParameter(
                            ParameterBean.builder("isBinary")
                                         .withDataType( DataType.BOOLEAN )
                                         .withLabel( "Binary" )
                                         .withTooltip( "This is binary data" )
                                         .build()
                    )
                    .withParameter (
                            ParameterBean.builder("isText")
                                         .withDataType( DataType.BOOLEAN )
                                         .withLabel( "Text" )
                                         .withTooltip( "This is text data" )
                                         .build()
                    )
                    .build();
    }




}
