/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.generator.generator;

import mmm.coffee.apprentice.generator.template.TemplateCatalog;
import mmm.coffee.apprentice.generator.template.TemplateVersion;
import mmm.coffee.apprentice.model.beans.*;
import mmm.coffee.apprentice.model.stereotypes.*;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.stringtemplate.v4.ST;

import java.io.File;
import java.io.IOException;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;





/**
 * Unit tests of the JavaCodeGenerator class
 */
public class JavaCodeGeneratorTest {

    // This is our handle to the code generator
    private JavaCodeGenerator generator;

    // The Project tells us where the working directory is, which is where
    // the generated code is written.
    private Project project;

    // This is the fully qualified path to the java source directory
    // This will look like: "/tmp/workspace/src/main/java"
    private String outputDirectory;

    // The generated Java class's have this as their package
    private static final String DEFAULT_PKG = "mmm.coffee.extensions";

    // Use the system's temp space to create a 'workspace' directory,
    // to which this test case writes its generated Java files
    private final File WORKSPACE_DIR = new File ( FileUtils.getTempDirectory(), "workspace" );

    private TemplateCatalog templateCatalog;


    @Before
    public void setUp()
    {
        // ------------------------------------------------------------------------------
        // Step 1: Define the DevTest version on which the plugin will be deployed
        // ------------------------------------------------------------------------------
        ApplicationTarget applicationTarget = ApplicationTargetBean.builder()
                                                    .withHomeDirectory("/Applications/DevTest")
                                                    .build();

        // ------------------------------------------------------------------------------
        // Step 2: Define the directory to which the generated code is written
        // ------------------------------------------------------------------------------
        WorkspaceTarget workspaceTarget = WorkspaceTargetBean.builder( WORKSPACE_DIR ).build();

        // ------------------------------------------------------------------------------
        // Step 3: Define the Messages class for localization support
        // ------------------------------------------------------------------------------
        MessageResource messageResource = MessageResourceBean.builder("mmm.coffee.i18n.Messages").build();


        project = ProjectBean.builder ( applicationTarget, workspaceTarget, messageResource )
                        .withPluginName ( "SamplePlugin" )
                        .build();

        // ----------------------------------------------------------------
        // Step 4: Conjure the FQ path to which files will be written.
        // ----------------------------------------------------------------
        outputDirectory = getOutputDirectory( project );

        generator = new JavaCodeGenerator( project );

        templateCatalog = new TemplateCatalog( TemplateVersion.LATEST );
    }

    @After
    public void cleanUp() throws Exception
    {
        // We want to start with a clean workspace directory with each pass,
        try {
            FileUtils.cleanDirectory( WORKSPACE_DIR );
        }
        catch ( IOException ioe ) {
            System.out.println ( ioe.getMessage() );
            throw ioe;
        }
    }

    @Test
    public void getProject_ReturnsProject()
    {
        Project project = generator.getProject();

        assertNotNull ( project );

        assertTrue ( project.equals ( this.project ));
    }

    @Test
    public void whenGenerateStep_JavaFileIsCreated() throws Exception
    {
        // --------------------------------------------------------------------
        // Step 1: Define the component to generate
        // --------------------------------------------------------------------
        StepBean step = StepBean.builder( DEFAULT_PKG + ".AddOnStep" )
                .withGenerateCode(true)
                .withParameter(ParameterBean.builder("hostName")
                .withDataType(DataType.STRING)
                .build())
                .build();

        // --------------------------------------------------------------------
        // Step 2: Fetch the template
        // --------------------------------------------------------------------
        ST template = templateCatalog.getStepTemplates().getModelTemplate();
        template.add ( ProjectGenerator.STEP_KEY, step );
        template.add ( ProjectGenerator.MESSAGE_KEY, project.getMessageResource() );

        // --------------------------------------------------------------------
        // Step 4: Generate the Java source for this component
        // --------------------------------------------------------------------
        File f = generator.generateJava( step, ProjectGenerator.STEP_KEY, template );

        runAssertions ( f, "AddOnStep.java" );
    }

    @Test
    public void whenGenerateFilter_JavaFileIsCreated() throws Exception
    {
        // --------------------------------------------------------------------
        // Step 1: Define the component to generate
        // --------------------------------------------------------------------
        FilterBean filter = FilterBean.builder(DEFAULT_PKG + ".AddOnFilter")
                .withGenerateCode(true)
                .withParameter(
                        ParameterBean.builder("hostName")
                            .withDataType(DataType.STRING)
                            .build())
                .build();

        // --------------------------------------------------------------------
        // Step 2: Fetch the template
        // --------------------------------------------------------------------
        ST template = templateCatalog.getFilterTemplates().getModelTemplate();
        template.add ( ProjectGenerator.FILTER_KEY, filter );
        template.add ( ProjectGenerator.MESSAGE_KEY, project.getMessageResource() );

        // --------------------------------------------------------------------
        // Step 4: Generate the Java source for this component
        // --------------------------------------------------------------------
        File f = generator.generateJava( filter, ProjectGenerator.FILTER_KEY, template );

        runAssertions ( f, "AddOnFilter.java" );
    }

    @Test
    public void whenGenerateAssertion_JavaFileIsCreated() throws Exception
    {
        // --------------------------------------------------------------------
        // Step 1: Define the component to generate
        // --------------------------------------------------------------------
        AssertionBean assertion = AssertionBean.builder(DEFAULT_PKG + ".AddOnAssertion")
                .withGenerateCode(true)
                .withParameter(
                        ParameterBean.builder("expression")
                            .withDataType(DataType.STRING)
                            .build())
                .build();

        // --------------------------------------------------------------------
        // Step 2: Fetch the template
        // --------------------------------------------------------------------
        ST template = templateCatalog.getAssertionTemplates().getModelTemplate();
        template.add ( ProjectGenerator.ASSERTION_KEY, assertion );
        template.add ( ProjectGenerator.MESSAGE_KEY, project.getMessageResource() );

        // --------------------------------------------------------------------
        // Step 4: Generate the Java source for this component
        // --------------------------------------------------------------------
        File f = generator.generateJava( assertion, ProjectGenerator.ASSERTION_KEY, template );

        runAssertions ( f, "AddOnAssertion.java" );
    }

    @Test
    public void whenGenerateCompanion_JavaFileIsCreated() throws Exception
    {
        // --------------------------------------------------------------------
        // Step 1: Define the component to generate
        // --------------------------------------------------------------------
        CompanionBean companion = CompanionBean.builder(DEFAULT_PKG + ".AddOnCompanion")
                .withGenerateCode(true)
                .withParameter(
                        ParameterBean.builder("fieldOne")
                            .withDataType(DataType.STRING)
                            .build())
                .withParameter (
                        ParameterBean.builder("fieldTwo")
                            .withDataType(DataType.STRING)
                            .build())
                .build();

        // --------------------------------------------------------------------
        // Step 3: Fetch the template
        // --------------------------------------------------------------------
        ST template = templateCatalog.getCompanionTemplates().getModelTemplate();
        template.add ( ProjectGenerator.COMPANION_KEY, companion );
        template.add ( ProjectGenerator.MESSAGE_KEY, project.getMessageResource() );

        // --------------------------------------------------------------------
        // Step 4: Generate the Java source for this component
        // --------------------------------------------------------------------
        File f = generator.generateJava( companion, ProjectGenerator.COMPANION_KEY, template );

        runAssertions( f, "AddOnCompanion.java" );
    }

    @Test
    public void whenGenerateDataSet_JavaFileIsCreated() throws Exception
    {
        // --------------------------------------------------------------------
        // Step 1: Define the component to generate
        // --------------------------------------------------------------------
        DataSetBean dataSet = DataSetBean.builder(DEFAULT_PKG + ".AddOnDataSet")
                .withGenerateCode(true)
                .withParameter(
                        ParameterBean.builder("fieldOne")
                            .withDataType(DataType.STRING)
                            .build())
                .withParameter (
                        ParameterBean.builder("fieldTwo")
                            .withDataType(DataType.STRING)
                            .build())
                .build();
        // --------------------------------------------------------------------
        // Step 2: Fetch the template
        // --------------------------------------------------------------------
        ST template = templateCatalog.getDataSetTemplates().getModelTemplate();
        template.add ( ProjectGenerator.MESSAGE_KEY, project.getMessageResource() );
        template.add ( ProjectGenerator.DATASET_KEY, dataSet );

        // --------------------------------------------------------------------
        // Step 4: Generate the Java source for this component
        // --------------------------------------------------------------------
        File f = generator.generateJava( dataSet, ProjectGenerator.DATASET_KEY, template );

        runAssertions( f, "AddOnDataSet.java" );
    }

    @Test
    public void whenGenerateDPH_JavaFileIsCreated() throws Exception
    {
        // --------------------------------------------------------------------
        // Step 1: Define the component to generate
        // --------------------------------------------------------------------
        DataProtocolBean dph = DataProtocolBean.builder(DEFAULT_PKG + ".AddOnProtocol")
                .withDisplayName("XYZ")
                .withDescription("handles XYZ data")
                .withResponseSide(true)
                .build();

        // --------------------------------------------------------------------
        // Step 2: Fetch and configure the template
        // --------------------------------------------------------------------
        ST template = templateCatalog.getDataProtocolTemplates().getModelTemplate();
        template.add ( ProjectGenerator.DPH_KEY, dph );

        // --------------------------------------------------------------------
        // Step 3: Generate the Java source for this component
        // --------------------------------------------------------------------
        File f = generator.generateJava( dph, ProjectGenerator.DPH_KEY, template );

        runAssertions ( f, "AddOnProtocol.java" );
    }

    private String getOutputDirectory ( Project project )
    {
        return project.getWorkspaceTarget().getBaseDirectory()
                + "/"
                + project.getWorkspaceTarget().getSourceDirectory();
    }

    private void runAssertions ( File f, String expectedFileName )
    {
        assertThat ( "The file handle should not be null", f, notNullValue() );
        assertThat ( "The source file should exist on the file system", f.exists() );
        assertThat ( "The source file name should be '" + expectedFileName + "'", f.getName(), is(equalTo( expectedFileName )));
        assertThat ( "The path to the source file should found within " + outputDirectory,
                     f.getAbsolutePath().startsWith ( outputDirectory ) );

    }

}
