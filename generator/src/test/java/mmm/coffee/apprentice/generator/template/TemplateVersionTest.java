/*
 * Copyright (c) 2016.  Jon Caulfield.
 *
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.generator.template;


import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.notNullValue;

/**
 * Unit tests of the TemplateVersion class
 */
public class TemplateVersionTest {

    @Test
    public void whenFromStringOf9_returnsVersion9() {
        TemplateVersion version = TemplateVersion.fromString("9");
        assertThat("Expected a non-null return value", version, notNullValue());
        assertThat("Expected version 9 to be returned", version, is(TemplateVersion.VERSION_9));

    }
    @Test
    public void whenFromStringOf95_returnsVersion9()
    {
        TemplateVersion version = TemplateVersion.fromString("9.5");
        assertThat ("Expected a non-null return value", version, notNullValue());
        assertThat ("Expected version 9 to be returned", version, is(TemplateVersion.VERSION_9));
    }

    @Test
    public void whenFromStringOf10_returnsVersion10()
    {
        TemplateVersion version = TemplateVersion.fromString("10");
        assertThat ("Expected a non-null return value", version, notNullValue());
        assertThat ("Expected version 10 to be returned", version, is(TemplateVersion.VERSION_10));
    }

    /**
     * This test demonstrates how a null is handled by the fromString method
     */
    @Test
    public void whenFromStringOfNull_returnsLatestVersion()
    {
        TemplateVersion version = TemplateVersion.fromString(null);
        assertThat ("Expected a non-null return value", version, notNullValue());
        assertThat ("Expected latest to be returned", version, is(TemplateVersion.LATEST));
    }

    /**
     * This test demonstrates how an empty string is handled by the fromString method.
     */
    @Test
    public void whenFromStringOfEmptyString_returnsLatestVersion()
    {
        TemplateVersion version = TemplateVersion.fromString( "" );
        assertThat ("Expected a non-null return value", version, notNullValue());
        assertThat ("Expected latest to be returned", version, is(TemplateVersion.LATEST));
    }

}
