/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.generator.generator;

import mmm.coffee.apprentice.generator.utils.FileHelper;
import mmm.coffee.apprentice.model.beans.*;
import mmm.coffee.apprentice.model.stereotypes.*;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;


/**
 * Unit tests of the LisaExtensionsWriter class
 */
public class LisaExtensionsWriterTest {


    private File extensionFile;
    private File scratchDirectory;
    private LisaExtensionsWriter writer;


    /**
     * Creates a scratch directory into which the extension file samples are written.
     * Initializes our state.
     *
     * @throws IOException if the scratch directory cannot be created
     */
    @Before
    public void setUp() throws IOException
    {
        scratchDirectory = new File ( FileUtils.getTempDirectory(), "scratch" );
        if ( !scratchDirectory.exists() )
            FileUtils.forceMkdir( scratchDirectory );

        extensionFile = new File ( FileUtils.getTempDirectory(), "sample.lisaextensions");
        writer = new LisaExtensionsWriter( extensionFile );
    }

    /**
     * Cleans out the content of the scratch directory
     *
     * @throws IOException if the directory content cannot be removed
     */
    @After
    public void cleanUp() throws IOException
    {
        FileUtils.cleanDirectory( scratchDirectory );


    }


    @Test
    public void writeAssertions_SilentlyIgnoresNullArg() throws Exception
    {
        writer.writeAssertions( null );
        assertThat ( "When there are no components, no code should be generated", FileHelper.isEmptyDirectory ( scratchDirectory));

    }

    @Test
    public void writeAssertions_SilentlyIgnoresEmptyList() throws Exception
    {
        writer.writeAssertions( new LinkedList<>() );
        assertThat ( "When there are no components, no code should be generated", FileHelper.isEmptyDirectory ( scratchDirectory));
    }

    @Test
    public void writeFilters_SilentlyIgnoresNullArg() throws Exception
    {
        writer.writeFilters(null);
        assertThat ( "When there are no components, no code should be generated", FileHelper.isEmptyDirectory ( scratchDirectory));
    }

    @Test
    public void writeFilters_SilentlyIgnoresEmptyList() throws Exception
    {
        writer.writeFilters( new LinkedList<>() );
        assertThat ( "When there are no components, no code should be generated", FileHelper.isEmptyDirectory ( scratchDirectory));
    }

    @Test
    public void writeSteps_SilentlyIgnoresNullArg() throws Exception
    {
        writer.writeSteps( null );
        assertThat ( "When there are no components, no code should be generated", FileHelper.isEmptyDirectory ( scratchDirectory));
    }

    @Test public void writeSteps_SilentlyIgnoresEmptyList() throws Exception
    {
        writer.writeSteps( new LinkedList<>() );
        assertThat ( "When there are no components, no code should be generated", FileHelper.isEmptyDirectory ( scratchDirectory));
    }

    @Test public void writeCompanions_SilentlyIgnoresNullArg() throws Exception
    {
        writer.writeCompanions( null );
        assertThat ( "When there are no components, no code should be generated", FileHelper.isEmptyDirectory ( scratchDirectory));
    }

    @Test public void writeCompanions_SilentlyIgnoresEmptyList() throws Exception
    {
        writer.writeCompanions( new LinkedList<>() );
        assertThat ( "When there are no components, no code should be generated", FileHelper.isEmptyDirectory ( scratchDirectory));
    }

    @Test public void writeDataProtocolHandlers_SilentlyIgnoresNullArg() throws Exception
    {
        writer.writeDataProtocolHandlers( null );
        assertThat ( "When there are no components, no code should be generated", FileHelper.isEmptyDirectory ( scratchDirectory));
    }

    @Test public void writeDataProtocolHandlers_SilentlyIgnoresEmtpyList() throws Exception
    {
        writer.writeDataProtocolHandlers( new LinkedList<>() );
        assertThat ( "When there are no components, no code should be generated", FileHelper.isEmptyDirectory ( scratchDirectory));
    }

    @Test public void writeDataSets_SilentlyIgnoresNullArg() throws Exception
    {
        writer.writeDataSets( null );
        assertThat ( "When there are no components, no code should be generated", FileHelper.isEmptyDirectory ( scratchDirectory));
    }

    @Test public void writeDataSets_SilentlyIgnoresEmptyList() throws Exception
    {
        writer.writeDataSets( new LinkedList<>() );
        assertThat ( "When there are no components, no code should be generated", FileHelper.isEmptyDirectory ( scratchDirectory));
    }

    @Test public void whenExtensionContainsAssertion_thenAssertionStanzaIsAdded() throws Exception
    {
        // Note: lisaextensions don't care about parameters, so we don't add any
        Assertion assertion = AssertionBean.builder("mmm.coffee.assert.AssertTrue")
                                .withGlobalScope(false)
                                .withLocalScope(true)
                                .build();

        List<Assertion> list = new LinkedList<>();
        list.add ( assertion );

        writer.writeAssertions( list );

        String s = FileUtils.readFileToString( extensionFile );

        assertThat ( "The assertion declaration is missing", s, containsString( "asserts=" + assertion.getClassName() ));
        assertThat ( "The assertion type map is missing", s, containsString( assertion.getClassName()+ "=com.itko.lisa.editor.DefaultAssertController,com.itko.lisa.editor.DefaultAssertEditor" ));
    }

    @Test public void whenExtensionContainsMultipleAssertions_thenAssertionStanzaIsAdded() throws Exception
    {
        // Note: lisaextensions only cares about assertion classnames, not any of its properties.
        Assertion a1 = AssertionBean.builder("mmm.coffee.extension.assertion.JsonAssertion")
                                .build();

        Assertion a2 = AssertionBean.builder("mmm.coffee.extension.assertion.XmlAssertion")
                                .build();

        List<Assertion> list = new LinkedList<>();
        list.add ( a1 );
        list.add ( a2 );

        writer.writeAssertions( list );

        String s = FileUtils.readFileToString( extensionFile );

        assertThat ( "The assertion declaration is invalid", s, containsString( "asserts=mmm.coffee.extension.assertion.JsonAssertion,") );
        assertThat ( "The Json assertion type map is invalid", s, containsString( "mmm.coffee.extension.assertion.JsonAssertion=com.itko.lisa.editor.DefaultAssertController,com.itko.lisa.editor.DefaultAssertEditor" ));
        assertThat ( "The XML assertion type map is invalid", s, containsString( "mmm.coffee.extension.assertion.XmlAssertion=com.itko.lisa.editor.DefaultAssertController,com.itko.lisa.editor.DefaultAssertEditor" ));
    }

    @Test public void whenExtensionContainsFilter_thenFilterStanzaIsAdded() throws Exception
    {
        Filter filter = FilterBean.builder("mmm.coffee.extension.filter.JsonFilter")
                            .build();

        List<Filter> list = new LinkedList<>();
        list.add ( filter );

        writer.writeFilters( list );

        String s = FileUtils.readFileToString( extensionFile );

        assertThat ("Filter declaration is missing or incorrect", s, containsString( "filters=mmm.coffee.extension.filter.JsonFilter") );
        assertThat ("Filter type map is missing or incorrect", s, containsString( "mmm.coffee.extension.filter.JsonFilter=com.itko.lisa.editor.FilterController,com.itko.lisa.editor.DefaultFilterEditor") );
    }

    @Test public void whenExtensionContainsMultipleFilters_thenFilterStanzaIsAdded() throws Exception
    {
        Filter f1 = FilterBean.builder("mmm.coffee.extension.filter.JsonFilter")
                            .build();

        Filter f2 = FilterBean.builder("mmm.coffee.extension.filter.XmlFilter")
                            .build();

        List<Filter> list = new LinkedList<>();
        list.add ( f1 );
        list.add ( f2 );

        writer.writeFilters( list );

        String s = FileUtils.readFileToString( extensionFile );

        // With multiple filters, we expect a comma to appear between their declarations,
        // e.g: filters=f1,f2,f3
        // Be aware, each entry is placed on a new line; they're not strung together

        assertThat ("Filter declaration is missing or incorrect", s, containsString( "filters=mmm.coffee.extension.filter.JsonFilter,") );
        assertThat ("Filter type map is missing or incorrect", s, containsString( "mmm.coffee.extension.filter.JsonFilter=com.itko.lisa.editor.FilterController,com.itko.lisa.editor.DefaultFilterEditor") );
        assertThat ("Filter type map is missing or incorrect", s, containsString( "mmm.coffee.extension.filter.XmlFilter=com.itko.lisa.editor.FilterController,com.itko.lisa.editor.DefaultFilterEditor") );
    }

    @Test public void whenExtensionContainsCompanion_thenCompanionStanzaIsAdded() throws Exception
    {
        Companion c = CompanionBean.builder("mmm.coffee.extension.companion.TestCompanion")
                        .build();

        List<Companion> list = new LinkedList<>();
        list.add ( c );

        writer.writeCompanions( list );

        String s = FileUtils.readFileToString( extensionFile );


        assertThat ( "Companion declaration is missing or incorrect", s, containsString ("companions=mmm.coffee.extension.companion.TestCompanion"));
        assertThat ( "Companion type map is missing or incorrect", s, containsString ("mmm.coffee.extension.companion.TestCompanion=com.itko.lisa.editor.CompanionController,com.itko.lisa.editor.SimpleCompanionEditor") );
    }

    @Test public void whenExtensionContainsMultipleCompanions_thenCompanionStanzaShowsMultipleCompanions() throws Exception
    {
        Companion c1 = CompanionBean.builder("mmm.coffee.extension.companion.YesCompanion")
                            .build();

        Companion c2 = CompanionBean.builder("mmm.coffee.extension.companion.NoCompanion")
                            .build();

        List<Companion> list = new LinkedList<>();
        list.add ( c1 );
        list.add ( c2 );

        writer.writeCompanions( list );

        String s = FileUtils.readFileToString( extensionFile );

        // Look for a comma following the first companion's declaration
        assertThat ( "Companion declaration is missing or incorrect", s, containsString( "companions=mmm.coffee.extension.companion.YesCompanion," ));

        // Look for the type map entries
        assertThat ( "Companion type map is missing or incorrect", s, containsString( "mmm.coffee.extension.companion.YesCompanion=com.itko.lisa.editor.CompanionController,com.itko.lisa.editor.SimpleCompanionEditor" ));
        assertThat ( "Companion type map is missing or incorrect", s, containsString( "mmm.coffee.extension.companion.NoCompanion=com.itko.lisa.editor.CompanionController,com.itko.lisa.editor.SimpleCompanionEditor" ));

    }

    @Test public void whenExtensionContainsStep_thenStepStanzaIsAdded() throws Exception
    {
        // Steps do not have a default editor or default controller, so we must define them.
        Step step = StepBean.builder("mmm.coffee.step.CustomStep")
                        .withEditor(
                                ComponentBean.builder("mmm.coffee.step.StepEditor")
                                        .build())
                        .withController(
                                ComponentBean.builder("mmm.coffee.step.StepController")
                                        .build())
                        .build();

        List<Step> list = new LinkedList<>();
        list.add ( step );

        writer.writeSteps( list );

        String s = FileUtils.readFileToString( extensionFile );

        assertThat ("Step declaration is missing or incorrect", s, containsString("nodes=mmm.coffee.step.CustomStep"));
        assertThat ("Step type map is missing or incorrect", s, containsString("mmm.coffee.step.CustomStep=mmm.coffee.step.StepController,mmm.coffee.step.StepEditor"));
    }

    @Test
    public void whenExtensionContainsMultipleSteps_thenStepStanzaShowsMultipleSteps() throws Exception
    {
        // Steps do not have a default editor or default controller, so we must define them.
        Step stepOne = StepBean.builder("mmm.coffee.step.StepOne")
                        .withEditor(
                                ComponentBean.builder("mmm.coffee.step.StepOneEditor")
                                        .build())
                        .withController(
                                ComponentBean.builder("mmm.coffee.step.StepOneController")
                                        .build())
                        .build();

        Step stepTwo = StepBean.builder("mmm.coffee.step.StepTwo")
                        .withEditor(
                                ComponentBean.builder("mmm.coffee.step.StepTwoEditor")
                                        .build())
                        .withController(
                                ComponentBean.builder("mmm.coffee.step.StepTwoController")
                                        .build())
                        .build();


        List<Step> list = new LinkedList<>();
        list.add ( stepOne );
        list.add ( stepTwo );

        writer.writeSteps( list );

        String s = FileUtils.readFileToString( extensionFile );

        // Look for a comma following the first entry
        assertThat ("Step declaration is missing or incorrect", s, containsString("nodes=mmm.coffee.step.StepOne,"));

        assertThat ("Step type map is missing or incorrect", s, containsString("mmm.coffee.step.StepOne=mmm.coffee.step.StepOneController,mmm.coffee.step.StepOneEditor"));
        assertThat ("Step type map is missing or incorrect", s, containsString("mmm.coffee.step.StepTwo=mmm.coffee.step.StepTwoController,mmm.coffee.step.StepTwoEditor"));

    }

    @Test public void whenExtensionContainsDataSet_thenDataSetStanzaIsCreated() throws Exception
    {
        DataSet ds = DataSetBean.builder("mmm.coffee.db.DataSet")
                        .withController( ComponentBean.builder("mmm.coffee.db.DataController")
                                            .build())
                        .withEditor ( ComponentBean.builder("mmm.coffee.db.DataEditor")
                                            .build())
                        .build();

        List<DataSet> list = new LinkedList<>();
        list.add ( ds );

        writer.writeDataSets( list );

        String s = FileUtils.readFileToString( extensionFile );

        assertThat ( "The data set declaration is missing or incorrect",
                s, containsString( "datasets=mmm.coffee.db.DataSet" ));

        assertThat ( "The data set type map is missing or incorrect",
                s, containsString("mmm.coffee.db.DataSet=mmm.coffee.db.DataController,mmm.coffee.db.DataEditor") );
    }

    @Test public void whenExtensionContainsMultipleDataSets_thenDataSetStanzaShowsMultipleDataSets() throws Exception
    {
        DataSet dsXml = DataSetBean.builder("mmm.coffee.db.XmlDataSet")
                        .withController( ComponentBean.builder("mmm.coffee.db.XmlDataController")
                                            .build())
                        .withEditor ( ComponentBean.builder("mmm.coffee.db.XmlDataEditor")
                                            .build())
                        .build();


        DataSet dsJson = DataSetBean.builder("mmm.coffee.db.JsonDataSet")
                        .withController( ComponentBean.builder("mmm.coffee.db.JsonDataController")
                                            .build())
                        .withEditor ( ComponentBean.builder("mmm.coffee.db.JsonDataEditor")
                                            .build())
                        .build();

        List<DataSet> list = new LinkedList<>();
        list.add ( dsXml );
        list.add ( dsJson );

        writer.writeDataSets( list );

        String s = FileUtils.readFileToString( extensionFile );

        // Look for a comma following the first entry
        assertThat ( "The data set declaration is missing or incorrect",
                s, containsString( "datasets=mmm.coffee.db.XmlDataSet," ));

        // Look for a type map entry for both data sets
        assertThat ( "The data set type map is missing or incorrect",
                s, containsString("mmm.coffee.db.XmlDataSet=mmm.coffee.db.XmlDataController,mmm.coffee.db.XmlDataEditor") );

        assertThat ( "The data set type map is missing or incorrect",
                s, containsString("mmm.coffee.db.JsonDataSet=mmm.coffee.db.JsonDataController,mmm.coffee.db.JsonDataEditor") );

    }

    @Test public void whenExtensionContainsDataProtocol_thenDataProtocolStanzaIsAdded() throws Exception
    {
        DataProtocol dph = DataProtocolBean.builder("mmm.coffee.dph.ChessProtocol")
                                .withDisplayName ("Chess Protocol")
                                .withDescription("Sample description")
                                .withRequestSide(true)
                                .withResponseSide(true)
                                .build();

        List<DataProtocol> list = new LinkedList<>();
        list.add ( dph );

        writer.writeDataProtocolHandlers( list );

        String s = FileUtils.readFileToString( extensionFile );

        // Look for vseProtocols declaration
        assertThat ( "The data protocol declaration is missing or incorrect",
                s, containsString( "vseProtocols=mmm.coffee.dph.ChessProtocol" ));

        // Look for a type map entry for both data sets
        assertThat ( "The data protocol type map is missing or incorrect",
                s, containsString("mmm.coffee.dph.ChessProtocol=data:req:resp,Chess Protocol,Sample description") );
    }

    @Test public void whenDataProtocolIsRequestOnly_thenOnlyRequestFlagIsRendered() throws Exception
    {
        DataProtocol dph = DataProtocolBean.builder("mmm.coffee.dph.RequestOnlyProtocol")
                                .withDisplayName("RequestOnly Protocol")
                                .withDescription("Sample description")
                                .withRequestSide(true)
                                .build();

        List<DataProtocol> list = new LinkedList<>();
        list.add(dph);

        writer.writeDataProtocolHandlers( list );

        String s = FileUtils.readFileToString( extensionFile );

        assertThat ( "The data protocol declaration is missing or invalid",
                s, containsString("vseProtocols=mmm.coffee.dph.RequestOnlyProtocol") );

        assertThat ( "The data protocol type map is missing or invalid",
                s, containsString("mmm.coffee.dph.RequestOnlyProtocol=data:req,RequestOnly Protocol,Sample description") );

    }

    @Test public void whenDataProtocolIsResponseOnly_thenOnlyResponseFlagIsRendered() throws Exception
    {
        DataProtocol dph = DataProtocolBean.builder("mmm.coffee.dph.ResponseOnlyProtocol")
                                .withDisplayName("ResponseOnly Protocol")
                                .withDescription("Sample description")
                                .withResponseSide(true)
                                .build();

        List<DataProtocol> list = new LinkedList<>();
        list.add(dph);

        writer.writeDataProtocolHandlers( list );

        String s = FileUtils.readFileToString( extensionFile );

        assertThat ( "The data protocol declaration is missing or invalid",
                s, containsString("vseProtocols=mmm.coffee.dph.ResponseOnlyProtocol") );

        assertThat ( "The data protocol type map is missing or invalid",
                s, containsString("mmm.coffee.dph.ResponseOnlyProtocol=data:resp,ResponseOnly Protocol,Sample description") );

    }


    @Test public void whenExtensionContainsMultipleDataProtocols_thenDphStanzaRendersAll() throws Exception
    {
        DataProtocol reqDPH = DataProtocolBean.builder("mmm.coffee.dph.RequestProtocol")
                                .withDisplayName("Request Protocol")
                                .withDescription("request description")
                                .withDisplayName("Request Protocol")
                                .withRequestSide(true)
                                .build();

        DataProtocol rspDPH = DataProtocolBean.builder("mmm.coffee.dph.ResponseProtocol")
                                .withDescription("response description")
                                .withDisplayName("Response Protocol")
                                .withResponseSide(true)
                                .build();

        DataProtocol rrDPH = DataProtocolBean.builder("mmm.coffee.dph.RRProtocol")
                                .withDescription("rr description")
                                .withDisplayName("RR Protocol")
                                .withRequestSide(true)
                                .withResponseSide(true)
                                .build();


        List<DataProtocol> list = new LinkedList<>();
        list.add ( reqDPH );
        list.add ( rspDPH );
        list.add ( rrDPH );

        writer.writeDataProtocolHandlers( list );

        String s = FileUtils.readFileToString( extensionFile );

        // Look for a comma following the first declaration
        assertThat ( "The data protocol declaration is missing or invalid",
                s, containsString("vseProtocols=mmm.coffee.dph.RequestProtocol,") );

        assertThat ( "The data protocol type map is missing or invalid",
                s, containsString("mmm.coffee.dph.RequestProtocol=data:req,Request Protocol,request description") );

        assertThat ( "The data protocol type map is missing or invalid",
                s, containsString("mmm.coffee.dph.ResponseProtocol=data:resp,Response Protocol,response description") );

        assertThat ( "The data protocol type map is missing or invalid",
                s, containsString("mmm.coffee.dph.RRProtocol=data:req:resp,RR Protocol,rr description") );

    }

}
