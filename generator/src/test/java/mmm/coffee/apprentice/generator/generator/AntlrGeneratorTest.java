/*
 * Copyright (c) 2016 CA.  All rights reserved.  
 * 
 * This software and all information contained therein is confidential and 
 * proprietary and shall not be duplicated, used, disclosed or disseminated 
 * in any way except as authorized by the applicable license agreement, 
 * without the express written permission of CA. All authorized reproductions 
 * must be marked with this language.  
 * 
 * EXCEPT AS SET FORTH IN THE APPLICABLE LICENSE AGREEMENT, TO THE EXTENT 
 * PERMITTED BY APPLICABLE LAW OR AS AGREED BY CA IN ITS APPLICABLE LICENSE 
 * AGREEMENT, CA PROVIDES THIS SOFTWARE "AS IS" WITHOUT WARRANTY OF ANY KIND, 
 * INCLUDING WITHOUT LIMITATION, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE, OR NONINFRINGEMENT. IN NO EVENT WILL CA 
 * BE LIABLE TO THE END USER OR ANY THIRD PARTY FOR ANY LOSS OR DAMAGE, DIRECT 
 * OR INDIRECT, FROM THE USE OF THIS SOFTWARE, INCLUDING WITHOUT LIMITATION, 
 * LOST PROFITS, LOST INVESTMENT, BUSINESS INTERRUPTION, GOODWILL, OR LOST 
 * DATA, EVEN IF CA IS EXPRESSLY ADVISED IN ADVANCE OF THE POSSIBILITY OF SUCH 
 * LOSS OR DAMAGE.
 */
package mmm.coffee.apprentice.generator.generator;

import mmm.coffee.apprentice.generator.decorator.ProjectDecorator;
import mmm.coffee.apprentice.generator.decorator.StepDecorator;
import mmm.coffee.apprentice.generator.fakes.FakeBuilder;
import mmm.coffee.apprentice.model.stereotypes.*;
import org.junit.BeforeClass;
import org.junit.Test;

import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupDir;

import java.util.MissingResourceException;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * These tests confirm the templates are loading and rendering.
 * These tests help debug specific templates, revealing errors such as
 * misspelled template names, incorrect parameter names, missing included templates,
 * and such.
 */
public class AntlrGeneratorTest {

    private static STGroup group;

    @BeforeClass
    public static void setUp() {
       group = new STGroupDir("templates/10.0");
       if ( group == null )
           throw new IllegalStateException("The template group was not found");
    }

    /**
     * Fetches the given StringTemplate
     * @param template the name of the template file (without the '.st' suffix)
     * @return the StringTemplate instance of {@code template}
     * @throws MissingResourceException
     */
    private ST getTemplate(String template) throws MissingResourceException
    {
       ST st = group.getInstanceOf( template );
       if ( st == null ) {
           String errMsg = String.format("The template '%s' was not found", template );
           throw new MissingResourceException( errMsg, group.getName(), template );
       }
       return st;
    }


    /**
     * Confirm the AntBuild template loads and renders
     */
    @Test
    public void whenAntBuildTemplateIsRendered_expectNonNullValue()
    {
        Project project = FakeBuilder.buildFakeProject();

        ST template = getTemplate("AntBuild");
        template.add("project", project);

        String result = template.render();
        assertThat ("Expected the AntBuild template to render content", result, notNullValue());
    }

    /**
     * Confirm the AssertionModel template loads and renders
     */
    @Test
    public void whenAssertionModelTemplateIsRendered_expectedNonNullValue()
    {
        Project project = FakeBuilder.buildFakeProject();
        Assertion assertion = FakeBuilder.buildFakeAssertion();

        project.getComponents().add ( assertion );

        ST template = getTemplate("AssertionModel");
        template.add("assertion", assertion);
        template.add("messages", project.getMessageResource());

        String result = template.render();
        assertThat ("Expected the AssertionModel template to render content", result, notNullValue());
    }

    /**
     * Confirm the DataSetModel template loads and renders
     */
    @Test
    public void whenDataSetModelTemplateIsRendered_expectNonNullValue()
    {
        Project project = FakeBuilder.buildFakeProject();
        DataSet dataSet = FakeBuilder.buildFakeDataSet();

        project.getComponents().add ( dataSet );

        ST template = getTemplate("DataSetModel");
        template.add ( "dataset", dataSet );
        template.add ( "messages", project.getMessageResource() );

        String result = template.render();
        assertThat ("Expected the DataSetModel template to render content", result, notNullValue());
    }

    /**
     * Confirm the CompanionModel template loads and renders
     */
    @Test
    public void whenCompanionModelTemplateIsRendered_expectNonNullValue()
    {
        Project project = FakeBuilder.buildFakeProject();
        Companion companion = FakeBuilder.buildFakeCompanion();

        project.getComponents().add ( companion );

        ST template = getTemplate("CompanionModel");
        template.add ("companion", companion );
        template.add ("messages", project.getMessageResource());

        String result = template.render();
        assertThat ("Expected the CompanionModel template to render content", result, notNullValue());
    }

    /**
     * Confirm the DphModel template loads and renders
     */
    @Test
    public void whenDphModelIsRendered_expectNonNullValue()
    {
        Project project = FakeBuilder.buildFakeProject();
        DataProtocol dph = FakeBuilder.buildFakeDataProtocol();

        project.getComponents().add ( dph );

        ST template = getTemplate ("DphModel");
        template.add ( "dph", dph );

        String result = template.render();
        assertThat ("Expected the DphModel template to render content", result, notNullValue());
    }

    /**
     * Confirm the FilterModel template loads and renders
     */
    @Test
    public void whenFilterModelIsRendered_expectNonNullValue()
    {
        Project project = FakeBuilder.buildFakeProject();
        Filter filter = FakeBuilder.buildFakeFilter();

        project.getComponents().add ( filter );

        ST template = getTemplate("FilterModel");
        template.add ( "filter", filter );
        template.add ( "messages", project.getMessageResource() );

        String result = template.render();
        assertThat ("Expected the FilterModel template to render content", result, notNullValue());
    }

    /**
     * Confirm the MessageLookup template loads and renders
     */
    @Test
    public void whenMessageLookupIsRendered_expectNonNullValue()
    {
        Project project = FakeBuilder.buildFakeProject();
        ProjectDecorator decorator = new ProjectDecorator( project );

        ST template = getTemplate("MessageLookup");
        template.add ("messages", project.getMessageResource() );
        template.add ("bundleName", decorator.getBundleName() );

        String result = template.render();
        assertThat ("Expected the MessageLookup template to render content", result, notNullValue());
    }

    /**
     * Confirm the StepController template loads and renders content
     */
    @Test
    public void whenStepControllerIsRendered_expectNonNullValue()
    {
        Project project = FakeBuilder.buildFakeProject();
        Step step = FakeBuilder.buildFakeStep();

        project.getComponents().add ( step );

        ST template = getTemplate("StepController");
        template.add ( "step", new StepDecorator ( step ) );
        template.add ( "messages", project.getMessageResource() );

        String result = template.render();
        assertThat ("Expected the StepController template to render content", result, notNullValue());
    }

    /**
     * Confirm the StepEditor template loads and renders
     */
    @Test
    public void whenStepEditorIsRendered_expectNonNullValue()
    {
        Project project = FakeBuilder.buildFakeProject();
        Step step = FakeBuilder.buildFakeStep();

        project.getComponents().add ( step );

        ST template = getTemplate("StepEditor");
        template.add ( "step", new StepDecorator( step ));
        template.add ( "messages", project.getMessageResource() );

        String content = template.render();
        assertThat ("Expected the StepEditor template to render content", content, notNullValue());
    }

    /**
     * Confirm the StepModel template loads and renders
     */
    @Test
    public void whenStepModelIsRendered_expectNonNullValue()
    {
        Project project = FakeBuilder.buildFakeProject();
        Step step = FakeBuilder.buildFakeStep();

        project.getComponents().add ( step );

        ST template = getTemplate("StepModel");
        template.add ( "step", new StepDecorator( step ));
        template.add ( "messages", project.getMessageResource() );

        String content = template.render();
        assertThat ("Expected the StepModel template to render content", content, notNullValue());
    }

    /**
     * Confirm the LisaExtensions template loads and renders
     */
    @Test
    public void testLisaExtensionTemplateWithFullBlownProject()
    {
        // For this test case, we add several of each kind of
        // component to confirm the template correctly renders
        // lists of components.
        Project project = FakeBuilder.buildFakeProject();

        Assertion ax1 = FakeBuilder.buildFakeAssertion();
        Assertion ax2 = FakeBuilder.buildAnotherFakeAssertion();
        project.getComponents().add ( ax1 );
        project.getComponents().add ( ax2 );

        Companion cx1 = FakeBuilder.buildFakeCompanion();
        Companion cx2 = FakeBuilder.buildAnotherFakeCompanion();
        project.getComponents().add ( cx1 );
        project.getComponents().add ( cx2 );

        DataSet   ds1 = FakeBuilder.buildFakeDataSet();
        DataSet   ds2 = FakeBuilder.buildAnotherFakeDataSet();
        project.getComponents().add ( ds1 );
        project.getComponents().add ( ds2 );

        Filter    fx1 = FakeBuilder.buildFakeFilter();
        Filter    fx2 = FakeBuilder.buildAnotherFakeFilter();
        project.getComponents().add ( fx1 );
        project.getComponents().add ( fx2 );

        Step sx1 = FakeBuilder.buildFakeStep();
        Step sx2 = FakeBuilder.buildAnotherFakeStep();
        project.getComponents().add ( sx1 );
        project.getComponents().add ( sx2 );

        DataProtocol dp1 = FakeBuilder.buildFakeDataProtocol();
        DataProtocol dp2 = FakeBuilder.buildRequestResponseDataProtocol();
        DataProtocol dp3 = FakeBuilder.buildRequestSideDataProtocol();
        project.getComponents().add ( dp1 );
        project.getComponents().add ( dp2 );
        project.getComponents().add ( dp3 );

        ProjectDecorator decorator = new ProjectDecorator( project );

        ST template = getTemplate("LisaExtensions");
        template.add("project", decorator );

        String content = template.render();
        assertThat ("Expected the LisaExtensions template to render content", content, notNullValue());
    }

    /**
     * Confirm the DataProtocolStanza template loads and renders.
     * Mostly, this test shows how to test the minor templates that get
     * imported by the main templates.  This is a useful approach when
     * you need to debug the minor templates. (By minor template, we simply
     * mean a template that is intended to be imported by another template.)
     */
    @Test
    public void testProtocol()
    {
        Project project = FakeBuilder.buildFakeProject();
        DataProtocol dp = FakeBuilder.buildRequestResponseDataProtocol();
        DataProtocol dp2 = FakeBuilder.buildRequestSideDataProtocol();
        DataProtocol dp3 = FakeBuilder.buildFakeDataProtocol();

        project.getComponents().add(dp);
        project.getComponents().add(dp2);
        project.getComponents().add(dp3);

        ProjectDecorator decorator = new ProjectDecorator( project );

        ST template = getTemplate("extensions/DataProtocolStanza");
        template.add("protocols", decorator.getDataProtocols() );

        String content = template.render();
        assertThat ("Expected teh DataProtocolStanza template to render content", content, notNullValue());
    }

}
