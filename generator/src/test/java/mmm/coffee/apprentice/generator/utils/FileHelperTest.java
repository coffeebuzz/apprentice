/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.generator.utils;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.io.File;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;


/**
 * Unit tests for the FileHelper class.
 */
public class FileHelperTest {

    private static final File TEMP_DIRECTORY = FileUtils.getTempDirectory();


    @Test
    public void getUniqueFileName_returnsNonEmptyString()
    {
        String fn = FileHelper.getUniqueFileName( "workspace" );

        assertThat ( "getUniqueFileName should not return a null value", fn, notNullValue());
        assertThat ( "getUniqueFileName should not return an empty string", fn.trim().length() > 0 );
    }


    @Test
    public void createFile_createsFileOnFileSystem() throws Exception
    {
        String fileName = FileHelper.getUniqueFileName( "workspace" );
        File tmpFile = new File ( TEMP_DIRECTORY, fileName );
        FileHelper.createFile( tmpFile );

        assertThat ( "createFile failed to create the file: " + tmpFile.getAbsolutePath(), tmpFile.exists() );
    }

    @Test
    public void isEmptyDirectory_returnsTrueForEmptyDirectory() throws Exception
    {
        String folderName = FileHelper.getUniqueFileName( "workspace" );
        File tmpFolder = new File ( TEMP_DIRECTORY, folderName );
        FileUtils.forceMkdir( tmpFolder );

        boolean isEmpty = FileHelper.isEmptyDirectory( tmpFolder );

        assertThat ( "isEmptyDirectory should return 'true' for empty directory", isEmpty );
    }

    @Test
    public void isEmptyDirectory_returnsFalseForNonEmptyDirectory() throws Exception
    {
        // Step 1: Create a scratch file in the temp directory.
        // This will guarantee a non-empty temp directory
        String fileName = FileHelper.getUniqueFileName( "workspace" );
        File tmpFile = new File ( TEMP_DIRECTORY, fileName );
        FileHelper.createFile( tmpFile );

        // Step 2: Confirm the temp directory is not empty
        boolean notEmpty = !FileHelper.isEmptyDirectory( TEMP_DIRECTORY );
        assertThat ( "isEmptyDirectory should return 'false' for a non-empty directory", notEmpty );
    }

}
