/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.generator.decorator;

import mmm.coffee.apprentice.generator.fakes.FakeBuilder;
import mmm.coffee.apprentice.model.beans.ApplicationTargetBean;
import mmm.coffee.apprentice.model.beans.MessageResourceBean;
import mmm.coffee.apprentice.model.beans.ProjectBean;
import mmm.coffee.apprentice.model.beans.WorkspaceTargetBean;
import mmm.coffee.apprentice.model.stereotypes.*;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Set;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.*;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * These are unit tests of the ProjectDecorator class.
 */
public class ProjectDecoratorTest {

    private Project project;
    private ProjectDecorator projectDecorator;
    private MessageResource messageResource;

    @Before
    public void setUp()
    {
        // ----------------------------------------------------------------
        // Step 1: Define the output directory for the generated code
        // ----------------------------------------------------------------
        WorkspaceTarget workspaceTarget = WorkspaceTargetBean.builder ( "/tmp/workspace" ).build();

        // ----------------------------------------------------------------
        // Step 2: Define the target version of DevTest
        // ----------------------------------------------------------------
        ApplicationTarget applicationTarget = ApplicationTargetBean.builder()
                                .withHomeDirectory("/Applications/DevTest")
                                .build();

        // ----------------------------------------------------------------
        // Step 3: Define the Messages class for localization
        // ----------------------------------------------------------------
        messageResource = MessageResourceBean.builder("mmm.coffee.i18n.Messages").build();


        // ----------------------------------------------------------------
        // Step 4: Create the project
        // ----------------------------------------------------------------
        project = ProjectBean.builder( applicationTarget, workspaceTarget, messageResource )
                             .withPluginName("MyExtension")
                             .build();

        // ----------------------------------------------------------------
        // Step 2: Construct a ProjectDecorator with our well-formed project
        // ----------------------------------------------------------------
        projectDecorator = new ProjectDecorator( project );

    }


    /**
     * Verify the ProjectDecorator constructor throws an NPE if a null Project object is used.
     */
    @Test (expected = NullPointerException.class )
    public void whenNullArgIsGivenToConstructor_ThenNPEIsThrown()
    {
        new ProjectDecorator( null );
    }


    @Test
    public void getAntBuildFile_ReturnsBuildXML()
    {
        File antFile = projectDecorator.getAntBuildFile();

        assertThat ( "The ant build file should be named 'build.xml'", antFile.getName(), is ("build.xml") );
    }

    @Test
    public void getAntBuildPropertiesFile_ReturnsBuildProperties()
    {
        File antPropsFile = projectDecorator.getAntBuildPropertiesFile();
        assertThat ( "The build.properties file should be named 'build.properties", antPropsFile.getName(), is ("build.properties") );
    }

    @Test
    public void getBundleName_NeverReturnsNull()
    {
        assertNotNull ( "The bundle name should never be null", projectDecorator.getBundleName() );
    }

    @Test
    public void getLisaExtensionsFile_ReturnsProperFilename()
    {
        File f = projectDecorator.getLisaExtensionsFile();

        assertNotNull ( "The extension file should never be null", f );

        // The project's plugin name is the base name of the extensions file.
        // Thus, if the plugin name is 'MyPlugin', the extensions file is MyPlugin.lisaextensions
        assertTrue  ( f.getName().startsWith ( project.getPluginName() ));

        assertTrue ( f.getName().endsWith(".lisaextensions"));
    }

    /**
     * The messages.properties file that gets generated has to reside in some directory.
     * Currently, the messages.properties is given the same package name as the project's MessageResource.
     * For instance, if the MessageResource declares the class "mmm.coffee.i18n.Messages" to be the
     * message class, then we generate "mmm.coffee.i18n.messages.properties" for the localized strings.
     */
    @Test
    public void getDefaultMessageBundleDirectory_matchesMessageResourcePackageName()
    {
        String bundleName = project.getDefaultMessageBundleDirectory();
        String packageName = messageResource.getPackageName();

        // The bundleName looks like a file path, such as "mmm/coffee/i18n"
        // The packageName looks like "mmm.coffee.i18n"
        // Make the separators match
        packageName = packageName.replace(".", "/");

        assertThat ("The bundle directory name should match the MessageResource package name", bundleName, is ( packageName ));
    }

    @Test
    public void whenGetAssertions_expectAllAssertionsAreReturned()
    {
        Project project = FakeBuilder.buildFakeProject();
        Assertion assertion = FakeBuilder.buildFakeAssertion();
        project.getComponents().add(assertion);

        ProjectDecorator decorator = new ProjectDecorator( project );

        Set<Assertion> rs = decorator.getAssertions();

        assertThat ("Expected a non-null return value", rs, notNullValue());
        assertThat ("Expected the set to contain one element", rs.size(), is(1));
    }

    @Test
    public void whenGetCompanions_expectAllCompanionsAreReturned()
    {
        Project project = FakeBuilder.buildFakeProject();
        Companion companion = FakeBuilder.buildFakeCompanion();
        project.getComponents().add(companion);

        ProjectDecorator decorator = new ProjectDecorator( project );

        Set<Companion> rs = decorator.getCompanions();

        assertThat ("Expected a non-null return value", rs, notNullValue());
        assertThat ("Expected the set to contain one element", rs.size(), is(1));
    }

    @Test
    public void whenGetDataSets_expectAllDataSetsAreReturned()
    {
        Project project = FakeBuilder.buildFakeProject();
        DataSet ds = FakeBuilder.buildFakeDataSet();
        project.getComponents().add(ds);

        ProjectDecorator decorator = new ProjectDecorator( project );

        Set<DataSet> rs = decorator.getDataSets();

        assertThat ("Expected a non-null return value", rs, notNullValue());
        assertThat ("Expected the set to contain one element", rs.size(), is(1));
    }

    @Test
    public void whenGetDataProtocols_expectAllDataProtocolsAreReturned()
    {
        Project project = FakeBuilder.buildFakeProject();
        DataProtocol dph = FakeBuilder.buildFakeDataProtocol();
        project.getComponents().add ( dph );

        ProjectDecorator decorator = new ProjectDecorator( project );

        Set<DataProtocol> rs = decorator.getDataProtocols();

        assertThat ("Expected a non-null return value", rs, notNullValue());
        assertThat ("Expected the set to contain one element", rs.size(), is(1));
    }

    @Test
    public void whenGetFilters_expectAllFiltersAreReturned()
    {
        Project project = FakeBuilder.buildFakeProject();
        Filter f = FakeBuilder.buildFakeFilter();

        project.getComponents().add(f);

        ProjectDecorator decorator = new ProjectDecorator( project );

        Set<Filter> filters = decorator.getFilters();

        assertThat ("Expected a non-null return value", filters, notNullValue());
        assertThat ("Expected the set to contain one element", filters.size(), is(1));
    }

    @Test
    public void whenGetSteps_expectAllStepsAreReturned()
    {
        Project project = FakeBuilder.buildFakeProject();
        Step step = FakeBuilder.buildFakeStep();

        project.getComponents().add(step);

        ProjectDecorator decorator = new ProjectDecorator( project );

        Set<Step> rs = decorator.getSteps();

        assertThat ("Expected a non-null return value", rs, notNullValue());
        assertThat ("Expected the set to contain one element", rs.size(), is(1));
    }

    @Test
    public void whenProjectHasSteps_expectHasStepsIsTrue()
    {
        Project project = FakeBuilder.buildFakeProject();
        Step step = FakeBuilder.buildFakeStep();
        project.getComponents().add(step);

        ProjectDecorator decorator = new ProjectDecorator( project );

        assertThat ("Expected getHasSteps to be true when the project contains a Step",
                decorator.getHasSteps(), is(true));
    }

    @Test
    public void whenProjectHasAssertions_expectHasAssertionsIsTrue()
    {
        Project project = FakeBuilder.buildFakeProject();
        Assertion assertion = FakeBuilder.buildFakeAssertion();
        project.getComponents().add(assertion);

        ProjectDecorator decorator = new ProjectDecorator( project );

        assertThat ("Expected getHasAssertions to be true when the project contains an Assertion",
                decorator.getHasAssertions(), is(true));
    }

    @Test
    public void whenProjectHasCompanions_expectHasCompanionsIsTrue()
    {
        Project project = FakeBuilder.buildFakeProject();
        Companion companion = FakeBuilder.buildFakeCompanion();
        project.getComponents().add(companion);

        ProjectDecorator decorator = new ProjectDecorator( project );

        assertThat ("Expected getHasCompanions to be true when the project contains a Companion",
                decorator.getHasCompanions(), is(true));
    }

    @Test
    public void whenProjectHasDataSets_expectHasDataSetsIsTrue()
    {
        Project project = FakeBuilder.buildFakeProject();
        DataSet ds = FakeBuilder.buildFakeDataSet();
        project.getComponents().add(ds);

        ProjectDecorator decorator = new ProjectDecorator( project );

        assertThat ("Expected getHasDataSets to be true when the project contains a DataSet",
                decorator.getHasDataSets(), is(true));
    }

    @Test
    public void whenProjectHasDataProtocols_expectHasDataSetsIsTrue()
    {
        Project project = FakeBuilder.buildFakeProject();
        DataProtocol dph = FakeBuilder.buildFakeDataProtocol();
        project.getComponents().add(dph);

        ProjectDecorator decorator = new ProjectDecorator( project );

        assertThat ("Expected getHasDataProtocols to be true when the project contains a DataProtocol",
                decorator.getHasDataProtocols(), is(true));
    }

    @Test
    public void whenProjectIsEmpty_expectHasAnyComponentIsFalse()
    {
        Project project = FakeBuilder.buildFakeProject();
        ProjectDecorator decorator = new ProjectDecorator( project );

        assertThat ("getHasAssertions must return false if there are no assertions in the project",
                decorator.getHasAssertions(), is(false));

        assertThat ("getHasCompanions must return false if there are no companions in the project",
                decorator.getHasCompanions(), is(false));

        assertThat ("getHasDataSets must return false if there are no data sets in the project",
                decorator.getHasDataSets(), is(false));

        assertThat ("getHasDataProtocols must return false if there are no data protocols in the project",
                decorator.getHasDataProtocols(), is(false));

        assertThat ("getHasFilters must return false if there are no filters in the project",
                decorator.getHasFilters(), is(false));

        assertThat ("getHasSteps must return false if there are no Steps in the project",
                decorator.getHasSteps(), is(false));
    }

}
