/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.generator;

import mmm.coffee.apprentice.generator.utils.FileHelper;
import mmm.coffee.apprentice.model.beans.*;
import mmm.coffee.apprentice.model.stereotypes.*;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Test;
import org.junit.Before;

import java.io.File;
import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit test of the GeneratorServiceImpl class.
 */
public class GeneratorServiceProviderTest {

    private static final File TEMP_FOLDER =FileUtils.getTempDirectory();

    private File WORKSPACE_FOLDER = new File ( TEMP_FOLDER, "workspace" );

    private GeneratorService generatorService;

    @Before
    public void setUp()
    {
        generatorService = new GeneratorServiceProvider();
    }
    @After
    public void cleanUp()
    {
        try {
            FileUtils.cleanDirectory( WORKSPACE_FOLDER );
        }
        catch (IOException ioe ) {
            // Print the exception; it will get captured in the logs
            System.out.println ( ioe.getMessage() );
        }
    }


    @Test
    public void generateService_generatesOutput() throws Exception
    {
        generatorService.generate( buildExampleProject() );

        assertThat ("The workspace folder should not be empty", ! FileHelper.isEmptyDirectory( WORKSPACE_FOLDER ) );
    }


    /**
     * Creates a Project instance suitable for testing
     * @return a Project instance suitable for testing
     */
    private Project buildExampleProject()
    {
        WorkspaceTarget wsTarget = WorkspaceTargetBean.builder( WORKSPACE_FOLDER )
                                        .build();

        ApplicationTarget appTarget = ApplicationTargetBean.builder()
                                        .withHomeDirectory("/Applications/Lisa9.5")
                                        .build();

        MessageResource msgResource = MessageResourceBean.builder("mmm.coffee.i18n.Messages").build();


        return ProjectBean.builder( appTarget, wsTarget, msgResource )
                    .withPluginName("MyExample")
                    .withComponent( buildExampleStep() )
                    .build();
    }

    /**
     * Returns a Step instance suitable for testing
     * @return a Step instance suitable for testing
     */
    private Step buildExampleStep()
    {
        return StepBean.builder("mmm.coffee.step.ExampleStep")
                    .withController("mmm.coffee.step.ExampleStepController")
                    .withEditor("mmm.coffee.step.ExampleStepEditor")
                    .withParameter(ParameterBean.builder( "hostName" )
                                        .withDataType( DataType.STRING )
                                        .withLabel("Host Name:")
                                        .build())
                    .withParameter(ParameterBean.builder( "port")
                                        .withDataType( DataType.STRING )
                                        .withLabel("Port:")
                                        .build())
                    .build();
    }

}
