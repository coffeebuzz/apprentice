/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.generator.template;


import org.junit.Before;
import org.junit.Test;
import org.stringtemplate.v4.ST;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;


/**
 * Unit tests of the TemplateCatalog class
 */
public class TemplateCatalogTest {

    private TemplateCatalog catalog;

    @Before
    public void setUp()
    {
        catalog = new TemplateCatalog( TemplateVersion.LATEST );
    }


    /**
     * Query for the step model/controller/editor templates and
     * verify non-null, non-empty strings are returned.
     */
    @Test public void verifyStepTemplatesAreDefined()
    {
        verifyTemplateProvider( catalog.getStepTemplates() );
    }

    /**
     * Query for the filter model/controller/editor templates and
     * verify non-null, non-empty strings are returned.
     */
    @Test public void verifyFilterTemplatesAreDefined()
    {
        verifyTemplateProvider( catalog.getFilterTemplates() );
    }

    /**
     * Query for the assertion model/controller/editor templates and
     * verify non-null, non-empty strings are returned.
     */
    @Test public void verifyAssertionTemplatesAreDefined()
    {
        verifyTemplateProvider( catalog.getAssertionTemplates() );
    }

    /**
     * Query for the companion model/controller/editor templates and
     * verify non-null, non-empty strings are returned.
     */
    @Test public void verifyCompanionTemplatesAreDefined()
    {
        verifyTemplateProvider( catalog.getCompanionTemplates() );
    }

    /**
     * Query for the data set model/controller/editor templates and
     * verify non-null, non-empty strings are returned.
     */
    @Test public void verifyDataSetTemplatesAreDefines()
    {
        verifyTemplateProvider( catalog.getDataSetTemplates() );
    }

    /**
     * Query for the data protocol model/controller/editor templates and
     * verify non-null, non-empty strings are returned.
     */
    @Test public void verifyDataProtocolModelTemplateIsDefined()
    {
        ST template = catalog.getDataProtocolTemplates().getModelTemplate();
        assertThat ("The DataProtocol model template should not be null", template, notNullValue());
    }

    /**
     * Queries the template catalog to confirm the presence of the GradleBuild and GradleSettings templates
     */
    @Test
    public void verifyGradleTemplatesAreDefined()
    {
        ST template = catalog.getGradleBuildTemplate();
        assertThat ("The GradleBuild template was not found", template, notNullValue());

        ST settingsTemplate = catalog.getGradleSettingsTemplate();
        assertThat ("The GradleSettings template was not found", settingsTemplate, notNullValue());
    }

    /**
     * Queries the template catalog to confirm the presence of the LoggingProperties template
     */
    @Test public void verifyLoggingPropertiesTemplateIsDefined()
    {
        ST template = catalog.getLoggingPropertiesTemplate();
        assertThat ( "The LoggingProperties template was not found", template, notNullValue());
    }

    @Test
    public void whenTemplateVersionIsNull_expectLatestCatalogIsCreated()
    {
        TemplateCatalog catalog = new TemplateCatalog ( (TemplateVersion)null );

        assertThat ("Expected a non-null TemplateCatalog", catalog, notNullValue() );
    }


    /**
     * For the given provider, verify the model/editor/controller templates
     * are neither null nor empty strings
     *
     * @param provider the provider to verify
     */
    private void verifyTemplateProvider ( TemplateProvider provider )
    {
        assertThat ("The Model template should not be null", provider.getModelTemplate(), notNullValue());
        assertThat ("The Editor template should not be null", provider.getEditorTemplate(), notNullValue());
        assertThat ("The Controller template should not be null", provider.getControllerTemplate(), notNullValue());
    }
}
