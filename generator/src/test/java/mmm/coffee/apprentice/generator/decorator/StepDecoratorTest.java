/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.generator.decorator;


import mmm.coffee.apprentice.model.beans.ParameterBean;
import mmm.coffee.apprentice.model.beans.StepBean;
import mmm.coffee.apprentice.model.stereotypes.DataType;
import mmm.coffee.apprentice.model.stereotypes.Step;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests of the StepDecorator
 */
public class StepDecoratorTest {

    @Test
    public void givenTwoStringParameters_expectTwoStringArgsInSignature()
    {
        Step step = StepBean.builder( "mmm.coffee.CustomStep" )
                            .withController( "mmm.coffee.CustomStepController" )
                            .withEditor ( "mmm.coffee.CustomStepEditor" )
                            .withParameter(
                                    ParameterBean.builder( "hostName" ).build())
                            .withParameter(
                                    ParameterBean.builder("portAddress").build())
                            .build();

        StepDecorator decorator = new StepDecorator( step );

        String argListOfExecuteMethod = decorator.getExecuteSignature();

        // --------------------------------------------------------------------------
        // The code generator generates an execute method that follows the style:
        // public void execute ( [argList] );
        // --------------------------------------------------------------------------
        assertThat ( "The argument list of the generated execute method is incorrect",
                argListOfExecuteMethod, equalTo ( "String hostName, String portAddress"));

    }

    @Test
    public void givenTwoFileParameters_expectTwoStringArgsInSignature()
    {
        Step step = StepBean.builder( "mmm.coffee.CustomStep" )
                            .withController( "mmm.coffee.CustomStepController" )
                            .withEditor ( "mmm.coffee.CustomStepEditor" )
                            .withParameter(
                                    ParameterBean.builder( "sourceFile" )
                                            .withDataType( DataType.FILE )
                                            .build())
                            .withParameter(
                                    ParameterBean.builder("targetFile")
                                            .withDataType( DataType.FILE )
                                            .build())
                            .build();

        StepDecorator decorator = new StepDecorator( step );

        String argListOfExecuteMethod = decorator.getExecuteSignature();

        // --------------------------------------------------------------------------
        // For File parameters, the fileName is what appears in the arg list
        // --------------------------------------------------------------------------
        assertThat ( "The argument list of the generated execute method is incorrect",
                argListOfExecuteMethod, equalTo ( "String sourceFile, String targetFile"));

    }

    @Test
    public void givenTwoDirectoryParameters_expectTwoStringArgsInSignature()
    {
        Step step = StepBean.builder( "mmm.coffee.CustomStep" )
                            .withController( "mmm.coffee.CustomStepController" )
                            .withEditor ( "mmm.coffee.CustomStepEditor" )
                            .withParameter(
                                    ParameterBean.builder( "sourceDirectory" )
                                            .withDataType( DataType.DIRECTORY )
                                            .build())
                            .withParameter(
                                    ParameterBean.builder("targetDirectory")
                                            .withDataType( DataType.DIRECTORY )
                                            .build())
                            .build();

        StepDecorator decorator = new StepDecorator( step );

        String argListOfExecuteMethod = decorator.getExecuteSignature();

        // --------------------------------------------------------------------------
        // For Directory parameters, the fileName is what appears in the arg list
        // --------------------------------------------------------------------------
        assertThat ( "The argument list of the generated execute method is incorrect",
                argListOfExecuteMethod, equalTo ( "String sourceDirectory, String targetDirectory"));

    }

    @Test
    public void givenTwoBooleanParameters_expectTwoBooleanArgsInSignature()
    {
        Step step = StepBean.builder( "mmm.coffee.CustomStep" )
                            .withController( "mmm.coffee.CustomStepController" )
                            .withEditor ( "mmm.coffee.CustomStepEditor" )
                            .withParameter(
                                    ParameterBean.builder( "isBinary" )
                                            .withDataType( DataType.BOOLEAN )
                                            .build())
                            .withParameter(
                                    ParameterBean.builder("isText")
                                            .withDataType( DataType.BOOLEAN )
                                            .build())
                            .build();

        StepDecorator decorator = new StepDecorator( step );

        String argListOfExecuteMethod = decorator.getExecuteSignature();

        // --------------------------------------------------------------------------
        // For Boolean parameters, boolean is the data type of the arguments
        // --------------------------------------------------------------------------
        assertThat ( "The argument list of the generated execute method is incorrect",
                argListOfExecuteMethod, equalTo ( "boolean isBinary, boolean isText"));

    }


    @Test
    public void givenNoParameters_expectEmptyArgListSignature()
    {
        Step step = StepBean.builder( "mmm.coffee.CustomStep" )
                            .withController( "mmm.coffee.CustomStepController" )
                            .withEditor ( "mmm.coffee.CustomStepEditor" )
                            .build();

        StepDecorator decorator = new StepDecorator( step );

        String argListOfExecuteMethod = decorator.getExecuteSignature();

        // --------------------------------------------------------------------------
        // For no parameters, expect an empty argument list
        // --------------------------------------------------------------------------
        assertThat ( "The argument list of the generated execute method is incorrect",
                argListOfExecuteMethod, equalTo ( "" ));

    }


    @Test
    public void verifyGetClassNameReflectsSetClassName()
    {
        String klassName = "mmm.coffee.CustomStep";
        String testKlassName = "mmm.coffee.CustomStepTest";

        Step step = StepBean.builder( klassName )
                .withController( "mmm.coffee.CustomStepController" )
                .withEditor ( "mmm.coffee.CustomStepEditor" )
                .build();

        StepDecorator decorator = new StepDecorator( step );

        decorator.setTestClassName( testKlassName );

        assertThat ("Expected getClassName to be reflexive of setClassName", decorator.getClassName(), is( klassName ));
        assertThat ("Expected getTestClassName to be reflexive of setTestClassName", decorator.getTestClassName(), is ( testKlassName ));

    }
}
