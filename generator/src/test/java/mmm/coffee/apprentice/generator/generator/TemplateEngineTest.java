/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.generator.generator;


import mmm.coffee.apprentice.generator.template.TemplateCatalog;
import mmm.coffee.apprentice.generator.template.TemplateVersion;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.stringtemplate.v4.ST;

import java.io.File;
import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit test of the TemplateEngine
 */
public class TemplateEngineTest {

    private static File TMP_FOLDER = new File ("/tmp" ); // FileUtils.getTempDirectory();
    private File WORKSPACE_FOLDER = new File ( TMP_FOLDER, "workspace" );

    @Before
    public void setUp() throws IOException
    {
        if ( !WORKSPACE_FOLDER.exists() )
            FileUtils.forceMkdir( WORKSPACE_FOLDER );
    }

    @After
    public void tearDown() throws IOException
    {
        if ( WORKSPACE_FOLDER.exists() )
            FileUtils.cleanDirectory( WORKSPACE_FOLDER );
    }


    /**
     * Confirm invalid arguments are detected and handled
     * @throws Exception if the invalid argument is detected
     */
    @Test ( expected = NullPointerException.class)
    public void whenGenerateCodeWithNullContext_thenExceptionIsThrown() throws Exception
    {
        TemplateEngine t = new TemplateEngine();
        t.generateCode( null, new File ("") );
    }

    /**
     * Confirm invalid arguments are detected and handled
     * @throws Exception if the invalid argument is detected
     */
    @Test ( expected = NullPointerException.class)
    public void whenGenerateCodeWithNullTemplate_thenExceptionIsThrown() throws Exception
    {
        TemplateEngine t = new TemplateEngine();
        t.generateCode( null, new File ("") );
    }

    /**
     * Confirm invalid arguments are detected and handled
     * @throws Exception if the invalid argument is detected
     */
    @Test ( expected = NullPointerException.class)
    public void whenGenerateCodeWithNullOutputFile_thenExceptionIsThrown() throws Exception
    {
        TemplateEngine t = new TemplateEngine();
        t.generateCode( null, null );
    }


    /**
     * Scenario: when valid arguments are passed to generateCode(), then an output
     * file is created with content.
     *
     * @throws Exception any errors
     */
    @Test
    public void whenHappyCase_thenOutputFileIsRendered() throws Exception
    {
        TemplateCatalog catalog = new TemplateCatalog ( TemplateVersion.LATEST );
        ST template = catalog.getFilterTemplates().getModelTemplate();
        File outputFile = new File(WORKSPACE_FOLDER, "MyFilter.java");

        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.generateCode(template, outputFile);

        // Confirm the output file was created
        assertThat("Output file was not generated", outputFile.exists());

        // Confirm the output file has content
        String s = FileUtils.readFileToString(outputFile);
        assertThat("The generated file should not be empty", s.length() > 0);
    }

}
