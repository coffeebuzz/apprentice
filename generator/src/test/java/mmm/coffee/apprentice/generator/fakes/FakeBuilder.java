/*
 * Copyright (c) 2016 CA.  All rights reserved.  
 * 
 * This software and all information contained therein is confidential and 
 * proprietary and shall not be duplicated, used, disclosed or disseminated 
 * in any way except as authorized by the applicable license agreement, 
 * without the express written permission of CA. All authorized reproductions 
 * must be marked with this language.  
 * 
 * EXCEPT AS SET FORTH IN THE APPLICABLE LICENSE AGREEMENT, TO THE EXTENT 
 * PERMITTED BY APPLICABLE LAW OR AS AGREED BY CA IN ITS APPLICABLE LICENSE 
 * AGREEMENT, CA PROVIDES THIS SOFTWARE "AS IS" WITHOUT WARRANTY OF ANY KIND, 
 * INCLUDING WITHOUT LIMITATION, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE, OR NONINFRINGEMENT. IN NO EVENT WILL CA 
 * BE LIABLE TO THE END USER OR ANY THIRD PARTY FOR ANY LOSS OR DAMAGE, DIRECT 
 * OR INDIRECT, FROM THE USE OF THIS SOFTWARE, INCLUDING WITHOUT LIMITATION, 
 * LOST PROFITS, LOST INVESTMENT, BUSINESS INTERRUPTION, GOODWILL, OR LOST 
 * DATA, EVEN IF CA IS EXPRESSLY ADVISED IN ADVANCE OF THE POSSIBILITY OF SUCH 
 * LOSS OR DAMAGE.
 */
package mmm.coffee.apprentice.generator.fakes;

import mmm.coffee.apprentice.model.beans.*;
import mmm.coffee.apprentice.model.stereotypes.*;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * TODO: Fill me in
 */
public class FakeBuilder {

    private static final String WORKSPACE_HOME = "/Users/joncaulfield/workspace/Scratch";

    /**
     * Builds a Project entity suitable for testing
     * @return the Project entity
     */
    public static Project buildFakeProject()
    {
        ApplicationTarget application = ApplicationTargetBean.builder()
                .withHomeDirectory("/Applications/Lisa9.5")
                .withVersion("10.0")
                .build();

        WorkspaceTarget workspace = WorkspaceTargetBean.builder( WORKSPACE_HOME ).build();

        MessageResource messages = MessageResourceBean.builder("mmm.coffee.messages.Messages").build();

        Project project = ProjectBean.builder( application, workspace, messages )
                .withPluginName("sample")
                .build();

        return project;
    }


    /**
     * Builds an Assertion entity suitable for testing
     *
     * @return an Assertion entity
     */
    public static Assertion buildFakeAssertion()
    {
        Parameter endPoint = ParameterBean.builder("endpoint")
                .withDataType(DataType.STRING)
                .withLabel("End Point URL:")
                .withTooltip("Enter the URL of the endpoint")
                .build();

        Parameter useSSL = ParameterBean.builder("useSSL")
                .withDataType(DataType.BOOLEAN)
                .withLabel("Use SSL?")
                .build();

        Assertion assertion = AssertionBean.builder("mmm.coffee.example.SampleAssertion")
                .withGlobalScope(false)
                .withLocalScope(true)
                .withParameter( endPoint )
                .withParameter ( useSSL )
                .build();

        return assertion;
    }

    public static Assertion buildAnotherFakeAssertion()
    {
        Parameter expression = ParameterBean.builder("expression")
                .withDataType(DataType.STRING)
                .withLabel("Expression:")
                .build();

        Assertion assertion = AssertionBean.builder("mmm.coffee.example.AnotherAssertion")
                .withGlobalScope(false)
                .withLocalScope(true)
                .withParameter( expression )
                .build();

        return assertion;
    }

    /**
     * Builds a DataSet entity suitable for testing
     * @return a sample DataSet
     */
    public static DataSet buildFakeDataSet()
    {
        Parameter driver = ParameterBean.builder("jdbcDriver")
                .withDataType(DataType.STRING)
                .withLabel("Database Driver:")
                .build();

        Parameter url = ParameterBean.builder("jdbcURL")
                .withDataType(DataType.STRING)
                .withLabel("Database URL:")
                .build();

        Parameter userName = ParameterBean.builder("jdbcUserName")
                .withDataType(DataType.STRING)
                .withLabel("Username:")
                .build();

        Parameter password = ParameterBean.builder("jdbcPassword")
                .withDataType(DataType.STRING)
                .withLabel("Password:")
                .build();


        DataSet dataSet = DataSetBean.builder("mmm.coffee.example.SampleDataSet")
                .withParameter( driver )
                .withParameter( url )
                .withParameter ( userName )
                .withParameter ( password )
                .build();

        return dataSet;
    }

    /**
     * Builds a DataSet entity suitable for testing
     * @return a sample DataSet
     */
    public static DataSet buildAnotherFakeDataSet()
    {
        Parameter p1 = ParameterBean.builder("fileName")
                .withDataType(DataType.FILE)
                .withLabel("File:")
                .build();

        Parameter p2 = ParameterBean.builder("dataDirectory")
                .withDataType(DataType.DIRECTORY)
                .withLabel("Directory:")
                .build();


        DataSet dataSet = DataSetBean.builder("mmm.coffee.example.FileDataSet")
                .withParameter( p1 )
                .withParameter( p2 )
                .build();

        return dataSet;
    }

    public static Companion buildFakeCompanion()
    {
        Parameter fieldOne = ParameterBean.builder("fieldOne")
                .withDataType(DataType.STRING)
                .withLabel("Field One:")
                .build();

        Parameter fieldTwo = ParameterBean.builder("fieldTwo")
                .withDataType(DataType.STRING)
                .withLabel("Field Two:")
                .build();

        Companion companion = CompanionBean.builder("mmm.coffee.example.SampleCompanion")
                .withParameter( fieldOne )
                .withParameter( fieldTwo )
                .build();

        return companion;
    }

    public static Companion buildAnotherFakeCompanion()
    {
        Parameter fieldOne = ParameterBean.builder("file")
                .withDataType(DataType.FILE)
                .withLabel("File:")
                .build();

        Parameter fieldTwo = ParameterBean.builder("directory")
                .withDataType(DataType.DIRECTORY)
                .withLabel("Directory:")
                .build();

        Parameter fieldThree = ParameterBean.builder("checkbox")
                .withDataType(DataType.BOOLEAN)
                .withLabel("Checkbox?")
                .build();

        Companion companion = CompanionBean.builder("mmm.coffee.example.FileCompanion")
                .withParameter( fieldOne )
                .withParameter( fieldTwo )
                .withParameter( fieldThree )
                .build();

        return companion;

    }

    public static DataProtocol buildFakeDataProtocol()
    {
        DataProtocol dph = DataProtocolBean.builder("mmm.coffee.example.ResponseOnlyProtocolHandler")
                .withRequestSide(false)
                .withResponseSide(true)
                .withDisplayName("Tweak Response")
                .withDescription("tweaks the response that arrives")
                .build();

        return dph;
    }

    public static DataProtocol buildRequestSideDataProtocol()
    {
        DataProtocol dph = DataProtocolBean.builder("mmm.coffee.example.RequestOnlyProtocolHandler")
                .withRequestSide(true)
                .withResponseSide(false)
                .withDisplayName("Tweak Request")
                .withDescription("tweaks the request that arrives")
                .build();

        return dph;
    }

    public static DataProtocol buildRequestResponseDataProtocol()
    {
        DataProtocol dph = DataProtocolBean.builder("mmm.coffee.example.RequestResponseProtocolHandler")
                .withRequestSide(true)
                .withResponseSide(true)
                .withDisplayName("Tweak Request or Response")
                .withDescription("tweaks either the request or response")
                .build();

        return dph;
    }


    public static Filter buildFakeFilter()
    {
        Parameter fieldOne = ParameterBean.builder("fieldOne")
                .withDataType(DataType.STRING)
                .withLabel("Field One:")
                .build();

        Parameter fieldTwo = ParameterBean.builder("fieldTwo")
                .withDataType(DataType.STRING)
                .withLabel("Field Two:")
                .build();

        Filter filter = FilterBean.builder("mmm.coffee.example.SampleFilter")
                .withGlobalScope(false)
                .withLocalScope(true)
                .withParameter( fieldOne )
                .withParameter ( fieldTwo )
                .build();

        return filter;
    }

    public static Filter buildAnotherFakeFilter()
    {
        Parameter fieldOne = ParameterBean.builder("fieldOne")
                .withDataType(DataType.STRING)
                .withLabel("Field One:")
                .build();

        Parameter fieldTwo = ParameterBean.builder("fieldTwo")
                .withDataType(DataType.STRING)
                .withLabel("Field Two:")
                .build();

        Filter filter = FilterBean.builder("mmm.coffee.example.AlternateFilter")
                .withGlobalScope(false)
                .withLocalScope(true)
                .withParameter( fieldOne )
                .withParameter ( fieldTwo )
                .build();

        return filter;
    }


    public static Step buildFakeStep()
    {
        Parameter fieldOne = ParameterBean.builder("fieldOne")
                .withDataType(DataType.STRING)
                .withLabel("Field One:")
                .build();

        Parameter fieldTwo = ParameterBean.builder("fileName")
                .withDataType(DataType.FILE)
                .withLabel("File Name:")
                .build();

        Parameter fieldThree = ParameterBean.builder("directoryName")
                .withDataType(DataType.DIRECTORY)
                .withLabel("Directory Name:")
                .build();

        Parameter fieldFour = ParameterBean.builder("useSSL")
                .withDataType(DataType.BOOLEAN)
                .withLabel("Use SSL?")
                .build();

        Step step = StepBean.builder("mmm.coffee.example.SampleStep")
                .withController("mmm.coffee.example.SampleStepController")
                .withEditor("mmm.coffee.example.SampleStepEditor")
                .withParameter( fieldOne )
                .withParameter( fieldTwo )
                .withParameter ( fieldThree )
                .withParameter ( fieldFour )
                .build();

        return step;

    }

    public static Step buildAnotherFakeStep()
    {
        Parameter fieldOne = ParameterBean.builder("fieldOne")
                .withDataType(DataType.STRING)
                .withLabel("Field One:")
                .build();

        Parameter fieldTwo = ParameterBean.builder("fileName")
                .withDataType(DataType.FILE)
                .withLabel("File Name:")
                .build();


        Step step = StepBean.builder("mmm.coffee.example.AlternateStep")
                .withController("mmm.coffee.example.AlternateStepController")
                .withEditor("mmm.coffee.example.AlternateStepEditor")
                .withParameter( fieldOne )
                .withParameter( fieldTwo )
                .build();

        return step;

    }
}
