/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.generator.generator;


import mmm.coffee.apprentice.generator.utils.FileHelper;
import mmm.coffee.apprentice.model.beans.*;
import mmm.coffee.apprentice.model.stereotypes.*;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests of the ProjectGenerator class.
 */
public class ProjectGeneratorTest {

    private File workspace = new File ( FileUtils.getTempDirectory(), FileHelper.getUniqueFileName("workspace"));

    private static final String DEVTEST_HOME = "/Applications/DevTest9.5";
    private static final String DEVTEST_VERSION = "10.0";

    private ProjectGenerator generator;
    private WorkspaceTarget workspaceTarget;
    private ApplicationTarget applicationTarget;
    private MessageResource messageResource;

    @Before
    public void setUp()
    {
        generator = new ProjectGenerator();

        // -----------------------------------------------------------------------------------
        // The workspaceTarget tells the generator where to create the generated code.
        // The idea is, this generated folder will look like an IDE workspace folder,
        // with a build.xml/build.properties in the root directory, a src/main/java directory
        // containing the generated Java code, and a src/main/resources containing the
        // generated lisaextensions file.
        // -----------------------------------------------------------------------------------
        workspaceTarget = new WorkspaceTargetBean ( workspace.getAbsolutePath() );

        // -----------------------------------------------------------------------------------
        // The applicationTarget information is referenced in the generated
        // build.xml and build.properties file.  To generate working, out-of-the-box
        // plugin code, and the plugin's compilation depends on DevTest jars,
        // the applicationTarget tells us where to find those DevTest jars.
        // -----------------------------------------------------------------------------------
        applicationTarget = ApplicationTargetBean.builder()
                                .withHomeDirectory( DEVTEST_HOME )
                                .withVersion( DEVTEST_VERSION )
                                .build();

        messageResource = MessageResourceBean.builder("mmm.coffee.i18n.Messages").build();

    }

    @After
    public void cleanUp() throws IOException
    {
        if ( workspace.exists() )
            FileUtils.cleanDirectory( workspace );
    }


    @Test
    public void whenVisitNullProject_ProjectIsSilentlyIgnored()
    {
        generator.visit( (Project) null );
    }

    @Test
    public void whenEmptyProjectisVisited_thenWorkspaceDirectoryIsCreated()
    {
        Project project = new ProjectBean ( applicationTarget, workspaceTarget, messageResource );

        generator.generate ( project );
        assertThat ( "The workspace target was not created", workspace.exists() );
    }

    @Test
    public void whenProjectHasSteps_thenStepCodeIsGenerated() throws Exception
    {
        Project project = buildDefaultProject();

        Step step = buildSampleStep();
        project.getComponents().add ( step );

        generator.generate ( project );

        assertThat ( "The workspace target was not created", workspace.exists() );


        String stepModel = step.getSimpleName();
        String stepController = step.getController().getSimpleName();
        String stepEditor = step.getEditor().getSimpleName();

        assertThat ( "The step model class was not generated",      javaFileWasGenerated ( stepModel) );
        assertThat ( "The step controller class was not generated", javaFileWasGenerated( stepController ));
        assertThat ( "The step editor class was not generated",     javaFileWasGenerated( stepEditor ));
    }

    @Test
    public void whenProjectHasAssertions_thenAssertionCodeIsGenerated() throws Exception
    {
        Project project = buildDefaultProject();

        Assertion assertion = buildSampleAssertion();
        project.getComponents().add ( assertion );

        generator.generate ( project );

        assertThat ( "The workspace directory was not created", workspace.exists() );

        String modelClass = assertion.getSimpleName();

        assertThat ( "The assertion model class was not generated", javaFileWasGenerated ( modelClass ) );
    }

    @Test
    public void whenProjectHasCompanions_thenCompanionCodeIsGenerated() throws Exception
    {
        Project project = buildDefaultProject();

        Companion companion = buildSampleCompanion();
        project.getComponents().add( companion );

        generator.generate( project );

        assertThat ( "The workspace directory was not created", workspace.exists() );

        // Our example companion uses the default editor and controller,
        // so only the model-layer class is generated
        assertThat ("The companion model class was not generated", javaFileWasGenerated( companion.getSimpleName() ));
    }

    @Test
    public void whenProjectHasDataSets_thenDataSetCodeIsGenerated() throws Exception
    {
        Project project = buildDefaultProject();

        DataSet dataSet = buildSampleDataSet();
        project.getComponents().add ( dataSet );

        generator.generate( project );

        assertThat ("The workspace directory was not created", workspace.exists() );

        // Our example data set uses the default editor and controller,
        // so only the model-layer class is generated
        assertThat ("The data set model class was not generated", javaFileWasGenerated( dataSet.getSimpleName() ));
    }

    @Test
    public void whenProjectHasDataProtocols_thenDataProtocolCodeIsGenerated() throws Exception
    {
        Project project = buildDefaultProject();

        DataProtocol dph = buildSampleDataProtocolHandler();
        project.getComponents().add ( dph );

        generator.generate( project );

        assertThat ( "The workspace directory was not created", workspace.exists() );

        assertThat ( "The data protocol model-layer class was not generated", javaFileWasGenerated( dph.getSimpleName() ));
    }

    @Test
    public void whenProjectHasFilters_thenFilterCodeIsGenerated() throws Exception
    {
        Project project = buildDefaultProject();

        Filter filter = buildSampleFilter();
        project.getComponents().add ( filter );

        generator.generate ( project );

        assertThat ("The workspace directory was not created", workspace.exists());

        assertThat ("The filter model-layer class was not generated", javaFileWasGenerated( filter.getSimpleName() ));
    }


    /**
     * Confirms whether the given Java class was generated.  The workspace directory is
     * scanned until an occurrence of the class is found, or all files have been checked.
     *
     * @param simpleName the simple class name, such as 'StepEditor' (no ".java" suffix is needed)
     * @return true if the Java class was found; false otherwise
     * @throws IOException if an I/O exception occurs when reading the file system
     */
    private boolean javaFileWasGenerated ( final String simpleName ) throws IOException
    {
        String javaFileName = simpleName + ".java" ;

        File f = FileHelper.findFileWithinDirectory( workspace, javaFileName );

        return f != null;

    }

    /**
     * Creates a simple Project for use by these unit tests.
     *
     * @return the Project instance
     */
    private Project buildDefaultProject()
    {
        return ProjectBean.builder( applicationTarget, workspaceTarget, messageResource )
                        .withPluginName("StepPlugin")
                        .build();
    }


    /**
     * Creates a simple Step for use with these unit tests
     *
     *
     * @return the Step instance
     */
    private Step buildSampleStep()
    {
        return StepBean.builder("mmm.coffee.step.CustomStep")
                        .withEditor ("mmm.coffee.step.CustomStepEditor")
                        .withController ( "mmm.coffee.step.CustomStepController" )
                        .withParameter(
                            ParameterBean.builder("fieldOne")
                                    .withLabel ( "Field One:" )
                                    .withTooltip( "Hint for fieldOne" )
                                    .build())
                        .withParameter (
                            ParameterBean.builder("fieldTwo")
                                    .withLabel( "Field Two:" )
                                    .withTooltip( "Hint for fieldTwo" )
                                    .build())
                        .build();
    }

    /**
     * Creates a simple Assertion entity for use with these tests
     * @return an Assertion instance
     */
    private Assertion buildSampleAssertion()
    {
        // Note: DevTest provides default editors and controllers for assertions,
        // so we only need the model-layer code to implement the business logic.
        return AssertionBean.builder("mmm.coffee.assertion.CustomAssertion")
                    .withGlobalScope(true)
                    .withLocalScope(true)
                    .withParameter(
                            ParameterBean.builder("regEx")
                                    .withDataType( DataType.STRING )
                                    .withLabel ( "Regular Expression:" )
                                    .build())
                    .withParameter(
                            ParameterBean.builder("expectedValue")
                                    .withLabel ( "Expected Value:" )
                                    .withDataType( DataType.STRING )
                                    .build())
                    .withParameter(
                            ParameterBean.builder("actualValue")
                                    .withLabel ( "Actual Value:" )
                                    .withDataType( DataType.STRING )
                                    .build())
                    .build();
    }

    /**
     * Creates a simple Filter entity for testing purposes.
     * @return a Filter instance
     */
    private Filter buildSampleFilter()
    {
        return FilterBean.builder("mmm.coffee.filter.SampleFilter")
                        .withParameter(
                                ParameterBean.builder("fieldOne")
                                        .withDataType( DataType.STRING )
                                        .withLabel ( "Field One:")
                                        .build())
                        .withParameter(
                                ParameterBean.builder("fieldTwo")
                                        .withDataType( DataType.STRING )
                                        .withLabel ( "Field Two:" )
                                        .build())
                        .build();
    }

    /**
     * Creates a Companion instance for testing purposes
     * @return a Companion instance
     */
    private Companion buildSampleCompanion()
    {
        return CompanionBean.builder("mmm.coffee.companion.SampleCompanion")
                        .withParameter(
                                ParameterBean.builder("fieldOne")
                                        .withDataType( DataType.STRING )
                                        .withLabel ( "Field One:" )
                                        .build())
                        .withParameter(
                                ParameterBean.builder("fieldTwo")
                                        .withDataType( DataType.STRING )
                                        .withLabel ( "Field Two:" )
                                        .build())
                        .build();
    }

    /**
     * Creates a DataSet instance suitable for testing purposes
     * @return a DataSet instance
     */
    private DataSet buildSampleDataSet()
    {
        return DataSetBean.builder("mmm.coffee.dataset.SampleDataSet")
                        .withParameter(
                                ParameterBean.builder( "fileName" )
                                        .withDataType( DataType.FILE )
                                        .build())
                        .withParameter(
                                ParameterBean.builder( "fieldDelimiter" )
                                        .withDataType( DataType.STRING )
                                        .build())
                        .build();
    }

    /**
     * A DataProtocol instance suitable for testing purposes
     * @return a DataProtocol instance
     */
    private DataProtocol buildSampleDataProtocolHandler()
    {
        return DataProtocolBean.builder("mmm.coffee.protocol.XYZProtocol")
                        .withDisplayName("XYZ")
                        .withDescription("XYZ data protocol")
                        .withRequestSide(true)
                        .build();
    }
}
