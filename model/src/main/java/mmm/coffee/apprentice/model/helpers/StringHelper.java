/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.model.helpers;


import javax.lang.model.SourceVersion;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;


/**
 * Miscellaneous string manipulation functions.
 */
public class StringHelper {

    public static final String EMPTY_STRING = "";

    private static final Set<String> javaKeywords = new HashSet<>(Arrays.asList(
            "abstract",     "assert",        "boolean",      "break",           "byte",
            "case",         "catch",         "char",         "class",           "const",
            "continue",     "default",       "do",           "double",          "else",
            "enum",         "extends",       "false",        "final",           "finally",
            "float",        "for",           "goto",         "if",              "implements",
            "import",       "instanceof",    "int",          "interface",       "long",
            "native",       "new",           "null",         "package",         "private",
            "protected",    "public",        "return",       "short",           "static",
            "strictfp",     "super",         "switch",       "synchronized",    "this",
            "throw",        "throws",        "transient",    "true",            "try",
            "void",         "volatile",      "while"
    ));

    private static final Pattern VALID_JAVA_IDENTIFIER = Pattern
            .compile("(\\p{javaJavaIdentifierStart}\\p{javaJavaIdentifierPart}*\\.)*\\p{javaJavaIdentifierStart}\\p{javaJavaIdentifierPart}*");



    /**
     * Tests whether {@code value} is null or zero-length.  Returns true if it is null or empty.
     *
     * @param value some value to check for null/zero-length.
     * @return true if {@code value} is null or, when trimmed, has zero length
     */
    public static boolean isEmpty ( String value ) {
        if ( value == null || value.trim().length() == 0)
            return true;
        return false;
    }

    /**
     * Returns the capitalized version of {@code word}.  If {@code word} is null, null is returned.
     * For example, <ul>
     *     <li>capitalize("a") returns "A"</li>
     *     <li>capitalize("") returns ""</li>
     *     <li>capitalize("weight") returns "Weight"</li>
     *     <li>capitalize(null) returns null</li>
     *     <li>capitalize("   ") returns ""</li>
     * </ul>
     *
     * @param word the word to capitalize
     * @return the capitalized version of {@code word}.
     */
     public static String capitalizeFirstCharacter ( String word ) {

        if ( word == null )
            return null;

        if ( word.trim().length() == 0)
            return EMPTY_STRING;

        String s = word.trim();

        if ( s.length() == 1 )
            return s.toUpperCase();


        return Character.toUpperCase( word.charAt(0) ) + s.substring(1);
    }


    /**
     * When {@code value} is null, the empty string is returned.  Otherwise, {@code value} is returned.
     *
     * @param value some possibly null string
     * @return a non-null value, which is either {@code value} or the empty string
     */
    public static String safeString ( String value ) {
        if (isEmpty(value))
            return EMPTY_STRING;
        return value;
    }


    /**
     * Determines whether the given {@code identifier} is a valid Java identifier.
     * Reserved words are not considered valid identifiers.  This method is
     * used to verify fully-qualified classnames, such as "mmm.coffee.assertion.MyAssertion"
     *
     * @param identifier the value to check
     * @return {@code true} if {@code identifier} is valid to use as a Java identifier
     */
    public static boolean isValidJavaIdentifier(String identifier) {

        if ( isEmpty ( identifier ))
            return false;

        // If 'identifier' matches the syntax and is not a reserved word, return true;
        return VALID_JAVA_IDENTIFIER.matcher(identifier).matches()
                && !javaKeywords.contains( identifier );
    }

    public static boolean isValidVariableName ( String variableName ) {

        if ( isEmpty( variableName ))
            return false;

        return SourceVersion.isName( variableName );
    }

}
