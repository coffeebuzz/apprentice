/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.model.beans;

import mmm.coffee.apprentice.model.helpers.EqualityHelper;
import mmm.coffee.apprentice.model.stereotypes.ApplicationTarget;
import mmm.coffee.apprentice.model.visitor.Visitor;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A {@code ApplicationTargetBean} defines a DevTest version that is the target for a plugin.
 * All fields of the {@code ApplicationTargetBean} are optional.
 *
 * The {@code homeDirectory} specifies the directory where DevTest is installed.  When the
 * {@code homeDirectory} is specified, the generated Ant build.properties contains a property entry
 * with the same value.  When the plugin is compiled, the path to the DEVTEST libraries
 * must be known, and the {@code homeDirectory} provides root of that path.  The Ant build.properties
 * file will still be generated if the {@code homeDirectory} is not specified, but the build.properties
 * will have to be edited before the plugin will compile.
 *
 * The {@code version} property enables the code generator to pick the best templates for generation.
 * Between major releases, DevTest sometimes has some change in an API or component loading and initialization.
 * By knowing the targeted DevTest version, code that incorporates these changes can be generated.
 * When the {@code version} is undefined, the latest version supported by the code generator is used.
 *
 * A Builder class within the ApplicationTargetBean simplifies the construction. Here's an example
 * of how to use it.
 * <code>
 *     ApplicationTarget target = ApplicationTargetBean.builder()
 *                                                     .withHomeDirectory("C:/Program Files/CA/DevTest")
 *                                                     .withVersion("9")
 *                                                     .build();
 * </code>
 */
public class ApplicationTargetBean implements ApplicationTarget, Serializable {

    private static final long serialVersionUID = 4620894853941508610L;

    // The home directory of DevTest
    private String homeDirectory;

    // The DevTest version number
    private String version;

    private String uuid;


    /**
     * Default constructor
     */
    public ApplicationTargetBean() {
        uuid = UUID.randomUUID().toString();
        homeDirectory = "";
        version = "";
    }

    /**
     * Constructor
     * @param homeDirectory home directory of DevTest
     * @param version the version of DevTest in which the plugin will be deployed
     */
    public ApplicationTargetBean(final String homeDirectory, final String version) {
        this();

        Objects.requireNonNull( homeDirectory );
        this.homeDirectory = homeDirectory;
        this.version = version;
    }

    /**
     * Constructor
     * @param homeDirectory home directory of DevTest
     */
    public ApplicationTargetBean ( final String homeDirectory ) {
        this();

        Objects.requireNonNull( homeDirectory );

        this.homeDirectory = homeDirectory;
    }


    /**
     * Construct an instance by copying the fields of {@code clone}.
     *
     * @param clone the prototype to clone
     */
    public ApplicationTargetBean(final ApplicationTarget clone)
    {
        this.homeDirectory = clone.getHomeDirectory();
        this.version = clone.getVersion();
        this.uuid = clone.getUUID();
    }

    /**
     * Updates the mutable fields of {@code this} with copies of the matching
     * fields from {@code clone}.  If {@code clone} is null, this method quietly returns.
     *
     * @param that {@code this} becomes a copy of {@code that}
     */
    public void copyOf ( final ApplicationTarget that )
    {
        if ( that != null )
        {
            setHomeDirectory( that.getHomeDirectory() );
            setVersion( that.getVersion() );
        }
    }


    /**
     * Returns a deep copy clone of us
     * @return a deep copy of us
     */
    public ApplicationTargetBean deepCopy() {
        return new ApplicationTargetBean( this );
    }


    final public String getUUID() {
        return uuid;
    }
    final public void setUUID(String uuid) {
        this.uuid = uuid;
    }

    final public String getHomeDirectory() {
        return homeDirectory;
    }
    final public void setHomeDirectory(String homeDirectory) {
        this.homeDirectory = homeDirectory;
    }
    final public String getVersion() {
        return version;
    }
    final public void setVersion ( String version ) { this.version = version; }


    /**
     * Accepts this visitor (enables the Visitor pattern).
     *
     * @param visitor the visitor; must not be null
     */
    public void accept ( Visitor visitor ) {
        visitor.visit ( this );
    }

    /**
     * Two TargetBeans are equal if both their homeDirectories are equal and their versions are equal.
     *
     * @param obj the object to test for equality
     * @return {@code true} of {@code obj} and {@code this} are equal.
     */
    @Override
    public boolean equals ( Object obj ) {
        if ( obj == null ) return false;
        if ( this == obj ) return true;
        if ( obj instanceof ApplicationTargetBean) {
            ApplicationTargetBean that = (ApplicationTargetBean)obj;
            return that.canEqual ( this )
                    && EqualityHelper.areEqual(this.getHomeDirectory(), that.getHomeDirectory())
                    && EqualityHelper.areEqual(this.getVersion(), that.getVersion());
        }
        return false;
    }

    /**
     * @see "http://www.artima.com/lejava/articles/equality.html"
     * @param other the object being compared
     * @return {@code true} if {@code other} is comparable to {@code this}
     */
    public boolean canEqual ( final Object other ) {
        return ( other instanceof ApplicationTargetBean);
    }

    @Override
    public int hashCode() {
        final int prime = 41;
        int result = 1;

        if ( homeDirectory != null )
            result = prime * result + homeDirectory.hashCode();
        if ( version != null )
            result = prime * result + version.hashCode();

        return result;
    }



    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append ("ApplicationTarget {");
        sb.append (" 'homeDirectory' : '").append(homeDirectory).append("',");
        sb.append (" 'version' : '").append(version).append("'");
        sb.append (" }");
        return sb.toString();
    }

    public static abstract class Builder<T extends Builder<T>> {
        private String homeDirectory;
        private String version;

        protected abstract T self();

        /**
         * The DevTest home directory. This value gets included in the
         * generated build.properties file since, to compile the plugin,
         * the dependent DevTest jars need to be in the compile-time classpath.
         * The code generation will succeed without this value, but the
         * generated build.properties of your plugin will need to be updated
         * before your plugin can be compiled.
         *
         * @param path the fully-qualified path of the DevTest home directory,
         *             such as {@code "C:/Program Files/CA/DevTest"} or
         *             {@code "/Applications/DevTest"}.
         * @return the Builder
         */
        public T withHomeDirectory(final String path) {
            this.homeDirectory = path;
            return self();
        }

        /**
         * Specifies the target version of DevTest to which the plugin
         * will be deployed.  When version is not specified, the latest
         * supported version of DevTest is used.  Its generally sufficient
         * to use whole numbers, such as "8" or "9".
         *
         * @param version the version of DevTest to which the plugin will be deployed
         * @return the Builder
         */
        public T withVersion(final String version) {
            this.version = version;
            return self();
        }

        public ApplicationTargetBean build() {
            return new ApplicationTargetBean(this);
        }
    }

    /**
     * Our concrete implementation of the Builder.
     */
    private static class TargetBeanBuilder extends Builder<TargetBeanBuilder> {
        @Override
        protected TargetBeanBuilder self() {
            return this;
        }
    }

    public static Builder<?> builder() {
        return new TargetBeanBuilder();
    }

    /**
     * Creates a TargetBean instance using the generator to load the properties
     *
     * @param builder an instance of our Builer2 class
     */
    protected ApplicationTargetBean(Builder<?> builder)
    {
        this.setHomeDirectory( builder.homeDirectory );
        this.setVersion( builder.version );
    }

}
