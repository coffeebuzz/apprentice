/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.model.stereotypes;

/**
 * Filters and Assertions support the notion of being having global scope or local scope.
 * Entities with local scope can be added to individual steps.  Entities with global
 * scope are applied to all steps.  An entity is allowed to have both global scope and
 * local scope.  Having both scopes allows the user to decide if the entity is assigned
 * to steps as needed, or categorically applied to all steps in a test.
 */
public interface ScopeSupport {

    /**
     * Is local scope enabled for this entity.  When {@code true}, the entity can be
     * added to individual steps.
     * @return whether local scope is enabled
     */
    boolean isLocalScope();


    /**
     * Is global scope enabled for this entity.  When {@code true}, the entity
     * is added as a global filter or assertion and is invoked by all steps.
     * @return whether global scope is enabled
     */
    boolean isGlobalScope();

    /**
     * Enable or disable the local scope of this entity
     * @param value true = enable local scope; false = disable local scope
     */
    void setLocalScope(boolean value);
    boolean getLocalScope();

    /**
     * Enable or disable the global scope of this entity
     * @param value true = enable global scope; false = disable global scope
     */
    void setGlobalScope(boolean value);
    boolean getGlobalScope();
}
