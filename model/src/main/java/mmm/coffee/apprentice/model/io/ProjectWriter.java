/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.model.io;

import mmm.coffee.apprentice.model.beans.*;
import mmm.coffee.apprentice.model.helpers.PrintHelper;
import mmm.coffee.apprentice.model.stereotypes.Component;
import mmm.coffee.apprentice.model.stereotypes.Message;
import mmm.coffee.apprentice.model.stereotypes.Parameter;
import mmm.coffee.apprentice.model.stereotypes.ScopeSupport;

import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.logging.Logger;


/**
 * The {@code ProjectWriter} writes a project model to a file.
 */
public class ProjectWriter extends BeanWriter {

    private static Logger logger = Logger.getLogger( ProjectWriter.class.getName() );

    private static final String DEFAULT_NAMESPACE = "http://mmm.servicevirtualization.buzz/ns/apprentice/1";
    private static final String XMLNS = "xmlns";

    // Where the output is written
    private Writer writer;

    private static int INDENT_LEVEL_01 = 4;
    private static int INDENT_LEVEL_02 = 8;
    private static int INDENT_LEVEL_03 = 12;


    /**
     * Constructs a {@code ProjectWriter} that will writes using {@code writer}.
     * If {@code writer} is null, a {@code NullPointerException} is thrown now
     * rather than later.
     *
     * @param writer the output destination; should not be null.
     */
    public ProjectWriter(Writer writer)
    {
        if (writer == null)
            throw new NullPointerException ("The Writer argument cannot be null.");

        this.writer = writer;
    }


    /**
     * This method handles cases where a {@code visit (T t)} method was not found.
     * Is usually indicates this class should implement the missing {@code visit (T t)} method.
     *
     * @param bean the object encountered where no visit method to handle this type of object
     *             was found.  If {@code bean} is null, its silently ignored.
     */
    final public void defaultVisit ( Object bean )
    {
        if ( bean == null )
            return;

        logger.info ("No visiting method was found for " + bean.getClass().getName());
    }

    final public void write ( ProjectBean project )
    {
        visit ( project );
    }


    /**
     * This is the entry point for writing a project model.
     *
     * @param project the project model; null values are silently ignored.
     */
    final public void visit ( ProjectBean project )
    {
        if ( project == null ) return;

        try {
            writer.write("<?xml version=\"1.0\"?>");

            // print: <project version='1.0'>
            int INDENT_LEVEL_00 = 0;
            PrintHelper.printOpenTag(writer, INDENT_LEVEL_00, Tags.PROJECT);
            PrintHelper.printAttribute(writer, Tags.VERSION, project.getVersion());
            PrintHelper.printAttribute(writer, XMLNS, DEFAULT_NAMESPACE);
            writer.write(">");

            PrintHelper.printSimpleTagValue(writer, INDENT_LEVEL_01, Tags.PLUGIN_NAME, project.getPluginName());

            if ( project.getApplicationTarget() != null ) {
                project.getApplicationTarget().accept(this);
                }

            if (project.getWorkspaceTarget() != null) {
                project.getWorkspaceTarget().accept(this);
                }

            PrintHelper.printStartTag(writer, INDENT_LEVEL_01, Tags.COMPONENT_LIST);
            for (Component bean : project.getComponents()) {
                bean.accept(this);
                }
            PrintHelper.printEndTag(writer, INDENT_LEVEL_01, Tags.COMPONENT_LIST);

            visit ( project.getMessageResource() );


            PrintHelper.printEndTag(writer, INDENT_LEVEL_00, Tags.PROJECT);
        }
        catch (IOException ioe ) {
            throw new WriteFailure ( ioe );
        }
    }

    /**
     * Writes the {@code bean}.  Null values are silently ignored.
     *
     * @param bean the object to write
     */
    final public void visit ( WorkspaceTargetBean bean )
    {
        if ( bean == null )
            return;

        try {
            PrintHelper.printStartTag(writer, INDENT_LEVEL_01, Tags.WORKSPACE);

            PrintHelper.printSimpleTagValue(writer, INDENT_LEVEL_02, Tags.BASE_DIRECTORY, bean.getBaseDirectory());
            PrintHelper.printSimpleTagValue(writer, INDENT_LEVEL_02, Tags.SOURCE_DIRECTORY, bean.getSourceDirectory());
            PrintHelper.printSimpleTagValue(writer, INDENT_LEVEL_02, Tags.RESOURCE_DIRECTORY, bean.getResourceDirectory());

            PrintHelper.printEndTag(writer, INDENT_LEVEL_01, Tags.WORKSPACE);
        }
        catch (IOException ioe ) {
            throw new WriteFailure ( ioe );
        }
    }

    /**
     * Writes the {@code bean}
     *
     * @param targetBean the object to write
     */
    final public void visit ( ApplicationTargetBean targetBean )
    {
        if ( targetBean == null )
            return;

        try {
            int firstIndent = INDENT_LEVEL_01;
            int secondIndent = INDENT_LEVEL_02;

            PrintHelper.printStartTag(writer, firstIndent, Tags.APPLICATION_TARGET);
            PrintHelper.printSimpleTagValue(writer, secondIndent, Tags.HOME_DIRECTORY, targetBean.getHomeDirectory());
            PrintHelper.printSimpleTagValue(writer, secondIndent, Tags.VERSION, targetBean.getVersion());
            PrintHelper.printEndTag(writer, firstIndent, Tags.APPLICATION_TARGET);
        }
        catch (IOException ioe) {
            throw new WriteFailure ( ioe );
        }
    }

    /**
     * Writes the {@code bean}.
     *
     * @param bean the object to write
     */
    final public void visit ( FilterBean bean )
    {
        if ( bean == null )
            return;

        writeModelLayerBeanWithScopeSupport(bean, Tags.FILTER);
    }

    /**
     * Writes the {@code bean}.
     *
     * @param bean the object to write
     */
    final public void visit ( AssertionBean bean )
    {
        if ( bean == null )
            return;

        writeModelLayerBeanWithScopeSupport(bean, Tags.ASSERTION);
    }

    /**
     * Writes the {@code bean}.
     *
     * @param bean the object to write
     */
    final public void visit ( StepBean bean )
    {
        if ( bean == null )
            return;

        writeModelLayerBean(bean, Tags.STEP);
    }

    /**
     * Writes the {@code bean}.
     *
     * @param bean the object to write
     */
    final public void visit ( CompanionBean bean )
    {
        if ( bean == null )
            return;

        writeModelLayerBean( bean, Tags.COMPANION );
    }

    /**
     * Writes the {@code bean}.
     *
     * @param bean the object to write
     */
    final public void visit ( DataSetBean bean )
    {
        if ( bean == null )
            return;

        writeModelLayerBean ( bean, Tags.DATASET );
    }

    /**
     * Writes the {@code bean}.
     *
     * @param bean the object to write
     */
    final public void visit ( DataProtocolBean bean )
    {
        if ( bean == null )
            return;

        try {
            PrintHelper.printOpenTag(writer, INDENT_LEVEL_02, Tags.DATA_PROTOCOL);
            PrintHelper.printAttribute(writer, Tags.CLASSNAME, bean.getClassName());
            PrintHelper.printAttribute(writer, Tags.GENERATE, Boolean.toString(bean.isGenerateCode()));
            writer.write(">");

            PrintHelper.printSimpleTagValue(writer, INDENT_LEVEL_03, Tags.REQUEST_PROTOCOL, Boolean.toString(bean.isRequestSide()));
            PrintHelper.printSimpleTagValue(writer, INDENT_LEVEL_03, Tags.RESPONSE_PROTOCOL, Boolean.toString(bean.isResponseSide()));

            PrintHelper.printSimpleTagValue(writer, INDENT_LEVEL_03, Tags.DISPLAY_NAME, bean.getDisplayName());
            PrintHelper.printSimpleTagValue(writer, INDENT_LEVEL_03, Tags.DESCRIPTION,  bean.getDescription());

            PrintHelper.printEndTag( writer, INDENT_LEVEL_02, Tags.DATA_PROTOCOL );
        }
        catch (IOException ioe) {
            throw new WriteFailure ( ioe );
        }
    }

    /**
     * Writes the {@code bean}.
     *
     * @param bean the object to write
     */
    final public void visit ( MessageResourceBean bean )
    {
        if ( bean == null )
            return;

        try {
            PrintHelper.printOpenTag(writer, INDENT_LEVEL_01, Tags.MESSAGE_RESOURCE);
            PrintHelper.printAttribute(writer, Tags.CLASSNAME, bean.getClassName());
            PrintHelper.printAttribute(writer, Tags.GENERATE, Boolean.toString(bean.isGenerateCode()));
            writer.write ("/>");
        }
        catch (IOException ioe) {
            throw new WriteFailure ( ioe );
        }
    }


    /**
     * Writes the XML stanza for the ModelLayerBean
     *
     * @param bean the object to write
     */
    private void writeModelLayerBean ( MVCBean bean, String tagName )
    {
        try {
            PrintHelper.printOpenTag(writer, INDENT_LEVEL_02, tagName);
            PrintHelper.printAttribute(writer, Tags.CLASSNAME, bean.getClassName());
            PrintHelper.printAttribute(writer, Tags.GENERATE, Boolean.toString(bean.isGenerateCode()));
            writer.write(">");

            writeModelBean(bean, writer, INDENT_LEVEL_03);
            // print </tagName>
            PrintHelper.printEndTag( writer, INDENT_LEVEL_02, tagName );
        }
        catch (IOException ioe) {
            throw new WriteFailure ( ioe );
        }
    }


    /**
     * Writes the XML of a ModelLayerBean that has ScopeSupport.  AssertionBean and FilterBean
     * are, at this time, the only entities with ScopeSupport
     * @param bean the bean being written
     * @param tagName the tag name used in the starting stanza for the bean
     */
    private void writeModelLayerBeanWithScopeSupport(MVCBean bean, String tagName)
    {
        try {
            PrintHelper.printOpenTag(writer, INDENT_LEVEL_02, tagName);
            PrintHelper.printAttribute(writer, Tags.CLASSNAME, bean.getClassName());
            PrintHelper.printAttribute(writer, Tags.GENERATE, Boolean.toString(bean.isGenerateCode()));
            writer.write(">");

            ScopeSupport ss = (ScopeSupport)bean;
            PrintHelper.printSimpleTagValue(writer, INDENT_LEVEL_03, Tags.LOCAL_SCOPE, Boolean.toString(ss.isLocalScope()));
            PrintHelper.printSimpleTagValue(writer, INDENT_LEVEL_03, Tags.GLOBAL_SCOPE, Boolean.toString(ss.isGlobalScope()));

            writeModelBean(bean, writer, INDENT_LEVEL_03);
            // print </tagName>
            PrintHelper.printEndTag( writer, INDENT_LEVEL_02, tagName );
        }
        catch (IOException ioe) {
            throw new WriteFailure ( ioe );
        }
    }



    /**
     * Writes the XML tags for the editor and controller, followed by the parameters
     *
     * @param bean the object to write
     * @param pw the output destination, as a writer
     * @param indent the indentation level of the first tag for this stanza
     */
    private static void writeModelBean ( MVCBean bean, Writer pw, int indent ) throws IOException
    {
        Component editorBean = bean.getEditor();
        if ( editorBean != null ) {
            PrintHelper.printOpenTag(pw, indent, Tags.EDITOR);
            PrintHelper.printAttribute(pw, Tags.CLASSNAME, editorBean.getClassName());
            PrintHelper.printAttribute(pw, Tags.GENERATE, Boolean.toString(editorBean.isGenerateCode()));
            pw.write("/>");
        }

        Component controllerBean = bean.getController();
        if ( controllerBean != null ) {
            PrintHelper.printOpenTag(pw, indent, Tags.CONTROLLER);
            PrintHelper.printAttribute(pw, Tags.CLASSNAME, controllerBean.getClassName());
            PrintHelper.printAttribute(pw, Tags.GENERATE, Boolean.toString(controllerBean.isGenerateCode()));
            pw.write("/>");
        }

        writeParameters (bean.getParameters(), pw, indent);
    }

    /**
     * Writes the XML stanza for the list of fields.
     *
     * @param fields the list of fields to write
     * @param pw the output destination
     * @param indent the indentation level of the first tag in the stanza
     * @throws IOException
     */
    private static void writeParameters  (final List<Parameter> fields, Writer pw, int indent ) throws IOException
    {
        if ( fields == null )
            return;

        int childIndent = indent + 4;

        PrintHelper.printStartTag ( pw, indent, Tags.PARAMETER_LIST);

        for ( Parameter bean : fields )
        {
            // Print <field variableName='...' type='...'>
            PrintHelper.printOpenTag(pw, childIndent, Tags.PARAMETER);
            PrintHelper.printAttribute(pw, Tags.VARIABLE_NAME, bean.getVariableName());
            PrintHelper.printAttribute(pw, Tags.TYPE, bean.getDataType().toString());
            pw.write(">");

            PrintHelper.printSimpleTagValue(pw, childIndent + 4, Tags.PRESENTS_PREFILL_COMBO, Boolean.toString(bean.getPresentsPrefillCombo()));
            PrintHelper.printSimpleTagValue(pw, childIndent+4, Tags.PRESENTS_FIND_BUTTON, Boolean.toString(bean.getPresentsFindButton()));
            PrintHelper.printSimpleTagValue(pw, childIndent+4, Tags.GET_METHOD, bean.getGetterMethodName());
            PrintHelper.printSimpleTagValue(pw, childIndent+4, Tags.SET_METHOD, bean.getSetterMethodName());
            PrintHelper.printSimpleTagValue(pw, childIndent+4, Tags.LABEL_NAME, bean.getLabelName());

            // Print <label key='...'>value</label>
            Message mbean = bean.getLabel();
            if ( mbean != null )
            {
                PrintHelper.printOpenTag ( pw, childIndent+4, Tags.LABEL );
                PrintHelper.printAttribute ( pw, Tags.KEY, mbean.getKey());
                pw.write(">");
                pw.write( mbean.getValue() );
                PrintHelper.printEndTag( pw, Tags.LABEL );
            }

            // Print <tooltip key='...'>value</tooltip>
            mbean = bean.getTooltip();
            if ( mbean != null )
            {
                PrintHelper.printOpenTag ( pw, childIndent+4, Tags.TOOLTIP );
                PrintHelper.printAttribute ( pw, Tags.KEY, mbean.getKey());
                pw.write(">");
                pw.write( mbean.getValue() );
                PrintHelper.printEndTag( pw, Tags.TOOLTIP );
            }

            // print </field>
            PrintHelper.printEndTag(pw, childIndent, Tags.PARAMETER);
        }
        // print </fieldList>
        PrintHelper.printEndTag(pw, indent, Tags.PARAMETER_LIST);
    }


    /**
     * The visit() method is not allowed to throw Exceptions.  We use exception tunneling
     * as a way to capture the IOExceptions that can occur without having to
     * refactor the Visitor interface.
     * See http://c2.com/cgi/wiki?ExceptionTunneling
     */
    private static class WriteFailure extends RuntimeException
    {
        static final long serialVersionUID = 4728749972688072390L;


        private Exception cause;

        public WriteFailure ( Exception t ) {
            super (t);
            cause = t;
        }
        @SuppressWarnings("unused")
        public WriteFailure ( String msg, Exception t ) {
            super (msg, t);
            cause = t;
        }
        public Exception getCause() {
            return cause;
        }
    }
}
