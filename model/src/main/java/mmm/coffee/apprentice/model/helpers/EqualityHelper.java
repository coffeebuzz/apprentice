/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.model.helpers;

import java.util.List;

/**
 * A collection of methods related to testing the equality of two objects.
 * These methods are tolerant of null values.
 */
public class EqualityHelper {

    /**
     * Safely test whether two Objects are equal.
     *
     * @param s1 some object, possibly null
     * @param s2 some object, possibly null
     * @return {@code true} if both Objects are equal
     */
    public static boolean areEqual ( Object s1, Object s2 )
    {
        if ( s1 == null && s2 == null ) return true;
        if ( s1 != null )
            return s1.equals (s2);
        return false;
    }


    /**
     * Determine if the contents of the two given lists are equal.
     * The conditions of equality are:
     * <ul>
     *     <li>both lists have the same number of items, or both lists are null</li>
     *     <li>each item in list {@code a} exists in list {@code b}</li>
     *     <li>each item in list {@code b} exists in list {@code a}</li>
     * </ul>
     * @param a some list of items; can be null or empty
     * @param b some list of items; can be null or empty
     * @return {@code true} if the lists contain the same items; the order of the items is irrelevant
     */
    public static boolean areEqual ( List<?> a, List<?> b )
    {
        // Condition 1: if both lists are null, return true
        if ( a == null && b == null )
            return true;

        // Condition 2: if one list nul and the other is not, return false
        if ( a == null || b == null )
            return false;


        // Condition 3: if the lists do not have the same size, return false
        if ( a.size() != b.size() )
            return false;

        // Condition 4: List b contains each item in List a; List a contains each item in List b.
        // Basically, we do not care about the order of items in the list, only that any given
        // element is found in both lists.
        for ( int i = 0; i < a.size(); i++ ) {
            if ( ! ( b.contains( a.get(i) )))
                return false;
            if ( ! ( a.contains ( b.get(i))))
                return false;
        }
        return true;
    }
}
