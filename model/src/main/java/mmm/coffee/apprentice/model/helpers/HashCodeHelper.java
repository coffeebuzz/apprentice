/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.model.helpers;

import java.util.List;

/**
 * Some helper methods for computing hash codes
 */
public class HashCodeHelper {

    /**
     * Returns the sum of the hashCodes of each element in the {@code list}.
     *
     * @param list some list of items; may be null
     * @return the sum total of each item's hashCode
     */
    public static int aggregateHashCode( List<?> list )
    {
        final int prime = 17;
        int result = 1;

        if ( list != null ) {
            for (int i = 0; i < list.size(); i++) {
                result = prime * result + list.get(i).hashCode();
            }
        }
        return result;
    }
}
