/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.model.beans;


import mmm.coffee.apprentice.model.helpers.EqualityHelper;
import mmm.coffee.apprentice.model.helpers.HashCodeHelper;
import mmm.coffee.apprentice.model.helpers.StringHelper;
import mmm.coffee.apprentice.model.stereotypes.Component;
import mmm.coffee.apprentice.model.stereotypes.MVCEntity;
import mmm.coffee.apprentice.model.stereotypes.Parameter;
import mmm.coffee.apprentice.model.visitor.Visitor;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;


/**
 * A ModelLayerBean contains the meta-data about a model component
 * that is linked to an editor and controller.
 */
public class MVCBean extends ComponentBean implements MVCEntity, Serializable {

    static final long serialVersionUID = 521309814964549814L;

    private ComponentBean editor;
    private ComponentBean controller;

    // These are actually ParameterBeans.
    private List<Parameter> parameters = new LinkedList<>();


    /**
     * Default constructor
     */
    public MVCBean()
    {
        // empty
    }

    /**
     * Creates a deep copy of {@code clone}.
     *
     * @param clone the prototype to clone; must not be null
     */
    public MVCBean(final MVCEntity clone)
    {
        super(clone);
        setEditor(clone.getEditor());
        setController( clone.getController() );

        for (Parameter param : clone.getParameters() )
        {
            parameters.add ( new ParameterBean ( param ));
        }
    }

    /**
     * Updates the instance variables of this object to match the instance variables of {@code delta}.
     * Invariant fields, such as the UUID, are not changed.  If {@code delta} is null, the request is
     * quietly ignored.
     *
     * @param delta the object whose state is copied into {@code this} object.
     */
    public synchronized void copyOf ( MVCEntity delta )
    {
        if ( delta != null ) {
            // Update our existing editor, or add new editor
            if (editor != null) {
                editor.copyOf(delta.getEditor());
            } else if (delta.getEditor() != null) {
                editor = new ComponentBean(delta.getEditor());
            }

            // Update our existing controller, or add a new controller
            if (controller != null) {
                controller.copyOf(delta.getController());
            } else if (delta.getController() != null) {
                controller = new ComponentBean(delta.getController());
            }

            // For parameters, we do a blanket replacement
            if ( delta.getParameters().size() > 0 )
            {
                parameters.clear();

                for ( Parameter p : delta.getParameters() )
                {
                    parameters.add ( new ParameterBean ( p ));
                }
            }
        }
    }



    /**
     * Constructs an MVCBean object using the {@code generator}'s properties
     * to load the MVCBean's properties.
     *
     * @param builder an instance of MVCBeanBuilder, or one of its subclasses
     */
    protected MVCBean (Builder<?> builder )
    {
        super ( builder );

        if ( builder.controller != null )
            this.setController( builder.controller );

        if ( builder.editor != null )
            this.setEditor ( builder.editor );

        this.setParameters( builder.parameters );
    }

    public synchronized MVCBean deepCopy() {
        return new MVCBean ( this );
    }

    /**
     * Returns the EditorBean for this Step
     * @return the EditorBean of the step
     */
    final synchronized public Component getEditor()
    {
        return editor;
    }

    /**
     * Assigns the editor by cloning {@code editor}
     * @param editor the editor
     */
    final synchronized public void setEditor ( final Component editor )
    {
        if ( editor == null )
            this.editor = null;
        else
            this.editor = new ComponentBean( editor );
    }

    /**
     * Assigns the editor as a reference to {@code editor}.
     *
     * @param editor the editor
     */
    final synchronized public void setEditor ( final ComponentBean editor ) {
        this.editor = editor;
    }

    /**
     * Returns the ControllerBean of this Step
     * @return our controller
     */
    final synchronized public Component getController()
    {
        return controller;
    }

    /**
     * Assigns the controller by cloning {@code controller}
     * @param controller the controller
     */
    final synchronized public void setController ( final Component controller )
    {
        if ( controller == null )
            this.controller = null;
        else
            this.controller = new ComponentBean( controller );
    }

    final synchronized public void setController ( final ComponentBean controller )
    {
        this.controller = controller;
    }

    /**
     * Returns the data parameters for this model object.
     * These parameters appear in the UI and allow the user to get and set data values.
     *
     * @return the bean's data parameters. Returns an empty list if no parameters are defined.
     */
    final synchronized public List<Parameter> getParameters()
    {
        if ( parameters == null )
            parameters = new LinkedList<>();

        return parameters;
    }

    /**
     * Assigns the parameters of this assertion.
     *
     * @param parameters the parameters for this assertion.  {@code Null} is allowed.
     */
    final synchronized public void setParameters(List<Parameter> parameters)
    {
        if ( parameters != null ) {
            for (Parameter parm : parameters) {
                this.parameters.add(new ParameterBean(parm));
            }
        }
        else {
            this.parameters = new LinkedList<>();
        }
    }


    /**
     * Accepts the {@code visitor}
     */
    @Override
    public void accept ( Visitor visitor ) {
        visitor.visit ( this );
    }


    public boolean canEqual ( final Object other ) {
        return ( other instanceof MVCEntity);
    }

    /**
     * {@inheritDoc}
     * @param other the object being compared
     * @return {@code true} if {@code object} have the same editor, controller,
     */
    @Override synchronized public boolean equals ( Object other ) {
        if ( this == other ) return true;
        if ( other == null ) return false;

        boolean result = false;
        if ( other instanceof MVCEntity) {
            MVCEntity that = (MVCEntity) other;
            result =  that.canEqual(this)
                && super.equals ( that )
                && EqualityHelper.areEqual ( this.editor, that.getEditor() )
                && EqualityHelper.areEqual ( this.controller, that.getController() )
                && EqualityHelper.areEqual ( this.getParameters(), that.getParameters() );
        }
        return result;
    }

    @Override synchronized public int hashCode()
    {
        final int prime = 41;

        int result = super.hashCode();

        if ( this.controller != null )
            result = prime * result + this.controller.hashCode();
        if ( this.editor != null )
            result = prime * result + this.editor.hashCode();

        result = prime * result + HashCodeHelper.aggregateHashCode(parameters);

        return result;
    }

    /**
     * Defines our abstract Builder. See the ClassBean for an explanation of pattern.
     * @param <T> the type of objects created by this builder
     */
    public static abstract class Builder<T extends Builder<T>> extends ComponentBean.Builder<T> {
        private ComponentBean editor;
        private ComponentBean controller;
        private List<Parameter> parameters = new LinkedList<>();

        protected ComponentBean getEditor() { return editor; }
        protected ComponentBean getController() { return controller; }

        public T withEditor ( final ComponentBean editor ) {
            this.editor = editor;
            return self();
        }

        public T withEditor ( String className )
        {
            this.editor = ComponentBean.builder( className ).withGenerateCode(true).build();
            return self();
        }

        public T withController ( final ComponentBean controller ) {
            this.controller = controller;
            return self();
        }

        public T withController ( String className )
        {
            this.controller = ComponentBean.builder ( className ).withGenerateCode( true ).build();
            return self();
        }


        public T withParameter ( final Parameter parameter ) {
            parameters.add ( parameter );
            return self();
        }

        public MVCBean build() {
            return new MVCBean(this);
        }
    }

    private static class MVCBeanBuilder extends Builder<MVCBeanBuilder> {

        protected MVCBeanBuilder ( String className )
        {
            if (!StringHelper.isValidJavaIdentifier(className))
                throw new IllegalArgumentException(
                         "The className argument '"
                        +StringHelper.safeString(className)
                        +"' is not a valid Java identifier");

            withClassName( className );
        }
        @Override
        protected MVCBeanBuilder self() {
            return this;
        }
    }

    /**
     * Returns an MVCBeanBuilder
     * @param className the fully-qualified classname of the model-layer class
     * @return an MVCBeanBuilder
     */
    public static Builder<?> builder( String className ) {
        return new MVCBeanBuilder( className );
    }

}
