/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.model.helpers;

import java.io.IOException;
import java.io.Writer;

/**
 * This class contains helper methods for printing XML.
 */
public class PrintHelper {

    private static final char SPACE = ' ';


    /**
     * Prints a starting tag of the form {@code &lt;mytag&gt;}.
     *
     * @param pw where the tag is written
     * @param indent how much to indent the tag
     * @param tag the tag to write
     * @throws IOException for I/O errors
     */
    final public static void printStartTag (Writer pw, int indent, String tag ) throws IOException
    {
        printOpenTag ( pw, indent, tag );
        pw.write (">");
    }


    /**
     * Prints a tag of the form "&lt;tag".  Notably, the tag is NOT closed, so that attributes can be appended.
     *
     * @param pw the Writer where the tag is written
     * @param indent how far in the tag is indented before being written
     * @param tag the tag name written
     *
     * @throws IOException for I/O errors
     */
    final public static void printOpenTag ( Writer pw, int indent, String tag ) throws IOException
    {
        if ( pw == null || tag == null )
            return;

        pw.write ( System.lineSeparator() );
        indent = Math.max(indent, 0);
        for (int i = 0; i < indent; i++) pw.write(SPACE);

        pw.write ("<");
        pw.write (tag);
    }


    /**
     * Prints a closing end tag, such as {@code &lt;/mytag&gt;}
     * @param pw where to print the tag
     * @param tag the tag written
     * @throws IOException for I/O errors
     */
    final public static void printEndTag ( Writer pw, String tag ) throws IOException
    {
        if ( pw == null || tag == null )
            return;

        pw.write ("</");
        pw.write ( tag );
        pw.write ( ">");
    }

    /**
     * Prints the closing XML tag
     * @param pw the writer printing the XML
     * @param indent how far to indent
     * @param tag the tag's name
     * @throws IOException for I/O errors
     */
    final public static void printEndTag ( Writer pw, int indent, String tag ) throws IOException
    {
        if ( pw == null || tag == null )
            return;

        pw.write ( System.lineSeparator() );

        int tab = Math.max(0, indent);
        for (int i = 0; i < tab; i++) pw.write( SPACE );

        pw.write ("</");
        pw.write ( tag );
        pw.write ( ">");

    }


    /**
     * Writes an XML tag of the form &lt;tag&gt;value&lt;/tag&gt;
     * @param pw the writer that prints the XML
     * @param indent how far to indent
     * @param tag the xml tag to print
     * @param value the value to print
     * @throws IOException for I/O errors
     */
    final public static void printSimpleTagValue ( Writer pw, int indent, String tag, String value ) throws IOException
    {
        if ( tag == null )
            return;

        // print <tag>
        printStartTag ( pw, indent, tag );

        // print the value
        if ( value != null)
            pw.write(value);

        // print </tag>
        printEndTag( pw, tag );
    }

    /**
     * Writes an XML tag's attribute and value
     * @param pw the writer that prints the XML
     * @param name the attribute name
     * @param value the attribute value
     * @throws IOException for I/O errors
     */
    final public static void printAttribute ( Writer pw, String name, String value ) throws IOException
    {
        if ( name == null || value == null )
            return;

        pw.write ( SPACE );
        pw.write ( name );
        pw.write ( "=\"");
        pw.write ( value );
        pw.write ("\"");
    }

}
