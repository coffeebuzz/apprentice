/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.model.stereotypes;

import mmm.coffee.apprentice.model.visitor.AcceptVisitor;
import mmm.coffee.apprentice.model.visitor.Visitor;

/**
 * Defines the stereotype for Bean objects.
 * Beans are visitable, and thus must implement an {@code accept} method.
 */
public interface BeanEntity extends AcceptVisitor {

    /**
     * Returns the UUID assigned to this entity
     * @return the UUID of this entity
     */
    String getUUID();

    /**
     * Assigns a UUID to this object
     * @param uuid the UUID to assign. A null value can be assigned, which essentially clears the UUID.
     */
    void setUUID ( String uuid );

    /**
     * Accepts the {@code visitor}.  This enables the Visitor pattern
     * to be applied any Bean.
     *
     * @param visitor the visitor; must not be null
     */
    void accept ( Visitor visitor );

    /**
     * Used by the equals() method to ensure the equals contract is met.
     * See http://www.artima.com/lejava/articles/equality.html
     * This essentially allows a subclass to compare itself to a superclass, but not vice-versa.
     *
     * @param other the object being compared
     * @return true if {@code that} can be compared for equality to this object.
     */
    boolean canEqual ( Object other );

    /**
     * Create a deep copy clone
     *
     * @return a deep copy clone
     */
    BeanEntity deepCopy();

}
