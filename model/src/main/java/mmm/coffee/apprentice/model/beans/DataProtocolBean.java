/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.model.beans;


import mmm.coffee.apprentice.model.helpers.EqualityHelper;
import mmm.coffee.apprentice.model.helpers.StringHelper;
import mmm.coffee.apprentice.model.stereotypes.DataProtocol;
import mmm.coffee.apprentice.model.visitor.Visitor;

import java.io.Serializable;

/**
 * The DataProtocolBean is a JavaBean for a Data Protocol Handler plugin.
 */
public class DataProtocolBean extends ComponentBean implements DataProtocol, Serializable {

    static final long serialVersionUID = 4173278373158631310L;

    // This tells us whether or not this protocol manipulates request objects
    private boolean isRequestProtocol;

    // This tells us whether or not this protocol manipulates response objects
    private boolean isResponseProtocol;

    // This is the name of this protocol as presented in the UI
    private String displayName;

    // This is the description of this protocol as presented in the UI
    private String description;



    /**
     * Default constructor
     */
    public DataProtocolBean() {
        // empty
    }

    /**
     * Construct a new instance by making a clone.
     *
     * @param clone the prototype to clone; must not be null
     */
    public DataProtocolBean ( final DataProtocol clone )
    {
        super ( clone );
        setIsRequestSide( clone.getIsRequestSide() );
        setIsResponseSide( clone.getIsResponseSide() );
        setDisplayName( clone.getDisplayName() );
        setDescription( clone.getDescription() );
    }


    /**
     * Constructs a DataProtocolBean object using the {@code generator}'s properties
     * to load the bean's properties.
     *
     * @param builder an instance of MVCBeanBuilder, or one of its subclasses
     */
    protected DataProtocolBean (Builder<?> builder )
    {
        super ( builder );

        setDescription( builder.description );
        setIsRequestSide( builder.isRequestProtocol );
        setIsResponseSide( builder.isResponseProtocol );

        // If the displayName was not give, use the simple name of the class
        if ( StringHelper.isEmpty( builder.displayName ))
            setDisplayName( getSimpleName() );
        else
            setDisplayName( builder.displayName );
    }


    public DataProtocolBean deepCopy() {
        return new DataProtocolBean( this );
    }


    final public boolean isRequestSide() {
        return isRequestProtocol;
    }

    final public boolean isResponseSide() {
        return isResponseProtocol;
    }

    final public void setIsRequestSide (final boolean state) {
        this.isRequestProtocol = state;
    }
    final public boolean getIsRequestSide () { return isRequestProtocol; }

    final public void setIsResponseSide(final boolean state) {
        this.isResponseProtocol = state;
    }
    final public boolean getIsResponseSide () {
        return isResponseProtocol;
    }

    final public void setDisplayName ( final String name ) {
        this.displayName = name;
    }

    final public String getDisplayName()
    {
        return StringHelper.safeString( displayName );
    }

    final public void setDescription ( final String description ) {
        this.description = description;
    }
    final public String getDescription()
    {
        return StringHelper.safeString( description );
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append ( "DataProtocolBean {");

        sb.append (" classname: '").append ( getClassName() ).append ("',");
        sb.append (" displayName: '").append ( getDisplayName() ).append("',");
        sb.append (" description: '").append ( getDescription() ).append(",");
        sb.append (" isRequestSide: '").append ( isRequestSide() ).append ("',");
        sb.append (" isResponseSide: '").append ( isResponseSide() ).append ("'");

        sb.append (" }");

        return sb.toString();
    }

    /**
     * Accepts the {@code visitor}
     * @param visitor {@inheritDoc}
     */
    public void accept ( Visitor visitor ) {
        visitor.visit ( this );
    }

    /**
     * Data Protocol Handlers are considered equal if every attribute is identical.
     *
     * @param other the object being compared to {@code this}
     * @return true if {@code other} equals {@code this}.
     */
    @Override public boolean equals ( Object other ) {
        if (this == other) return true;
        if (other == null) return false;

        boolean result = false;

        if ( other instanceof DataProtocol) {
            DataProtocol that = (DataProtocol)other;
            result = ( that.canEqual(this) )
                    && super.equals ( that )
                    && this.isResponseProtocol == that.isResponseSide()
                    && this.isRequestProtocol == that.isRequestSide()
                    && EqualityHelper.areEqual( this.getDisplayName(), that.getDisplayName() )
                    && EqualityHelper.areEqual( this.getDescription(), that.getDescription() );
        }
        return result;
    }

    @Override public int hashCode()
    {
        final int prime = 53;

        int result = super.hashCode();

        result = prime * result + Boolean.toString( isRequestProtocol ).hashCode();
        result = prime * result + Boolean.toString( isResponseProtocol ).hashCode();
        result = prime * result + getDisplayName().hashCode();
        result = prime * result + getDescription().hashCode();

        return result;
    }


    public boolean canEqual ( final Object other ) {
        return ( other instanceof DataProtocol);
    }


    /**
     * Defines our abstract Builder. See the ClassBean for an explanation of this pattern.
     * @param <T> the DataProtocolBeanBuilder
     */
    public static abstract class Builder<T extends Builder<T>> extends ComponentBean.Builder<T> {

        private boolean isRequestProtocol;
        private boolean isResponseProtocol;
        private String displayName;
        private String description;

        /**
         * Declare whether or not this data protocol manipulates request objects
         * @param flag if {@code true} this protocol manipulates request objects
         * @return the Builder instance
         */
        public T withRequestSide(boolean flag ) {
            this.isRequestProtocol = flag;
            return self();
        }

        /**
         * Declare whether or not this data protocol manipulates response objects
         * @param flag if {@code true} this protocol manipulates response objects
         * @return the Builder instance
         */
        public T withResponseSide(boolean flag ) {
            this.isResponseProtocol = flag;
            return self();
        }

        /**
         * Sets the display name of this protocol. This is the name presented in the Portal UI
         * and in the Workstation UI.
         * @param displayName the display name
         * @return the Builder instance
         */
        public T withDisplayName ( final String displayName ) {
            this.displayName = displayName;
            return self();
        }

        /**
         * Sets the short description of this protocol. This description is presented in the
         * Portal UI and Workstation UI.
         *
         * @param description the short description of this protocol handler
         * @return the Builder instance
         */
        public T withDescription ( final String description ) {
            this.description = description;
            return self();
        }

        /**
         * Builds a DataProtocolBean using the generator's current state
         *
         * @return a DataProtocolBean based on the generator's state
         */
        public DataProtocolBean build() {

            // For the sake of helping users catch misconfiguration errors, we'll throw
            // this exception if the protocol has not been declared to support at least
            // the requests or responses.
            if ( !this.isRequestProtocol && !this.isResponseProtocol )
                throw new IllegalStateException ("A DataProtocol must handle either requests, responses, or both.");

            return new DataProtocolBean( this );
        }
    }

    private static class DPHBuilder extends Builder<DPHBuilder> {

        /**
         * Constructor
         * @param className the className for the model-layer class. This is required and
         *                  may not be null nor an empty string
         */
        protected DPHBuilder ( String className )
        {
            withClassName( className );
        }

        @Override
        protected DPHBuilder self() {
            return this;
        }
    }

    public static Builder<?> builder( String className )
    {
        return new DPHBuilder( className );
    }


}
