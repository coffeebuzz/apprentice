/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.model.stereotypes;


/**
 * This stereotype is for entities that describe the output meta-data
 * for the code generator.
 */
public interface WorkspaceTarget extends BeanEntity {

    /**
     * Returns the root directory into which the code generator writes any files
     * @return the root directory of the generated project
     */
    String getBaseDirectory();
    void setBaseDirectory( String dir );

    /**
     * Returns the directory into which the code generator writes Java source
     * files; for example, "src/main/java".  This directory is relative to the
     * base directory
     *
     * @return the directory into which source code is generated
     */
    String getSourceDirectory();
    void setSourceDirectory( String dir );


    /**
     * Returns the directory into which the code generator writes Java test source,
     * for example, "src/test/java".  This directory is relative to the
     * base directory
     *
     * @return the directory into which source code is generated
     */
    String getTestSourceDirectory();
    void setTestSourceDirectory( String dir );

    /**
     * Returns the directory into which resource files, such as the lisaextensions file,
     * are generated.  For example, "src/main/resources".
     *
     * @return the directory into which resource files are written.
     */
    String getResourceDirectory();
    void setResourceDirectory ( String dir );
}
