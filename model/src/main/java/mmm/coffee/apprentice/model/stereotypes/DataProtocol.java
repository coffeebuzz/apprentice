/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.model.stereotypes;


/**
 * This defines the stereotype for a data protocol handler,
 * in the context of a code generator.
 */
public interface DataProtocol extends Component {

    boolean isRequestSide();
    boolean isResponseSide();

    void setIsRequestSide(boolean state);
    boolean getIsRequestSide();

    void setIsResponseSide(boolean state);
    boolean getIsResponseSide();

    void setDisplayName ( String name );
    String getDisplayName();

    void setDescription ( String description );
    String getDescription();
}
