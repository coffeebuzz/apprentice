/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.model.stereotypes;


/**
 * This interface defines the {@code Step} stereotype, which corresponds
 * to a Step entity in the DevTest Workstation.
 */
public interface Step extends MVCEntity {

    /**
     * {@inheritDoc}
     * @param that make {@code this} a copy of {@code that}
     */
    void copyOf ( Step that );
}
