/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.model.io;

import mmm.coffee.apprentice.model.visitor.Visitor;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * This is the base class for Visitors that write bean information
 */
public abstract class BeanWriter implements Visitor {

    /**
     * This method is called if a visit method cannot be matched to the class of {@code obj}.
     * @param obj the object for which no {@code visit} method was found
     */
    public abstract void defaultVisit ( Object obj );

    /**
     * Uses reflection to invoke the visit() method of the underlying object
     *
     * @param obj the target processed by the visit() method
     */
    public void visit ( Object obj )
    {
        if ( obj == null )
            return;

        try
        {
            Method visitMethod = this.getClass().getMethod("visit", obj.getClass());
            visitMethod.invoke(this, obj );
        }
        catch (NoSuchMethodException noSuchMethodException)
        {
            // Reflection is peculiar about interfaces. In some cases, reflection does not
            // match a method, say, "visit(Project p)", where Project is an interface and 'p' is a ProjectBean,
            // which implements the Project interface.
            //
            // To handle cases where the "visit(T t)" method takes an interface T as its argument,
            // and the call to getMethod("visit", T) fails to find the visit() method,
            // we'll try the interfaces implemented by the "obj" class and see if we get a match.
            // Once we do find a visit method to invoke, we invoke it then break out of the loop.
            Class<?>[] ifaces = obj.getClass().getInterfaces();
            for ( Class<?> iface : ifaces ) {
                try {
                    // Method visitMethod = this.getClass().getMethod("visit", new Class<?>[]{iface});
                    Method visitMethod = this.getClass().getMethod("visit", iface );
                    visitMethod.invoke ( this, obj );
                    break;
                }
                catch (NoSuchMethodException ex ) {
                    // If there's no "visit(T t)" method for the interface we tried,
                    // move on to the next interface of the class.
                }
                catch (InvocationTargetException | IllegalAccessException err ) {
                    this.defaultVisit( obj );
                }
            }
        }
        catch ( InvocationTargetException | IllegalAccessException err ) {
            this.defaultVisit( obj );
        }
    }
}
