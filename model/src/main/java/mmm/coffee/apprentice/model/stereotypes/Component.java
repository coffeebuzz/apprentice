/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.model.stereotypes;

/**
 * This interface defines the contract for Beans that are translated
 * into Java classes.
 */
public interface Component extends BeanEntity {

    /**
     * Returns the package name of a class
     * @return the component's package name, such as {@code "mmm.coffee.widget"}
     */
    String getPackageName();

    /**
     * Returns the fully qualified classname
     * @return the component's classname, such as {@code "mmm.coffee.widget.WidgetStep"}
     */
    String getClassName();

    /**
     * Assigns the classname of the component. This relates to the component being generated.
     * @param className the classname of the component being generated, such as {@code "mmm.coffee.widget.WidgetFilter"}
     */
    void setClassName(String className);

    /**
     * Returns the fully qualified junit test classname
     * @return the component's junit test classname, such as {@code "mmm.coffee.widget.WidgetStepTest"}
     */
    String getTestClassName();

    /**
     * Assigns the test classname of the component. This relates to the component being generated.
     * @param className the test classname of the component being generated, such as {@code "mmm.coffee.widget.WidgetFilterTest"}
     */
    void setTestClassName(String className);



    /**
     * Returns the classname without the package name.
     * For example, if the classname is 'org.sample.MyApp',
     * this method returns 'MyApp'
     *
     * @return the class name without the package name
     */
    String getSimpleName();

    /**
     * Returns the test classname without the package name.
     * For example, if the test classname is 'org.sample.MyAppTest',
     * this method returns 'MyAppTest'
     *
     * @return the class name without the package name
     */
    String getSimpleTestName();


    /**
     * Returns {@code true} if the code generator should generate this class.
     * Returns {@code false} when a default class provided by DevTest is used.
     *
     * @return {@code true} if the code generator should generate this class; {@code false} otherwise.
     */
    boolean isGenerateCode();

    /**
     * Enables or disables the generation of a Java class for this component.  Generally, custom Steps
     * need custom editors and controllers, while the other components (e.g., assertions and filters)
     * have default editors and controllers provided by DevTest.  These default editors and controllers
     * have proven to be sufficient for most needs.
     *
     * @param flag when {@code true}, the code generator will generate a Java class for this component.
     */
    void setGenerateCode( boolean flag );

    /**
     * Returns the value of the {@code generateCode} flag
     * @return the value of the {@code generateCode} flag
     */
    boolean getGenerateCode();

}
