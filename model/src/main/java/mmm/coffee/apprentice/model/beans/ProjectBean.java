/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.model.beans;

import mmm.coffee.apprentice.model.helpers.EqualityHelper;
import mmm.coffee.apprentice.model.helpers.HashCodeHelper;
import mmm.coffee.apprentice.model.helpers.StringHelper;
import mmm.coffee.apprentice.model.stereotypes.*;
import mmm.coffee.apprentice.model.visitor.Visitor;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * The {@code ProjectBean} contains information about the DevTest
 * plugins being generated, such as the base directory for the workspaceTarget,
 * the source directory for Java files, and which components to generate.
 *
 * A Builder class is available which makes it easier to assemble the components
 * of a project.  An example for using the builder is:
 * <code>
 *  // Step 1: Define the home directory and version of DevTest that will use the plugin
 *  ApplicationTarget devtestTarget = ApplicationTargetBean.builder()
 *                                                    .withHomeDirectory( Constants.DEVTEST_HOME )
 *                                                    .withVersion( Constants.DEVTEST_VERSION )
 *                                                    .build();
 *
 *  // Step 2: Define the directory where the generated code is written
 *  WorkspaceTarget workspaceTarget = WorkspaceTargetBean.builder( "/Users/joe.cool/workspace/MyPlugin" )
 *                                                    .build();
 *
 *  // Step 3: Define the messages class that will enable localized strings
 *  MessageResource messageResource = MessageResourceBean.builder("mmm.coffee.example.i18n.Messages")
 *                                                    .build();
 *
 *  // Step 4: Define the project
 *  Project myPlugin = ProjectBean.builder( devtestTarget, workspaceTarget, messageResource )
 *                        .withPluginName("StepPlugin")
 *                            // The pluginName becomes the name of the jar that's created
 *                        .withComponent(
 *                            // The plugin will include this custom step
 *                            StepBean.builder ("mmm.coffee.example.MyCustomStep")
 *                                        // The builder constructor needs the name of the
 *                                        // model-layer (a.k.a. Node) class to generate.
 *                                    .withController("mmm.coffee.example.MyCustomStepController")
 *                                    .withEditor("mmm.coffee.example.MyCustomStepEditor")
 *                                    .withParameter(
 *                                            ParameterBean.builder("hostName")
 *                                                    // The parameter's builder constructor takes the
 *                                                    // instance variable of the parameter,
 *                                                    // so please follow Java's naming rules to avoid
 *                                                    // compilation errors
 *                                                .withDataType( DataType.STRING )
 *                                                .withLabel("Host Name:")
 *                                                .build())
 *                                    .withParameter(
 *                                            ParameterBean.builder("port")
 *                                                .withDataType (DataType.STRING)
 *                                                .withLabel("Port:")
 *                                                .build())
 *                                    .build())
 *                        .build();
 * // Step 5: Generate the project
 * ProjectGenerator generator = new ProjectGenerator();
 * generator.generate( myPlugin );
 * </code>
 *
 * Programmer's Notebook:
 * We decided that each project will have one and only one class that fetches
 * localized strings. This class shows up in the generated code as something like:
 * <code>Messages.resources.get("executeButton.label")</code>.  By requiring a
 * message resource class to exist, the Velocity templates are simplified
 * and localization is provided.
 *
 */
public class ProjectBean implements Project, Serializable {


    static final long serialVersionUID = -2112323053558697904L;


    private static final String ERROR_REQUIRED_FIELD_APPLICATION_TARGET =
            "An ApplicationTarget is required and cannot be null";
    private static final String ERROR_REQUIRED_FIELD_WORKSPACE_TARGET =
            "A WorkspaceTarget is required and cannot be null";
    private static final String ERROR_REQUIRED_FIELD_MESSAGE_RESOURCE =
            "A MessageResource is required and cannot be null";

    /*
     * The schema version of our imaginary project DTD
     */
    private static final String version = "1.0";

    // The name of the generated Jar file
    private String pluginName;

    // The version of DevTest where the plugin will be deployed.
    // There are compile-time dependencies on DevTest jars, and the home directory needs
    // to be known to generate a working build.properties file for the plugin.
    private ApplicationTarget applicationTarget;


    // The workspaceTarget properties tells the code generator where to write
    // the generated filed
    private WorkspaceTarget workspaceTarget;

    // A list of the components to generate, such as a Filter, Assertion,
    // Step, Data Set, Data Protocol Handler, or Companion.
    private List<Component> components = new LinkedList<>();

    // This defines the Messages resource class that will be used by
    // all the components.
    private MessageResource messageResource;

    private String uuid;


    /**
     * Constructs a ProjectBean having the given {@code applicationTarget},
     * {@code workspace}, and {@code messageResource}.
     *
     * @param applicationTarget the location of DevTest to which the generated plugin
     *                          will be deployed
     * @param workspace the directory into which the generated code is written
     * @param messageResource the name of the Messages class that enables localization support
     */
    public ProjectBean( ApplicationTarget applicationTarget, WorkspaceTarget workspace, MessageResource messageResource )
    {
        if ( applicationTarget == null )
            throw new IllegalArgumentException( ERROR_REQUIRED_FIELD_APPLICATION_TARGET );
        if ( workspace == null )
            throw new IllegalArgumentException( ERROR_REQUIRED_FIELD_WORKSPACE_TARGET );
        if ( messageResource == null )
            throw new IllegalArgumentException( ERROR_REQUIRED_FIELD_MESSAGE_RESOURCE );

        uuid = UUID.randomUUID().toString();

        setMessageResource( messageResource );
        setApplicationTarget( applicationTarget );
        setWorkspaceTarget( workspace );
    }

    /**
     * Creates a deep copy of {@code clone}
     *
     * @param clone the prototype to clone; must not be null
     */
    public ProjectBean ( final Project clone )
    {
        if ( clone != null ) {
            setUUID(clone.getUUID());
            this.copyOf ( clone );
        }
    }


    /**
     * Constructs a ProjectBean instance using the generator's properties
     * to load the bean's properties
     *
      * @param builder a ProjectBuilder instance
     */
    protected ProjectBean ( final Builder<?> builder )
    {
        this( builder.applicationTarget, builder.workspaceTarget, builder.messageResource );

        // If the Builder captured a UUID, copy it into this object.
        // Our goal is to reflect all the state information captured in the Builder.
        if ( !StringHelper.isEmpty ( builder.uuid ))
            this.setUUID ( builder.uuid );

        this.setPluginName( builder.pluginName );
        this.setComponents( builder.components );
    }

    /**
     * Copies the mutable fields of the clone into {@code this} object.
     * @param clone the Project to clone
     */
    public void copyOf ( final Project clone )
    {
        if ( clone != null ) {
            setPluginName(clone.getPluginName());
            setApplicationTarget(clone.getApplicationTarget());
            setWorkspaceTarget(clone.getWorkspaceTarget());
            setMessageResource(clone.getMessageResource());
            setComponents(clone.getComponents());
        }
    }


    /**
     * Returns the UUID. Never returns null.
     * @return the UUID
     */
    final synchronized public String getUUID() {
        if ( uuid == null )
            uuid = UUID.randomUUID().toString();
        return uuid;
    }

    /**
     * Set the UUID.
     *
     * @param uuid the UUID to assign. A null value can be assigned, which essentially clears the UUID.
     */
    final synchronized public void setUUID ( final String uuid ) {
        this.uuid = uuid;
    }

    public synchronized ProjectBean deepCopy() {
        return new ProjectBean(this);
    }


    final synchronized public String getPluginName() {
        return pluginName;
    }

    /**
     * Assigns {@code name} to the plugin name
     * @param name the plugin name
     */
    final synchronized public void setPluginName(final String name) {
        pluginName = name;
    }

    /**
     * Returns the DevTest target of the plugin.  These are the versions of DevTest
     * for which plugin versions will be generated.
     *
     * @return the DevTest target of the plugin.
     */
    final synchronized public ApplicationTarget getApplicationTarget() {
        return applicationTarget;
    }

    /**
     * Assigns the fully qualified path to the DevTest home directory.
     *
     * @param applicationTarget the DevTest versions to be supported by the plugin
     */
    final synchronized public void setApplicationTarget (final ApplicationTarget applicationTarget) {
        if ( applicationTarget == null )
            throw new IllegalArgumentException( ERROR_REQUIRED_FIELD_APPLICATION_TARGET );

        // Since we have no idea if the 'applicationTarget' is serializable and we
        // want to maintain our serialization contract, we clone the applicationTarget object.
        this.applicationTarget = new ApplicationTargetBean( applicationTarget );
    }

    /**
     * Returns the schema version of the project XML file
     * @return the schema version of the project XML file
     */
    final synchronized public String getVersion() {
        return version;
    }

    /**
     * Returns the OutputTargetBean, which tells the code generator where to write files.
     *
     * @return the OutputTarget for this project
     */
    final synchronized public WorkspaceTarget getWorkspaceTarget() {
        return workspaceTarget;
    }

    /**
     * Sets the workspaceTarget, which defines the directories where the project artifacts are written
     * by the code generator.
     *
     * @param workspace the workspace to which the generated code is written
     */
    final synchronized public void setWorkspaceTarget ( final WorkspaceTarget workspace )
    {
        if ( workspace == null )
            throw new IllegalArgumentException( ERROR_REQUIRED_FIELD_WORKSPACE_TARGET );

        // Since we have no idea if the 'workspace' object is serializable and we
        // want to maintain our serialization contract, we clone the workspace object.
        // For example, suppose WorkspaceTarget referenced an entity bean, problems would
        // show up later if this ProjectBean were serialized.
        this.workspaceTarget = new WorkspaceTargetBean( workspace );
    }

    /**
     * Returns the components to be generated.  Returns an empty list if no components are defined.
     *
     * @return the components to generate. Always returns at least an empty list; never returns null.
     */
    final synchronized public List<Component> getComponents() {
        if ( components == null )
            components = new LinkedList<>();
        return components;
    }

    /**
     * Sets the list of components to generate
     *
     * @param components the components to generate; can be null or empty
     */
    final synchronized public void setComponents ( final List<Component> components) {
        if ( components != null ) {
            getComponents().clear();
            for ( Component clone : components ) {
                this.getComponents().add ( (Component) clone.deepCopy() );
            }
        }
        else {
            this.components = new LinkedList<>();
        }
    }

    /**
     * Returns the MessageResource, or the default if the MessageResource has not been explicitly set
     * before this method call.
     * @return the MessageResource, which defines the class that provides localized message retrieval
     */
    final synchronized public MessageResource getMessageResource() {
        return messageResource;
    }
    final synchronized public void setMessageResource ( final MessageResource bean )
    {
        if ( bean == null )
            throw new IllegalArgumentException( "A MessageResource is required for a ProjectBean");

        // Since we have no idea if 'bean' is serializable and we want to maintain
        // our serialization contract, we make a copy of the 'bean' object.
        this.messageResource = new MessageResourceBean( bean );
    }

    /**
     * Returns the relative path of the messages.properties file
     * (a parent directory must be prepended to this to acquire a fully-qualified path).
     * @return the relative path to the messages.properties file
     */
    final public String getDefaultMessageBundleDirectory() {

        StringBuilder sb = new StringBuilder()
                                .append ( StringHelper.safeString( messageResource.getPackageName() ));

        return sb.toString().replace(".", "/");
    }

    /**
     * Accepts the {@code visitor}
     *
     * @param visitor the visitor; must not be null
     */
    @Override public void accept ( Visitor visitor ) {
        visitor.visit ( this );
    }




    public boolean canEqual ( final Object other ) {
        return ( other instanceof Project);
    }


    /**
     * Compares {@code other} for equality to this project.
     *
     * Two {@code ProjectBean}s are considered equal if all their instance variables match.
     *
     * Naturally, if {@code other} is not a ProjectBean, this method returns {@code false}.
     *
     * @param other the entity tested for equality against {@code this}
     * @return true if {@code this} and {@code other} are equal.
     */
    @Override public boolean equals ( Object other ) {
        if ( this == other ) return true;
        if ( other == null ) return false;

        if ( other instanceof Project) {
            Project that = (Project) other;

            return that.canEqual ( this )
                    && EqualityHelper.areEqual ( this.getUUID(), that.getUUID() )
                    && EqualityHelper.areEqual ( this.getVersion(), that.getVersion() )
                    && EqualityHelper.areEqual ( this.getPluginName(), that.getPluginName() )
                    && EqualityHelper.areEqual ( this.getApplicationTarget(), that.getApplicationTarget())
                    && EqualityHelper.areEqual ( this.getWorkspaceTarget(), that.getWorkspaceTarget() )
                    && EqualityHelper.areEqual ( this.getComponents(), that.getComponents() )
                    && EqualityHelper.areEqual ( this.getMessageResource(), that.getMessageResource() );

        }
        return false;
    }

    @Override synchronized public int hashCode() {
        final int prime = 67;

        int result = 1;

        if ( uuid != null )
            result = prime * result + uuid.hashCode();
        result = prime * result + version.hashCode();
        if ( pluginName != null )
            result = prime * result + pluginName.hashCode();
        if ( applicationTarget != null )
            result = prime * result + applicationTarget.hashCode();
        if ( workspaceTarget != null )
            result = prime * result + workspaceTarget.hashCode();
        if ( components != null )
            result = prime * result + HashCodeHelper.aggregateHashCode( components );
        if ( messageResource != null )
            result = prime * result + messageResource.hashCode();

        result = prime * result + getMessageResource().hashCode();

        return result;
    }

    /**
     * The Builder for a ProjectBean
     * @param <T> the type of entity being built
     */
    public static abstract class Builder<T extends Builder<T>> {
        private String uuid;
        private String pluginName;
        private ApplicationTarget applicationTarget;
        private WorkspaceTarget workspaceTarget;
        private List<Component> components = new LinkedList<>();
        private MessageResource messageResource;

        protected abstract T self();

        public T withPluginName(final String name) {
            this.pluginName = name;
            return self();
        }
        public T withApplicatonTarget(final ApplicationTarget applicationTarget) {
            this.applicationTarget = applicationTarget;
            return self();
        }
        public T withWorkspaceTarget ( final WorkspaceTarget workspace ) {
            this.workspaceTarget = workspace;
            return self();
        }

        public T withMessageResource ( final MessageResource resource ) {
            this.messageResource = resource;
            return self();
        }

        /**
         * Adds a single component to the project.
         *
         * @param component the component to add
         *
         * @return the Builder
         */
        public T withComponent ( final Component component ) {
            if ( component != null )
                this.components.add ( component );
            return self();
        }




        /**
         * Copies all fields from prototype into the generator
         * @param prototype the prototype
         * @return the Builder
         */
        @SuppressWarnings("unused")
        public T copyOf ( final Project prototype )
        {
            if ( prototype != null ) {
                this.uuid = prototype.getUUID();
                this.components = prototype.getComponents();
                this.workspaceTarget = prototype.getWorkspaceTarget();
                this.pluginName = prototype.getPluginName();
                this.applicationTarget = prototype.getApplicationTarget();
                this.messageResource = prototype.getMessageResource();
            }
            return self();
        }


        /**
         * Create a ProjectBean instance based on this generator's properties
         * @return a ProjectBean who's properties match those of this generator
         */
        public ProjectBean build() {
            return new ProjectBean ( this );
        }
    }

    private static class ProjectBeanBuilder extends Builder<ProjectBeanBuilder> {

        protected ProjectBeanBuilder ( final ApplicationTarget application, final WorkspaceTarget workspace, final MessageResource messageResource )
        {
            withApplicatonTarget ( application );
            withWorkspaceTarget( workspace );
            withMessageResource( messageResource );
        }

        @Override
        protected ProjectBeanBuilder self() {
            return this;
        }
    }

    public static Builder<?> builder( final ApplicationTarget application, final WorkspaceTarget workspace, final MessageResource messageResource )
    {
        return new ProjectBeanBuilder( application, workspace, messageResource );
    }

    /**
     * Creates a Project
     * @param devTestHomeDirectory the fully-qualified path to the DevTest; for example, "C:/Program Files/CA/DevTest"
     * @param workspaceDirectory the fully-qualified path of the folder that will contain the generated code;
     *                           for example, "C:/Users/JoeProgrammer/Workspace"
     * @param messageResource the name of the message class that returns i18n strings; for example, "mmm.coffee.widgets.i18n.Messages"
     * @return the Builder instance
     */
    public static Builder<?> builder( final String devTestHomeDirectory, final String workspaceDirectory, final String messageResource )
    {
        Objects.requireNonNull(devTestHomeDirectory, "The devTestHomeDirectory cannot be null");
        Objects.requireNonNull(workspaceDirectory, "The workspaceDirectory cannot be null");
        Objects.requireNonNull(messageResource, "The classname of the MessageResource cannot be null");

        ApplicationTarget application = new ApplicationTargetBean( devTestHomeDirectory );
        WorkspaceTarget workspaceTarget = new WorkspaceTargetBean( workspaceDirectory );
        MessageResource messages = new MessageResourceBean();
        messages.setClassName( messageResource );

        return new ProjectBeanBuilder( application, workspaceTarget, messages );
    }


}
