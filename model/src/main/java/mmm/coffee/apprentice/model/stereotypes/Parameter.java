/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.model.stereotypes;


/**
 * A Parameter is a decorator for an instance variable that will
 * be placed in a generated class.  A Parameter has two data types:
 * its Java data type, such as 'String' or 'boolean', and its
 * Swing data type, such as JTextField or PrefillCombo. A Parameter
 * also has an XML tag, which is the tag name that will be used
 * by this Parameter in DevTest TST files.
 */
public interface Parameter extends BeanEntity {

    DataType getDataType();
    void setDataType(DataType type);

    String getLabelName();
    void setLabelName(String value);

    /**
     * {@inheritDoc}
     * @param that make {@code this} a copy of {@code that}
     */
    void copyOf ( Parameter that );


    /**
     * Indicates if this Parameter's value is text-based. For instance,
     * the value is rendered in a JTextField or PrefillCombo.
     * @return true if this Parameter's value is text-based
     */
    boolean isText();

    /**
     * Indicates if this Parameter is rendered as a boolean. For instance,
     * if the value is rendered as a radio button or checkbox, its boolean.
     *
     * @return true if this parameter's value is a boolean
     */
    boolean isBoolean();

    String getVariableName();
    void setVariableName(String variableName);


    /**
     * This is a helper method for the code generator.
     * @return an alternative variable name
     */
    String getAlternateVariableName();

    void setXmlTag ( String tag ) ;
    String getXmlTag();


    /**
     * Returns the display name of this parameter, as a Message
     * @return the display name of this parameter
     */
    Message getLabel() ;

    /**
     * Returns the tool-tip text of this parameter, as a Message
     * @return the tool-tip text of this parameter
     */
    Message getTooltip();

    /**
     * Set the display name of this parameter
     * @param labelBean the display name, in the form of a Message
     */
    void setLabel(Message labelBean);

    /**
     * Set the tool-tip of this parameter
     * @param tooltipBean the tool tip, in the form of a Message
     */
    void setTooltip(Message tooltipBean);


    String getGetterMethodName();
    void setGetterMethodName(String methodName);

    String getSetterMethodName();
    void setSetterMethodName( String methodName );


    /**
     * This is a hint to the code generator to use a prefillComboBox as the data entry editor
     * for this field
     *
     * @return if this field presents a PrefillComboBox as its editor
     */
    boolean getPresentsPrefillCombo();
    void setPresentsPrefillCombo(boolean flag);

    /**
     * This is a hint to the code generator to include a Find button.
     *
     * @return {@code true} if a {@code Find}-style button should be generated for this field
     */
    boolean getPresentsFindButton();
    void setPresentsFindButton(boolean flag) ;

    String getFindButtonName();


    /**
     * Returns the instance variable name given the Swing component that denotes this field.
     * Every field maps to a data entry field in an editor dialog. Each data entry field is
     * wrapped by a Swing component to capture the user input.  If a field is a String, a JTextField
     * is used to capture the input. This method returns the name of the variable of the JTextField.
     *
     * @return the variable name given the Swing component tied to this field
     */
    String getSwingVariableName();
    String getSwingType();

}
