/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.model.io;

import mmm.coffee.apprentice.model.beans.*;
import mmm.coffee.apprentice.model.helpers.StringHelper;
import mmm.coffee.apprentice.model.helpers.XMLHelper;
import mmm.coffee.apprentice.model.stereotypes.Component;
import mmm.coffee.apprentice.model.stereotypes.DataType;
import mmm.coffee.apprentice.model.stereotypes.Parameter;
import mmm.coffee.apprentice.model.stereotypes.ScopeSupport;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 * This class is responsible building a ProjectBean from a Project XML file
 */
public class ProjectReader {

    private static final Logger LOG = LogManager.getLogManager().getLogger( ProjectReader.class.getName() );

    /**
     * Reads the given Project XML file and returns the equivalent @{code ProjectBean}.
     * The {@code ProjectBean} is our equivalent of a Document object.
     *
     * @param fileName the Project XML file to read
     *
     * @return the {@code ProjectBean} equivalent of the project XML content.
     */
    public static ProjectBean read (String fileName )
    {
        ProjectBean projectBean = null;

        Document document = loadDocument (fileName);

        if ( document != null ) {
            Element projectTag = document.getDocumentElement();
            projectBean = parseProject( projectTag );
            }

        return projectBean;
    }

    /**
     * Read {@code file} into a {@code Document} object.
     * @param fileName the project XML file
     * @return the {@code Document} object for the Project XML
     */
    private static Document loadDocument(String fileName)
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            return builder.parse(new File(fileName));
            }
        catch (ParserConfigurationException | IOException | SAXException err) {
            err.printStackTrace();
            }

        return null;
    }


    private static ProjectBean parseProject ( Element projectTag )
    {
        if ( projectTag == null )
            return null;

        preconditionTagsMatch(projectTag, Tags.PROJECT);

        // --------------------------------------------------------------------------------
        // Step 1: Find the required entities: ApplicationTarget, WorkspaceTarget,
        // and MessageResource
        // --------------------------------------------------------------------------------

        // --------------------------------------------------------------------------------
        // Step 1.1: Parse the applicationTarget tag
        // --------------------------------------------------------------------------------
        Element targetTag = XMLHelper.findElement( projectTag, Tags.APPLICATION_TARGET);
        ApplicationTargetBean target = parseTarget ( targetTag );

        // --------------------------------------------------------------------------------
        // Step 1.2: Parse the workspaceTarget
        // --------------------------------------------------------------------------------
        Element outputTag = XMLHelper.findElement ( projectTag, Tags.WORKSPACE);
        WorkspaceTargetBean outputBean = parseOutput( outputTag );

        // --------------------------------------------------------------------------------
        // Step 1.3: Parse the messageResource
        // --------------------------------------------------------------------------------
        Element messageResourceNode = XMLHelper.findElement( projectTag, Tags.MESSAGE_RESOURCE );
        MessageResourceBean mrbean = parseMessageResourceElement( messageResourceNode );

        ProjectBean projectBean = new ProjectBean( target, outputBean, mrbean );

        // --------------------------------------------------------------------------------
        // Step 2: Parse the plugin name, default package, classNamePrefix, and devTestHome
        // --------------------------------------------------------------------------------
        projectBean.setPluginName(XMLHelper.findElementText(projectTag, Tags.PLUGIN_NAME));

        // --------------------------------------------------------------------------------
        // Step 3: Parse the componentList
        // --------------------------------------------------------------------------------
        Element componentNode = XMLHelper.findElement ( projectTag, Tags.COMPONENT_LIST );
        List<Component> componentList = parseComponentList ( componentNode );
        projectBean.setComponents(componentList);

        return projectBean;
    }

    /**
     * Parses an XML {@code output} stanza into an {@code OutputTargetBean}.
     * @param outputNode an Element with tagName='output'
     * @return the OutputTargetBean build from {@code outputNode}
     */
    private static WorkspaceTargetBean parseOutput ( Element outputNode )
    {
        String baseDir = XMLHelper.findElementText ( outputNode, Tags.BASE_DIRECTORY );
        String sourceDir = XMLHelper.findElementText ( outputNode, Tags.SOURCE_DIRECTORY);
        String resourceDir = XMLHelper.findElementText( outputNode, Tags.RESOURCE_DIRECTORY );
        String testResourceDir = XMLHelper.findElementText( outputNode, Tags.TEST_SOURCE_DIRECTORY );

        WorkspaceTargetBean outputBean = new WorkspaceTargetBean();
        outputBean.setBaseDirectory( baseDir  );
        outputBean.setSourceDirectory( sourceDir );
        outputBean.setResourceDirectory( resourceDir );
        outputBean.setTestSourceDirectory( testResourceDir );

        return outputBean;
    }

    /**
     * Parses a {@code target stanza and returns the {@code TargetBean} constructed with the stanza.
     *
     * @param targetTag the 'target' XML tag
     * @return the equivalent TargetBean
     */
    private static ApplicationTargetBean parseTarget ( Element targetTag )
    {
        if ( targetTag == null )
            return null;

        String homeDirectory = XMLHelper.findElementText(targetTag, Tags.HOME_DIRECTORY);
        String version = XMLHelper.findElementText(targetTag, Tags.VERSION);

        return new ApplicationTargetBean( homeDirectory, version );
    }


    /**
     * Parses the {@code componentList} stanza. The componentList contains the
     * custom steps, filters, assertions, etc. that are being generated.
     *
     * @param componentListTag the Element for the <i>componentList</i> tag.
     * @return the components of this project
     */
    private static List<Component> parseComponentList ( Element componentListTag )
    {
        if ( componentListTag == null )
            return null;

        LinkedList<Component> componentList = new LinkedList<>();

        NodeList nodeList = componentListTag.getChildNodes();
        int count = nodeList.getLength();

        for ( int i = 0; i < count; i ++ ) {
            Node node = nodeList.item(i);

            if ( ! (node instanceof Element) ) {
                // if its not an Element, skip it
                continue;
                }

            Element element = (Element)node;
            String tagName = element.getTagName();

            switch ( tagName ) {
                case Tags.STEP:
                case Tags.DATASET:
                case Tags.COMPANION:
                    MVCBean bean = parseModelLayerElement(element);
                    componentList.add(bean);
                    break;

                case Tags.ASSERTION:
                case Tags.FILTER:
                    MVCBean bean2 = parseScopeSupportElement ( element );
                    componentList.add(bean2);
                    break;

                case Tags.DATA_PROTOCOL:
                    DataProtocolBean dphBean = parseDataProtocolElement(element);
                    componentList.add ( dphBean );
                    break;

                case Tags.MESSAGE_RESOURCE:
                    MessageResourceBean mrBean = parseMessageResourceElement ( element );
                    componentList.add ( mrBean );
                    break;

                default:
                    LOG.warning ("An unrecognized tagName was encountered: " + tagName );
                    break;
            }

        }
        return componentList;
    }

    /**
     * Parses the <i>step</i> tag.
     *
     * @param element the Element for the <i>step</i> tag
     * @return a StepBean loaded with the data from the tag
     */
    private static MVCBean parseModelLayerElement(Element element)
    {
        if ( element == null )
            return null;


        MVCBean bean = createBeanForElement ( element.getTagName() );

        if ( bean == null )
            return null;

        String className = XMLHelper.getAttributeText(element, Tags.CLASSNAME);
        if ( className != null )
            bean.setClassName( className );

        bean.setGenerateCode( Boolean.parseBoolean ( XMLHelper.getAttributeText( element, Tags.GENERATE )));

        Element editorTag = XMLHelper.findElement( element, Tags.EDITOR);
        if ( editorTag != null ) {
            ComponentBean editorBean = parseComponent(editorTag);
            bean.setEditor ( editorBean );
        }

        Element controllerTag = XMLHelper.findElement( element, Tags.CONTROLLER );
        if ( controllerTag != null ) {
            ComponentBean controllerBean = parseComponent(controllerTag);
            bean.setController ( controllerBean );
        }

        Element paramListTag = XMLHelper.findElement(element, Tags.PARAMETER_LIST);

        List<Parameter> fields = parseParameterList(paramListTag);

        bean.setParameters(fields);

        return bean;
    }

    /**
     * Parses an element that denotes a ScopeSupport bean.  A ScopeSupport bean
     * is a ModelLayerBean with the extra attributes of local scope and global scope.
     *
     * We do take some liberties with this method. We know that only FilterBeans and
     * AssertionBeans implement the ScopeSupport interface.
     *
     * @param element the node being parsed
     * @return the materialized ModelLayerBean
     */
    private static MVCBean parseScopeSupportElement (Element element)
    {
        if ( element == null )
            return null;

        MVCBean bean = parseModelLayerElement(element);
        try {
            ScopeSupport ss = (ScopeSupport) bean;

            String globalScope = XMLHelper.findElementText(element, Tags.GLOBAL_SCOPE);
            String localScope = XMLHelper.findElementText(element, Tags.LOCAL_SCOPE);

            ss.setGlobalScope(Boolean.valueOf(globalScope));
            ss.setLocalScope(Boolean.valueOf(localScope));
        }
        catch (ClassCastException cce) {
            // If we get a ClassCastException, we'll return the ModelLayerBean w/o the ScopeSupport fields.
            // A ClassCastException indicates there may be a bug.  See the createBeanForElement method and
            // make sure its returning a bean that implements ScopeSupport for this tag.
//            LOG.warning ("The bean object created for tag {} does implement ScopeSupport and caused this exception: {}",
//                      element.getTagName(), cce.getMessage());
        }

        return bean;
    }

    private static MVCBean createBeanForElement( String tagName )
    {
        switch ( tagName ) {
            case Tags.STEP:
                return new StepBean();
            case Tags.ASSERTION:
                return new AssertionBean();
            case Tags.FILTER:
                return new FilterBean();
            case Tags.COMPANION:
                return new CompanionBean();
            case Tags.DATASET:
                return new DataSetBean();
        }
//        LOG.warn ("A tag was encountered that does not translate into a bean: {}", tagName);
        return null;
    }

    /**
     * Parses the <i>parameterList</i> tag
     * @param parameterListTag the XML element for the tag {@code PARAMETER_LIST}
     * @return the Parameters found in the tag. Returns null if {@code parameterListTag} is null.
     */
    private static List<Parameter> parseParameterList(Element parameterListTag)
    {
        if (parameterListTag == null)
            return null;
        preconditionTagsMatch( parameterListTag, Tags.PARAMETER_LIST);

        LinkedList<Parameter> resultSet = new LinkedList<>();

        NodeList nodeList = parameterListTag.getElementsByTagName( Tags.PARAMETER);

        // Iterate through all the <field> tags contained in the <fieldList>
        int count = nodeList.getLength();

        for ( int i = 0; i < count; i++ ) {
            Element parameterTag = (Element) nodeList.item(i);
            ParameterBean parameterBean = parseParameter(parameterTag);
            resultSet.add ( parameterBean );
        }

        return resultSet;
    }

    /**
     * Parses a {@code parameter} tag
     *
     * @param parameterTag the XML Element for the tag {@code PARAMETER}
     * @return the {@code ParameterBean} equivalent to the tag
     */
    private static ParameterBean parseParameter(Element parameterTag)
    {
        if ( parameterTag == null )
            return null;

        preconditionTagsMatch(parameterTag, Tags.PARAMETER);

        ParameterBean bean = new ParameterBean();

        bean.setVariableName(XMLHelper.getAttributeText(parameterTag, Tags.VARIABLE_NAME));

        String sType = XMLHelper.getAttributeText( parameterTag, Tags.TYPE);
        bean.setDataType(DataType.decode(sType));

        String sUsePrefill = XMLHelper.findElementText( parameterTag, Tags.PRESENTS_PREFILL_COMBO);
        bean.setPresentsPrefillCombo(Boolean.getBoolean(sUsePrefill));

        bean.setLabelName(XMLHelper.findElementText(parameterTag, Tags.LABEL_NAME));

        bean.setGetterMethodName( XMLHelper.findElementText( parameterTag, Tags.GET_METHOD ));
        bean.setSetterMethodName( XMLHelper.findElementText( parameterTag, Tags.SET_METHOD ));

        // Read the <label key="">value</label> tag
        Element labelTag = XMLHelper.findElement(parameterTag, Tags.LABEL);
        if ( labelTag != null ) {
            MessageBean labelBean = parseMessage ( labelTag );
            bean.setLabel( labelBean );
        }

        // Read the <tooltip key="">value</tooltip> tag
        Element tooltipTag = XMLHelper.findElement(parameterTag, Tags.TOOLTIP);
        if ( tooltipTag != null ) {
            MessageBean tooltipBean = parseMessage ( tooltipTag );
            bean.setTooltip ( tooltipBean );
        }

        return bean;
    }

    /**
     * Parses the {@code element} into a {@code ClassBean}
     * @param element the XML element for a component tag, such as a Step, Assertion, Filter, etc.
     * @return the {@code ClassBean} equivalent of {@code element}
     */
    private static ComponentBean parseComponent (Element element)
    {
        if ( element == null )
            return null;

        ComponentBean bean = new ComponentBean();
        bean.setClassName( XMLHelper.getAttributeText(element, Tags.CLASSNAME) );
        bean.setGenerateCode( Boolean.parseBoolean ( XMLHelper.getAttributeText( element, Tags.GENERATE )));
        return bean;
    }

    /**
     * Parses tags that follow the pattern <tag-name key='some key'>some value</tag-name>.
     * Tags known to follow this pattern are <i>label</i> and <i>tooltip</i>.
     *
     * @param element the XML element of either a {@code Tags.LABEL} or {@code Tags.TOOLTIP} tag.
     * @return the MessageBean created with the content of the tag; returns null if {@code element} is null
     */
    private static MessageBean parseMessage ( Element element )
    {
        if ( element == null )
            return null;

        MessageBean msgBean = new MessageBean();

        msgBean.setKey(XMLHelper.getAttributeText(element, Tags.KEY));
        msgBean.setValue(element.getTextContent());

        return msgBean;
    }

    /**
     * Parses an <i>assertion</i> stanza
     *
     * @param element the element for the <i>assertion</i> tag
     * @return an AssertionBean initialized by {@code element}
     */
    private static DataProtocolBean parseDataProtocolElement ( Element element )
    {
        if ( element == null )
            return null;

        DataProtocolBean bean = new DataProtocolBean();
        bean.setClassName ( XMLHelper.getAttributeText( element, Tags.CLASSNAME ));

        String req = XMLHelper.findElementText( element, Tags.REQUEST_PROTOCOL );
        String rsp = XMLHelper.findElementText( element, Tags.RESPONSE_PROTOCOL );
        String displayName = XMLHelper.findElementText( element, Tags.DISPLAY_NAME );
        String desc = XMLHelper.findElementText( element, Tags.DESCRIPTION );

        bean.setIsRequestSide ( Boolean.valueOf( req ));
        bean.setIsResponseSide ( Boolean.valueOf( rsp ));
        bean.setDescription(StringHelper.safeString(desc));
        bean.setDisplayName(StringHelper.safeString(displayName));

        return bean;
    }

    /**
     * Parses {@code element} into a {@code MessageResourceBean}.}
     *
     * @param element the MessageResource stanza
     * @return the {@code MessageResourceBean} equivalent of {@code element}
     */
    private static MessageResourceBean parseMessageResourceElement ( Element element )
    {
        if ( element == null )
            return null;

        MessageResourceBean bean = new MessageResourceBean();

        bean.setClassName(XMLHelper.getAttributeText(element, Tags.CLASSNAME));
        bean.setGenerateCode(Boolean.parseBoolean(XMLHelper.getAttributeText(element, Tags.GENERATE)));

        return bean;
    }

    /**
     * Verifies whether the nodeName of {@code element} is {@code expectedTag}.
     * @param element the element to check
     * @param expectedTag the expected nodeName of {@code element}
     */
    private static void preconditionTagsMatch (Element element, String expectedTag)
    {
        if ( !element.getNodeName().equals(expectedTag))
            throw new IllegalArgumentException( "Expected an XML tag of '" + expectedTag + "' but instead encountered '" + element.getNodeName()+ "'");
    }


}
