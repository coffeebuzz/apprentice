/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.model.beans;

import mmm.coffee.apprentice.model.stereotypes.Step;
import mmm.coffee.apprentice.model.visitor.Visitor;

import java.io.Serializable;

/**
 * A {@code StepBean} is a Java bean for a DevTest Step plugin.
 */
public class StepBean extends MVCBean implements Step, Serializable {

    static final long serialVersionUID = 4873381052183200204L;

    /**
     * Default constructor
     */
    public StepBean()
    {
        // empty
    }
    public StepBean ( final Step clone )
    {
        super(clone);
    }

    /**
     * Copies the state of {@code step} into {@code this} object
     *
     * @param step the object imitate; if null, the request is quietly ignored
     */
    public void copyOf ( Step step )
    {
        if ( step != null )
            super.copyOf ( step );
    }

    /**
     * Returns a deep copy of this StepBean object
     *
     * @return a deep copy of us
     */
    public StepBean deepCopy() {
        return new StepBean(this);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append ("Step {");
        sb.append (" classname: '").append(getClassName()).append ("', ");
        sb.append (" generate: '").append(getGenerateCode()).append("' ");

        if ( getEditor() != null ) {
            sb.append (" editor { ");
            sb.append ( getEditor().toString() );
            sb.append (" }");
        }
        if ( getController() != null ) {
            sb.append (" controller { ");
            sb.append ( getController().toString() );
            sb.append ( " }");
        }

        sb.append ("}");
        return sb.toString();
    }


    /**
     * Accepts the {@code visitor} and invokes its {@code visit} method.
     *
     * @param visitor the object that wants to process this StepBean, must not be null.
     */
    public void accept ( Visitor visitor ) { visitor.visit ( this ); }

    public boolean canEqual ( final Object other ) {
        return ( other instanceof StepBean );
    }


    public static abstract class Builder<T extends Builder<T>> extends MVCBean.Builder<T> {

        public StepBean build() {
            // Because steps are the only entities that require an editor and
            // controller class to be defined, and in the interest of making the builder API
            // easier to use, we'll allow the programmer to define only the model-layer class
            // for a step and add a custom controller and editor class, with their default
            // names being the model-layer's name + "Controller" or "Editor".
            // For example, if the model layer is "mmm.coffee.custom.CustomStep",
            // then we'll create "mmm.coffee.custom.CustomStepEditor" and
            // "mmm.coffee.custom.CustomStepController"
            if ( this.getController() == null ) {
                String controllerClass = this.getClassName() + "Controller";
                this.withController( controllerClass );
            }
            if ( this.getEditor() == null ) {
                String editorClass = this.getClassName() + "Editor";
                this.withEditor( editorClass );
            }

            return new StepBean ( this );
        }


        /**
         * A convenience method to define the Step's Controller class.
         * The generated controller will have its generateCode flag enabled, meaning
         * the code generator will generate source code for this controller class.
         *
         * @param className the fully-qualified className of the step's controller class
         * @return the current Builder instance
         */
        public T withController ( String className )
        {
            ComponentBean controller = ComponentBean.builder( className )
                                            .withGenerateCode( true )
                                            .build();

            withController( controller );
            return self();
        }

        /**
         * A convenience method to define the Step's editor class. This editor class will have
         * its generateCode flag enabled, meaning the code generator will generate source code
         * for this editor class.
         *
         * @param className the fully-qualified className of this step's editor class.
         *
         * @return the current Builder instance
         */
        public T withEditor ( String className )
        {
            ComponentBean editor = ComponentBean.builder ( className )
                                        .withGenerateCode( true )
                                        .build();

            withEditor ( editor );
            return self();
        }


    }

    private static class StepBeanBuilder extends Builder<StepBeanBuilder> {

        /**
         * Constructor
         * @param className the classname of the model-layer class.  This may not be null
         *                  nor an empty string. An example value is 'mmm.coffee.step.MyCustomStep'.
         */
        protected StepBeanBuilder ( String className )
        {
            withClassName( className );
        }
        @Override
        protected StepBeanBuilder self() {
            return this;
        }
   }

    /**
     * Returns a Builder for building StepBean objects
     * @param className the classname for the model-layer class, such as {@code "mmm.coffee.step.CustomStep"}
     * @return a StepBean generator
     */
    public static Builder<?> builder( String className ) {
        return new StepBeanBuilder( className );
    }

    /**
     * Constructs a StepBean instance using the {@code generator}'s properties
     *
     * @param builder an instance of StepBeanBuilder
     */
    protected StepBean(Builder<?> builder) {
        super(builder);
    }
}
