/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.model.beans;

import mmm.coffee.apprentice.model.stereotypes.DataSet;
import mmm.coffee.apprentice.model.stereotypes.Parameter;
import mmm.coffee.apprentice.model.visitor.Visitor;

import java.io.Serializable;

/**
 * A Bean for a DataSet
 */
public class DataSetBean extends MVCBean implements DataSet, Serializable {

    static final long serialVersionUID = 7376274784219445468L;


    public static final String DEFAULT_EDITOR     = "com.itko.lisa.editor.DefaultDataSetEditor";
    public static final String DEFAULT_CONTROLLER = "com.itko.lisa.editor.DataSetController";

    /**
     * Default constructor
     */
    public DataSetBean() {
        setEditor( new ComponentBean( DEFAULT_EDITOR, false ));
        setController ( new ComponentBean( DEFAULT_CONTROLLER, false ));
    }

    /**
     * Creates a clone of {@code clone}.
     *
     * @param clone the prototype to copy; must not be null
     */
    public DataSetBean ( final DataSet clone )
    {
        super ( clone );
        if ( clone.getEditor() == null )
            setEditor ( new ComponentBean( DEFAULT_EDITOR, false ));

        if ( clone.getController() == null )
            setController ( new ComponentBean( DEFAULT_CONTROLLER, false ));
    }


    /**
     * Constructs a DataSetBean instance using the {@code generator}'s properties
     *
     * @param builder an instance of DataSetBeanBuilder
     */
    protected DataSetBean (Builder<?> builder) {
        super(builder);

        // If a custom controller was not specified, initialize with the default
        if ( builder.getController() == null )
            setController ( new ComponentBean( DEFAULT_CONTROLLER, false ));

        // If a custom editor was not specified, initialize with the default
        if ( builder.getEditor() == null )
            setEditor( new ComponentBean( DEFAULT_EDITOR, false ));
    }



    public DataSetBean deepCopy() {
        return new DataSetBean ( this );
    }


    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append ( "DataSetBean { ").append ( System.lineSeparator() );

        sb.append ( "classname: '").append ( getClassName() ).append ("',").append ( System.lineSeparator() );
        sb.append (" generate: '").append ( getGenerateCode() ).append ("',").append ( System.lineSeparator() );

        if (getEditor() != null ) {
            sb.append (" editor { ");
            sb.append ( getEditor().toString() );
            sb.append ( "}").append ( System.lineSeparator() );
        }

        if ( getController() != null ) {
            sb.append ( " controller { ");
            sb.append ( getController().toString() );
            sb.append ( "}").append ( System.lineSeparator() );
        }

        sb.append ( "fields: [");
        boolean needsComma = false;
        for ( Parameter field : getParameters()) {
            if ( needsComma )
                sb.append(", ");
            sb.append (field.toString());
            needsComma = true;
        }
        sb.append("]");

        sb.append(" }");

        return sb.toString();
    }

    /**
     * Accepts the {@code visitor}
     * @param visitor {@inheritDoc}
     */
    public void accept ( Visitor visitor ) {
        visitor.visit ( this );
    }


    /**
     * DataSets are considered equal if they will cause the same source code to be emitted.
     * @param other the object being compared
     * @return {@code true} if {@code other} is a {@code DataSet} that is equal to this {@code DataSet}
     */
    @Override synchronized public boolean equals ( Object other )
    {
        if (this == other) return true;
        if (other == null) return false;

        if (other instanceof DataSet) {
            DataSet that = (DataSet) other;
            return that.canEqual(this)
                && super.equals(that);
        }
        return false;
    }

    public boolean canEqual (final Object other ) {
        return ( other instanceof DataSet);
    }


    /**
     * Our basic Builder
     * @param <T> our DataSetBuilder type
     */
    public static abstract class Builder<T extends Builder<T>> extends MVCBean.Builder<T> {

        public DataSetBean build() {
            return new DataSetBean(this);
        }
    }

    private static class DataSetBeanBuilder extends Builder<DataSetBeanBuilder> {

        /**
         * Constructor
         *
         * @param className the className of the model-layer class. This may not be null
         *                  nor an empty string.
         */
        protected DataSetBeanBuilder(String className) {
            withClassName(className);
        }

        @Override
        protected DataSetBeanBuilder self() {
            return this;
        }

    }

    /**
     * Returns a Builder for building DataSetBean objects
     * @param className the name of the model-layer class for the data set, such as {@code "mmm.coffee.data.CustomDataSet"}
     * @return a DataSetBean generator
     */
    public static Builder<?> builder (String className)
    {
        return new DataSetBeanBuilder(className);
    }

}
