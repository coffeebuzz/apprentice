/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.model.beans;


import mmm.coffee.apprentice.model.helpers.EqualityHelper;
import mmm.coffee.apprentice.model.stereotypes.Message;
import mmm.coffee.apprentice.model.visitor.Visitor;

import java.io.Serializable;
import java.util.UUID;

/**
 * The {@code MessageResourceBean} denotes an object that maps
 * to an entry in a message resource bundle, specifically for localization/i18n support.
 *
 * String messages are stored in resource bundles as key/value pairs.
 * Naturally, {@code getKey()} returns the key part, and {@code getValue()} returns
 * the value part.
 */
public class MessageBean implements Message, Serializable {

    static final long serialVersionUID = -1509524314546496759L;



    private String key;
    private String value;
    private String uuid;

    /**
     * Default constructor
     */
    public MessageBean() {
        uuid = UUID.randomUUID().toString();
    }

    public MessageBean (final String key, final String value ) {
        this();
        this.key = key;
        this.value = value;
    }

    /**
     * Creates a clone
     *
     * @param clone the prototype to clone; must not be null
     */
    public MessageBean ( final Message clone )
    {
        this.uuid = clone.getUUID();
        this.key = clone.getKey();
        this.value = clone.getValue();
    }

    /**
     * Updates the properties of this MessageBean to match the properties of {@code copy}.
     * Essentially, this is a mechanisim to update a group of properties on this MessageBean
     * by providing a MessageBean that contains the new property values.
     *
     * @param copy the Message to copy
     */
    public void copyOf ( final Message copy )
    {
        if ( copy != null ) {
            this.key = copy.getKey();
            this.value = copy.getValue();
        }
    }

    /**
     * Creates a clone of this MessageBean that contains deep copies of every property
     * of this MessageBean.
     *
     * @return the clone of this MessageBean
     */
    public MessageBean deepCopy() {
        return new MessageBean( this );
    }

    /**
     * Returns the message's corresponding resource bundle key
     *
     * @return the message's corresponding resource bundle key
     */
    final public String getKey() {
        return key;
    }

    /**
     * Sets the resource bundle's key for this message
     *
     * @param key the resource bundle's key
     */
    final public void setKey(final String key) {
        this.key = key;
    }

    final public String getValue() {
        return value;
    }
    final public void setValue(final String value) {
        this.value = value;
    }

    public String getUUID() {
        return uuid;
    }
    public void setUUID(final String uuid) {
        this.uuid = uuid;
    }

    /**
     * Accepts the {@code visitor} (to enable the Visitor pattern)
     * @param visitor {@inheritDoc}
     */
    public void accept (Visitor visitor ) {
        visitor.visit ( this );
    }


    public boolean canEqual ( final Object other ) {
        return ( other instanceof MessageBean );
    }

    /**
     * MessageBeans are considered equal if their keys match and values match.
     * The UUID is irrelevant.
     * @param other {@inheritDoc}
     * @return {@inheritDoc}
     */
    @Override public boolean equals ( Object other ) {
        if ( this == other ) return true;
        if ( other == null ) return false;

        if ( other instanceof Message) {
            Message that = (Message) other;

            return that.canEqual(this)
                    && EqualityHelper.areEqual( this.getValue(), that.getValue() )
                    && EqualityHelper.areEqual( this.getKey(), that.getKey() );
        }
        return false;
    }

    @Override public int hashCode() {
        final int prime = 59;

        int result = 1;

        if ( key != null )
            result = prime * result + key.hashCode();
        if ( value != null )
            result = prime * result + value.hashCode();

        return result;
    }

    @Override public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append ( "messageBean { ");
        sb.append ("key: '").append(key).append("', ");
        sb.append ("value: '").append(value).append("' }");
        return sb.toString();
    }



    public static abstract class Builder<T extends Builder<T>> {

        private String key;
        private String value;

        protected abstract T self();

        public T withKey ( final String key ) {
            this.key = key;
            return self();
        }
        public T withValue ( final String value ) {
            this.value = value;
            return self();
        }

        public T copyOf ( final Message m )
        {
            this.key = m.getKey();
            this.value = m.getValue();
            return self();
        }

        public MessageBean build() {
            return new MessageBean ( this );
        }
    }

    private static class MessageBeanBuilder extends Builder<MessageBeanBuilder> {
        @Override
        protected MessageBeanBuilder self() {
            return this;
        }
    }

    /**
     * Returns a Builder for building StepBean objects
     * @return a StepBean generator
     */
    public static Builder<?> builder() {
        return new MessageBeanBuilder();
    }

    /**
     * Constructs a ParameterBean instance using the {@code generator}'s properties
     *
     * @param builder an instance of ParameterBeanBuilder
     */
    protected MessageBean (Builder<?> builder) {
        this();
        this.setKey ( builder.key );
        this.setValue ( builder.value );
    }

}
