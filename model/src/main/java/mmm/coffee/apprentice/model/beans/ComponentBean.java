/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.model.beans;


import mmm.coffee.apprentice.model.helpers.EqualityHelper;
import mmm.coffee.apprentice.model.helpers.StringHelper;
import mmm.coffee.apprentice.model.stereotypes.Component;
import mmm.coffee.apprentice.model.visitor.Visitor;

import java.io.Serializable;
import java.util.UUID;

/**
 * A ClassBean denotes objects that are transformed into a Java class.
 * As such, these objects have a className and packageName.
 */
public class ComponentBean implements Component, Serializable {

    static final long serialVersionUID = -4386393147473004816L;

    // If the 'generateCode' flag is not explicitly set, the default is this
    private static final boolean DEFAULT_GENERATE_CODE_FLAG = true;

    // the unique identifier of this instance.
    private String uuid;

    // the fully-qualified className of the component to generate,
    // such as "mmm.coffee.widget.CustomAssertion"
    private String className;

    // the fully-qualified test's className of the component to generate,
    // such as "mmm.coffee.widget.CustomAssertionTest"
    private String testClassName;

    // the packageName of the component to generate.
    // This is derived from the className
    private String packageName;

    // the simple className of the component to generate, such as 'CustomAssertion'
    private String simpleName;

    // the simple classname of the generated test, such as 'CustomAssertionTest'
    private String simpleTestName;

    // The default name of test classes is the simpleName + Tests,
    // for example: MyCustomStepTests.
    private static final String DEFAULT_TESTCASE_SUFFIX = "Tests";



    // This field indicates whether a Java class should be generated.
    // For example, a custom filter typically relies on the default filter editor and controller,
    // in which case, code is generated for the custom filter model object, but not for the
    // editor and controller.  The XML might look like this:
    // <filter classname="my.example.CustomFilter" generate="true">
    //   <editor classname="com.itko.lisa.editor.DefaultFilterEditor" generate="false" />
    //   <controller classname="com.itko.lisa.editor.DefaultFilterController" generate="false" />
    // </filter>
    private boolean generateCode = DEFAULT_GENERATE_CODE_FLAG;

    /**
     * Default constructor
     */
    public ComponentBean() {
        uuid = UUID.randomUUID().toString();
    }

    /**
     * Makes a deep copy of {@code clone}, including the UUID.
     *
     * @param clone the component being copied.
     */
    public ComponentBean(final Component clone)
    {
        if ( clone == null )
            throw new NullPointerException("The clone argument is null");

        setUUID(clone.getUUID());
        // Note: calling setClassName causes the packageName and simpleName to be set
        setClassName(clone.getClassName());
        setGenerateCode(clone.getGenerateCode());
        setTestClassName( clone.getTestClassName() );
    }


    /**
     * Constructor
     * @param classname the fully qualified classname of this object
     *
     * @param generateCode {@code true} if the code generator should generate this class
     */
    public ComponentBean(final String classname, final boolean generateCode)
    {
        // Initialize our UUID
        this();

        this.className = classname;
        this.generateCode = generateCode;
        this.testClassName = className + DEFAULT_TESTCASE_SUFFIX;
    }

    /**
     * Constructs a ClassBean using data retrieved from the {@code generator}.
     * This allows ClassBean generator subclasses to have the ClassBean generator load
     * the ClassBean instance variables, while the subclass generator handles its own
     * instance variables. See the subclasses of ClassBean for examples of how
     * this is used.
     *
     * @param builder an instance of ClassBeanBuilder, or one of its subclasses
     */
    protected ComponentBean(Builder<?> builder)
    {
        // Initialize our UUID
        this();

        this.setClassName ( builder.className );
        this.setTestClassName( builder.className + DEFAULT_TESTCASE_SUFFIX );
        this.setGenerateCode( builder.generateFlag );
    }

    /**
     * Updates the state of this object to match the state of {@code delta}.
     * This object's UUID is not modified.
     *
     * @param delta contains the new instance variable values.
     */
    public void copyOf ( final Component delta )
    {
        if ( delta == null )
            return;

        // Note: packageName and simpleName are dynamically determined by parsing the className.
        // Therefore, its sufficient to only call setClassName, since that method will parse the value
        // and update the packageName and simpleName
        this.setClassName( delta.getClassName() );
        this.setGenerateCode( delta.getGenerateCode() );
        this.setTestClassName( delta.getTestClassName() );
    }



    final public String getUUID() {
        return uuid;
    }
    final public void setUUID ( final String uuid ) {
        this.uuid = uuid;
    }

    public ComponentBean deepCopy()
    {
        return new ComponentBean( this );
    }

    /**
     * Returns the classname
     * @return the classname
     */
    final public String getClassName() {
        return className;
    }

    /**
     * Assigns a fully qualified classname, such as "com.example.plugin.WidgetStep".
     * A side effect is: the packageName is updated to reflect the change.
     *
     * @param className the fully qualified className of the component to generate
     */
    final public void setClassName( final String className ) {
        this.className = className;

        updateClassNameDependents ( this.className );
    }

    /**
     * Returns the classname
     * @return the classname
     */
    final public String getTestClassName() {
        return testClassName;
    }

    /**
     * Assigns a fully qualified classname, such as "com.example.plugin.WidgetStep".
     * A side effect is: the packageName is updated to reflect the change.
     *
     * @param className the fully qualified className of the component to generate
     */
    final public void setTestClassName( final String className ) {
        this.testClassName = className;
    }

    /**
     * Returns the package of the component.
     *
     * This value is derived from the className, so no setter is provided.
     *
     * @return the package of the component
     */
    final public String getPackageName() {
        // If the package name has not been determined yet, do so
        if ( packageName == null )
            updateClassNameDependents ( this.className );

        // return the value
        return packageName;
    }


    /**
     * Returns the simple class name of the component. That is, the class name without the package prefix.
     * For example, for the class name "com.example.plugin.WidgetStep", getSimpleName() returns "WidgetStep".
     * (This is the same behavior of the java.lang.Class.getSimpleName() method.)
     *
     * This value is derived from the className, so no setter is provided.
     *
     * @return the classname without its package
     */
    final public String getSimpleName() {
        // if the simpleName has not been determined yet, do so
        if ( simpleName == null )
            updateClassNameDependents( this.className );

        // return the value
        return simpleName;
    }

    /**
     * Returns the simple class name of the test-side component. That is, the test's class name without the package prefix.
     * For example, for the class name "com.example.plugin.WidgetStepTest", getSimpleTestName() returns "WidgetStepTest".
     *
     * This value is derived from the test-side className, so no setter is provided.
     *
     * @return the unit test's classname without its package
     */
    final public String getSimpleTestName() {
        // if the simpleName has not been determined yet, do so
        if ( simpleTestName == null )
            updateClassNameDependents( this.className );

        // return the value
        return simpleTestName;
    }


    /**
     * Indicates whether code needs to be generated. If this ClassBean is a placeholder
     * for default editors or controllers already implemented in DevTest, this method
     * should return {@code false}.  In general, step's will have custom editors and
     * controllers generated, while companions, filters, and assertions will use the
     * default editors and controllers built into DevTest.
     *
     * @return {@code false} if this class is already built into DevTest and, therefore,
     * does not need to be generated.  Return {@code true} if a class file should be
     * generated.
     */
    final public boolean isGenerateCode() {
        return generateCode;
    }

    final public void setGenerateCode(final boolean flag) {
        this.generateCode = flag;
    }

    final public boolean getGenerateCode() {
        return generateCode;
    }

    @Override
    public String toString() {
        // Note: the output coincidentally looks like JSON. There is no requirement
        // for the output to be a syntactically correct JSON string.

        return new StringBuilder ( "{ ")
                        .append ( "classname : '").append( className ).append ("', ")
                        .append ( "generate : ").append( generateCode ).append(", ")
                        .append ( "testclass : '").append(testClassName).append("' ")
                        .append (" }")
                        .toString();
    }

    /**
     * Two ClassBeans are considered equal if their class names are equal and their
     * generateCode flags match.
     *
     * @param other the object being compared
     * @return {@code true} if {@code obj} equals this ClassBean.
     */
    @Override
    public boolean equals ( Object other ) {
        if ( this == other ) return true;
        if ( other == null ) return false;

        boolean result = false;
        if ( other instanceof Component) {
            Component that = (Component) other;
            result = that.canEqual(this)
                    && EqualityHelper.areEqual(this.getClassName(), that.getClassName())
                    && EqualityHelper.areEqual(this.getTestClassName(), that.getTestClassName())
                    && this.generateCode == that.getGenerateCode();
        }
        return result;
    }

    public boolean canEqual ( final Object other ) {
        return ( other instanceof Component);
    }



    @Override
    public int hashCode() {
        final int prime = 37;

        int result = 1;

        if ( className != null )
            result = prime * result + getClassName().hashCode();
        if ( testClassName != null )
            result = prime * result + getTestClassName().hashCode();

        result = prime * result + Boolean.toString( generateCode ).hashCode();

        return result;
    }

    /**
     * {@inheritDoc}
     * @param visitor {@inheritDoc}
     */
    public void accept ( Visitor visitor ) {
        visitor.visit ( this );
    }


    /**
     * Determines the packageName and simpleName, based on the className.
     *
     * @param className the fully-qualified class name, such as 'mmm.coffee.widget.SimpleStep'
     */
    private void updateClassNameDependents ( String className )
    {
        if ( className != null ) {
            int i = className.lastIndexOf(".");
            if ( i > 0 ) {
                // Handles the case of className is qualified with package, such as "com.example.Widget"
                packageName = className.substring(0, i);
                simpleName = className.substring(i + 1);
                simpleTestName = simpleName + DEFAULT_TESTCASE_SUFFIX;
            }
            else {
                // Handles the case of className has not package, such as "Widget"
                packageName = "";
                simpleName = className;
                simpleTestName = simpleName + DEFAULT_TESTCASE_SUFFIX;
            }
        }
    }


    // This pattern is inspired by this blog post:
    // https://weblogs.java.net/blog/emcmanus/archive/2010/10/25/using-builder-pattern-subclasses

    /**
     * Infrastructure to enable using the Builder pattern
     * @param <T> the concrete generator's type
     */
    public static abstract class Builder<T extends Builder<T>> {
        private String className;
        private boolean generateFlag = DEFAULT_GENERATE_CODE_FLAG;

        protected abstract T self();

        /**
         * Set the className.
         * @param className the full classname of a component, such as {@code "mmm.coffee.step.StepController"}
         * @return the Builder instance
         */
        public T withClassName(final String className) {
            if (!StringHelper.isValidJavaIdentifier( className ))
                throw new IllegalArgumentException("The className '"+StringHelper.safeString(className)+"' is not a valid Java identifier");

            this.className = className;
            return self();
        }

        protected final String getClassName()
        {
            return className;
        }

        /**
         * Set the generateCode flag.  When {@code flag} is true, a Java class will be emmitted by the
         * code generator for this class.  When {@code flag} is false, no Java code is emmitted for this
         * component.  Many components, like filters and assertions, have built-in controllers and
         * editors that are sufficient for most needs.  For those components, code generation of the
         * controller and editor can be disabled by setting {@code withGenerateCode (false)}.  Do be aware
         * that this code library already knows which components generally use built-in controllers
         * and editors so the developer using this library can rely on the library to set the
         * {@code generateCode} flag to a reasonable value.  When the default behavior is not the
         * desired behavior then, obviously, call this method to achieve the desired result.
         *
         * @param flag When this is {@code true}, java code will be emmitted for this class.
         * @return the Builder instance
         */
        public T withGenerateCode (final boolean flag) {
            this.generateFlag = flag;
            return self();
        }

        /**
         * Constructs the ComponentBean instance
         * @return the constructed ComponentBean
         */
        public ComponentBean build() {
            return new ComponentBean( className, generateFlag );
        }
    }


    private static class ClassBeanBuilder extends Builder<ClassBeanBuilder> {

        protected ClassBeanBuilder ( String className )
        {
            withClassName( className );
        }

        @Override
        protected ClassBeanBuilder self() {
            return this;
        }
    }

    /**
     * Returns a Builder capable of building ClassBean objects
     * @param className the classname of the component, such as {@code "mmm.coffee.step.StepEditor"}
     * @return a Builder capable of building ClassBean objects
     */
    public static Builder<?> builder( String className )
    {
        return new ClassBeanBuilder( className );
    }



}
