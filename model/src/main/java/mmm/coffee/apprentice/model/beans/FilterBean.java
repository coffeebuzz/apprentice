/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.model.beans;

import mmm.coffee.apprentice.model.stereotypes.Filter;
import mmm.coffee.apprentice.model.visitor.Visitor;

import java.io.Serializable;

/**
 * A {@code FilterBean} contains the meta-data of a Filter
 */
public class FilterBean extends MVCBean implements Filter, Serializable {

    static final long serialVersionUID = -5943768552385527097L;

    public static final String DEFAULT_EDITOR = "com.itko.lisa.editor.DefaultFilterEditor";
    public static final String DEFAULT_CONTROLLER = "com.itko.lisa.editor.FilterController";

    private boolean isGlobalScope = DEFAULT_IS_GLOBAL_SCOPE;
    private boolean isLocalScope = DEFAULT_IS_LOCAL_SCOPE;

    public static final boolean DEFAULT_IS_GLOBAL_SCOPE = false;
    public static final boolean DEFAULT_IS_LOCAL_SCOPE = true;


    /**
     * Default constructor
     */
    public FilterBean() {
        setController( new ComponentBean( DEFAULT_CONTROLLER, false ));
        setEditor ( new ComponentBean( DEFAULT_EDITOR, false ));
    }

    /**
     * Create a clone of {@code clone}
     *
     * @param clone the prototype to clone; must not be null
     */
    public FilterBean ( final Filter clone )
    {
        super ( clone );
        setLocalScope( clone.getLocalScope() );
        setGlobalScope( clone.getGlobalScope() );

        if ( clone.getController() == null )
            setController ( new ComponentBean( DEFAULT_CONTROLLER, false ));
        if ( clone.getEditor() == null )
            setEditor ( new ComponentBean( DEFAULT_EDITOR, false ));
    }

    public FilterBean deepCopy() {
        return new FilterBean ( this );
    }


    final public void setGlobalScope(final boolean isGlobal) {
        this.isGlobalScope = isGlobal;
    }
    final public boolean getGlobalScope() {
        return this.isGlobalScope;
    }
    final public boolean isGlobalScope() {
        return isGlobalScope;
    }

    final public void setLocalScope(final boolean isLocal) {
        this.isLocalScope = isLocal;
    }
    final public boolean getLocalScope() {
        return isLocalScope;
    }
    final public boolean isLocalScope() {
        return isLocalScope;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append ("Filter {");
        sb.append (" classname: '").append(getClassName()).append ("', ");
        sb.append (" generate: '").append(getGenerateCode()).append("' ");
        sb.append (" isGlobalScope: '").append ( isGlobalScope ).append("'");
        sb.append (" isLocalScope: '").append ( isLocalScope ).append("'");

        if ( getEditor() != null ) {
            sb.append (" editor { ");
            sb.append ( getEditor().toString() );
            sb.append ("}");
        }
        if ( getController() != null ) {
            sb.append (" controller {") ;
            sb.append ( getController().toString() );
            sb.append ( "}");
        }

        sb.append("}");
        return sb.toString();
    }

    /**
     * Accepts the {@code visitor}
     *
     * @param visitor the visitor
     */
    @Override public void accept ( Visitor visitor ) {
        visitor.visit ( this );
    }

    @Override public boolean equals ( Object other ) {
        if ( this == other ) return true;
        if ( other == null ) return false;

        if ( other instanceof Filter) {
            Filter that = (Filter) other;
            return that.canEqual ( this )
                    && super.equals ( that )
                    && this.getLocalScope() == that.getLocalScope()
                    && this.getGlobalScope() == that.getGlobalScope();

        }
        return false;
    }

    public boolean canEqual ( final Object other ) {
        return ( other instanceof Filter);
    }

    @Override public int hashCode() {
        final int prime = 29;

        int result = super.hashCode();

        result = prime * result + Boolean.toString( getLocalScope() ).hashCode();
        result = prime * result + Boolean.toString( getGlobalScope() ).hashCode();

        return result;
    }


    public static abstract class Builder<T extends Builder<T>> extends MVCBean.Builder<T> {
        private boolean isLocalScope;
        private boolean isGlobalScope;

        public T withLocalScope ( boolean f ) {
            this.isLocalScope = f;
            return self();
        }
        public T withGlobalScope ( boolean f ) {
            this.isGlobalScope = f;
            return self();
        }
        public FilterBean build() {
            return new FilterBean ( this );
        }
    }

    private static class FilterBeanBuilder extends Builder<FilterBeanBuilder> {

        /**
         * Constructor
         * @param className the classname of the model-layer class. This is a required
         *                  field and may not be null nor an empty string.
         *                  An example value is 'mmm.coffee.filter.MyCustomFilter'
         */
        protected FilterBeanBuilder (String className)
        {
            withClassName( className );
        }

        @Override
        protected FilterBeanBuilder self() {
            return this;
        }
    }

    /**
     * Returns a Builder for building StepBean objects
     * @param className the name of the model-layer class for the filter, such as {@code "mmm.coffee.filter.CustomFilter"}
     * @return a StepBean generator
     */
    public static Builder<?> builder( String className ) {
        return new FilterBeanBuilder( className );
    }

    /**
     * Constructs a FilterBean instance using the {@code generator}'s properties
     *
     * @param builder an instance of FilterBeanBuilder
     */
    protected FilterBean(Builder<?> builder) {
        super(builder);
        this.setGlobalScope( builder.isGlobalScope );
        this.setLocalScope( builder.isLocalScope );

        if ( builder.getController() == null )
            setController ( new ComponentBean( DEFAULT_CONTROLLER, false ));
        if ( builder.getEditor() == null )
            setEditor ( new ComponentBean( DEFAULT_EDITOR, false ));
    }

}
