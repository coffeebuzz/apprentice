/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.model.beans;

import mmm.coffee.apprentice.model.stereotypes.Component;
import mmm.coffee.apprentice.model.stereotypes.MessageResource;
import mmm.coffee.apprentice.model.visitor.Visitor;

/**
 * This bean maps to the Messages.java file.  The Messages.java file implements localization/i18n support.
 */
public class MessageResourceBean extends ComponentBean implements MessageResource {

    static final long serialVersionUID = 2287341876470366747L;



    /**
     * Default constructor.  If this is used, the caller is responsible for setting the className of this object.
     * Without a className, the code generator will not generate the source for this class.
     *
     */
    public MessageResourceBean() {
        // Turn on code generation by default.
        setGenerateCode(true);
    }


    /**
     * Creates a clone
     *
     * @param clone the prototype to clone; must not be null
     */
    public MessageResourceBean ( final Component clone )
    {
        super ( clone );
    }


    /**
     * Constructs a MessageResourceBean object using the {@code generator}'s properties
     * to load the MessageResourceBean's properties.
     *
     * @param builder an instance of a MessageResourceBeanBuilder, or one of its subclasses
     */
    protected MessageResourceBean (Builder<?> builder )
    {
        super ( builder );
    }


    public MessageResourceBean deepCopy()
    {
        return new MessageResourceBean( this );
    }


    /**
     * Implements the Visitor pattern
     *
     * @param visitor the visitor; not nullable
     */
    public void accept ( Visitor visitor )
    {
        visitor.visit (this);
    }

    public boolean canEqual (final Object other ) {
        return ( other instanceof  MessageResourceBean );
    }


    /**
     * Defines our abstract Builder. See the ClassBean for an explanation of pattern.
     * @param <T> the type of object created by the builder
     */
    public static abstract class Builder<T extends Builder<T>> extends ComponentBean.Builder<T> {

        public MessageResourceBean build() {
            return new MessageResourceBean ( this );
        }
    }

    private static class MessageResourceBeanBuilder extends Builder<MessageResourceBeanBuilder> {
        protected MessageResourceBeanBuilder ( String className )
        {
            withClassName ( className );
            withGenerateCode( true );
        }

        @Override
        protected MessageResourceBeanBuilder self() {
            return this;
        }
    }

    /**
     * Creates a class that provides localized string resolution.
     * @param className the name of the class to generate, such as {@code "mmm.coffee.i18n.Messages"}
     * @return the Builder instance
     */
    public static Builder<?> builder( String className )
    {
        return new MessageResourceBeanBuilder( className );
    }

}
