/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.model.beans;

import mmm.coffee.apprentice.model.stereotypes.Companion;
import mmm.coffee.apprentice.model.visitor.Visitor;

import java.io.Serializable;

/**
 * An {@code CompanionBean} contains the data about a custom companion.
 */
public class CompanionBean extends MVCBean implements Companion, Serializable {

    static final long serialVersionUID =  -9179735870554832813L;


    // TODO: RESEARCH: DevTest has a CompanionController and CompactCompanionController
    // a) what's the difference between them?
    // b) when does the user pick one over the other?
    // c) how do I present the choice of which one to pick to the user?
    // Note: a built-in controller will almost always be used, but a custom editor is usually written
    public static final String DEFAULT_CONTROLLER = "com.itko.lisa.editor.CompanionController";
    public static final String DEFAULT_EDITOR = "com.itko.lisa.editor.SimpleCompanionEditor";

    /**
     * Default constructor
     */
    public CompanionBean() {
        setController( new ComponentBean( DEFAULT_CONTROLLER, false ));
        setEditor ( new ComponentBean( DEFAULT_EDITOR, false ));
    }


    /**
     * Creates an instance using the generator to load the properties.
     *
     * @param builder the generator
     */
    protected CompanionBean ( Builder<?> builder )
    {
        super ( builder );
        if ( builder.getController() == null )
            setController( new ComponentBean( DEFAULT_CONTROLLER, false ));

        if ( builder.getEditor() == null )
            setEditor ( new ComponentBean( DEFAULT_EDITOR, false ));
    }


    public CompanionBean ( final Companion clone )
    {
        super ( clone );
    }

    public CompanionBean deepCopy() {
        return new CompanionBean ( this );
    }


    @Override public String toString() {
        // Note: the output coincidentally looks like a JSON string.
        // There is no requirement for the output to be a syntactically correct JSON string

        StringBuilder sb = new StringBuilder();
        sb.append ("Companion { ");
        sb.append (" classname: '").append(getClassName()).append ("', ");
        sb.append (" generate: '").append(getGenerateCode()).append("' ");

        if ( getEditor() != null ) {
            sb.append (" editor { ");
            sb.append ( getEditor().toString() );
            sb.append ( " }");
        }
        if ( getController() != null ) {
            sb.append (" controller { ");
            sb.append ( getController().toString() );
            sb.append (" }");
        }
        sb.append ("}");
        return sb.toString();
    }

    /**
     * Accepts the {@code visitor}
     *
     * @param visitor the visitor; must not be null
     */
    public void accept ( Visitor visitor ) {
        visitor.visit ( this );
    }

    /**
     * CompanionBeans can only equal other CompanionBeans.
     *
     * @param other the object being compared for equality
     * @return true if {@code other} is a CompanionBean
     */
    public boolean canEqual ( final Object other ) {
        return ( other instanceof Companion);
    }


    public static abstract class Builder<T extends Builder<T>> extends MVCBean.Builder<T> {

        public CompanionBean build() {
            return new CompanionBean (this);
        }
    }

    private static class CompanionBeanBuilder extends Builder<CompanionBeanBuilder> {

        /**
         * Constructor
         * @param className the class name of the model-layer class.  This is a required
         *                  field and may not be null nor an empty string.
         */
        protected CompanionBeanBuilder ( String className )
        {
            withClassName( className );
        }

        @Override
        public CompanionBeanBuilder self() { return this; }
    }

    public static Builder<?> builder( String className )
    {
            return new CompanionBeanBuilder( className );
    }
}
