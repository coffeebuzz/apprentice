/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.model.beans;

import mmm.coffee.apprentice.model.stereotypes.Assertion;
import mmm.coffee.apprentice.model.visitor.Visitor;

import java.io.Serializable;


/**
 * An {@code AssertionBean} contains the data about a custom assertion.
 *
 * A Builder class simplifies the construction of an AssertionBean.
 * Here's an example:
 * <code>
 *     Assertion assertion = AssertionBean.builder ("mmm.coffee.example.XYZAssertion")
 *                                        .withLocalScope(true)
 *                                        .build();
 *
 * </code>
 * With the method {@code builder("mmm.coffee.example.XYZAssertion"}, the model-layer classname
 * is specified. Since DevTest has a built-in default editor and controller for assertions,
 * its sufficient for us to only declare the model-layer class.
 * With the method {@code withLocalScope(true)}, we indicate to the DevTest framework to
 * present the XYZAssertion as a choice under each step of a test model.
 *
 */
public class AssertionBean extends MVCBean implements Assertion, Serializable {

    private static final long serialVersionUID = -4665001716310527782L;

    public static final String DEFAULT_EDITOR     = "com.itko.lisa.editor.DefaultAssertEditor";
    public static final String DEFAULT_CONTROLLER = "com.itko.lisa.editor.DefaultAssertController";

    public static final boolean DEFAULT_IS_GLOBAL_SCOPE = false;
    public static final boolean DEFAULT_IS_LOCAL_SCOPE = true;


    private boolean isGlobalScope = DEFAULT_IS_GLOBAL_SCOPE;
    private boolean isLocalScope = DEFAULT_IS_LOCAL_SCOPE;


    /**
     * Default constructor
     */
    public AssertionBean() {
        setController( new ComponentBean( DEFAULT_CONTROLLER, false ));
        setEditor ( new ComponentBean( DEFAULT_EDITOR, false ));
    }

    /**
     * Constructs a new instance by copying the fields from {@code clone}.
     * @param clone a non-null value
     */
    public AssertionBean ( final Assertion clone )
    {
        super ( clone );
        setGlobalScope(clone.getGlobalScope());
        setLocalScope(clone.getLocalScope());
    }


    /**
     * Constructs a FilterBean instance using the {@code generator}'s properties
     *
     * @param builder an instance of AssertionBeanBuilder
     */
    protected AssertionBean(Builder<?> builder) {
        super(builder);
        this.setGlobalScope( builder.isGlobalScope );
        this.setLocalScope( builder.isLocalScope );

        // If the editor is undefined, assign a default editor
        if ( builder.getEditor() == null )
            this.setEditor ( new ComponentBean( DEFAULT_EDITOR, false ));

        // If the controller is undefined, assign a default controller
        if ( builder.getController() == null )
            this.setController( new ComponentBean( DEFAULT_CONTROLLER, false ));
    }

    /**
     * Creates a deep copy of this AssertionBean
     * @return a deep copy of this AssertionBean
     */
    public AssertionBean deepCopy()
    {
        return new AssertionBean( this );
    }

    /**
     * Assigns the global scope flag.  An assertion with global scope can be created
     * at the test-case scope, without having to be linked to a specific step.
     * As a global assertion, it is automatically evaluated on each step.
     *
     * @param isGlobal {@code true} marks this assertion as "global"
     */
    final public void setGlobalScope(final boolean isGlobal) {
        this.isGlobalScope = isGlobal;
    }

    /**
     * Returns {@code true} if this is a global assertion, {@code false} otherwise
     * @return {@code true} if this is a global assertion, {@code false} otherwise
     */
    final public boolean getGlobalScope() {
        return this.isGlobalScope;
    }

    /**
     * Returns {@code true} if this is a global assertion, {@code false} otherwise.
     * @return {@code true} if this is a global assertion, {@code false} otherwise
     */
    final public boolean isGlobalScope() {
        return isGlobalScope;
    }

    /**
     * Assigns the local scope flag.  An assertion with local scope is presented as
     * a choice under each test step, and can be enabled on specific steps.
     * A local assertion is only evaluated on the step's that fire that assertion.
     *
     * @param isLocal {@code true} marks this assertion as "local"
     */
    final public void setLocalScope(final boolean isLocal) {
        this.isLocalScope = isLocal;
    }

    /**
     * Returns {@code true} if this is a local assertion, {@code false} otherwise
     * @return {@code true} if this is a local assertion, {@code false} otherwise
     */
    final public boolean getLocalScope() {
        return isLocalScope;
    }

    /**
     * Returns {@code true} if this is a local assertion, {@code false} otherwise
     * @return {@code true} if this is a local assertion, {@code false} otherwise
     */
    final public boolean isLocalScope() {
        return isLocalScope;
    }

    final public String toString() {
        // Note: the output coincidentally looks like JSON. There is no requirement
        // for the output to be a syntactically correct JSON string.

        StringBuilder sb = new StringBuilder();
        sb.append ("Assertion { ");

        sb.append (" classname: '").append( getClassName() ).append("', ");
        sb.append (" generate: '").append ( getGenerateCode() ).append ("'");
            sb.append (" isGlobalScope: '").append ( isGlobalScope ).append("'");
            sb.append (" isLocalScope: '").append ( isLocalScope ).append("'");

        if (getEditor() != null) {
            sb.append (" editor { ");
            sb.append ( getEditor().toString());
            sb.append (" }");
        }

        if ( getController() != null ) {
            sb.append ( " controller { ");
            sb.append ( getController().toString() );
            sb.append (" }");
        }

        sb.append(" }");
        return sb.toString();
    }

    /**
     * Accepts the {@code visitor} (to enable the Visitor pattern)
     * @param visitor the object being traversed
     */
    @Override
    final public void accept ( Visitor visitor ) {
        visitor.visit ( this );
    }

    /**
     * Compares two Assertion entities for equality.
     * Two Assertions are considered equal if they would generate the same source code.
     * As such, only instance variables that affect code generation are considered.
     * @param other the object compared for equality to {@code this}
     * @return if both objects are equal
     */
    @Override final public boolean equals ( Object other ) {
        if ( this == other ) return true;
        if ( other == null ) return false;

        boolean result = false;
        if ( other instanceof Assertion) {
            Assertion that = (Assertion) other;
            result = that.canEqual ( this )
                    && super.equals(that)
                    && this.isGlobalScope == that.isGlobalScope()
                    && this.isLocalScope == that.isLocalScope();
        }
        return result;
    }

    final public boolean canEqual ( final Object other ) {
        return ( other instanceof Assertion);
    }

    @Override final public int hashCode()
    {
        final int prime = 31;

        int result = super.hashCode();

        result = prime * result + super.hashCode();
        result = prime * result + Boolean.toString( isGlobalScope ).hashCode();
        result = prime * result + Boolean.toString( isLocalScope ).hashCode();

        return result;
    }

    /**
     * This class implements the generator pattern for AssertionBeans.
     *
     * @param <T> in our case, AssertionBean or any subclass thereof.
     */
    public static abstract class Builder<T extends Builder<T>> extends MVCBean.Builder<T> {
        private boolean isLocalScope;
        private boolean isGlobalScope;

        public T withLocalScope ( final boolean f ) {
            this.isLocalScope = f;
            return self();
        }
        public T withGlobalScope ( final boolean f ) {
            this.isGlobalScope = f;
            return self();
        }
        public AssertionBean build() {
            return new AssertionBean ( this );
        }
    }

    /**
     * Our concrete generator class.
     */
    private static class AssertionBeanBuilder extends Builder<AssertionBeanBuilder> {

        /**
         * Constructor
         * @param className the className for the model-layer class.  This may not be null
         *                  nor an empty string.
         */
        protected AssertionBeanBuilder ( String className )
        {
            withClassName( className );
        }

        @Override
        protected AssertionBeanBuilder self() {
            return this;
        }
    }

    /**
     * Returns a Builder for building StepBean objects
     * @param className the name of the model-layer class, such as {@code "mmm.coffee.plugin.CustomAssertion"}
     * @return a StepBean generator
     */
    public static Builder<?> builder( String className )
    {
        return new AssertionBeanBuilder( className );
    }


}
