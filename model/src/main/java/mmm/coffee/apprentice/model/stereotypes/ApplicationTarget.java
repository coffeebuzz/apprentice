/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.model.stereotypes;

/**
 * This stereotype denotes DevTest target entities.  A DevTest target
 * tells the code generator where to find the DevTest jar files that will
 * be needed by the generated Ant build, and the DevTest version number
 * to enable generating code for the correct version of DevTest.
 */
public interface ApplicationTarget extends BeanEntity {

    /**
     * Returns the home directory of DevTest workstation,
     * such as "C:/Program Files/CA/DevTest".  Its not actually
     * required that DevTest Workstation be installed, but the jar files
     * of DevTest must exist in the correct subdirectory of this home directory.
     *
     * @return the home directory of DevTest Workstation
     */
    String getHomeDirectory();
    void setHomeDirectory(String homeDirectory);

    /**
     * Returns the DevTest version number, such as "8.0" or "8.5".
     * The code templates are specific to versions, but usually only two places
     * of precision, as shown in these examples, is sufficient.  We anticipate
     * APIs or life cycle changes can occur between major versions and maybe
     * between minor versions. That being said, the value returned by this
     * method can be as precise as desired. The code generator will inspect this
     * and determine the appropriate code templates to use.
     *
     * @return the DevTest version on which the plugin will be deployed.
     */
    String getVersion();
    void setVersion(String versionNumber);
}
