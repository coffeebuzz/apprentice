/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.model.helpers;

import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * This is a collection of methods to aid in navigating XML
 */
public class XMLHelper {

    /**
     * Fetches a specific child tag of the given {@code parent} tag.
     *
     * @param parent the parent tag
     * @param tagName the name of the tag to fetch
     * @return the tag of {@code tagName}
     */
    public static Element findElement(Element parent, String tagName)
    {
        if ( parent == null || tagName == null)
            return null;

        NodeList nodeList = parent.getElementsByTagName( tagName );
        if ( nodeList != null ) {
            if (nodeList.getLength() > 0)
                return (Element)nodeList.item(0);
        }
        return null;
    }

    /**
     * Returns the text content of the given {@code element}.  When you have
     * an XML stanza like:
     * <code>
     * &lt;parent&gt;
     *     *lt;tag&gt;Some value&lt;/tag&gt;
     * &lt;/parent&gt;"
     * </code>
     * where {@code element} is the element of 'parent', and {@code tagName} is "tag",
     * this method returns "Some value".
     *
     * @param parent the parent element.  If its null, null is returned.
     * @param tagName the child within {@code element}. The text value of this tag is returned.
     *                If null, null is returned.
     * @return the text value of the {@code tagName} tag.
     */
    public static String findElementText(Element parent, String tagName)
    {
        if ( parent == null || tagName == null )
            return null;

        Element e = findElement ( parent, tagName );
        if ( e != null )
            return e.getTextContent();

        return null;
    }


    /**
     * Returns the text content of the attribute, {@code attributeName}, where {@code attribute}
     * is part of {@code element}.
     *
     * For example, let element = {@code &lt;property key="color" value="red"/&gt;}
     * {@code getAttributeText ( element, "key" )} returns "color"
     * {@code getAttributeText ( element, "value" )} returns "red"
     *
     * @param element the tag to consider
     * @param attributeName the attribute to fetch
     * @return the attribute's text value.  Null is returned if {@code element} or {@code attributeName} are null.
     */
    public static String getAttributeText ( Element element, String attributeName )
    {
        if ( element == null || attributeName == null )
            return null;

        NamedNodeMap map = element.getAttributes();
        if ( map != null ) {
            Node n = map.getNamedItem(attributeName);
            if (n != null)
                return n.getTextContent();
                    // found it!
        }
        return null;
    }
}
