/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.model.stereotypes;


import java.util.List;

/**
 * A stereotype for model-view-controller combinations.
 *
 * In usage, the model class implements the {@code MVCStereotype} interface.
 * From the model, the view and controller entities are acquired with
 * the {@code getEditor()} and {@code getController()} methods, respectively.
 */
public interface MVCEntity extends Component {


    /**
     * Returns the Editor for this entity
     * @return the Editor of the entity
     */
    Component getEditor();

    /**
     * Assigns the Editor to this entity
     * @param editor the editor
     */
    void setEditor ( Component editor );

    /**
     * Returns the Controller of this entity
     * @return the controller
     */
    Component getController();

    /**
     * Assigns the ControllerBean
     * @param controller the controller
     */
    void setController ( Component controller );

    /**
     * Returns the data fields for this assertion.
     * These fields appear in the UI and allow the user to set parameters for the assertion.
     *
     * @return the assertion's data fields. May return {@code null}.
     */
    List<Parameter> getParameters();

    /**
     * Assigns the fields of this assertion.
     *
     * @param fields the fields for this assertion.  {@code Null} is allowed.
     */
    void setParameters(List<Parameter> fields);

}
