/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.model.beans;

import mmm.coffee.apprentice.model.helpers.EqualityHelper;
import mmm.coffee.apprentice.model.helpers.StringHelper;
import mmm.coffee.apprentice.model.stereotypes.DataType;
import mmm.coffee.apprentice.model.stereotypes.Message;
import mmm.coffee.apprentice.model.stereotypes.Parameter;
import mmm.coffee.apprentice.model.visitor.Visitor;

import java.io.Serializable;
import java.util.UUID;

/**
 * A {@code ParameterBean} captures the meta-data of a DevTest parameter
 * that will be generated in the code. This meta-data includes:
 * <ul>
 *     <li>the instance variable name to use</li>
 *     <li>the field's Java data type</li>
 *     <li>the field's Swing data type</li>
 *     <li>the names of the get/set methods for this field</li>
 *     <li>the text to render in the UI label</li>
 *     <li>the text to render in the UI tool tip</li>
 * </ul>
 * The data field maps to several elements in the generated code:
 * <ul>
 *     <li>an instance variable</li>
 *     <li>a data entry field, such as a JText object</li>
 *     <li>a label, such as a JLabel</li>
 *     <li>an optional tooltip</li>
 *     <li>an entry in the generated messages.properties, to enable i18n support</li>
 * </ul>
 *
 * The ParameterBean includes a builder object that enables easier construction
 * of a Parameter, with reasonable defaults being used on the optional fields.
 * For instance, calling
 * <code>
 *     Parameter parameter = ParameterBean.builder ( "fieldOne" ).build();
 * </code>
 * will yield a ParameterBean with these properties:
 * <ul>
 *     <li>a String parameter named 'fieldOne'</li>
 *     <li>the default label text will be 'FieldOne:'</li>
 * </ul>
 * For this example parameter, these effects will occur in the generated code:
 * <ul>
 *  <li>the model-layer class will have a String instance variables named 'fieldOne'</li>
 *  <li>the model-layer class will have a {@code String getFieldOne()} method and a {@code void setFieldOne(String arg0)} method</li>
 *  <li>the editor-layer class will have a JLabel, with the label 'FieldOne:'</li>
 *  <li>the editor-layer will have a JTextField to input the value of 'fieldOne'</li>
 *  <li>An entry in messages.properties, "fieldOne.label=FieldOne:", to enable i18n support</li>
 * </ul>
 *
 * This class strives to provide reasonable behavior it terms of how a Parameter instance
 * affects the generated code.
 */
public class ParameterBean implements Parameter, Serializable {

    static final long serialVersionUID = 8265437836529926805L;


    private DataType dataType;

    private String variableName;
        // the name of the instance variable for this field

    private String xmlTag;
        // When the field is saved in the testcase TST file, this is its xml tag.

    private MessageBean labelBean;

    private MessageBean tooltipBean;

    private String labelName;
        // This is the name given the label constant.  For example, in the
        // generated code, this value is used like this:
        // public static final String {{labelConstant}} = "Enter value:" ;

    private String findButtonName;
        // The variable name given the Find button (the Find button is the one with "..." as its label).

    private boolean presentsPrefillCombo;
    private boolean presentsFindButton;

    private String getterMethodName;
    private String setterMethodName;

    private String uuid;


    /**
     * Default constructor
     */
    public ParameterBean() {
        uuid = UUID.randomUUID().toString();
    }

    /**
     * Creates a deep copy of {@code clone}
     *
     * @param clone the prototype to clone; must not be null
     */
    public ParameterBean ( final Parameter clone )
    {
        setUUID ( clone.getUUID() );
        setDataType( clone.getDataType() );
        setVariableName( clone.getVariableName() );
        setXmlTag( clone.getXmlTag() );
        this.findButtonName = clone.getFindButtonName();
        setPresentsFindButton ( clone.getPresentsFindButton() );
        setPresentsPrefillCombo( clone.getPresentsPrefillCombo() );
        setLabelName ( clone.getLabelName() );
        setGetterMethodName( clone.getGetterMethodName() );
        setSetterMethodName( clone.getSetterMethodName() );

        if ( clone.getLabel() != null)
            labelBean = new MessageBean ( clone.getLabel() );
        if ( clone.getTooltip() != null )
            tooltipBean = new MessageBean ( clone.getTooltip() );

    }

    /**
     * Constructs a ParameterBean instance using the {@code generator}'s properties
     *
     * @param builder an instance of ParameterBeanBuilder
     */
    protected ParameterBean (Builder<?> builder) {
        // Initialize the ID
        this();

        if ( !StringHelper.isValidVariableName( builder.variableName ))
            throw new IllegalArgumentException("The parameter '"+ StringHelper.safeString(variableName) + "' is not a valid Java identifier");


        // Copy the generator fields into this
        this.setVariableName( builder.variableName );

        // Default to String if no DataType is given
        if ( builder.dataType == null )
            this.setDataType( DataType.STRING );
        else
            this.setDataType( builder.dataType );

        this.setDataType( builder.dataType );
        if ( builder.label != null )
            this.setLabel( builder.label );
        if ( builder.tooltip != null )
            this.setTooltip( builder.tooltip );

        // File and Directory fields use PrefillCombo's, and present Find buttons
        // to enable the FileFinder
        if ( this.dataType == DataType.FILE || this.dataType == DataType.DIRECTORY ) {
            this.presentsFindButton = true;
            this.presentsPrefillCombo = true;
        }
        else {
            this.presentsFindButton = false;
            this.presentsPrefillCombo = false;
        }

    }

    /**
     * Copies the fields of {@code clone} into this object.
     * Essentially, this object is updated according to the values found in {@code clone}.
     *
     * This object's UUID is not modified. Once a UUID is assigned, it should be considered an immutable value.
     *
     * @param clone the instance variables of {@code clone} are copied into {@code this} object
     */
    public void copyOf ( final Parameter clone )
    {
        if ( clone != null ) {
            setDataType(clone.getDataType());
            setVariableName(clone.getVariableName());
            setXmlTag(clone.getXmlTag());
            this.findButtonName = clone.getFindButtonName();
            setPresentsFindButton(clone.getPresentsFindButton());
            setPresentsPrefillCombo(clone.getPresentsPrefillCombo());
            setLabelName(clone.getLabelName());
            setGetterMethodName(clone.getGetterMethodName());
            setSetterMethodName(clone.getSetterMethodName());

            // If we have an existing label, update our copy
            if ( labelBean != null )
                labelBean.copyOf ( clone.getLabel() );
            else if ( clone.getLabel() != null ) {
                // If we don't have a label, but the clone does, make a copy
                labelBean = new MessageBean ( clone.getLabel() );
            }

            // If we have an existing toolTip, update our copy
            if ( tooltipBean != null )
                tooltipBean.copyOf ( clone.getTooltip() );
            else if ( clone.getTooltip() != null ) {
                // If we don't have a toolTip, but the clone does, make a copy
                tooltipBean = new MessageBean ( clone.getTooltip() );
            }
        }
    }


    final public String getUUID() {
        return uuid;
    }
    final public void setUUID ( final String uuid ) {
        this.uuid = uuid;
    }


    public ParameterBean deepCopy() {
        return new ParameterBean ( this );
    }

    /**
     * Returns the data type of this field
     * @return the data type of this field
     */
    final public DataType getDataType() {
        if ( dataType == null )
            return DataType.STRING;
        return dataType;
    }

    /**
     * Sets the data type of this field
     * @param type the data type
     */
    final public void setDataType(final DataType type) {
        dataType = type;
    }

    final public String getLabelName() {
        if ( labelName == null ) {
            generateLabelConstantName();
        }
        return labelName;
    }
    final public void setLabelName(final String value) {
        labelName = value;
    }

    /**
     * Generates the constant name for the field's label. This will be a string like, "FIRSTNAME_LABEL".
     * The code generator uses this constant name to generate a line of code something like:
     * <code>private static final String FIRSTNAME_LABEL = Messages.get("firstName.label");</code>
     */
    private void generateLabelConstantName() {
        String labelConstant = new StringBuilder()
                                .append ( variableName )
                                .append ("_LABEL")
                                .toString()
                                .toUpperCase();
        setLabelName( labelConstant );
    }

    /**
     * Answers the question: does this field accept text input?  Most fields accept
     * text input.  Fields like radio buttons and check boxes would not.  If the field
     * accepts text, the code generator generates a text input field for this field.
     *
     * @return true if this field accepts text input
     */
    final public boolean isText() {
        if ( dataType != null )
        {
            switch ( dataType ) {
                case STRING:
                case FILE:
                case DIRECTORY:
                    return true;
                default:
                    return false;
            }
        }
        return false;
    }

    /**
     * Answers the question: is this a boolean field?
     *
     * @return true if this field is for boolean values.
     */
    final public boolean isBoolean() {
        if ( dataType != null ) {
            switch ( dataType ) {
                case BOOLEAN:
                    return true;
                default:
                    return false;
            }
        }
        return false;
    }

    /**
     * Returns the name of the instance variable for this field.
     * When this field appears in a Java class, this is the name given
     * the instance variable that represents this field.
     *
     * @return the name for the instance variable that denotes this field in a Java class
     */
    final public String getVariableName() {
        return variableName;
    }

    /**
     * Assigns a variableName to this field
     * @param variableName the java variable name the code generator will use when
     *                     creating instance variables of this field
     */
    final public void setVariableName(final String variableName) {

        if ( StringHelper.isEmpty( variableName ))
            throw new IllegalArgumentException("A null or empty string cannot be assigned to the variable name of a ParameterBean");

        this.variableName = variableName;

        if (StringHelper.isEmpty ( getterMethodName) )
            updateGetterMethodName(variableName);
        if ( setterMethodName == null )
            updateSetterMethodName ( variableName );

        if ( StringHelper.isEmpty ( xmlTag ))
            setXmlTag( variableName );
    }

    /**
     * This is a helper method for the code generator.
     * @return an alternative variable name
     */
    final public String getAlternateVariableName() {
        return "parsed_" + variableName;
    }

    final public void setXmlTag ( final String tag ) {
        this.xmlTag = tag;
    }
    final public String getXmlTag() {
        return xmlTag;
    }



    final public Message getLabel() { return labelBean; }
    final public Message getTooltip() { return tooltipBean; }



    /**
     * Set our label. We clone the argument so that our label is a specifically a bean,
     * since the argument can be anything the implements the Message stereotype.
     *
     * @param labelStereotype the value to which our label is set
     */
    final public void setLabel(final Message labelStereotype) {

        if (labelStereotype == null) {
            labelBean = null;
        } else {
            if ( labelBean != null )
                labelBean.copyOf( labelStereotype );
            else
                labelBean = new MessageBean( labelStereotype );
        }
    }


    /**
     * A convenience method for setting the label text.
     *
     * @param labelText the text to place in the label; for example "Host Name:".
     *
     * @throws IllegalStateException if the variableName is null when this method is called
     */
    final public void setLabel ( String labelText )
    {
        if ( variableName == null )
            throw new IllegalStateException("The variableName must be set before invoking 'setLabel'");
        labelBean = buildDefaultLabel( labelText );
    }



    /**
     * Set our tooltip.  The argument is cloned so that our instance variable is specifically a bean,
     * since the argument can be anything that implements MessageStereotype.
     *
     * @param toolTip the value to which our tooltip is set
     */
    final public void setTooltip(final Message toolTip ) {

        if (toolTip != null )
        {
            if ( tooltipBean != null )
                tooltipBean.copyOf ( toolTip );
            else
                tooltipBean = new MessageBean( toolTip );
        }
        else tooltipBean = null;
    }

    /**
     * A convenience method for setting the tooltip's pop-up text.
     *
     * @param tooltipText the text presented in the tool-tip; for example "This is the endpoint's IP address".
     *
     * @throws IllegalStateException if the variableName is null when this method is called
     */
    final public void setTooltip ( String tooltipText )
    {
        if ( variableName == null )
            throw new IllegalStateException("The variableName must be set before invoking 'setTooltip'");

        tooltipBean = buildDefaultTooltip( tooltipText );
    }


    final public String getGetterMethodName() {
        return getterMethodName;
    }
    final public void setGetterMethodName(final String methodName) {
        this.getterMethodName = methodName;
    }

    final public String getSetterMethodName() {
        return setterMethodName;
    }
    final public void setSetterMethodName( final String methodName ) {
        this.setterMethodName = methodName;
    }


    /**
     * This is a hint to the code generator to use a prefillComboBox as the data entry editor
     * for this field
     *
     * @return if this field presents a PrefillComboBox as its editor
     */
    final public boolean getPresentsPrefillCombo() {
        return presentsPrefillCombo;
    }
    final public void setPresentsPrefillCombo(final boolean flag) {
        presentsPrefillCombo = flag;
    }

    /**
     * This is a hint to the code generator to include a Find button.
     *
     * @return true if a Find button is rendered with this parameter in the UI
     */
    final public boolean getPresentsFindButton() {
        return presentsFindButton;
    }

    final public void setPresentsFindButton(final boolean flag) {
        this.presentsFindButton = flag;
    }

    final public String getFindButtonName() {
        if ( findButtonName == null ) {
            generateFindButtonName();
        }
        return findButtonName;
    }

    private void generateFindButtonName() {
        StringBuilder sb = new StringBuilder();
        sb.append ( variableName ).append ( "Btn" );
        findButtonName = sb.toString();
    }

    /**
     * Returns the instance variable name given the Swing component that denotes this field.
     * Every field maps to a data entry field in an editor dialog. Each data entry field is
     * wrapped by a Swing component to capture the user input.  If a field is a String, a JTextField
     * is used to capture the input. This method returns the name of the variable of the JTextField.
     *
     * @return the variable name given the Swing component tied to this field
     */
    final public String getSwingVariableName() {

        if ( variableName == null )
            return null;

        if ( dataType != null) {
            switch (dataType) {
                case STRING:
                    return variableName + "TF";
                case FILE:
                    return variableName + "CB";
                case DIRECTORY:
                    return variableName + "CB";
                default:
                    return variableName + "Field";
            }
        }
        // if we get here, we don't know the data type so go with a default
        return "_" + variableName;
    }

    final public String getSwingType() {

        if ( dataType == null ) // our fail-safe for badly defined fields
            return "JTextField";

        switch ( dataType ) {
            case STRING:
                return "JTextField";
            case FILE:
                return "PrefillCombo";
            case DIRECTORY:
                return "PrefillCombo";
            case BOOLEAN:
                return "JCheckBox";
            default:
                return "JTextField";
        }
    }

    private void updateGetterMethodName ( String variableName ) {
        StringBuilder sb = new StringBuilder ();
        String x = StringHelper.capitalizeFirstCharacter( variableName );
        sb.append ("get").append( x );
        setGetterMethodName(sb.toString());
    }

    private void updateSetterMethodName ( String variableName ) {
        StringBuilder sb = new StringBuilder();
        String x = StringHelper.capitalizeFirstCharacter( variableName );
        sb.append ("set").append ( x );
        setSetterMethodName( sb.toString() );
    }


    private MessageBean buildDefaultLabel ( String labelText )
    {
        if ( labelText == null )
            labelText = "";

        return new MessageBean( variableName + ".label", labelText );
    }

    private MessageBean buildDefaultTooltip ( String tooltipText )
    {
        if ( tooltipText == null )
            tooltipText = "";

        return new MessageBean( variableName + ".tooltip", tooltipText );
    }



    /**
     * Accepts the {@code visitor}
     */
    @Override public void accept ( Visitor visitor ) {
        visitor.visit ( this );
    }


    /**
     * Compares two FieldBeans for equality. FieldBeans are considered equal when:
     * <ul>
     *     <li>their data types are equal</li>
     *     <li>their variableNames are equal</li>
     *     <li>their XML tags are equal</li>
     *     <li>their isText() return values are equal</li>
     *     <li>their getPresentsFindButton() return values are equal</li>
     *     <li>their getPresentsPrefillCombo() return values are equal</li>
     * </ul>
     * Essentially, all instance variables are identical in content.
     *
     */
    @Override public boolean equals ( Object other )
    {
        if ( this == other ) return true;
        if ( other == null ) return false;

        boolean result = false;

        if ( other instanceof Parameter) {
            Parameter that = (Parameter) other;
            result = that.canEqual(this)
                    && ( EqualityHelper.areEqual ( getDataType(), that.getDataType() ))
                    && ( this.presentsFindButton == that.getPresentsFindButton() )
                    && ( this.presentsPrefillCombo == that.getPresentsPrefillCombo() )
                    && ( EqualityHelper.areEqual( getVariableName(), that.getVariableName() ))
                    && ( EqualityHelper.areEqual ( getLabel(), that.getLabel()) )
                    && ( EqualityHelper.areEqual ( getTooltip(), that.getTooltip() ))
                    && ( EqualityHelper.areEqual( this.getterMethodName, that.getGetterMethodName() ))
                    && ( EqualityHelper.areEqual( this.setterMethodName, that.getSetterMethodName() ))
                    && ( EqualityHelper.areEqual ( this.getLabelName(), that.getLabelName()))
                    && ( EqualityHelper.areEqual( this.xmlTag, that.getXmlTag() ));
        }
        return result;
    }

    public boolean canEqual ( final Object other ) {
        return ( other instanceof Parameter);
    }

    @Override public int hashCode() {
        final int prime = 37;
        int result = 1;

        if ( dataType != null )
            result = prime * result + dataType.hashCode();

        result = prime * result + ( Boolean.toString( presentsFindButton )).hashCode();
        result = prime * result + ( Boolean.toString( presentsPrefillCombo )).hashCode();

        if ( variableName != null )
            result = prime * result + variableName.hashCode();

        if ( labelBean != null )
            result = prime * result + labelBean.hashCode();

        if ( tooltipBean != null )
            result = prime * result + tooltipBean.hashCode();

        if ( getterMethodName != null )
            result = prime * result + getterMethodName.hashCode();

        if ( setterMethodName != null )
            result = prime * result + setterMethodName.hashCode();

        if ( getLabelName() != null )
            result = prime * result + getLabelName().hashCode();

        if ( xmlTag != null )
            result = prime * result + xmlTag.hashCode();

        return result;
    }


    @Override public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append ( " parameterBean { ");
        sb.append ( "variableName: '").append(variableName).append ("', ");
        sb.append ( "xmlTag: '").append(xmlTag).append("', ");
        sb.append ( "labelName: '").append(labelName).append("', ");
        sb.append ( "presentsPrefillCombo: '").append(presentsPrefillCombo).append("', ");
        sb.append ( "presentsFindButton: '").append(presentsFindButton).append("', ");
        sb.append ( "getterMethodName: '").append(getterMethodName).append ("', ");
        sb.append ( "setterMethodName: '").append(setterMethodName).append("', ");
        sb.append ( "dataType: ").append(dataType).append(", ");
        sb.append ( "labelBean: ").append(labelBean).append(", ");
        sb.append ( "tooltipBean: ").append(tooltipBean).append("");
        sb.append ( "}" );

        return sb.toString();
    }

    /**
     * This is a debug method that prints a message for each mismatch found between this object and the {@code other}.
     * @param other some other {@code FieldBean} being compared to this one.
     */
    final public void diagnoseEquals ( Object other )
    {
        if ( other == null )
            return;

        if ( other instanceof ParameterBean) {
            ParameterBean that = (ParameterBean) other;
            if ( ! that.canEqual(this) )
                System.out.println ("mismatch on canEqual");

            if ( !EqualityHelper.areEqual ( this.dataType, that.dataType ))
                System.out.println ("mismatch on dataType");

            if ( ! this.presentsFindButton == that.presentsFindButton )
                System.out.println ("mismatch on presentsFindButton");

            if ( ! this.presentsPrefillCombo == that.presentsPrefillCombo )
                System.out.println ( "mismatch on presentsPrefillCombo");

            if (! EqualityHelper.areEqual( this.variableName, that.variableName ))
                System.out.println ("mismatch on variableName");

            if (! EqualityHelper.areEqual ( this.labelBean, that.labelBean) )
                System.out.println ("Mismatch on label");

            if  (!EqualityHelper.areEqual ( this.tooltipBean, that.tooltipBean ))
                System.out.println ("mismatch on tooltip");

            if (! EqualityHelper.areEqual(this.getterMethodName, that.getterMethodName ))
                System.out.println ("mismatch on getter methodName");

            if ( ! EqualityHelper.areEqual( this.setterMethodName, that.setterMethodName ))
                System.out.println ("mismatch on setter methodName: this.setterMethodName: " + this.setterMethodName + ", that.setterMethodName: " + that.setterMethodName);

            if ( ! EqualityHelper.areEqual ( this.getLabelName(), that.getLabelName()))
                System.out.println ("mismatch on labelName: this.labelName=" + this.getLabelName() + ", that.labelName="+that.getLabelName());

            if ( ! EqualityHelper.areEqual( this.xmlTag, that.xmlTag ))
                System.out.println ("mismatch on xmlTag");
        }
        else {
            System.out.println ("Mismatch because 'other' is not instanceof FieldBean");
        }
    }


    /**
     * This Builder class enables the generator pattern for creating instances of a ParameterBean.
     * @param <T> the concrete generator's type
     */
    public static abstract class Builder<T extends Builder<T>> {

        private String variableName;
        private DataType dataType;
        private MessageBean label;
        private MessageBean tooltip;

        protected abstract T self();

        public T withVariableName ( final String variableName ) {
            this.variableName = variableName;
            return self();
        }
        public T withDataType ( final DataType type ) {
            this.dataType = type;
            return self();
        }
        public T withLabel ( final MessageBean label ) {
            this.label = label;
            return self();
        }
        public T withTooltip( final MessageBean tooltip ) {
            this.tooltip = tooltip;
            return self();
        }

        /**
         * Assigns the labelText of the Parameter under construction.
         * The default key of the label is "{@code variableName}.label"
         * @param labelText the label text
         * @return the current generator
         */
        public T withLabel ( final String labelText )
        {
            this.label =  new MessageBean ( variableName + ".label", labelText );
            return self();
        }

        /**
         * Assigns the tooltip of the Parameter under construction.
         * The default key of this tooltip is "{@code variableName}.tooltip"
         * @param toolTipText the tooltip text
         * @return the current Builder
         */
        public T withTooltip ( final String toolTipText )
        {
            this.tooltip = new MessageBean ( variableName + ".tooltip", toolTipText );
            return self();
        }


        public T copyOf ( final Parameter p )
        {
            variableName = p.getVariableName();
            dataType = p.getDataType();
            if ( p.getLabel() != null )
                label = MessageBean.builder().copyOf ( p.getLabel() ).build();
            if ( p.getTooltip() != null )
                tooltip = MessageBean.builder().copyOf ( p.getTooltip() ).build();

            return self();
        }
        public ParameterBean build()
        {
            return new ParameterBean ( this );
        }
    }

    /**
     * Our concrete generator class. It implements the self()
     */
    private static class ParameterBeanBuilder extends Builder<ParameterBeanBuilder> {

        protected ParameterBeanBuilder ( final String variableName )
        {
            withVariableName( variableName );
        }

        @Override
        protected ParameterBeanBuilder self() {
            return this;
        }
    }

    /**
     * Returns a Builder for building StepBean objects
     * @param variableName the instance variable name for this parameter
     * @return a StepBean generator
     */
    public static Builder<?> builder( final String variableName ) {
        return new ParameterBeanBuilder( variableName );
    }




}
