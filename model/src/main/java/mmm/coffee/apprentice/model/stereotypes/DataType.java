/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.model.stereotypes;

/**
 * This class represents the data types the code generator supports.
 */
public enum DataType {

    STRING    ("String",    "String",  "String"),
    FILE      ("File",      "String",  "java.io.File"),
    DIRECTORY ("Directory", "String",  "java.io.File"),
    BOOLEAN   ("Boolean",   "boolean", "Boolean");

    // This is the string value from the project.xml that indicates the data type
    private String encodedValue;

    // This is the Java type used for instance variables of this DataType
    private String javaType;

    // This is the Java type used in Parameter instances of this DataType
    private String parameterType;


    /**
     * Constructor
     * @param encodedValue when the enum is encoded into a String, this is the value given
     * @param javaType when this DataType is used as an instance variable in generated code, this type is used
     * @param parameterType when this DataType is used in a Parameter, this value is used.
     */
    private DataType(String encodedValue, String javaType, String parameterType)
    {
        this.encodedValue = encodedValue;
        this.javaType = javaType;
        this.parameterType = parameterType;
    }

    public String encode() {
        return encodedValue;
    }

    public String getJavaType() {
        return javaType;
    }

    public String getParameterType() { return parameterType; }

    public String toString() {
        return encodedValue;
    }

    /**
     * Returns the DataType equivalent of {@code code}, or {@code null} if {@code code} is invalid.
     * @param code the String equivalent of a DataType.
     * @return the DataType equivalent of {@code code}, or {@code null} if equivalent is found.
     */
    public static DataType decode ( String code )
    {
        if ( code == null )
            return null;

        if ( STRING.encode().equalsIgnoreCase(code) )
            return STRING;

        if (FILE.encode().equalsIgnoreCase(code))
            return FILE;

        if (DIRECTORY.encode().equalsIgnoreCase(code))
            return DIRECTORY;

        if ( BOOLEAN.encode().equalsIgnoreCase(code))
            return BOOLEAN;

        return null;
    }

    public boolean isDirectory() {
        if ( this == DataType.DIRECTORY)
            return true;
        return false;
    }

    public boolean isBoolean() {
        if ( this == DataType.BOOLEAN)
            return true;
        return false;
    }

    public boolean isString() {
        if ( this == DataType.STRING || this == DataType.FILE || this == DataType.DIRECTORY )
            return true;

        return false;
    }

}
