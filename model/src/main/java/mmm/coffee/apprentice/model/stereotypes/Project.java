/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.model.stereotypes;



import java.util.List;

/**
 * This class defines the stereotype for plugin project entities
 */
public interface Project extends BeanEntity {


    String getPluginName();
    void setPluginName ( String name );

    ApplicationTarget getApplicationTarget();
    void setApplicationTarget ( ApplicationTarget applicationTarget);

    WorkspaceTarget getWorkspaceTarget();
    void setWorkspaceTarget ( WorkspaceTarget outputTarget );

    List<Component> getComponents();
    void setComponents ( List<Component> components );

    MessageResource getMessageResource();
    void setMessageResource ( MessageResource resource );

    /**
     * Returns the schema version of the Project entity. If the schema of a project
     * changes, the version number will change as well.
     *
     * @return the schema version of the Project entity
     */
    String getVersion();

    /**
     * Returns the directory into which the code generator will write message.properites files.
     *
     * @return the directory where i18n message property files are placed
     */
    String getDefaultMessageBundleDirectory();
}
