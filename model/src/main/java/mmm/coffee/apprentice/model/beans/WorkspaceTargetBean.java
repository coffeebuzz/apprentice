/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.model.beans;


import mmm.coffee.apprentice.model.helpers.EqualityHelper;
import mmm.coffee.apprentice.model.helpers.StringHelper;
import mmm.coffee.apprentice.model.stereotypes.WorkspaceTarget;
import mmm.coffee.apprentice.model.visitor.Visitor;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.UUID;

/**
 * An {@code OutputBean} contains information about where
 * the code generator writes the output files.
 *
 * The base directory indicates the root of the project workspace
 * (its the equivalent of ${basedir} in an Ant file). For example,
 * it might be '/Users/stevejobs/workspace/WidgetPlugin'.
 *
 * The source directory indicates where the root of the source files
 * are written, such as 'src' or 'src/main/java', depending on the preferred
 * style.  The source directory, of course, is contained in the base directory.
 */
public class WorkspaceTargetBean implements WorkspaceTarget, Serializable {

    static final long serialVersionUID = -4529960608851971743L;

    // The default location of the generated Java source.
    // This folder will be a subdirectory of the baseDirectory.
    public static final String DEFAULT_SOURCE_DIRECTORY = "src/main/java";

    // The default location of the generated Java source.
    // This folder will be a subdirectory of the baseDirectory.
    public static final String DEFAULT_TEST_SOURCE_DIRECTORY = "src/test/java";

    // The default location of the resources, in particular, the '.lisaextensions' file
    // and the i18n message.properties file.
    // This folder will be a subdirectory of the baseDirectory.
    public static final String DEFAULT_RESOURCE_DIRECTORY = "src/main/resources";


    // The base directory for the generated plugin. This is treated as the workspace directory
    // for the generated code.  This base directory will contain the assets generated for
    // the plugin: the ant build file, the lisaextensions file, and the Java source files.
    private String baseDirectory;

    // The subdirectory, relative to the baseDirectory, where the Java source files are written.
    // Typically, this will be 'src/main/java'.
    private String sourceDirectory;

    // The subdirectory, relative to the baseDirectory, where the Java source files are written.
    // Typically, this will be 'src/test/java'.
    private String testSourceDirectory;


    // Resource files reside in this directory.
    // Typically, this will be 'src/main/resources', or 'src/resources'.
    private String resourceDirectory;

    private String uuid;

    private static final String ERROR_INVALID_BASE_DIRECTORY = "A null baseDirectory may not be assigned to a WorkspaceTarget";


    public WorkspaceTargetBean() {
        uuid = UUID.randomUUID().toString();
    }

    /**
     * Creates a clone
     *
     * @param clone the prototype to clone; must not be null
     */
    public WorkspaceTargetBean(final WorkspaceTarget clone)
    {
        this.uuid = clone.getUUID();
        this.baseDirectory = clone.getBaseDirectory();
        this.sourceDirectory = clone.getSourceDirectory();
        this.resourceDirectory = clone.getResourceDirectory();
        this.testSourceDirectory = clone.getTestSourceDirectory();
    }

    /**
     * Create an instance of a WorkspaceTargetBean having {@code baseDirectory} as the
     * home directory for the generated assets.  Using this constructor, the
     * source folder will be {@code DEFAULT_SOURCE_DIRECTORY} and the resources folder
     * will default to {@code DEFAULT_RESOURCE_DIRECTORY}.
     *
     * @param baseDirectory the directory to which the generated artifacts are written
     */
    public WorkspaceTargetBean ( final String baseDirectory )
    {
        this();

        if ( StringHelper.isEmpty( baseDirectory ))
            throw new IllegalArgumentException( ERROR_INVALID_BASE_DIRECTORY );

        this.baseDirectory = baseDirectory;
        this.sourceDirectory = DEFAULT_SOURCE_DIRECTORY;
        this.resourceDirectory = DEFAULT_RESOURCE_DIRECTORY;
        this.testSourceDirectory = DEFAULT_TEST_SOURCE_DIRECTORY;
    }


    /**
     * Constructs an OutputTargetBean using data retrieved from the {@code generator}.
     *
     * @param builder an instance of OutputTargetBeanBuilder, or one of its subclasses
     */
    protected WorkspaceTargetBean(final Builder<?> builder) {
        this();

        if ( builder.baseDirectory == null )
            throw new IllegalArgumentException( ERROR_INVALID_BASE_DIRECTORY );

        setBaseDirectory( builder.baseDirectory );
        setSourceDirectory( builder.sourceDirectory );
        setResourceDirectory( builder.resourceDirectory );
        setTestSourceDirectory( builder.testSourceDirectory );
    }

    /**
     * Converts {@code this} Workspace instance into a copy of {@code clone}.
     * @param clone the workspace to mimic
     */
    public void copyOf ( final WorkspaceTarget clone )
    {
        if ( clone != null )
        {
            setBaseDirectory( clone.getBaseDirectory() );
            setSourceDirectory( clone.getSourceDirectory() );
            setResourceDirectory( clone.getResourceDirectory() );
            setTestSourceDirectory( clone.getTestSourceDirectory() );
        }
    }


    /**
     * Returns a deep copy of this OutputTargetBean object
     * @return a deep copy of this object
     */
    public WorkspaceTargetBean deepCopy() {
        return new WorkspaceTargetBean( this );
    }


    final public String getUUID() {
        return uuid;
    }
    final public void setUUID ( final String uuid ) {
        this.uuid = uuid;
    }

    /**
     * {@inheritDoc}
     */
    final public String getBaseDirectory() {
        return baseDirectory;
    }

    /**
     * {@inheritDoc }
     * @param dir {@inheritDoc}
     */
    final public void setBaseDirectory( final String dir ) {
        baseDirectory = dir;
    }


    /**
     * {@inheritDoc}
     */
    final public String getSourceDirectory() {
        if ( StringHelper.isEmpty ( sourceDirectory) )
            return DEFAULT_SOURCE_DIRECTORY;
        return sourceDirectory;
    }

    /**
     * Defines the source directory for the Java code. Usually, this does not need to be called,
     * unless the default path, "src/main/java" is not the one you want.
     * @param dir the relative path to the Java code
     */
    final public void setSourceDirectory( final String dir ) {
        sourceDirectory = dir;
    }


    /**
     * {@inheritDoc}
     */
    final public String getTestSourceDirectory() {
        if ( StringHelper.isEmpty ( testSourceDirectory) )
            return DEFAULT_TEST_SOURCE_DIRECTORY;
        return testSourceDirectory;
    }

    /**
     * Defines the source directory for the Java code. Usually, this does not need to be called,
     * unless the default path, "src/main/java" is not the one you want.
     * @param dir the relative path to the Java code
     */
    final public void setTestSourceDirectory( final String dir ) {
        testSourceDirectory = dir;
    }



    /**
     * Returns the resource directory.  If the resource directory has not been set, the default value is returned.
     * @return the relative path to the resource directory. Usually, this is "src/main/resources".
     */
    final public String getResourceDirectory() {
        if ( StringHelper.isEmpty(resourceDirectory) )
            return DEFAULT_RESOURCE_DIRECTORY;
        return resourceDirectory;
    }

    /**
     * {@inheritDoc}
     * @param dir {@inheritDoc}
     */
    final public void setResourceDirectory ( String dir ) { this.resourceDirectory = dir; }


    /**
     * Two OuputTargetBeans are considered equal if their base directories match and their source directories match.
     *
     * @param obj the object being compared
     * @return {@code true} if {@code obj} equals this OutputTargetBean
     */
    @Override
    public boolean equals ( Object obj ) {
        if ( this == obj ) return true;
        if ( obj == null ) return false;

        synchronized ( this ) {
            if (obj instanceof WorkspaceTarget) {
                WorkspaceTarget that = (WorkspaceTarget) obj;
                return that.canEqual ( this )
                        && EqualityHelper.areEqual(this.getBaseDirectory(), that.getBaseDirectory())
                        && EqualityHelper.areEqual(this.getSourceDirectory(), that.getSourceDirectory())
                        && EqualityHelper.areEqual(this.getResourceDirectory(), that.getResourceDirectory())
                        && EqualityHelper.areEqual(this.getTestSourceDirectory(), that.getTestSourceDirectory());
            }
        }
        return false;
    }

    public boolean canEqual ( final Object other ) {
        return ( other instanceof WorkspaceTarget);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        if ( baseDirectory != null )
            result = prime * result + baseDirectory.hashCode();
        if ( sourceDirectory != null )
            result = prime * result + sourceDirectory.hashCode();
        if (resourceDirectory != null )
            result = prime * result + resourceDirectory.hashCode();
        if ( testSourceDirectory != null )
            result = prime * result + testSourceDirectory.hashCode();

        return result;
    }

    @Override
    public String toString() {
        // Note: its coincidental the output looks like a JSON string.
        // There is no requirement for this output to be a syntactically correct JSON string

        StringBuilder sb = new StringBuilder();

        sb.append (" output { ");
        sb.append (" baseDirectory: '").append ( baseDirectory ).append("', ");
        sb.append ( "sourceDirectory: '").append ( sourceDirectory ).append ("' ");
        sb.append ( "resourceDirectory: '").append ( sourceDirectory ).append ("' ");
        sb.append ( "testSourceDirectory: '").append ( testSourceDirectory ).append("' ");

        sb.append (" }");
        return sb.toString();
    }


    /**
     * Accepts the {@code visitor}
     *
     * @param visitor the visitor; must not be null
     */
    @Override public void accept ( Visitor visitor ) {
        visitor.visit ( this );
    }


    /**
     * Infrastructure to enable using the Builder pattern
     * @param <T> the concrete generator's type
     */
    public static abstract class Builder<T extends Builder<T>> {
        private String baseDirectory;
        private String sourceDirectory;
        private String resourceDirectory;
        private String testSourceDirectory;

        protected abstract T self();

        public T withBaseDirectory (final String baseDirectory) {
            this.baseDirectory = baseDirectory;
            return self();
        }
        public T withBaseDirectory ( final File baseDirectory )
        {
            if ( baseDirectory == null )
                throw new IllegalArgumentException("A null baseDirectory is not allowed");

            String fqPath = null;
            try {
                fqPath = baseDirectory.getCanonicalPath();
            }
            catch (IOException ioe) {
                fqPath = baseDirectory.getAbsolutePath();
            }

            withBaseDirectory( fqPath );

            return self();
        }

        /**
         * Declare the desired source directory in the generated workspace.
         * Call this if the default, {@code src/main/java}, is undesired.
         * @param directoryName the relative path to the generated Java code within the workspace
         * @return the Builder
         */
        public T withSourceDirectory(final String directoryName) {
            this.sourceDirectory = directoryName;
            return self();
        }

        /**
         * Declare the desired source directory in the generated workspace.
         * Call this if the default, {@code src/main/java}, is undesired.
         * @param directoryName the relative path to the generated Java code within the workspace
         * @return the Builder
         */
        public T withTestSourceDirectory(final String directoryName) {
            this.testSourceDirectory = directoryName;
            return self();
        }


        /**
         * Declare the desired resource directory in the generated workspace.
         * Call this method if the default, {@code src/main/resources}, is undesired
         * @param directory the relative path to the resources (such as the lisaextensions file)
         * @return the Builder
         */
        public T withResourceDirectory(final String directory) {
            this.resourceDirectory = directory;
            return self();
        }


        public WorkspaceTargetBean build() {
            return new WorkspaceTargetBean( this );
        }
    }


    private static class WorkspaceTargetBeanBuilder extends Builder<WorkspaceTargetBeanBuilder> {

        protected WorkspaceTargetBeanBuilder ( final String workspaceDirectory )
        {
            withBaseDirectory( workspaceDirectory );
        }

        protected WorkspaceTargetBeanBuilder ( final File fWorkspace )
        {
            withBaseDirectory( fWorkspace );
        }
        @Override
        protected WorkspaceTargetBeanBuilder self() {
            return this;
        }
    }

    /**
     * Returns a Builder capable of building WorkspaceTarget objects
     * @param baseDirectory the path to the workspace directory.
     * @return a Builder capable of building WorkspaceTarget objects
     */
    public static Builder<?> builder( final String baseDirectory ) {
        return new WorkspaceTargetBeanBuilder( baseDirectory );
    }

    /**
     * Returns a Builder capable of building WorkspaceTarget objects
     * @param fBaseDirectory the path to the workspace directory, as a File handle
     * @return a Builder capable of building WorkspaceTarget objects
     */
    public static Builder<?> builder ( final File fBaseDirectory ) {
        return new WorkspaceTargetBeanBuilder( fBaseDirectory );
    }




}
