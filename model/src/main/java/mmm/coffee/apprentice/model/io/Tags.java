/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.model.io;

/**
 * This is a catalog of the XML tags found in a project XML file
 */
public class Tags {

    public static final String APPLICATION_TARGET = "applicationTarget";
    public static final String ASSERTION = "assertion";

    public static final String BASE_DIRECTORY = "baseDirectory";


    public static final String CLASSNAME = "classname";
    public static final String CLASSNAME_PREFIX = "classNamePrefix";
    public static final String COMPANION = "companion";
    public static final String COMPONENT_LIST = "componentList";
    public static final String CONTROLLER = "controller";

    public static final String DATASET = "dataset";
    public static final String DATA_PROTOCOL = "dataProtocolHandler";
    public static final String DEFAULT_PACKAGE = "defaultPackage";
    public static final String DISPLAY_NAME = "displayName";
    public static final String DESCRIPTION = "description";

    public static final String EDITOR = "editor";

    public static final String FILTER = "filter";

    public static final String GENERATE = "generate";
    public static final String GET_METHOD = "getMethod";
    public static final String GLOBAL_SCOPE = "globalScope";

    public static final String HOME_DIRECTORY = "homeDirectory";

    public static final String KEY = "key";

    public static final String LABEL = "label";
    public static final String LABEL_NAME = "labelName";
    public static final String LOCAL_SCOPE = "localScope";

    public static final String MESSAGE_RESOURCE = "messageResource";

    public static final String WORKSPACE = "workspace";

    public static final String PARAMETER = "parameter";
    public static final String PARAMETER_LIST = "parameterList";
    public static final String PRESENTS_PREFILL_COMBO = "presentsPrefillCombo";
    public static final String PRESENTS_FIND_BUTTON = "presentsFindButton";
    public static final String PROJECT = "project";
    public static final String PLUGIN_NAME = "pluginName";

    public static final String REQUEST_PROTOCOL = "processRequest";
    public static final String RESPONSE_PROTOCOL = "processResponse";
    public static final String RESOURCE_DIRECTORY = "resourceDirectory";


    public static final String SET_METHOD = "setMethod";
    public static final String SOURCE_DIRECTORY = "sourceDirectory";
    public static final String STEP = "step";

    public static final String TEST_SOURCE_DIRECTORY = "testSourceDirectory";
    public static final String TOOLTIP = "tooltip";
    public static final String TYPE = "type";

    public static final String UNDEFINED = "undefined";

    public static final String VARIABLE_NAME = "variableName";
    public static final String VALUE = "value";
    public static final String VERSION = "version";


}
