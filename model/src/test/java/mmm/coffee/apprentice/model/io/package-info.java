/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * These unit tests verify the codes ability to write a ProjectBean to an XML file, and
 * to re-materialize the ProjectBean from that XML file.
 * <p>
 *     The {@code MockProjectBeanFactory} creates the ProjectBean that is read and written.
 *     The {@code ProjectWriterTest} writes the ProjectBean to this module's {@code scratch} directory
 *     (which is created if it does not exist).  The {@code ProjectReaderTest} reads the XML file
 *     created by the {@code ProjectWriterTest}.  Its probably obvious the {@code ProjectWriterTest}
 *     must run before the {@code ProjectReaderTest}.
 * </p>
 */
package mmm.coffee.apprentice.model.io;