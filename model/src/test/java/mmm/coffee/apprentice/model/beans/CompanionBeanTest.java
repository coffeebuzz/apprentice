/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.model.beans;

import org.junit.Test;
import org.junit.Assert;

/**
 * Unit tests of the CompanionBean
 */
public class CompanionBeanTest {

    @Test public void testNewInstancesAreEqual()
    {
        CompanionBean b1 = new CompanionBean();
        CompanionBean b2 = new CompanionBean();

        Assert.assertTrue ( b1.equals ( b2 ));
    }

    @Test public void testNotEqualOnClassNameMismatch()
    {
        CompanionBean b1 = new CompanionBean();
        CompanionBean b2 = new CompanionBean();

        b1.setClassName("com.example.foo.Companion");

        Assert.assertNotEquals( b1, b2 );
    }

    @Test public void testNotEqualOnGenerateCodeMismatch()
    {
        CompanionBean b1 = new CompanionBean();
        CompanionBean b2 = new CompanionBean();

        b1.setGenerateCode(true);
        b2.setGenerateCode(false);

        Assert.assertNotEquals( b1, b2 );
    }

    @Test public void testCanEqual ()
    {
        CompanionBean cBean = new CompanionBean();
        MVCBean modelBean = new MVCBean();
        ComponentBean classBean = new ComponentBean();

        // CompanionBeans can equal other CompanionBeans,
        // but cannot equal instances of a superclass.
        Assert.assertTrue ( cBean.canEqual( cBean ));
        Assert.assertFalse ( cBean.canEqual( modelBean ));
        Assert.assertFalse ( cBean.canEqual( classBean ));
    }
}
