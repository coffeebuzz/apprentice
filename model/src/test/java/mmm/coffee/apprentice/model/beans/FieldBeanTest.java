/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.model.beans;

import mmm.coffee.apprentice.model.stereotypes.DataType;
import org.junit.Assert;
import org.junit.Test;

/**
 * Unit tests of the FieldBean
 */
public class FieldBeanTest {


    @Test public void testNewInstancesAreEqual()
    {
        ParameterBean b1 = new ParameterBean();
        ParameterBean b2 = new ParameterBean();

        Assert.assertEquals ( b1, b2 );
    }

    @Test public void testNotEqualOnGetMethodMismatch()
    {
        ParameterBean b1 = new ParameterBean();
        ParameterBean b2 = new ParameterBean();

        b1.setGetterMethodName("getSize");
        b2.setGetterMethodName("getWeight");

        Assert.assertNotEquals( b1, b2 );
    }

    @Test public void testNotEqualOnSetMethodMismatch()
    {
        ParameterBean b1 = new ParameterBean();
        ParameterBean b2 = new ParameterBean();

        b1.setSetterMethodName("setSize");
        b2.setSetterMethodName("setWeight");

        Assert.assertNotEquals( b1, b2 );
    }

    @Test public void testNotEqualOnDataTypeMismatch()
    {
        ParameterBean b1 = new ParameterBean();
        ParameterBean b2 = new ParameterBean();

        // Compare String & Directory
        b1.setDataType( DataType.STRING );
        b2.setDataType ( DataType.DIRECTORY );
        Assert.assertNotEquals( b1, b2 );

        // Compare File & Directory
        b1.setDataType( DataType.FILE );
        Assert.assertNotEquals(b1, b2);

        // Compare nul & Directory
        b1.setDataType(null);
        Assert.assertNotEquals(b1, b2);
    }

    @Test public void testEqualOnLabelMatch()
    {
        ParameterBean b1 = new ParameterBean();
        ParameterBean b2 = new ParameterBean();

        MessageBean mbean = new MessageBean();
        mbean.setKey("key");
        mbean.setValue("value");

        b1.setLabel(mbean);
        b2.setLabel(mbean);

        Assert.assertEquals ( b1, b2 );
    }


    @Test public void testEqualOnTooltipMatch()
    {
        ParameterBean b1 = new ParameterBean();
        ParameterBean b2 = new ParameterBean();

        MessageBean mbean = new MessageBean();
        mbean.setKey("key");
        mbean.setValue("value");

        b1.setTooltip(mbean);
        b2.setTooltip(mbean);

        Assert.assertEquals ( b1, b2 );
    }

    @Test public void testNotEqualOnLabelMismatch()
    {
        ParameterBean b1 = new ParameterBean();
        ParameterBean b2 = new ParameterBean();

        MessageBean m1 = new MessageBean( "key", "value1");
        MessageBean m2 = new MessageBean( "key", "value2");

        b1.setLabel(m1);
        b2.setLabel(m2);

        Assert.assertNotEquals(b1, b2);
    }

    @Test public void testNotEqualOnTooltiplMismatch()
    {
        ParameterBean b1 = new ParameterBean();
        ParameterBean b2 = new ParameterBean();

        MessageBean m1 = new MessageBean( "key", "value1");
        MessageBean m2 = new MessageBean( "key", "value2");

        b1.setTooltip(m1);
        b2.setTooltip(m2);

        Assert.assertNotEquals ( b1, b2 );
    }

    @Test public void testNotEqualOnLabelConstantMismatch()
    {
        ParameterBean b1 = new ParameterBean();
        ParameterBean b2 = new ParameterBean();

        b1.setLabelName("foo_label");
        b2.setLabelName("bar_label");

        Assert.assertNotEquals( b1, b2 );
    }


    @Test public void testNotEqualOnFindButtonMismatch() {
        ParameterBean b1 = new ParameterBean();
        ParameterBean b2 = new ParameterBean();

        b1.setPresentsFindButton(true);
        b2.setPresentsFindButton(false);

        Assert.assertNotEquals(b1, b2);
    }


    @Test public void testNotEqualOnVariableNameMismatch() {
        ParameterBean b1 = new ParameterBean();
        ParameterBean b2 = new ParameterBean();

        b1.setVariableName("firstName");
        b2.setVariableName("lastName");

        Assert.assertNotEquals(b1, b2);
    }



}
