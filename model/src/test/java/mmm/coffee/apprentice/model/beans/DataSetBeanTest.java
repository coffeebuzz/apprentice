/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.model.beans;

import mmm.coffee.apprentice.model.stereotypes.DataType;
import org.junit.Test;
//import static org.junit.Assert.assertEquals;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests for the DataSetBean
 */
public class DataSetBeanTest {


    @Test
    public void confirmNewInstancesAreEqual()
    {
        DataSetBean b1 = new DataSetBean();
        DataSetBean b2 = new DataSetBean();

        assertThat ( "Empty DataSetBeans should be equal", b1, equalTo( b2 ));
    }

    @Test
    public void onClassNameMismatch_EqualsReturnsFalse()
    {
        DataSetBean b1 = DataSetBean.builder( "mmm.coffee.dataset.FooDataSet").build();
        DataSetBean b2 = DataSetBean.builder( "mmm.coffee.dataset.BarDataSet").build();

        assertThat ( "Equals method should detect mismatch on className field", b1, not (equalTo (b2)));
    }

    @Test
    public void onGenerateCodeMismatch_EqualsReturnsFalse()
    {
        final String className = "mmm.coffee.dataset.FooDataSet";

        DataSetBean b1 = DataSetBean.builder( className ).withGenerateCode( true ).build();
        DataSetBean b2 = DataSetBean.builder( className ).withGenerateCode( false ).build();

        assertThat ( "Equals method should detect mismatch on generateCode field", b1, not (equalTo (b2)));
    }


    @Test
    public void confirmParametersAddedWithBuilderAreCarriedIntoDataSetBean() {

        ParameterBean pFileName = ParameterBean.builder("fileName")
                                        .withDataType(DataType.FILE)
                                        .build();

        ParameterBean pByteMark = ParameterBean.builder("byteOrderMark")
                                        .withDataType(DataType.BOOLEAN)
                                        .build();

        ParameterBean pEncoding = ParameterBean.builder("encoding")
                                        .withDataType(DataType.STRING)
                                        .build();

        DataSetBean bean = DataSetBean.builder("mmm.coffee.db.MyDataSet")
                                .withParameter( pFileName )
                                .withParameter( pByteMark )
                                .withParameter( pEncoding )
                                .build();

        assertThat ( "Expected the DataSetBean to have 3 parameters", bean.getParameters().size() == 3 );

        assertThat ( "The parameter 'fileName' was not found", bean.getParameters(), hasItem ( pFileName ));
        assertThat ( "The parameter 'byteOrderMark' was not found", bean.getParameters(), hasItem ( pByteMark ));
        assertThat ( "The parameter 'encoding' was not found", bean.getParameters(), hasItem ( pEncoding ));
    }


    /**
     * This test shows the builder can build the same DataSet again and again,
     * and the same DataSet will be produced each time.
     */
    @Test
    public void confirmBuilderCanCreateEqualBeans() {
        // ------------------------------------------------------------------------
        // Given        : the DataSetBean.builder is used to construct several beans
        // and          : the className is the same each time
        // then expect  : both DataSetBeans are equal
        // ------------------------------------------------------------------------
        DataSetBean b1 = DataSetBean.builder("mmm.coffee.db.MyDataSet").build();
        DataSetBean b2 = DataSetBean.builder("mmm.coffee.db.MyDataSet").build();

        assertThat( "Expected DataSetBeans to be equal", b1, equalTo( b2 ));
    }
}
