/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.model.beans;

import org.junit.Before;
import org.junit.Test;

//import java.util.logging.ConsoleHandler;
//import java.util.logging.LogManager;
//import java.util.logging.Logger;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;


/**
 * Unit test of AssertionBean
 */
public class AssertionBeanTest {

    @Before
    public void setup()
    {
        // This is an example of how to get Gradle to log test messages.
        // Gradle only reports INFO and above,
        // so FINE, FINEST, DEBUG, etc. are not logged
        // LogManager.getLogManager().reset();
        // Logger.getLogger("").addHandler(new ConsoleHandler());
        // Logger.getAnonymousLogger().info("setup finished");
    }

    @Test public void withDefaultConstruction_BeansAreEqual()
    {
        AssertionBean b1 = new AssertionBean();
        AssertionBean b2 = new AssertionBean();

        assertThat ("Empty AssertionBeans should be equal", b1, equalTo(b2));
        assertThat ("Equal objects should yield same hashCode", b1.hashCode() == b2.hashCode() );
    }

    @Test public void whenGlobalScopeMismatch_BeansAreNotEqual()
    {
        AssertionBean b1 = AssertionBean.builder( "mmm.coffee.core.SimpleAssertion" )
                .withGlobalScope(true)
                .withLocalScope(true)
                .build();

        AssertionBean b2 = AssertionBean.builder( "mmm.coffee.core.SimpleAssertion" )
                .withGlobalScope(false)
                .withLocalScope(true)
                .build();

        assertThat ( "Equals method should detect mismatch of globalScope field", b1, not ( equalTo (b2)));
    }

    @Test public void whenLocalScopeMismatch_BeansAreNotEqual()
    {
        AssertionBean b1 = AssertionBean.builder( "mmm.coffee.core.SimpleAssertion" )
                .withGlobalScope(true)
                .withLocalScope(true)
                .build();

        AssertionBean b2 = AssertionBean.builder( "mmm.coffee.core.SimpleAssertion" )
                .withGlobalScope(true)
                .withLocalScope(false)
                .build();

        assertThat ( "Equals method should detect mismatch of localScope field", b1, not ( equalTo (b2)));
    }


    @Test public void whenGenerateCodeMismatch_BeansAreNotEqual()
    {
        AssertionBean b1 = AssertionBean.builder( "mmm.coffee.core.SimpleAssertion" )
                .withGlobalScope(true)
                .withLocalScope(true)
                .withGenerateCode(true)
                .build();

        AssertionBean b2 = AssertionBean.builder( "mmm.coffee.core.SimpleAssertion" )
                .withGlobalScope(true)
                .withLocalScope(true)
                .withGenerateCode(false)
                .build();

        assertThat ( "Equals method should detect mismatch of generateCode field", b1, not ( equalTo (b2)));
    }



    @Test public void confirmGetAndSetMethodsAreReflexive()
    {
        AssertionBean bean = new AssertionBean();

        bean.setGlobalScope(true);
        assertThat ( "set/getGlobalScope methods are not reflexive", bean.getGlobalScope() );


        bean.setGlobalScope(false);
        assertThat ( "set/getGlobalScope methods are not reflexive", not (bean.getGlobalScope()) );


        bean.setLocalScope( true );
        assertThat ( "set/getLocalScope methods are not reflexive", bean.getLocalScope() );


        bean.setLocalScope( false );
        assertThat ( "set/getLocalScope methods are not reflexive", not ( bean.getLocalScope()) );


        bean.setGenerateCode( true );
        assertThat ( "set/getGenerateCode methods are not reflexive", bean.getGenerateCode());


        bean.setGenerateCode( false );
        assertThat ( "set/getGenerateCode methods are not reflexive", not ( bean.getGenerateCode()) );
    }

    /**
     * An AssertionBean can equal another AssertionBean,
     * but cannot equal instances of its superclass.
     */
    @Test public void whenSuperClassIsTestedForEqualityToSubclass_equalsReturnsFalse()
    {
        AssertionBean assertBean = new AssertionBean();
        MVCBean mlBean = new MVCBean();
        ComponentBean classBean = new ComponentBean();

        // the bean should equal itself
        assertThat ( "An AssertionBean should equal itself", assertBean, equalTo( assertBean ));
        assertThat ( "An AssertionBean cannot equal an MVCBean", assertBean, not ( equalTo( mlBean)));
        assertThat ( "An AssertionBean cannot equal a ComponentBean", assertBean, not ( equalTo (classBean)));
    }

    @Test public void confirmCanEqualMethodRejectsParentClasses()
    {
        AssertionBean assertBean = new AssertionBean();
        MVCBean modelBean = new MVCBean();
        ComponentBean classBean = new ComponentBean();

        assertThat ( "canEqual method should return 'true' when comparing an AssertionBean", assertBean.canEqual( assertBean ));
        assertThat ( "canEqual method should return 'false' when comparing a superclass (an MVCBean)", not (assertBean.canEqual ( modelBean )));
        assertThat ( "canEqual method should return 'false' when comparing a superclass (a ComponentBean)", not (assertBean.canEqual( classBean )));
    }
}
