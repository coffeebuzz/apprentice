/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.model.beans;

import org.junit.Test;
import org.junit.Assert;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests of the OutputTargetBean.
 */
public class WorkspaceTargetBeanTest {

    @Test public void testNewInstancesAreEqual()
    {
        WorkspaceTargetBean b1 = new WorkspaceTargetBean();
        WorkspaceTargetBean b2 = new WorkspaceTargetBean();

        Assert.assertEquals("New instances should be equal", b1, b2);
        Assert.assertEquals("Equal objects should have the same hashCode", b1.hashCode(), b2.hashCode());
    }

    @Test public void testGetSetMethods()
    {
        WorkspaceTargetBean bean = new WorkspaceTargetBean();

        String baseDirectory = "workspace/widget";
        String sourceDirectory = "src/main/java";
        String testSourceDirectory = "src/test/java";

        bean.setBaseDirectory( baseDirectory );
        Assert.assertEquals("setBaseDirectory/getBaseDirectory mismatch", baseDirectory, bean.getBaseDirectory());

        bean.setSourceDirectory(sourceDirectory);
        Assert.assertEquals("setSourceDirectory/getSourceDirectory mismatch", sourceDirectory, bean.getSourceDirectory());

        bean.setTestSourceDirectory( testSourceDirectory );
        assertThat ("setTestSourceDirectory/getTestSourceDirectory do not match", bean.getTestSourceDirectory(), is(testSourceDirectory));

    }

    @Test public void testNotEqualsOnBaseDirectoryMismatch()
    {
        WorkspaceTargetBean b1 = new WorkspaceTargetBean();
        WorkspaceTargetBean b2 = new WorkspaceTargetBean();

        b1.setBaseDirectory("workspace/projectA");
        b2.setBaseDirectory("workspace/projectB");

        Assert.assertNotEquals("Equals method failed to detect baseDirectory mismatch", b1, b2);
    }

    @Test public void testNotEqualsOnSourceDirectoryMismatch()
    {
        WorkspaceTargetBean b1 = new WorkspaceTargetBean();
        WorkspaceTargetBean b2 = new WorkspaceTargetBean();

        b1.setSourceDirectory("src/main/java");
        b2.setSourceDirectory("src");

        Assert.assertNotEquals("Equals method failed to detect sourceDirectory mismatch", b1, b2);
    }

    @Test public void testNotEqualsOnTestSourceDirectoryMismatch()
    {
        WorkspaceTargetBean b1 = new WorkspaceTargetBean();
        WorkspaceTargetBean b2 = new WorkspaceTargetBean();

        b1.setTestSourceDirectory("src/test/java");
        b2.setTestSourceDirectory("tests");

        Assert.assertNotEquals("Equals method failed to detect a testSourceDirectory mismatch", b1, b2);
    }
}
