/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.model.helpers;

import org.junit.Test;
import java.util.List;
import java.util.LinkedList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;

/**
 * Unit tests of HashCodeHelper
 */
public class HashCodeHelperTest {


    @Test
    public void aggregateHashCodeOfNull_isAlwaysTheSame() {

        int hashCode0 = HashCodeHelper.aggregateHashCode(null);
        int hashCode1 = HashCodeHelper.aggregateHashCode(null);

        // Two objects that are equal should have the same hashCode
        assertThat("The aggregate hashCode of null should always be the same", hashCode0, is ( hashCode1));
    }

    @Test
    public void aggregateHashCodeOfEmptyList_isAlwaysTheSame()
    {
        int hashCode0 = HashCodeHelper.aggregateHashCode(new LinkedList<>());
        int hashCode1 = HashCodeHelper.aggregateHashCode(new LinkedList<>());

        assertThat ("The aggregate hashCode of two different empty lists should be the same", hashCode0, is ( hashCode1 ));
    }

    @Test
    public void aggregateHashCodeOfEqualLists_isAlwaysTheSame()
    {
        List<String> listOne = new LinkedList<>();
        listOne.add("Apple");
        listOne.add("Banana");
        listOne.add("Cherry");

        List<String> listTwo = new LinkedList<> ( listOne );


        int hashCode0 = HashCodeHelper.aggregateHashCode( listOne );
        int hashCode1 = HashCodeHelper.aggregateHashCode( listTwo );

        assertThat ("The aggregate hashCode of two identical lists should be the same", hashCode0, is ( hashCode1 ));
    }


}
