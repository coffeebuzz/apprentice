/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.model.beans;

import mmm.coffee.apprentice.model.stereotypes.ApplicationTarget;
import mmm.coffee.apprentice.model.stereotypes.MessageResource;
import mmm.coffee.apprentice.model.stereotypes.Project;
import mmm.coffee.apprentice.model.stereotypes.WorkspaceTarget;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;


/**
 * Unit tests of the ProjectBean
 */
public class ProjectBeanTest {

    private ApplicationTarget applicationTarget;
    private WorkspaceTarget workspaceTarget;
    private MessageResource messageResource;


    @Before
    public void setUp()
    {
        applicationTarget = ApplicationTargetBean.builder()
                                    .withHomeDirectory("/Applications/DevTest")
                                    .withVersion("9")
                                    .build();

        workspaceTarget = WorkspaceTargetBean.builder("/tmp/workspace").build();

        messageResource = MessageResourceBean.builder("mmm.coffee.i18n.Messages").build();


    }



    @Test public void confirmGetSetMethodsAreReflexive()
    {
        ProjectBean projectBean = new ProjectBean( applicationTarget, workspaceTarget, messageResource );

        String pluginName = "MyPlugin";
        projectBean.setPluginName( pluginName );
        assertThat ("setPluginName/getPluginName mismatch", pluginName, equalTo (projectBean.getPluginName()));
    }



    @Test public void onPluginNameMismatch_EqualsReturnsFalse()
    {
        ProjectBean b1 = new ProjectBean( applicationTarget, workspaceTarget, messageResource );
        ProjectBean b2 = new ProjectBean( applicationTarget, workspaceTarget, messageResource );

        b1.setPluginName("pluginX");
        b2.setPluginName("pluginZ");

        assertThat ( "Equals method failed to detect pluginName mismatch", b1, not(equalTo(b2)));
    }



    @Test public void onComponentMismatch_EqualsReturnsFalse()
    {
        ProjectBean b1 = new ProjectBean( applicationTarget, workspaceTarget, messageResource );
        ProjectBean b2 = new ProjectBean( applicationTarget, workspaceTarget, messageResource );

        // Add a Step to b1
        b1.getComponents().add (
                StepBean.builder("mmm.coffee.foo.FooStep")
                        .withController("mmm.coffee.foo.FooStepController")
                        .withEditor("mmm.coffee.foo.FooStepEditor")
                        .build());

        // Add a Companion to b2
        b2.getComponents().add (
                CompanionBean.builder("mmm.coffee.bar.BarCompanion").build());

        assertThat ("Equals method failed to detect componentList mismatch", b1, not(equalTo(b2)));
    }

    @Test public void onWorkspaceTargetMismatch_EqualsReturnsFalse()
    {
        ProjectBean b1 = new ProjectBean( applicationTarget, workspaceTarget, messageResource );
        ProjectBean b2 = new ProjectBean( applicationTarget, workspaceTarget, messageResource );

        WorkspaceTarget ws1 = WorkspaceTargetBean.builder("/some/arbitrary/path").build();
        b1.setWorkspaceTarget( ws1  );

        assertThat ( "Equals method failed to detect workspaceTarget mismatch", b1, not ( equalTo (b2) ));
    }

    @Test public void onMessageResourceMismatch_EqualsReturnsFalse()
    {
        ProjectBean b1 = new ProjectBean( applicationTarget, workspaceTarget, messageResource );
        ProjectBean b2 = new ProjectBean( applicationTarget, workspaceTarget, messageResource );

        b1.getMessageResource().setClassName( "com.widget.i18n.Messages" );
        b2.getMessageResource().setClassName(" com.widget.Messages" );

        assertThat( "Equals method failed to detect MessageResource mismatch", b1, not (equalTo(b2)));
    }

    /**
     * Instead of requiring explicit ApplicationTarget, WorkspaceTarget, and MessageResource
     * classes to be created, the programmer can imply these objects by providing their names.
     * That is, the programmer can now do this:
     *
     *     Project project = ProjectBean.builder("C:/Program Files/CA/DevTest",
     *                                           "C:/Users/JoeProgrammer/Workspace/MyExtension",
     *                                           "mmm.coffee.myextension.i18n.Messages")
     *                                           .build();
     */
    @Test
    public void whenUsingImpliedObjects_expectProjectEntityIsReturned()
    {
        final String devTestHome = "C:/Program Files/CA/DevTest";
        final String workspace = "C:/Workspace/Widget";
        final String messages = "mmm.coffee.widget.i18n.Messages";

        Project project = ProjectBean.builder( devTestHome, workspace, messages ).build();

        assertThat ("Expected a non-null project entity", project, notNullValue());
        assertThat ("Expected a non-null ApplicationTarget", project.getApplicationTarget(), notNullValue());
        assertThat ("Expected a non-null WorkspaceTarget", project.getWorkspaceTarget(), notNullValue());
        assertThat ("Expected a non-null MessageResource", project.getMessageResource(), notNullValue());

        assertThat ("", project.getApplicationTarget().getHomeDirectory(), is( devTestHome ));
        assertThat ("", project.getWorkspaceTarget().getBaseDirectory(), is( workspace ));
        assertThat ("", project.getMessageResource().getClassName(), is( messages ));

    }

}
