/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.model.beans;

import mmm.coffee.apprentice.model.stereotypes.Component;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;



/**
 * Unit tests of the ClassBean
 */
public class ComponentBeanTest {

    @Test public void withDefaultConstruction_BeansAreEqual()
    {
        // If we know 2 empty beans are equal, then we know
        // that changing any field in either bean can make them unequal.
        ComponentBean b1 = new ComponentBean();
        ComponentBean b2 = new ComponentBean();

        assertThat ("Empty ComponentBeans should be equal", b1, equalTo (b2));
        assertThat ("Equal objects should yield the same hashCode",  b1.hashCode() == b2.hashCode() );
    }

    @Test public void onGenerateCodeMismatch_BeansAreNotEqual()
    {
        ComponentBean b1 = ComponentBean.builder( "mmm.coffee.Component" ).withGenerateCode(true).build();
        ComponentBean b2 = ComponentBean.builder( "mmm.coffee.Component" ).withGenerateCode(false).build();

        assertThat ( "Equals method should detect mismatch of generateCode field", b1, not ( equalTo (b2)));
    }

    @Test public void onClassNameMismatch_BeansAreNotEqual()
    {
        ComponentBean b1 = ComponentBean.builder( "mmm.coffee.Component" ).withClassName("foo").build();
        ComponentBean b2 = ComponentBean.builder( "mmm.coffee.Component" ).withClassName("rex").build();

        assertThat ("Equals method should detect mismatch of className",  b1, not ( equalTo ( b2 )));
    }

    @Test public void confirmGetAndSetMethodsAreReflexive()
    {
        ComponentBean bean = new ComponentBean();

        // set/getClassName
        bean.setClassName ("com.example.plugin.Widget");
        assertThat ( "set/getClassName method is not reflexive", bean.getClassName(), equalTo ( "com.example.plugin.Widget" ));

        // set/getGenerateCode
        bean.setGenerateCode( true );
        assertThat ( "set/getGenerateCode method is not reflexive", bean.getGenerateCode() );

        bean.setGenerateCode( false );
        assertThat ( "set/getGenerateCode method is not reflexive", not ( bean.getGenerateCode()));
    }

    /**
     * When the className is set, the packageName and simpleName of the component are updated.
     */
    @Test public void whenClassNameIsSet_PackageNameIsCalculated()
    {
        ComponentBean bean = new ComponentBean();

        bean.setClassName ("com.example.plugin.Widget");

        assertThat ( "The packageName was miscalculated", bean.getPackageName(), equalTo("com.example.plugin"));
        assertThat ( "The simpleName was miscalculated", bean.getSimpleName(), equalTo("Widget") );
    }

    @Test public void whenSuperClassIsTestedForEqualityToSubclass_falseIsAlwaysReturned()
    {
        ComponentBean componentBean = new ComponentBean();
        MVCBean superclass = new MVCBean();

        assertThat ( "A ComponentBean should equal itself", componentBean, equalTo (componentBean));
        assertThat ( "A ComponentBean cannot equal a superclass", componentBean, not ( equalTo ( superclass )));
    }

    @Test public void confirmCanEqualMethodAcceptsSubclasses()
    {
        ComponentBean componentBean = new ComponentBean();
        AssertionBean subclassTwo = new AssertionBean();
        MVCBean subclassOne = new MVCBean();

        assertThat ( "A ComponentBean should be comparable to a subclass (MVCBean)", componentBean.canEqual( subclassOne ));
        assertThat ( "A ComponentBean should be comparable to a subclass (AssertionBean)", componentBean.canEqual( subclassTwo ));
        assertThat ( "A ComponentBean should not be comparable to a superclass (Object)", not ( componentBean.canEqual ( new Object())));
    }

    @Test (expected = IllegalArgumentException.class)
    public void whenBuildingWithNullClassName_expectIllegalArgumentException()
    {
        ComponentBean.builder(null).build();
    }

    @Test (expected = IllegalArgumentException.class)
    public void whenBuildingWithEmptyStringForClassName_expectIllegalArgumentException()
    {
        ComponentBean.builder("").build();
    }

    @Test (expected = IllegalArgumentException.class)
    public void whenBuildingWithReservedWordForClassName_expectIllegalArgumentException()
    {
        ComponentBean.builder("boolean").build();
    }

    @Test
    public void whenBuildingWithValidClassName_expectBuildReturnsComponent()
    {
        Component component = ComponentBean.builder("mmm.coffee.Example").build();

        assertThat ("The build() method must not return null when all arguments are valid", component, notNullValue());
        assertThat ("The className in the Component does not match the className from the builder", component.getClassName(), is("mmm.coffee.Example"));
    }


}
