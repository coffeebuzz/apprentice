/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.model.beans;

import org.junit.Test;
import org.junit.Assert;

/**
 * Unit tests of MessageBeans
 */
public class MessageBeanTest {


    @Test public void testNewInstancesAreEqual()
    {
        MessageBean m1 = new MessageBean();
        MessageBean m2 = new MessageBean();

        Assert.assertEquals( "New instances should be equal", m1, m2 );
        Assert.assertEquals( "Equal objects should return same hashCode", m1.hashCode(), m2.hashCode() );
    }


    @Test public void testGetSetMethods()
    {
        MessageBean bean = new MessageBean();


        bean.setKey("key");
        Assert.assertEquals("Return value incorrect for getKey() method", "key", bean.getKey());

        bean.setValue("value");
        Assert.assertEquals("Return value incorrect for getValue() method", "value", bean.getValue());
    }


    @Test public void testNotEqualOnKeyMismatch()
    {
        MessageBean m1 = new MessageBean();
        MessageBean m2 = new MessageBean();

        m1.setKey("color");
        m2.setKey("size");

        Assert.assertNotEquals("Equal method did not detect key mismatch", m1, m2 );
    }

    @Test public void testNotEqualOnValueMismatch()
    {
        MessageBean m1 = new MessageBean();
        MessageBean m2 = new MessageBean();

        m1.setValue("color");
        m2.setValue("size");

        Assert.assertNotEquals("Equal method did not detect value mismatch", m1, m2 );
    }


}
