/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.model.beans;

import mmm.coffee.apprentice.model.stereotypes.DataType;
import mmm.coffee.apprentice.model.stereotypes.Parameter;
import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests of the ParameterBean
 */
public class ParameterBeanTest {


    @Test
    public void whenDefaultConstructorIsUsed_DefaultValuesAreUsed()
    {
        // --------------------------------------------------------------------------
        // With the default constructor, the dataType defaults to DataType.STRING.
        // All other fields should be null.
        // --------------------------------------------------------------------------

        ParameterBean b1 = new ParameterBean();

        assertThat ("The default data type should be 'string'", b1.getDataType(), equalTo( DataType.STRING ));

        assertThat ("The default label should be null",     b1.getLabel(), nullValue());
        assertThat ("The default tooltip should be null",   b1.getTooltip(), nullValue());
        assertThat ("The getter method should be null",     b1.getGetterMethodName(), nullValue());
        assertThat ("The setter method should be null",     b1.getSetterMethodName(), nullValue());
    }


    @Test
    public void equalParametersShouldBeEqual()
    {
        ParameterBean b1 = new ParameterBean();
        ParameterBean b2 = new ParameterBean();

        assertThat ("Empty ParameterBeans should be equal", b1, equalTo(b2));

        b1.setDataType(DataType.STRING);
        b2.setDataType(DataType.STRING);

        b1.setVariableName("fieldOne");
        b2.setVariableName("fieldOne");

        b1.setLabel( "Field One:" );
        b2.setLabel( "Field One:" );

        b1.setTooltip("Tooltip text");
        b2.setTooltip("Tooltip text");

        assertThat ("Expected parameters to be equal", b1, equalTo(b2));
    }

    @Test
    public void verifyGetAndSetMethodsMatch()
    {
        ParameterBean p = new ParameterBean();

        p.setDataType( DataType.BOOLEAN );
        p.setVariableName( "fieldOne" );
        p.setLabel( "Field One:" );
        p.setTooltip( "Tooltip text" );

        assertThat ("DataType should be 'BOOLEAN", p.getDataType(), equalTo( DataType.BOOLEAN ));
        assertThat ("Variable name should be 'fieldOne", p.getVariableName(), equalTo("fieldOne"));
        assertThat ("The label key should be 'fieldOne.label'", p.getLabel().getKey(), equalTo("fieldOne.label"));
        assertThat ("The label value should be 'Field One:'", p.getLabel().getValue(), equalTo("Field One:"));
        assertThat ("The tooltip key should be 'fieldOne.tooltip'", p.getTooltip().getKey(), equalTo ("fieldOne.tooltip"));
        assertThat ("The tooltip value should be 'Tooltip text'", p.getTooltip().getValue(), equalTo ("Tooltip text"));
    }


    @Test
    public void whenOnlyFieldNameIsGiven_thenDefaultsAreUsed()
    {
        Parameter b1 = ParameterBean.builder("fieldOne").build();

        assertThat ("The build method should not return a null value", b1, notNullValue() );
        assertThat ("The default data type should be 'string'", b1.getDataType(), equalTo( DataType.STRING ));

        assertThat ("The default label should be null", b1.getLabel(), nullValue());
        assertThat ("The default tooltip should be null", b1.getTooltip(), nullValue());

        assertThat ("The getter method should be named 'getFieldOne'", b1.getGetterMethodName(), equalTo("getFieldOne"));
        assertThat ("The setter method should be named 'setFieldOne'", b1.getSetterMethodName(), equalTo("setFieldOne"));

        assertThat ("The default value of 'presentsFindButtton' should be 'false')", b1.getPresentsFindButton() == false );
        assertThat ("The default value of 'presentsPrefillCombo' should be 'false'", b1.getPresentsPrefillCombo() == false );
    }

    @Test (expected = IllegalArgumentException.class)
    public void whenParameterNameIsInvalidJavaIdentifier_expectIllegalArgException()
    {
        ParameterBean.builder("#class").build();
    }

    @Test public void whenFileParameter_expectPostConditions()
    {
        Parameter param = ParameterBean.builder("fileName")
                                       .withDataType(DataType.FILE)
                                       .build();

        assertThat ( "A File parameter is-a-text value", param.isText(), is(true));
        assertThat ( "A File parameter is-not a boolean", param.isBoolean(), is(false));
        assertThat ( "A File parameter should render a PrefillCombo", param.getSwingType(), is ( "PrefillCombo") );
        assertThat ( "A File parameter should present a Find button", param.getPresentsFindButton(), is(true));
        assertThat ( "A File parameter should present a PrefillCombo", param.getPresentsPrefillCombo(), is(true));
    }

    @Test public void whenDirectoryParameter_expectPostConditions()
    {
        Parameter param = ParameterBean.builder("directoryName")
                .withDataType(DataType.DIRECTORY)
                .build();

        assertThat ( "A Directory parameter is-a-text value", param.isText(), is(true));
        assertThat ( "A Directory parameter is-not a boolean", param.isBoolean(), is(false));
        assertThat ( "A Directory parameter should render a PrefillCombo", param.getSwingType(), is ( "PrefillCombo") );
        assertThat ( "A Directory parameter should present a Find button", param.getPresentsFindButton(), is(true));
        assertThat ( "A Directory parameter should present a PrefillCombo", param.getPresentsPrefillCombo(), is(true));
    }

    @Test public void whenStringParameter_expectPostConditions()
    {
        Parameter param = ParameterBean.builder("hostName")
                .withDataType(DataType.STRING)
                .build();

        assertThat ( "A String parameter is-a-text value", param.isText(), is(true));
        assertThat ( "A String parameter is-not a boolean", param.isBoolean(), is(false));
        assertThat ( "A String parameter should render a JTextField", param.getSwingType(), is ( "JTextField") );
        assertThat ( "A String parameter does not present a Find button", param.getPresentsFindButton(), is(false));
        assertThat ( "A String parameter does not present a PrefillCombo", param.getPresentsPrefillCombo(), is(false));
    }

    @Test public void whenBooleanParameter_expectPostConditions()
    {
        Parameter param = ParameterBean.builder("someFlag")
                .withDataType(DataType.BOOLEAN)
                .build();

        assertThat ( "A Boolean parameter is-not a text value", param.isText(), is(false));
        assertThat ( "A Boolean parameter is-a boolean", param.isBoolean(), is(true));
        assertThat ( "A Boolean parameter should render a JCheckBox", param.getSwingType(), is ( "JCheckBox") );
        assertThat ( "A Boolean parameter does not present a Find button", param.getPresentsFindButton(), is(false));
        assertThat ( "A Boolean parameter does not present a PrefillCombo", param.getPresentsPrefillCombo(), is(false));
    }

}
