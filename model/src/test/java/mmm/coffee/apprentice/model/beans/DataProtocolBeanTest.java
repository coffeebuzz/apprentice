/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.model.beans;

import mmm.coffee.apprentice.model.stereotypes.DataProtocol;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;





/**
 * Unit tests for the DataProtocolBean
 */
public class DataProtocolBeanTest {


    @Test public void whenNewDefaultObjects_equalsReturnsTrue()
    {
        // Two new instances should be equal. This simply allows us
        // to show that, by only modifying one field, we can create
        // inequality since we know that, up until the field changed,
        // the objects were equal.
        DataProtocolBean b1 = new DataProtocolBean();
        DataProtocolBean b2 = new DataProtocolBean();

        assertEquals ( b1, b2 );
    }

    /**
     * Confirm the equals method returns false when the
     * isResponseSide properties of different DataProtocolBeans
     * do not match.
     */
    @Test public void whenResponseProtocolMismatch_equalsReturnsFalse()
    {
        DataProtocolBean b1 = DataProtocolBean.builder ("mmm.coffee.dph.MyDPH")
                .withDisplayName("MyDPH")
                .withRequestSide(true)
                .withResponseSide(true)
                .build();
        DataProtocolBean b2 = DataProtocolBean.builder("mmm.coffee.dph.MyDPH")
                .withDisplayName("MyDPH")
                .withRequestSide(true)
                .withResponseSide(false)
                .build();

        // Verify the Builder set these properties correctly
        assertThat ( "The responseProtocol should return 'true' when set to 'true'", b1.getIsResponseSide() );
        assertThat ( "The responseProtocol should return 'false' when set to 'false'", not(b2.getIsResponseSide()));

        // Verify DataProtocolBeans having different isResponseSide
        // values are not equal
        assertThat ( "When responseProtocol setting is different, equals should return false", b1, not( b2 ));

        // Verify we're not getting a false positive in the above test.
        b2.setIsResponseSide(true);
        assertThat ( "When responseProtocol setting is same, equals should return true", b1, is( b2 ));
    }

    /**
     * Confirm the equals method returns false when the
     * isRequestSide properties of different DataProtocolBeans
     * do not match.
     */
    @Test public void whenRequestProtocolMismatch_equalsReturnsFalse()
    {
        DataProtocolBean b1 = DataProtocolBean.builder("mmm.coffee.dph.MyDPH")
                .withDisplayName("MyDPH")
                .withRequestSide(true)
                .withResponseSide(true)
                .build();
        DataProtocolBean b2 = DataProtocolBean.builder("mmm.coffee.dph.MyDPH")
                .withDisplayName("MyDPH")
                .withRequestSide(false)
                .withResponseSide(true)
                .build();

        // Verify the generator set the isRequestSide property correctly
        assertThat ( "The requestProtocol should return 'true' when set to 'true'", b1.isRequestSide() );
        assertThat ( "The requestProtocol should return 'false' when set to 'false'", not (b2.isRequestSide()));

        // Having different response protocol flag values breaks equality
        assertThat ( "When the requestProtocol of two objects is not the same, equals should return false", b1, not(b2));
    }

    /**
     * Confirm the equals method returns false when
     * the generateCode values of two different DataProtocolBeans
     * do not match.
     */
    @Test public void whenGenerateCodeMismatch_equalsReturnsFalse()
    {
        DataProtocolBean b1 = DataProtocolBean.builder("mmm.coffee.dph.MyDPH")
                .withGenerateCode(true)
                .withRequestSide(true)
                .build();
        DataProtocolBean b2 = DataProtocolBean.builder("mmm.coffee.dph.MyDPH")
                .withGenerateCode(false)
                .withRequestSide(true)
                .build();

        // Having different generateCode flags breaks equality
        b1.setGenerateCode(true);
        b2.setGenerateCode(false);

        assertNotEquals(b1, b2);
    }

    /**
     * Confirm the equals method returns false when
     * classNames of two DataProtocolBeans do not match.
     */
    @Test public void whenClassNameMismatch_equalsReturnsFalse()
    {
        DataProtocolBean b1 = DataProtocolBean.builder("mmm.coffee.dph.MyDPH")
                .withDisplayName("MyDPH")
                .withRequestSide(true)
                .build();
        DataProtocolBean b2 = DataProtocolBean.builder("mmm.coffee.dph.MyOtherDPH")
                .withDisplayName("MyDPH")
                .withRequestSide(true)
                .build();

        // Having different classNames breaks equality
        assertNotEquals(b1, b2);
    }

    /**
     * Verify the canEqual method correctly handles superclasses.
     * A DataProtocolBean can equal another DataProtocolBean or
     * one of its subclasses, but it cannot be equal a superclass.
     */
    @Test public void whenTestingEqualityOfSuperclass_equalsReturnsFalse()
    {
        DataProtocolBean dphBean = new DataProtocolBean();
        MVCBean mvcBean = new MVCBean();
        ComponentBean classBean = new ComponentBean();

        // A DataProtocolBean can equal another DataProtocolBean,
        // but cannot equal a vanilla instance of a superclass.
        assertTrue(dphBean.canEqual(dphBean));
        assertFalse(dphBean.canEqual(mvcBean));
        assertFalse(dphBean.canEqual(classBean));
    }

    /**
     * These test confirm get/set methods are reflexive
     */
    @Test public void confirmGetSetMethodsAreReflexive()
    {
        DataProtocolBean bean = new DataProtocolBean();

        // Verify the set/get methods are reflexive
        String className = "com.example.dph.MyDPH";
        bean.setClassName( className );
        assertEquals(className, bean.getClassName());

        bean.setGenerateCode(true);
        assertTrue(bean.getGenerateCode());

        bean.setGenerateCode(false);
        assertFalse(bean.getGenerateCode());

        bean.setIsRequestSide(true);
        assertTrue(bean.isRequestSide());

        bean.setIsRequestSide(false);
        assertFalse(bean.isRequestSide());

        bean.setIsResponseSide(true);
        assertTrue(bean.isResponseSide());

        bean.setIsResponseSide(false);
        assertFalse(bean.isResponseSide());
    }

    /**
     * This test confirms that properties are set correctly
     * when using the generator methods.
     */
    @Test public void testBuilderMethods()
    {
        String desc = "Text Protocol";
        String displayName = "Text Protocol:";

        DataProtocolBean bean = DataProtocolBean.builder("mmm.coffee.dph.MyDPH" )
                .withDisplayName( displayName )
                .withDescription( desc )
                .withRequestSide( true )
                .withResponseSide( true )
                .build();

        // Confirm the getter methods return the
        // same property values as we assigned via the generator
        assertEquals( desc, bean.getDescription() );
        assertEquals( displayName, bean.getDisplayName() );
        assertTrue(bean.getIsRequestSide());
        assertTrue ( bean.getIsResponseSide() );

        DataProtocolBean bean2 = DataProtocolBean.builder("mmm.coffee.dph.MyDPH" )
                .withDisplayName( displayName )
                .withRequestSide( true )
                .withResponseSide( false )
                .build();

        assertTrue (bean2.getIsRequestSide());
        assertFalse(bean2.getIsResponseSide());
    }

    /**
     * Given: the Builder is used to construct a DataProtocol
     *        withDisplayName is -not- called
     * Expect: after build(), getDisplayName() returns simpleName of the model-layer class
     */
    @Test
    public void whenDisplayNameIsUndefined_DefaultValueIsSimpleName()
    {
        DataProtocol dph = DataProtocolBean.builder("mmm.coffee.dph.ChessProtocol")
                                           .withDescription("handles chess notation")
                                           .withRequestSide(true)
                                           .withResponseSide(true)
                                           .build();

        assertThat ( "When the displayName is undefined, its default should be the simpleName", dph.getDisplayName(), is ( dph.getSimpleName() ));
    }

}
