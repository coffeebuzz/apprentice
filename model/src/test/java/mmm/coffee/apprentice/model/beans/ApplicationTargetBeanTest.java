/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.model.beans;

import mmm.coffee.apprentice.model.stereotypes.ApplicationTarget;
import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


/**
 * Unit tests of TargetBean
 */
public class ApplicationTargetBeanTest {

    private static final String DEVTEST_HOME = "/Applications/DevTest";
    private static final String DEVTEST_VERSION = "10.0";


    /**
     * Two new instances of TestBean should be equal
     */
    @Test public void testNewInstancesAreEqual()
    {
        ApplicationTargetBean t1 = new ApplicationTargetBean();
        ApplicationTargetBean t2 = new ApplicationTargetBean();

        assertThat ("Empty instances should be equal", t1, equalTo(t2));
        assertThat ("Equal objects should have the same hashCode", t1.hashCode() == t2.hashCode());
    }

    /**
     * Verify the reflexive property of get/set methods: if setX(f), then getX() == f.
     */
    @Test public void confirmGetSetMethodsAreReflexive()
    {
        ApplicationTargetBean bean = new ApplicationTargetBean();

        String homeDirectory = "home.directory";
        bean.setHomeDirectory( homeDirectory );
        assertThat ( "Expected getHomeDirectory to reflect setHomeDirectory", homeDirectory, equalTo ( bean.getHomeDirectory()));

        String version = "8.0";
        bean.setVersion( version );
        assertThat ("Expected getVersion to reflect setVersion value", version, equalTo ( bean.getVersion()));
    }

    /**
     * Given two ApplicationTargets with different home directory attributes,
     * Expect: equals method must return false
     */
    @Test public void onHomeDirectoryMismatch_expectEqualsReturnsFalse()
    {
        String h1 = "home.directory";
        String h2 = "user.dir";
        String version = "8.0";

        ApplicationTargetBean b1 = new ApplicationTargetBean( h1, version );
        ApplicationTargetBean b2 = new ApplicationTargetBean( h2, version );

        assertThat ("Equals method should detect homeDirectory mismatch", b1, not ( equalTo( b2 )));
    }

    /**
     * Given two ApplicationTargets with different version attributes
     * Expect: equals method must return false
     */
    @Test public void onVersionMismatch_expectEqualsReturnsFalse()
    {
        String h1 = "home.directory";
        String v1 = "8.0";
        String v2 = "8.5";

        ApplicationTargetBean b1 = new ApplicationTargetBean( h1, v1 );
        ApplicationTargetBean b2 = new ApplicationTargetBean( h1, v2 );

        Assert.assertNotEquals("Equals method should detect version mismatch", b1, b2);
    }

    /**
     * Given ApplicationTarget b is a deep copy of ApplicationTarget a,
     * Expect: a and b are equal
     */
    @Test public void whenDeepCopyIsMade_CopyEqualsOriginal()
    {
        ApplicationTargetBean original = ApplicationTargetBean.builder()
                                                .withHomeDirectory( DEVTEST_HOME )
                                                .withVersion( DEVTEST_VERSION )
                                                .build();

        ApplicationTargetBean copy = original.deepCopy();

        assertThat ("Expected both objects to be equal", copy, equalTo ( original ));
    }

    /**
     * Given: an ApplicationTarget constructed using the Builder
     * Expect: the attributes of the constructed ApplicationTarget match the values given to the builder
     */
    @Test public void confirmBuilderConstruction()
    {
        String homeDirectory = "C:/Program Files/CA/DevTest";
        String version = "9";

        ApplicationTarget target = ApplicationTargetBean.builder()
                                        .withHomeDirectory( homeDirectory )
                                        .withVersion( version )
                                        .build();

        assertThat ("Builder failed to set homeDirectory correctly", target.getHomeDirectory(), is ( homeDirectory ));
        assertThat ("Builder failed to set version correctly",       target.getVersion(), is ( version ));

    }

    @Test
    public void whenNoVersionGiven_expectLatestIsDefault()
    {
        ApplicationTarget target = new ApplicationTargetBean( DEVTEST_HOME );

        assertThat ( "Expected the home directory to match", target.getHomeDirectory(), is ( DEVTEST_HOME ));
    }

    @Test
    public void whenBuilderIsUsedWithoutVersion_expectEntityIsCreated()
    {
        ApplicationTarget target = ApplicationTargetBean.builder().withHomeDirectory( DEVTEST_HOME ).build();

        assertThat ( "Expected the home directory to match", target.getHomeDirectory(), is ( DEVTEST_HOME ));
    }

}
