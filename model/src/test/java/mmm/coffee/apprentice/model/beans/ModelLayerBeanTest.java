/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.model.beans;

import mmm.coffee.apprentice.model.stereotypes.Parameter;
import org.junit.Test;
import org.junit.Assert;

import java.util.LinkedList;
import java.util.List;

/**
 * Unit test of ModelLayerBean
 */
@SuppressWarnings("unchecked")
public class ModelLayerBeanTest {


    /**
     * New instances are expected to be equal.
     */
    @Test public void givenNewDefaultInstances_expectObjectsAreEqual()
    {
        MVCBean m1 = new MVCBean();
        MVCBean m2 = new MVCBean();

        Assert.assertEquals("New instances should be equal", m1, m2);
        Assert.assertEquals("Equal instances should have the same hashCode", m1.hashCode(), m2.hashCode());
    }

    /**
     * Get/Set methods should be reflexive: if setX(f), then getX() == f.
     */
    @Test public void confirmGetSetMethodsAreReflexive()
    {
        MVCBean bean = new MVCBean();

        ComponentBean editor = new ComponentBean();
        editor.setClassName("com.example.MyEditor");

        bean.setEditor(editor);
        Assert.assertEquals("The getEditor() return value does not match the value assigned with setEditor()", editor, bean.getEditor());


        ComponentBean controller = new ComponentBean();
        controller.setClassName("com.example.MyController");

        bean.setController(controller);
        Assert.assertEquals("getController() value does not match setController() value", controller, bean.getController());

        List<Parameter> list = buildParameterList();

        bean.setParameters(list);

        Assert.assertEquals ("The getParameters() return value does not match the value assigned with setParameters()", list, bean.getParameters() );
    }

    /**
     * Two ModelLayerBeans with different controllers are not equal
     */
    @Test public void testNotEqualsOnControllerMismatch()
    {
        MVCBean m1 = new MVCBean();
        MVCBean m2 = new MVCBean();

        ComponentBean c1 = new ComponentBean();
        c1.setClassName("com.example.MyController");

        ComponentBean c2 = new ComponentBean();
        c2.setClassName("net.window.MyController");


        m1.setController( c1 );
        m2.setController(c2);

        Assert.assertNotEquals( "Equals method did not detect controller mismatch", m1, m2 );
    }


    /**
     * Two ModelLayerBeans with different editors are not equal
     */
    @Test public void testNotEqualsOnEditorMismatch()
    {
        MVCBean m1 = new MVCBean();
        MVCBean m2 = new MVCBean();

        ComponentBean c1 = new ComponentBean();
        c1.setClassName("com.example.MyEditor");

        ComponentBean c2 = new ComponentBean();
        c2.setClassName("net.window.MyEditor");


        m1.setEditor(c1);
        m2.setEditor(c2);

        Assert.assertNotEquals( "Equals method did not detect editor mismatch", m1, m2 );
    }

    /**
     * Two ModelLayerBeans with different fields are not equal
     */
    @Test public void testNoEqualsOnFieldMismatch()
    {
        MVCBean m1 = new MVCBean();
        MVCBean m2 = new MVCBean();

        m1.setParameters( buildParameterList() );
        m2.setParameters( buildAlternateListOfFieldBean() );

        Assert.assertNotEquals("Equals method did not detect fields mismatch", m1, m2 );
    }

    /**
     * Two ModelLayerBeans with different generateCode values are not equal
     */
    @Test public void testNotEqualsOnGenerateCodeMismatch()
    {
        MVCBean m1 = new MVCBean();
        MVCBean m2 = new MVCBean();

        m1.setGenerateCode(true);
        m2.setGenerateCode(false);

        Assert.assertNotEquals("Equals method did not detect generateCode mismatch", m1, m2);
    }


    /**
     * Two ModelLayerBeans with different classNames are not equal
     */
    @Test public void testNotEqualsOnClassNameMismatch()
    {
        MVCBean m1 = new MVCBean();
        MVCBean m2 = new MVCBean();

        m1.setClassName("com.example.MyMVC");
        m2.setClassName("net.window.MyMVC");

        Assert.assertNotEquals("Equals method did not detect className mismatch", m1, m2);
    }

    private List<Parameter> buildParameterList()
    {
        List<Parameter> result = new LinkedList<>();

        ParameterBean firstName = ParameterBean.builder("firstName")
                                        .withLabel("First Name:")
                                        .build();

        ParameterBean lastName = ParameterBean.builder("lastName")
                                        .withLabel("Last Name:")
                                        .build();

        ParameterBean homeAddress = ParameterBean.builder("homeAddress")
                                        .withLabel ("Home Address:")
                                        .build();

        ParameterBean mailingAddress = ParameterBean.builder("mailingAddress")
                                        .withLabel("Mailing Address:")
                                        .build();

        result.add(firstName);
        result.add(lastName);
        result.add(homeAddress);
        result.add(mailingAddress);

        return result;
    }

    private List<Parameter> buildAlternateListOfFieldBean()
    {
        List<Parameter> result = new LinkedList<>();

        ParameterBean fieldOne = ParameterBean.builder("moleWeight")
                                        .withLabel("Mole Weight:")
                                        .build();

        ParameterBean fieldTwo = ParameterBean.builder("size")
                                        .withLabel("Size:")
                                        .build();

        ParameterBean fieldThree = ParameterBean.builder("mass")
                                        .withLabel("Mass:")
                                        .build();

        ParameterBean fieldFour = ParameterBean.builder("atomicWeight")
                                        .withLabel("Atomic Weight:")
                                        .build();

        result.add(fieldOne);
        result.add(fieldTwo);
        result.add(fieldThree);
        result.add(fieldFour);

        return result;
    }

}
