/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.model.beans;

import mmm.coffee.apprentice.model.stereotypes.Parameter;
import org.junit.Test;


import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit tests of the MVCBean class
 */
public class MVCBeanTest {


    @Test (expected = IllegalArgumentException.class)
    public void whenBuildingWithNullClassName_expectIllegalArgException()
    {
        MVCBean.builder(null).build();
    }

    @Test (expected = IllegalArgumentException.class)
    public void whenBuildingWithEmptyStringForClassName_expectIllegalArgException()
    {
        MVCBean.builder("").build();
    }

    @Test (expected = IllegalArgumentException.class)
    public void whenBuildingWithReservedWordForClassName_expectIllegalArgException()
    {
        MVCBean.builder("assert").build();
    }

    @Test
    public void whenBuildingWithValidClassName_expectBuildReturnsValidObject()
    {
        MVCBean bean = MVCBean.builder("mmm.coffee.MyAssertion").build();

        assertThat ( "The className from the builder must carry over into the MVCBean",
                bean.getClassName(), is ("mmm.coffee.MyAssertion"));

        assertThat ("The packageName was not determined correctly", bean.getPackageName(), is ("mmm.coffee"));

    }

    @Test
    public void withDefaultConstruction_getParametersReturnsEmptyList()
    {
        MVCBean bean = new MVCBean();

        assertThat ( "With a default construction, getParameters() must not return null", bean.getParameters(), notNullValue() );
        assertThat ( "With a default construction, getParameters() must return an empty list", bean.getParameters().size(), is(0));
    }

    @Test
    public void withDefaultConstruction_getClassNameReturnsNull()
    {
        MVCBean bean = new MVCBean();
        assertThat ( "With a default construction, getClassName() must return null", bean.getClassName() == null );
    }

    @Test
    public void withDefaultConstruction_getPackageNameReturnsNull()
    {
        MVCBean bean = new MVCBean();
        assertThat ( "With a default construction, getPackageName() must return null", bean.getPackageName() == null );
    }

    /**
     * Given: An MVCBean is constructed using the builder
     *        parameters are added using the withParameter method
     * Expect: the MVCBean constructed by the builder includes all the parameters we added
     */
    @Test
    public void whenBuildingWithParameters_getParametersReturnsAllParameters()
    {
        MVCBean bean = MVCBean.builder("mmm.coffee.Example")
                                .withParameter( ParameterBean.builder("fieldOne").build() )
                                .withParameter( ParameterBean.builder("fieldTwo").build() )
                                .withParameter( ParameterBean.builder("fieldThree").build() )
                                .build();

        assertThat ( "When parameters are added, getParameters() must not return null", bean.getParameters(), notNullValue() );
        assertThat ( "The parameter list size does not match the number of parameters added", bean.getParameters().size(), is(3));
        assertThat ( "The parameter 'fieldOne' is missing", parameterListContains ( bean.getParameters(), "fieldOne"));
        assertThat ( "The parameter 'fieldTwo' is missing", parameterListContains ( bean.getParameters(), "fieldTwo"));
        assertThat ( "The parameter 'fieldThree' is missing", parameterListContains ( bean.getParameters(), "fieldThree"));
    }


    /**
     * Determines if the given list contains a Parameter with the given
     * @param list the list to search
     * @param variableName the name to find
     * @return {@code true} if the parameter is found
     */
    private boolean parameterListContains (List<Parameter> list, String variableName)
    {
        if ( list == null || list.size() == 0 ) return false;

        for ( Parameter p: list )
        {
            if ( p.getVariableName().equals(variableName))
                return true;
        }
        return false;
    }




}
