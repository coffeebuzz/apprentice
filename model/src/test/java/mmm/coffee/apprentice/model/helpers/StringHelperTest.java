/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.model.helpers;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


/**
 * Unit tests of the StringHelper class.
 */
public class StringHelperTest {


    /**
     * Given: a valid Java identifier
     * Expect: isValidJavaIdentifier returns true
     */
    @Test public void whenJavaIdentifierIsValid_isValidJavaIdentifierReturnsTrue()
    {
        String[] validNames = { "i", "mString", "fieldOne", "$fieldOne", "FIELD_ONE", "mmm.coffee.Part" };

        for ( String s : validNames )
        {
            boolean isValid = StringHelper.isValidJavaIdentifier( s );
            assertThat ("Value '"+s+"' is not valid", isValid );
        }
    }

    /**
     * Given: an invalid Java identifier, or a Java reserved word
     * Expect: isValidJavaIdentifier returns false
     */
    @Test public void whenJavaIdentifierIsInvalid_isValidJavaIdentifierReturnsFalse()
    {
        String[] validNames = { "a b c", "#five", "(ABC)", "abstract", "case", "boolean", "" };

        for ( String s : validNames )
        {
            boolean isValid = StringHelper.isValidJavaIdentifier( s );
            assertThat ("The value '"+s+"' should not be accepted as a valid identifier", !isValid );
        }

        // Special case for null
        boolean isValid = StringHelper.isValidJavaIdentifier( null );
        assertThat ("A null value should not be accepted as a valid identifier", !isValid );
    }

    @Test public void whenNull_isEmptyReturnsTrue()
    {
        assertThat ( "A null value should be recognized as an empty string", StringHelper.isEmpty(null));
    }

    @Test public void whenEmpty_isEmptyReturnsTrue()
    {
        assertThat ( "A zero-length String should be recognized as an empty string", StringHelper.isEmpty(""));
    }

    @Test public void whenNullArgument_safeStringReturnsEmptyString()
    {
        assertThat ( "safeString() should convert a null value into an empty string", StringHelper.safeString(null), is(""));
    }

    @Test public void whenEmptyString_safeStringReturnsEmtpyString()
    {
        assertThat ( "safeString() should leave an empty string as-is", StringHelper.safeString(""), is(""));
    }

    @Test public void whenLeadingBlanks_safeStringRetainsLeadingBlanks()
    {
        assertThat ( "safeString() should leave any leading blanks in the string", StringHelper.safeString(" hello"), is(" hello"));
    }

    @Test public void whenTrailingBlanks_safeStringRetainsTrailingBlanks()
    {
        assertThat ( "safeString() should leave any trailing blanks in the string", StringHelper.safeString("hello "), is("hello "));
    }

    @Test public void whenNullArgument_capitalizeReturnsNull()
    {
        assertThat ( "capitalizeFirstCharacter() of a null value should return null", StringHelper.capitalizeFirstCharacter(null) == null );
    }

    @Test public void whenEmptyStringArgument_capitalizeReturnsEmptyString()
    {
        assertThat ( "capitalizeFirstCharacter() of an empty string should return an emtpy string", StringHelper.capitalizeFirstCharacter(""), is (""));
    }

    @Test public void whenSingleCharacter_capitalizeReturnsCapitalizedCharacter()
    {
        assertThat ("capitalizeFirstCharacter() should capitalize a single character value", StringHelper.capitalizeFirstCharacter("a"), is ("A"));
    }

    @Test public void whenCamelCase_capitalizeCapitalizesFirstCharacter()
    {
        assertThat ("capitalizeFirstCharacter() should return capitalize the first character", StringHelper.capitalizeFirstCharacter("fieldOne"), is ("FieldOne"));
    }

    @Test public void whenAllUpperCase_capitalizeCapitalizesFirstCharacter()
    {
        assertThat ("When first character is already capitalized, capitalizeFirstCharacter() should do nothing", StringHelper.capitalizeFirstCharacter("FieldOne"), is ("FieldOne"));
    }


}
