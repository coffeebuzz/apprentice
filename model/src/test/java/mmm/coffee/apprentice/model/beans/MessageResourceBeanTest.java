/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.model.beans;


import org.junit.Test;
import org.junit.Assert;


/**
 * Unit test of MessageResourceBean
 */
public class MessageResourceBeanTest {

    @Test public void testNewInstancesAreEqual()
    {
        MessageResourceBean b1 = new MessageResourceBean();
        MessageResourceBean b2 = new MessageResourceBean();


        Assert.assertEquals( "New instances should be equal", b1, b2);
        Assert.assertEquals( "Equal instances should have same hashCode", b1.hashCode(), b2.hashCode());
    }

    @Test public void testGetSetMethods()
    {
        MessageResourceBean bean = new MessageResourceBean();

        bean.setClassName("com.example.i18n.Messages");
        Assert.assertEquals("getClassName() does not match setClassName()'s value", "com.example.i18n.Messages", bean.getClassName());


        bean.setGenerateCode(true);
        Assert.assertEquals("getGenerateCode() does not match setGenerateCode()'s value", true, bean.getGenerateCode());
    }

    @Test public void testNotEqualsOnClassNameMismatch()
    {
        MessageResourceBean b1 = new MessageResourceBean();
        MessageResourceBean b2 = new MessageResourceBean();

        b1.setClassName("com.example.i18.Messages");
        b2.setClassName("net.window.i18n.Messages");

        Assert.assertNotEquals("Equals method did not detect className mismatch", b1, b2);
    }

    @Test public void testNotEqualsOnGenerateCodeMismatch()
    {
        MessageResourceBean b1 = new MessageResourceBean();
        MessageResourceBean b2 = new MessageResourceBean();

        b1.setGenerateCode(true);
        b2.setGenerateCode(false);

        Assert.assertNotEquals("Equals method did not detect generateCode mismatch", b1, b2);
    }


}
