/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.model.beans;

import mmm.coffee.apprentice.model.stereotypes.Parameter;
import org.junit.Test;
import org.junit.Assert;

import java.util.LinkedList;
import java.util.List;

/**
 * Unit tests of the FilterBean
 */
@SuppressWarnings("unchecked")
public class FilterBeanTest {


    /**
     * Two empty FilterBean instances are equal
     */
    @Test public void testNewInstancesAreEqual()
    {
        FilterBean f1 = new FilterBean();
        FilterBean f2 = new FilterBean();

        Assert.assertEquals ( f1, f2 );
        Assert.assertEquals ( f1.hashCode(), f2.hashCode() );
    }

    /**
     * Two FilterBeans are not equal if their globalScopes are not equal
     */
    @Test public void testNotEqualsOnGlobalScopeMismatch()
    {
        FilterBean f1 = new FilterBean();
        FilterBean f2 = new FilterBean();

        f1.setGlobalScope(true);
        f2.setGlobalScope(false);

        Assert.assertNotEquals(f1, f2);
    }

    /**
     * Two FilterBeans are not equal if their localScopes are not equal
     */
    @Test public void testNotEqualsOnLocalScopeMismatch()
    {
        FilterBean f1 = new FilterBean();
        FilterBean f2 = new FilterBean();

        f1.setLocalScope(true);
        f2.setLocalScope(false);

        Assert.assertNotEquals(f1, f2);
    }

    /**
     * Two FilterBeans are not equal if their generateCode flags are not equal
     */
    @Test public void testNotEqualsOnGenerateCodeMismatch()
    {
        FilterBean f1 = new FilterBean();
        FilterBean f2 = new FilterBean();

        f1.setGenerateCode(true);
        f2.setGenerateCode(false);

        Assert.assertNotEquals(f1, f2);
    }

    /**
     * Two FilterBeans are not equal if their classNames are not equal
     */
    @Test public void testNotEqualsOnClassNameMismatch()
    {
        FilterBean f1 = new FilterBean();
        FilterBean f2 = new FilterBean();

        f1.setClassName("com.company.widget.Filter");
        f2.setClassName("net.company.widget.Filter");

        Assert.assertNotEquals( f1, f2 );
    }

    /**
     * Two FilterBeans are equal if their controllers are equal
     */
    @Test public void testEqualsOnControllerMatch()
    {
        FilterBean f1 = new FilterBean();
        FilterBean f2 = new FilterBean();

        ComponentBean controller = new ComponentBean();

        f1.setController(controller);
        f2.setController(controller);

        Assert.assertEquals(f1, f2);
    }

    /**
     * Two FilterBeans are equal if their editors are equal
     */
    @Test public void testEqualsOnEditorsMatch()
    {
        FilterBean f1 = new FilterBean();
        FilterBean f2 = new FilterBean();

        ComponentBean editor = new ComponentBean();

        f1.setEditor(editor);
        f2.setEditor(editor);

        Assert.assertEquals(f1, f2);
    }

    /**
     * Two FilterBeans are not equal if their controllers are not equal
     */
    @Test public void testNotEqualsOnControllerMismatch()
    {
        FilterBean f1 = new FilterBean();
        FilterBean f2 = new FilterBean();

        ComponentBean c1 = new ComponentBean();
        ComponentBean c2 = new ComponentBean();

        c1.setClassName("com.widget.Controller");
        c2.setClassName("net.gears.Controller");

        f1.setController(c1);
        f2.setController(c2);

        Assert.assertNotEquals(f1, f2);
    }

    /**
     * Two FilterBeans are not equal if their classNames are not equal
     */
    @Test public void testNotEqualsOnEditorMismatch()
    {
        FilterBean f1 = new FilterBean();
        FilterBean f2 = new FilterBean();

        ComponentBean c1 = new ComponentBean();
        ComponentBean c2 = new ComponentBean();

        c1.setClassName("com.widget.Editor");
        c2.setClassName("net.gears.Editor");

        f1.setEditor(c1);
        f2.setEditor(c2);

        Assert.assertNotEquals( f1, f2 );
    }


    /**
     * Two FilterBeans are equal if their fields are equal
     */
    @Test public void testEqualsOnFieldsMatch()
    {
        FilterBean f1 = new FilterBean();
        FilterBean f2 = new FilterBean();

        f1.setParameters( buildParameterList() );
        f2.setParameters( buildParameterList() );

        Assert.assertEquals ( f1, f2 );
    }

    /**
     * Two FilterBeans are not equal if their fields are not equal
     */
    @Test public void testNotEqualsOnFieldsMismatch()
    {
        FilterBean f1 = new FilterBean();
        FilterBean f2 = new FilterBean();

        f1.setParameters( buildParameterList() );
        f2.setParameters( buildAlternateListOfFieldBean() );

        Assert.assertNotEquals ( f1, f2 );
    }


    private List<Parameter> buildParameterList()
    {
        List<Parameter> result = new LinkedList<>();

        ParameterBean firstName = ParameterBean.builder("firstName")
                                        .withLabel("First Name:")
                                        .build();

        ParameterBean lastName = ParameterBean.builder("lastName")
                                        .withLabel("Last Name:")
                                        .build();

        ParameterBean homeAddress = ParameterBean.builder("homeAddress")
                                        .withLabel ("Home Address:")
                                        .build();

        ParameterBean mailingAddress = ParameterBean.builder("mailingAddress")
                                        .withLabel("Mailing Address:")
                                        .build();

        result.add(firstName);
        result.add(lastName);
        result.add(homeAddress);
        result.add(mailingAddress);

        return result;
    }

    private List<Parameter> buildAlternateListOfFieldBean()
    {
        List<Parameter> result = new LinkedList<>();

        ParameterBean fieldOne = ParameterBean.builder("moleWeight")
                                        .withLabel("Mole Weight:")
                                        .build();

        ParameterBean fieldTwo = ParameterBean.builder("size")
                                        .withLabel("Size:")
                                        .build();

        ParameterBean fieldThree = ParameterBean.builder("mass")
                                        .withLabel("Mass:")
                                        .build();

        ParameterBean fieldFour = ParameterBean.builder("atomicWeight")
                                        .withLabel("Atomic Weight:")
                                        .build();

        result.add(fieldOne);
        result.add(fieldTwo);
        result.add(fieldThree);
        result.add(fieldFour);

        return result;
    }


}
