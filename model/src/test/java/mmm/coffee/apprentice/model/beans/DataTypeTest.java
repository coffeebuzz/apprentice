/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.model.beans;

import mmm.coffee.apprentice.model.stereotypes.DataType;
import org.junit.Test;
import org.junit.Assert;

/**
 * Unit tests of the DataType
 */
public class DataTypeTest {

    @Test
    public void testGetters() {
        Assert.assertNotNull(DataType.STRING.getJavaType());
        Assert.assertNotNull(DataType.BOOLEAN.getJavaType());
        Assert.assertNotNull(DataType.DIRECTORY.getJavaType());
        Assert.assertNotNull(DataType.FILE.getJavaType());

        Assert.assertNotNull(DataType.STRING.getParameterType());
        Assert.assertNotNull(DataType.BOOLEAN.getParameterType());
        Assert.assertNotNull(DataType.DIRECTORY.getParameterType());
        Assert.assertNotNull(DataType.FILE.getParameterType());

        Assert.assertNotNull(DataType.STRING.encode());
        Assert.assertNotNull(DataType.BOOLEAN.encode());
        Assert.assertNotNull(DataType.DIRECTORY.encode());
        Assert.assertNotNull(DataType.FILE.encode());
    }

    @Test
    public void testIsBoolean()
    {
        Assert.assertTrue  (DataType.BOOLEAN.isBoolean());

        Assert.assertFalse(DataType.STRING.isBoolean());
        Assert.assertFalse(DataType.DIRECTORY.isBoolean());
        Assert.assertFalse(DataType.FILE.isBoolean());
    }

    @Test public void testIsDirectory()
    {
        Assert.assertTrue  (DataType.DIRECTORY.isDirectory());

        Assert.assertFalse (DataType.STRING.isDirectory());
        Assert.assertFalse (DataType.BOOLEAN.isDirectory());
        Assert.assertFalse (DataType.FILE.isDirectory());
    }
}
