/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.model.beans;

import mmm.coffee.apprentice.model.stereotypes.Parameter;
import mmm.coffee.apprentice.model.stereotypes.Step;
import org.junit.Test;
import org.junit.Assert;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.hasItem;


/**
 * Unit tests of the StepBean
 */
public class StepBeanTest {

    /**
     * Empty instances of StepBean should be equal
     */
    @Test public void testNewInstancesAreEqual()
    {
        StepBean b1 = new StepBean();
        StepBean b2 = new StepBean();

        Assert.assertEquals ( "New instances should be equal", b1, b2 );
        Assert.assertEquals ( "Equal objects should have the same hashCode", b1.hashCode(), b2.hashCode() );
    }

    /**
     * Verify the reflexive property of get/set methods: if setX(f), then getX() == f.
     */
    @Test public void testGetSetMethods()
    {
        StepBean bean = new StepBean();


        String className = "com.example.widget.WidgetStep";
        bean.setClassName( className );
        Assert.assertEquals("setClassName/getClassName should match", className, bean.getClassName());

        bean.setGenerateCode(true);
        Assert.assertTrue ( "setGenerateCode/getGenerateCode should match", bean.getGenerateCode() );

        ComponentBean controller = ComponentBean.builder("mmm.coffee.foo.FooController").build();
        bean.setController( controller );
        Assert.assertEquals("setController/getController should match", controller, bean.getController());

        ComponentBean editor = ComponentBean.builder("mmm.coffee.foo.FooEditor").build();
        bean.setEditor(editor);
        Assert.assertEquals("setEditor/getEditor should match", editor, bean.getEditor());

        Parameter pHost = ParameterBean.builder("hostName").build();
        Parameter pPort = ParameterBean.builder("port").build();
        Parameter pProxy = ParameterBean.builder("proxy").build();

        bean.getParameters().add ( pHost );
        bean.getParameters().add ( pPort );
        bean.getParameters().add ( pProxy );

        assertThat ( "The parameterList is missing item 'hostName'", bean.getParameters(), hasItem ( pHost ));
        assertThat ( "The parameterList is missing item 'port'",     bean.getParameters(), hasItem ( pPort ));
        assertThat ( "The parameterList is missing item 'proxy'",    bean.getParameters(), hasItem ( pProxy ));
    }

    /**
     * Two StepBeans are not equal if their classNames are not equal
     */
    @Test public void testNotEqualsOnClassNameMismatch()
    {
        StepBean b1 = StepBean.builder("com.example.Step").build();
        StepBean b2 = StepBean.builder("org.example.StepNode").build();

        Assert.assertNotEquals("Equals method should detect className mismatch", b1, b2 );
    }

    /**
     * Two StepBeans are not equal if their controllers are not equal
     */
    @Test public void testNotEqualsOnControllerMismatch()
    {
        StepBean b1 = StepBean.builder("mmm.coffee.foo.FooStep")
                              .withController("mmm.coffee.foo.FooController")
                              .build();

        StepBean b2 = StepBean.builder("mmm.coffee.foo.FooStep")
                              .withController("mmm.coffee.bar.BarController")
                              .build();

        Assert.assertNotEquals("Equals method should detect className mismatch", b1, b2);
    }


    /**
     * Two StepBeans are not equal if their editors are not equal
     */
    @Test public void testNotEqualsOnEditorMismatch()
    {
        StepBean b1 = StepBean.builder    ( "mmm.coffee.foo.FooStep" )
                              .withEditor ( "mmm.coffee.foo.FooEditor" )
                              .build();

        StepBean b2 = StepBean.builder    ( "mmm.coffee.foo.FooStep" )
                              .withEditor ( "mmm.coffee.foo.BarEditor" )
                              .build();

        Assert.assertNotEquals("Equals method should detect className mismatch", b1, b2);
    }

    /**
     * Two StepBeans are not equal if their fields are not equal
     */
    @Test public void testNotEqualsOnFieldMismatch()
    {
        StepBean b1 = StepBean.builder    ( "mmm.coffee.foo.FooStep" )
                              .withEditor ( "mmm.coffee.foo.FooEditor" )
                              .withParameter(
                                      ParameterBean.builder("firstName").build())
                              .withParameter (
                                      ParameterBean.builder("lastName").build())
                              .build();

        StepBean b2 = StepBean.builder    ( "mmm.coffee.foo.FooStep" )
                              .withEditor ( "mmm.coffee.foo.FooEditor" )
                              .withParameter(
                                      ParameterBean.builder("hostName").build())
                              .withParameter (
                                      ParameterBean.builder("port").build())
                              .build();

        Assert.assertNotEquals("Equals method should detect parameterList mismatch", b1, b2 );
    }

    /**
     * The Step entity is the only one that requires both a controller and editor to
     * be defined.  To make the API easier, we'll enhance the StepBean's builder to handle
     * this:
     *
     *   Step step = StepBean.builder("mmm.coffee.custom.CustomStep")
     *                       .withParameter(...)
     *                       .build();
     *
     * Specifically, we're not requiring the programmer to also call
     *
     *                       .withController(...)
     *                       .withEditor(...)
     *
     * Instead, the builder will conjure default names for the step's controller
     * and editor by appending 'Controller' and 'Editor' to the end of the model-layer's name;
     * for example, "mmm.coffee.custom.CustomStepController" and "mmm.coffee.custom.CustomStepEditor"
     */
    @Test
    public void whenOnlyModelIsDefined_expectDefaultNamesForControllerAndEditor()
    {
        Step step = StepBean.builder("mmm.coffee.custom.CustomStep")
                            .build();

        assertThat ( "Expected a non-null Step entity from the builder", step, notNullValue() );

        assertThat ( "Expected a default name to be given to the controller",
                step.getController().getClassName(), is("mmm.coffee.custom.CustomStepController"));

        assertThat ( "Expected a default name to be given to the editor",
                step.getEditor().getClassName(), is("mmm.coffee.custom.CustomStepEditor"));

    }


}
