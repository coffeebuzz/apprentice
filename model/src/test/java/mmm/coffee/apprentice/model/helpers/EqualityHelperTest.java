/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mmm.coffee.apprentice.model.helpers;

import mmm.coffee.apprentice.model.beans.ParameterBean;
import mmm.coffee.apprentice.model.stereotypes.DataType;
import org.junit.Test;
import org.junit.Assert;

import java.util.LinkedList;
import java.util.List;


/**
 * Unit tests of the Utilities class.
 */
public class EqualityHelperTest {


    @Test public void testEqualityOnList()
    {
        ParameterBean b1 = new ParameterBean();
        b1.setDataType(DataType.STRING);
        b1.setLabelName("VALUE_LABEL");
        b1.setGetterMethodName("getValue");
        b1.setSetterMethodName("setValue");

        ParameterBean b2 = new ParameterBean();
        b2.setDataType(DataType.STRING);
        b2.setLabelName("NAME_LABEL");
        b2.setGetterMethodName("getName");
        b2.setSetterMethodName("setName");

        ParameterBean b3 = new ParameterBean();
        b3.setDataType(DataType.FILE);
        b3.setLabelName("FILE_LABEL");
        b3.setGetterMethodName("getFile");
        b3.setSetterMethodName("setFile");

        List<ParameterBean> listA = new LinkedList<>();
        listA.add ( b1 );
        listA.add ( b2 );
        listA.add ( b3 );

        List<ParameterBean> listB = new LinkedList<>();
        listB.add ( b3 );
        listB.add ( b2 );
        listB.add ( b1 );

        // Two lists containing the same elements are equal,
        // regardless of the element order.
        Assert.assertTrue(EqualityHelper.areEqual(listA, listB));
    }

    @Test public void testEqualityOnEmptyLists()
    {
        List<String> listOne = new LinkedList<>();
        List<String> listTwo = new LinkedList<>();

        Assert.assertTrue ( EqualityHelper.areEqual( listOne, listTwo ));
    }

    @Test public void testEqualityOnNullLists()
    {
        List<String> listOne = null;
        List<String> listTwo = null;

        Assert.assertTrue ( EqualityHelper.areEqual( listOne, listTwo ));
    }
}
