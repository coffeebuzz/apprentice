/*
 * Copyright 2016 Jon Caulfield
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mmm.coffee.apprentice.model.helpers;

import org.junit.Before;
import org.junit.Test;

import java.io.StringWriter;
import java.io.Writer;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.not;


/**
 * Unit tests of the PrintHelper
 */
public class PrintHelperTest {

    private Writer writer;


    @Before
    public void setUp()
    {
        writer = new StringWriter();
    }


    @Test
    public void printStartTag_writesStartTag() throws Exception
    {
        PrintHelper.printStartTag(writer, 0, "hello");
        assertThat ( "Expected start tag to be well-formed", writer.toString(), containsString("<hello>"));
    }

    @Test
    public void printOpenTag_writesOpenTag() throws Exception
    {
        PrintHelper.printOpenTag(writer, 0, "hello");
        assertThat ( "Expected printOpenTag to write the tag: '<hello' (a tag without a closing bracket) ", writer.toString(), containsString("<hello"));
        assertThat ( "Expected printOpenTag to NOT write the tag: '<hello>'", writer.toString(), not(containsString("<hello>")));
    }

    @Test
    public void printAttribute_writesWellFormedAttribute() throws Exception
    {
        PrintHelper.printAttribute(writer, "size", "100");
        assertThat ( "Expected printAttribute to correctly format an attribute", writer.toString(), containsString("size=\"100\""));
    }

    @Test
    public void printEndTag_writesWellFormedEndTag() throws Exception
    {
        int indent = 4;
        final String fourSpaces = "    ";
        PrintHelper.printEndTag(writer, indent, "hello");

        assertThat ("Expected end tag to be well-formed", writer.toString(), containsString("</hello>"));
        assertThat ("Expected end tag to be indented", writer.toString(), containsString( fourSpaces ));

    }

    @Test
    public void printEndTagWithoutIndentation_writesWellFormedEndTag() throws Exception
    {
        PrintHelper.printEndTag( writer, "hello" );
        assertThat ("Expected end tag to be well-formed", writer.toString(), containsString("</hello>"));
    }

    @Test
    public void printSimpleTagValue_writesWellFormedTagWrappingAValue() throws Exception
    {
        PrintHelper.printSimpleTagValue( writer, 0, "fruit", "apple");
        assertThat ("Expected a tag wrapping a value to be well-formed", writer.toString(), containsString("<fruit>apple</fruit>"));
    }

}
